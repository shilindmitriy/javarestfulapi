package com.mycompany.backend.movierating.service;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;

public class FilmographyServiceTest extends BaseTest {

    @Autowired
    private FilmographyService filmographyService;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Test
    public void testGetFilmography() {

        Movie movie1 = generator.generatePersistentMovie();
        Movie movie2 = generator.generatePersistentMovie();
        Movie movie3 = generator.generatePersistentMovie();
        Movie movie4 = generator.generatePersistentMovie();

        CreativePerson person = generator.generatePersistentCreativePerson();
        CreativePerson personNotFound = generator.generatePersistentCreativePerson();

        CreativeWork work1 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work1.setDepartment(Department.ACTING);
        CreativeWork work2 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work2.setDepartment(Department.ACTING);
        CreativeWork work3 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work3.setDepartment(Department.ACTING);
        CreativeWork work4 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work4.setDepartment(Department.ACTING);
        CreativeWork work5 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work5.setDepartment(Department.ACTING);
        CreativeWork work6 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work6.setDepartment(Department.DIRECTING);

        work1.setMovie(movie1);
        work1.setCreativePerson(person);
        work2.setMovie(movie2);
        work2.setCreativePerson(person);
        work3.setMovie(movie3);
        work3.setCreativePerson(person);
        work4.setMovie(movie4);
        work4.setCreativePerson(person);
        work5.setMovie(movie1);
        work5.setCreativePerson(personNotFound);
        work6.setMovie(movie1);
        work6.setCreativePerson(person);

        work1 = workRepository.save(work1);
        work2 = workRepository.save(work2);
        work3 = workRepository.save(work3);
        work4 = workRepository.save(work4);
        work5 = workRepository.save(work5);
        work6 = workRepository.save(work6);

        List<FilmographyReadDTO> filmographyByLeadingActor = filmographyService.getFilmography(person.getId(),
                Department.ACTING);
        Assert.assertFalse(filmographyByLeadingActor.isEmpty());
        Assert.assertTrue(filmographyByLeadingActor.size() == 4);
        Assertions.assertThat(filmographyByLeadingActor).extracting("movieId").containsExactlyInAnyOrder(movie1.getId(),
                movie2.getId(), movie3.getId(), movie4.getId());
        Assertions.assertThat(filmographyByLeadingActor).extracting("workId").containsExactlyInAnyOrder(work1.getId(),
                work2.getId(), work3.getId(), work4.getId());

        List<FilmographyReadDTO> filmographyByDirector = filmographyService.getFilmography(person.getId(),
                Department.DIRECTING);
        Assert.assertFalse(filmographyByDirector.isEmpty());
        Assert.assertTrue(filmographyByDirector.size() == 1);
        Assertions.assertThat(filmographyByDirector).extracting("movieId").containsExactlyInAnyOrder(movie1.getId());
        Assertions.assertThat(filmographyByDirector).extracting("workId").containsExactlyInAnyOrder(work6.getId());
    }

}
