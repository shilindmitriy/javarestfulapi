package com.mycompany.backend.movierating.repository;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.service.BaseTest;

public class CreativeWorkRepositoryTest extends BaseTest {

    @Autowired
    private CreativeWorkRepository workRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        CreativeWork work = new CreativeWork();
        workRepository.save(work);
    }

    @Test
    public void testCreatedAtIsSet() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        Instant createdAtBeforeReload = work.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        work = workRepository.findById(work.getId()).get();
        Instant createdAtAfterReload = work.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        Instant createdAtBeforeUpdate = work.getCreatedAt();
        Instant updatedAtBeforeUpdate = work.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        work.setCharacter("update");
        work = workRepository.save(work);

        Instant createdAtAfterUpdate = work.getCreatedAt();
        Instant updatedAtAfterUpdate = work.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

    @Test
    public void testGetIdsOfCreativeWorks() {
        Set<UUID> expectedIdsOfCreativeWorks = new HashSet<>();
        expectedIdsOfCreativeWorks.add(generator.generatePersistentCreativeWork().getId());
        expectedIdsOfCreativeWorks.add(generator.generatePersistentCreativeWork().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfCreativeWorks,
                    workRepository.getIdsOfCreativeWorks().collect(Collectors.toSet()));
        });
    }

}