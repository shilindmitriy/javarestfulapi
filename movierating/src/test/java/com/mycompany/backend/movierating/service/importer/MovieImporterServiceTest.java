package com.mycompany.backend.movierating.service.importer;

import java.time.LocalDate;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.client.TheMovieDbClient;
import com.mycompany.backend.movierating.client.themoviedb.dto.CastReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CountryReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditsReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CrewReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.GenreReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.PersonReadDTO;
import com.mycompany.backend.movierating.domain.ExternalSystemImport;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.domain.constants.ImportedEntityType;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.exception.ImportedEntityAlreadyExistException;
import com.mycompany.backend.movierating.repository.ExternalSystemImportRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.service.BaseTest;

public class MovieImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private MovieImporterService movieImporterService;

    @Test
    public void testImportMovieFromWebPage() {

        String movieExternalId = "1234";
        MovieReadDTO read = getMovieReadDTO();

        CrewReadDTO crew = generator.generateObject(CrewReadDTO.class);
        crew.setDepartment(Department.CAMERA.name());
        CastReadDTO cast = generator.generateObject(CastReadDTO.class);
        CreditsReadDTO credits = new CreditsReadDTO();
        credits.getCast().add(cast);
        credits.getCrew().add(crew);

        CreditReadDTO credit = generator.generateObject(CreditReadDTO.class);
        credit.setDepartment(Department.ACTING.name());
        PersonReadDTO person1 = generator.generateObject(PersonReadDTO.class);
        person1.setBirthday(LocalDate.parse("1956-05-05"));
        person1.setDeathday(LocalDate.parse("2001-12-01"));
        PersonReadDTO person2 = generator.generateObject(PersonReadDTO.class);
        person2.setBirthday(LocalDate.parse("1977-05-05"));
        person2.setDeathday(LocalDate.parse("2019-12-01"));

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(credits);
        Mockito.when(movieDbClient.getCredit(cast.getCreditId())).thenReturn(credit);
        Mockito.when(movieDbClient.getPerson(cast.getId())).thenReturn(person1);
        Mockito.when(movieDbClient.getPerson(crew.getId())).thenReturn(person2);

        UUID movieId = movieImporterService.importMovieFromWebPage(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();
            Assert.assertEquals(read.getTitle(), movie.getTitle());
            Assertions.assertThat(movie.getFilmCrew()).extracting("name").containsExactlyInAnyOrder(person1.getName(),
                    person2.getName());
            Assertions.assertThat(movie.getCreativeWorks()).extracting("post")
                    .containsExactlyInAnyOrder(credit.getJob(), crew.getJob());
        });
    }

    @Test(expected = AlreadyExistsException.class)
    public void testImportMovieFromWebPageAlreadyExists() {

        String movieExternalId = "1234";
        Movie existingMovie = generator.generatePersistentMovie();

        MovieReadDTO read = generator.generateObject(MovieReadDTO.class);
        read.setTitle(existingMovie.getTitle());
        read.setReleaseDate(LocalDate.parse("1991-07-03"));
        read.setRuntime(137L);
        read.setStatus("Released");

        read.getProductionCountries().clear();
        CountryReadDTO country = new CountryReadDTO();
        country.setName("United States of America");
        read.getProductionCountries().add(country);

        read.getGenres().clear();
        GenreReadDTO genre = new GenreReadDTO();
        genre.setName("Action");
        read.getGenres().add(genre);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        movieImporterService.importMovieFromWebPage(movieExternalId);
    }

    @Test
    public void testImportMovie() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = getMovieReadDTO();

        CrewReadDTO crew = generator.generateObject(CrewReadDTO.class);
        crew.setDepartment(Department.CAMERA.name());
        CastReadDTO cast = generator.generateObject(CastReadDTO.class);
        CreditsReadDTO credits = new CreditsReadDTO();
        credits.getCast().add(cast);
        credits.getCrew().add(crew);

        CreditReadDTO credit = generator.generateObject(CreditReadDTO.class);
        credit.setDepartment(Department.ACTING.name());
        PersonReadDTO person1 = generator.generateObject(PersonReadDTO.class);
        person1.setBirthday(LocalDate.parse("1956-05-05"));
        person1.setDeathday(LocalDate.parse("2001-12-01"));
        PersonReadDTO person2 = generator.generateObject(PersonReadDTO.class);
        person2.setBirthday(LocalDate.parse("1977-05-05"));
        person2.setDeathday(LocalDate.parse("2019-12-01"));

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(credits);
        Mockito.when(movieDbClient.getCredit(cast.getCreditId())).thenReturn(credit);
        Mockito.when(movieDbClient.getPerson(cast.getId())).thenReturn(person1);
        Mockito.when(movieDbClient.getPerson(crew.getId())).thenReturn(person2);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();
            Assert.assertEquals(read.getTitle(), movie.getTitle());
            Assertions.assertThat(movie.getFilmCrew()).extracting("name").containsExactlyInAnyOrder(person1.getName(),
                    person2.getName());
            Assertions.assertThat(movie.getCreativeWorks()).extracting("post")
                    .containsExactlyInAnyOrder(credit.getJob(), crew.getJob());
        });
    }

    @Test
    public void testImportMovieWithNullFields()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = getMovieReadDTO();
        read.setReleaseDate(null);
        read.setRuntime(null);

        CrewReadDTO crew = generator.generateObject(CrewReadDTO.class);
        crew.setDepartment(Department.CAMERA.name());
        CastReadDTO cast = generator.generateObject(CastReadDTO.class);
        CreditsReadDTO credits = new CreditsReadDTO();
        credits.getCast().add(cast);
        credits.getCrew().add(crew);

        CreditReadDTO credit = generator.generateObject(CreditReadDTO.class);
        credit.setDepartment(Department.ACTING.name());
        PersonReadDTO person1 = generator.generateObject(PersonReadDTO.class);
        person1.setBirthday(null);
        person1.setDeathday(null);
        PersonReadDTO person2 = generator.generateObject(PersonReadDTO.class);
        person2.setBirthday(null);
        person2.setDeathday(null);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(credits);
        Mockito.when(movieDbClient.getCredit(cast.getCreditId())).thenReturn(credit);
        Mockito.when(movieDbClient.getPerson(cast.getId())).thenReturn(person1);
        Mockito.when(movieDbClient.getPerson(crew.getId())).thenReturn(person2);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();
            Assert.assertTrue(movie.getReleaseDate() == null);
            Assert.assertTrue(movie.getRunningTime() == null);
            Assertions.assertThat(movie.getFilmCrew()).extracting("death").containsOnlyNulls();
            Assertions.assertThat(movie.getFilmCrew()).extracting("born").containsOnlyNulls();
        });
    }

    @Test
    public void testImportMovieUpdateExternalSystemImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = getMovieReadDTO();

        CrewReadDTO crew = generator.generateObject(CrewReadDTO.class);
        crew.setDepartment(Department.CAMERA.name());
        CastReadDTO cast = generator.generateObject(CastReadDTO.class);
        CreditsReadDTO credits = new CreditsReadDTO();
        credits.getCast().add(cast);
        credits.getCrew().add(crew);

        CreditReadDTO credit = generator.generateObject(CreditReadDTO.class);
        credit.setDepartment(Department.ACTING.name());
        PersonReadDTO person1 = generator.generateObject(PersonReadDTO.class);
        person1.setBirthday(LocalDate.parse("1956-05-05"));
        person1.setDeathday(LocalDate.parse("2001-12-01"));
        PersonReadDTO person2 = generator.generateObject(PersonReadDTO.class);
        person2.setBirthday(LocalDate.parse("1977-05-05"));
        person2.setDeathday(LocalDate.parse("2019-12-01"));

        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setIdInExternalSystem(person1.getId());
        esi.setEntityType(ImportedEntityType.CREATIVE_PERSON);
        esi.setEntityId(UUID.randomUUID());
        externalSystemImportRepository.save(esi);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(credits);
        Mockito.when(movieDbClient.getCredit(cast.getCreditId())).thenReturn(credit);
        Mockito.when(movieDbClient.getPerson(cast.getId())).thenReturn(person1);
        Mockito.when(movieDbClient.getPerson(crew.getId())).thenReturn(person2);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Movie movie = movieRepository.findById(movieId).get();
            Assert.assertEquals(read.getTitle(), movie.getTitle());
            Assertions.assertThat(movie.getFilmCrew()).extracting("name").containsExactlyInAnyOrder(person1.getName(),
                    person2.getName());
            Assertions.assertThat(movie.getCreativeWorks()).extracting("post")
                    .containsExactlyInAnyOrder(credit.getJob(), crew.getJob());
        });
    }

    @Test
    public void testImportMovieImportAlreadyExist() {

        String movieExternalId = "id1";

        Movie existingMovie = generator.generatePersistentMovie();

        MovieReadDTO read = generator.generateObject(MovieReadDTO.class);
        read.setTitle(existingMovie.getTitle());
        read.setReleaseDate(LocalDate.parse("1991-07-03"));
        read.setRuntime(137L);
        read.setStatus("Released");

        read.getProductionCountries().clear();
        CountryReadDTO country = new CountryReadDTO();
        country.setName("United States of America");
        read.getProductionCountries().add(country);

        read.getGenres().clear();
        GenreReadDTO genre = new GenreReadDTO();
        genre.setName("Action");
        read.getGenres().add(genre);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions.catchThrowableOfType(
                () -> movieImporterService.importMovie(movieExternalId), ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Movie.class, ex.getEntityClass());
        Assert.assertEquals(existingMovie.getId(), ex.getEntityId());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = getMovieReadDTO();

        CrewReadDTO crew = generator.generateObject(CrewReadDTO.class);
        crew.setDepartment(Department.CAMERA.name());
        CastReadDTO cast = generator.generateObject(CastReadDTO.class);
        CreditsReadDTO credits = new CreditsReadDTO();
        credits.getCast().add(cast);
        credits.getCrew().add(crew);

        CreditReadDTO credit = generator.generateObject(CreditReadDTO.class);
        credit.setDepartment(Department.ACTING.name());
        PersonReadDTO person1 = generator.generateObject(PersonReadDTO.class);
        person1.setBirthday(LocalDate.parse("1956-05-05"));
        person1.setDeathday(LocalDate.parse("2001-12-01"));
        PersonReadDTO person2 = generator.generateObject(PersonReadDTO.class);
        person2.setBirthday(LocalDate.parse("1977-05-05"));
        person2.setDeathday(LocalDate.parse("2019-12-01"));

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(credits);
        Mockito.when(movieDbClient.getCredit(cast.getCreditId())).thenReturn(credit);
        Mockito.when(movieDbClient.getPerson(cast.getId())).thenReturn(person1);
        Mockito.when(movieDbClient.getPerson(crew.getId())).thenReturn(person2);

        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService.importMovie(movieExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);
        Mockito.verifyNoInteractions(movieDbClient);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    private MovieReadDTO getMovieReadDTO() {
        MovieReadDTO read = generator.generateObject(MovieReadDTO.class);
        read.setReleaseDate(LocalDate.parse("1991-07-03"));
        read.setRuntime(137L);
        read.setStatus("Released");

        read.getProductionCountries().clear();
        CountryReadDTO country = new CountryReadDTO();
        country.setName("United States of America");
        read.getProductionCountries().add(country);

        read.getGenres().clear();
        GenreReadDTO genre = new GenreReadDTO();
        genre.setName("Action");
        read.getGenres().add(genre);
        return read;
    }

}
