package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.CreativeWorkRatingRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class CreativeWorkRatingServiceTest extends BaseTest {

    @Autowired
    private CreativeWorkRatingService ratingService;

    @Autowired
    private CreativeWorkRatingRepository ratingRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetRating() {
        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();
        RatingReadDTO read = ratingService.getRating(rating.getParentEntity().getId(), rating.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testGetShortRating() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        CreativeWork workNotFound = generator.generatePersistentCreativeWork();

        CreativeWorkRating rating1 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating1.setRating(3);
        rating1.setParentEntity(work);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = ratingRepository.save(rating1);

        CreativeWorkRating rating2 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating2.setRating(10);
        rating2.setParentEntity(work);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = ratingRepository.save(rating2);

        CreativeWorkRating rating3 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating3.setRating(6);
        rating3.setParentEntity(work);
        rating3.setUser(generator.generatePersistentUser());
        rating3 = ratingRepository.save(rating3);

        CreativeWorkRating rating4 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating4.setParentEntity(workNotFound);
        rating4.setUser(generator.generatePersistentUser());
        rating4 = ratingRepository.save(rating4);

        ShortRatingReadDTO shortRating = ratingService.getShortRating(work.getId());
        Assert.assertTrue(shortRating.getNumberOfAllVotes() == 3L);
        Assert.assertEquals(shortRating.getAverageRating(),
                (double) (rating1.getRating() + rating2.getRating() + rating3.getRating()) / 3, 0.01d);
    }

    @Test
    public void testCreateRating() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);

        RatingReadDTO read = ratingService.createRating(work.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getId() != null);
        Assert.assertTrue(read.getAge() == Period.between(user.getBirthday(), LocalDate.now()).getYears());
        Assert.assertEquals(read.getCountryUser(), user.getCountry());
        Assert.assertEquals(read.getGender(), user.getGender());

        CreativeWorkRating rating = ratingRepository.findByParentIdAndId(work.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(user.getId().equals(read.getUserId()));

        Assert.assertTrue(userRepository.existsByRatingIdAndEmail(rating.getId(), user.getEmail()));
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateRatingWrongRatingAlreadyExists() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        RatingCreateDTO firstCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(work.getId(), firstCreate, userDetails);

        RatingCreateDTO secondCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(work.getId(), secondCreate, userDetails);
    }

    @Test
    public void testPatchRating() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(), patch);
        Assert.assertEquals(read.getRating(), patch.getRating());

        CreativeWorkRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testPatchRatingEmptyPatch() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(),
                new RatingPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));

        CreativeWorkRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testUpdateRating() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        RatingReadDTO read = ratingService.updateRating(rating.getParentEntity().getId(), rating.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        CreativeWorkRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateRatingNotFoundRating() {
        ratingService.updateRating(UUID.randomUUID(), UUID.randomUUID(), new RatingPutDTO());
    }

    @Test
    public void testDeleteRating() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();
        Assert.assertTrue(ratingRepository.existsById(rating.getId()));

        ratingService.deleteRating(rating.getParentEntity().getId(), rating.getId());
        Assert.assertFalse(ratingRepository.existsById(rating.getId()));
    }

}