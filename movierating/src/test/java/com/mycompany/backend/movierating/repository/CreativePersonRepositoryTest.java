package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.service.BaseTest;

public class CreativePersonRepositoryTest extends BaseTest {

    @Autowired
    private CreativePersonRepository personRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        CreativePerson person = new CreativePerson();
        personRepository.save(person);
    }

    @Test
    public void testCreatedAtIsSet() {

        CreativePerson person = generator.generatePersistentCreativePerson();

        Instant createdAtBeforeReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        person = personRepository.findById(person.getId()).get();
        Instant createdAtAfterReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        CreativePerson person = generator.generatePersistentCreativePerson();

        Instant createdAtBeforeUpdate = person.getCreatedAt();
        Instant updatedAtBeforeUpdate = person.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        person.setName("updated");
        person = personRepository.save(person);

        Instant createdAtAfterUpdate = person.getCreatedAt();
        Instant updatedAtAfterUpdate = person.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}