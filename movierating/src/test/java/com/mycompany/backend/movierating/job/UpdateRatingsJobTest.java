package com.mycompany.backend.movierating.job;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonRating;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.repository.CreativePersonRatingRepository;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRatingRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRatingRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.NewsRepository;
import com.mycompany.backend.movierating.service.BaseTest;
import com.mycompany.backend.movierating.service.CreativePersonService;
import com.mycompany.backend.movierating.service.CreativeWorkService;
import com.mycompany.backend.movierating.service.MovieService;
import com.mycompany.backend.movierating.service.NewsService;

public class UpdateRatingsJobTest extends BaseTest {

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    private MovieRepository movieRepository;

    @SpyBean
    private MovieService movieService;

    @Autowired
    private CreativePersonRatingRepository personRatingRepository;

    @Autowired
    private CreativePersonRepository personRepository;

    @SpyBean
    private CreativePersonService personService;
    
    @Autowired
    private CreativeWorkRatingRepository workRatingRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @SpyBean
    private CreativeWorkService workService;

    @Autowired
    private NewsRatingRepository newsRatingRepository;

    @Autowired
    private NewsRepository newsRepository;

    @SpyBean
    private NewsService newsService;

    @Autowired
    private UpdateRatingsJob updateRatingsJob;

    @Test
    public void testUpdateRatingOfMovies() {

        Movie movie = generator.generatePersistentMovie();

        MovieRating rating1 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating1.setParentEntity(movie);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = movieRatingRepository.save(rating1);

        MovieRating rating2 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating2.setParentEntity(movie);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = movieRatingRepository.save(rating2);

        updateRatingsJob.updateRatingOfMovies();

        movie = movieRepository.findById(movie.getId()).get();

        Assert.assertEquals(((double) rating1.getRating() + rating2.getRating()) / 2, movie.getAverageRating(),
                Double.MIN_NORMAL);
        Assert.assertTrue(movie.getNumberOfAllVotes() == 2L);
    }

    @Test
    public void testMoviesUpdatedIndependetly() {

        MovieRating rating = generator.generatePersistentMovieRating();
        Movie movie = rating.getParentEntity();
        movie.setAverageRating(null);
        movie.setNumberOfAllVotes(null);
        movieRepository.save(movie);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(movieService).updateRating(Mockito.any());

        updateRatingsJob.updateRatingOfMovies();

        for (Movie m : movieRepository.findAll()) {
            if (m.getId().equals(failedId[0])) {
                Assert.assertNull(m.getAverageRating());
                Assert.assertNull(m.getNumberOfAllVotes());
            } else {
                Assert.assertNotNull(m.getAverageRating());
                Assert.assertNotNull(m.getNumberOfAllVotes());
            }
        }
    }
    
    @Test
    public void testUpdateRatingOfCreativeWorks() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        CreativeWorkRating rating1 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating1.setParentEntity(work);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = workRatingRepository.save(rating1);

        CreativeWorkRating rating2 = generator.generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating2.setParentEntity(work);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = workRatingRepository.save(rating2);

        updateRatingsJob.updateRatingOfCreativeWorks();

        work = workRepository.findById(work.getId()).get();

        Assert.assertEquals(((double) rating1.getRating() + rating2.getRating()) / 2, work.getAverageRating(),
                Double.MIN_NORMAL);
        Assert.assertTrue(work.getNumberOfAllVotes() == 2L);
    }

    @Test
    public void testCreativeWorksUpdatedIndependetly() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();
        CreativeWork work = rating.getParentEntity();
        work.setAverageRating(null);
        work.setNumberOfAllVotes(null);
        workRepository.save(work);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(movieService).updateRating(Mockito.any());

        updateRatingsJob.updateRatingOfCreativeWorks();

        for (CreativeWork w : workRepository.findAll()) {
            if (w.getId().equals(failedId[0])) {
                Assert.assertNull(w.getAverageRating());
                Assert.assertNull(w.getNumberOfAllVotes());
            } else {
                Assert.assertNotNull(w.getAverageRating());
                Assert.assertNotNull(w.getNumberOfAllVotes());
            }
        }
    }

    @Test
    public void testUpdateRatingOfCreativePersons() {

        CreativePerson person = generator.generatePersistentCreativePerson();

        CreativePersonRating rating1 = generator.generateFlatEntityWithoutId(CreativePersonRating.class);
        rating1.setParentEntity(person);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = personRatingRepository.save(rating1);

        CreativePersonRating rating2 = generator.generateFlatEntityWithoutId(CreativePersonRating.class);
        rating2.setParentEntity(person);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = personRatingRepository.save(rating2);

        updateRatingsJob.updateRatingOfCreativePersons();

        person = personRepository.findById(person.getId()).get();

        Assert.assertEquals(((double) rating1.getRating() + rating2.getRating()) / 2, person.getAverageRating(),
                Double.MIN_NORMAL);
        Assert.assertTrue(person.getNumberOfAllVotes() == 2L);
    }

    @Test
    public void testCreativePersonsUpdatedIndependetly() {

        CreativePersonRating rating = generator.generatePersistentCreativePersonRating();
        CreativePerson person = rating.getParentEntity();
        person.setAverageRating(null);
        person.setNumberOfAllVotes(null);
        personRepository.save(person);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(personService).updateRating(Mockito.any());

        updateRatingsJob.updateRatingOfCreativePersons();

        for (CreativePerson p : personRepository.findAll()) {
            if (p.getId().equals(failedId[0])) {
                Assert.assertNull(p.getAverageRating());
                Assert.assertNull(p.getNumberOfAllVotes());
            } else {
                Assert.assertNotNull(p.getAverageRating());
                Assert.assertNotNull(p.getNumberOfAllVotes());
            }
        }
    }

    @Test
    public void testUpdateRatingOfNews() {

        News news = generator.generatePersistentNews();

        NewsRating rating1 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating1.setRating(1);
        rating1.setParentEntity(news);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = newsRatingRepository.save(rating1);

        NewsRating rating2 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating2.setRating(-1);
        rating2.setParentEntity(news);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = newsRatingRepository.save(rating2);

        updateRatingsJob.updateRatingOfNews();

        news = newsRepository.findById(news.getId()).get();

        Assert.assertTrue(news.getLike() == 1L);
        Assert.assertTrue(news.getDislike() == 1L);
    }

    @Test
    public void testNewsUpdatedIndependetly() {

        NewsRating rating = generator.generatePersistentNewsRating();
        News news = rating.getParentEntity();
        news.setLike(null);
        news.setDislike(null);
        newsRepository.save(news);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(newsService).updateRating(Mockito.any());

        updateRatingsJob.updateRatingOfNews();

        for (News n : newsRepository.findAll()) {
            if (n.getId().equals(failedId[0])) {
                Assert.assertNull(n.getLike());
                Assert.assertNull(n.getDislike());
            } else {
                Assert.assertNotNull(n.getLike());
                Assert.assertNotNull(n.getDislike());
            }
        }
    }

}
