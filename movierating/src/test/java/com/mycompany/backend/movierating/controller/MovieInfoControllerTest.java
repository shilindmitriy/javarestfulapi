package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;
import com.mycompany.backend.movierating.service.MovieInfoService;

@WebMvcTest(controllers = MovieInfoController.class)
public class MovieInfoControllerTest extends BaseControllerTest {

    @MockBean
    private MovieInfoService infoService;

    @Test
    public void testGetCompanies() throws Exception {

        UUID movieId = UUID.randomUUID();
        CompanyReadDTO company = generator.generateObject(CompanyReadDTO.class);

        List<CompanyReadDTO> expectedCompanies = List.of(company);

        Mockito.when(infoService.getCompanies(movieId)).thenReturn(expectedCompanies);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/production-companies", movieId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<CompanyReadDTO> resultCpmpanies = objectMapper.readValue(resultJson,
                new TypeReference<List<CompanyReadDTO>>() {
                });
        Assertions.assertThat(expectedCompanies).isEqualTo(resultCpmpanies);
        Assert.assertEquals(expectedCompanies, resultCpmpanies);

        Mockito.verify(infoService, Mockito.times(1)).getCompanies(movieId);
    }

    @Test
    public void testGetCastAndCrew() throws Exception {

        UUID movieId = UUID.randomUUID();
        CastAndCrewReadDTO castAndCrew = generator.generateObject(CastAndCrewReadDTO.class);

        List<CastAndCrewReadDTO> expectedCastAndCrew = List.of(castAndCrew);

        Mockito.when(infoService.getCastAndCrew(movieId, Department.SOUND)).thenReturn(expectedCastAndCrew);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/cast-and-crew/{department}", movieId, Department.SOUND))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<CastAndCrewReadDTO> resultCastAndCrew = objectMapper.readValue(resultJson,
                new TypeReference<List<CastAndCrewReadDTO>>() {
                });
        Assertions.assertThat(expectedCastAndCrew).isEqualTo(resultCastAndCrew);
        Assert.assertEquals(expectedCastAndCrew, resultCastAndCrew);

        Mockito.verify(infoService, Mockito.times(1)).getCastAndCrew(movieId, Department.SOUND);
    }
}