package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.CreativePersonRating;
import com.mycompany.backend.movierating.service.BaseTest;

public class CreativePersonRatingRepositoryTest extends BaseTest {

    @Autowired
    private CreativePersonRatingRepository ratingRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        CreativePersonRating rating = new CreativePersonRating();
        ratingRepository.save(rating);
    }

    @Test
    public void testCreatedAtIsSet() {

        CreativePersonRating rating = generator.generatePersistentCreativePersonRating();

        Instant createdAtBeforeReload = rating.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        rating = ratingRepository.findById(rating.getId()).get();
        Instant createdAtAfterReload = rating.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        CreativePersonRating rating = generator.generatePersistentCreativePersonRating();

        Instant createdAtBeforeUpdate = rating.getCreatedAt();
        Instant updatedAtBeforeUpdate = rating.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        rating.setRating(9999);
        rating = ratingRepository.save(rating);

        Instant createdAtAfterUpdate = rating.getCreatedAt();
        Instant updatedAtAfterUpdate = rating.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}