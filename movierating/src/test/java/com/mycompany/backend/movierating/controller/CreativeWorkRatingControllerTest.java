package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.CreativeWorkRatingService;

@WebMvcTest(controllers = CreativeWorkRatingController.class)
public class CreativeWorkRatingControllerTest extends BaseControllerTest {

    @MockBean
    private CreativeWorkRatingService ratingService;

    @Test
    public void testGetRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        UUID workId = UUID.randomUUID();

        Mockito.when(ratingService.getRating(workId, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/creative-works/{workId}/ratings/{id}", workId, expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(ratingService, Mockito.times(1)).getRating(workId, expectedRead.getId());
    }

    @Test
    public void testGetShortRating() throws Exception {

        ShortRatingReadDTO expectedShortRating = generator.generateObject(ShortRatingReadDTO.class);
        UUID workId = UUID.randomUUID();

        Mockito.when(ratingService.getShortRating(workId)).thenReturn(expectedShortRating);
        String resultJson = mvc.perform(get("/api/v1/creative-works/{workId}/ratings", workId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        ShortRatingReadDTO resultShortRating = objectMapper.readValue(resultJson, ShortRatingReadDTO.class);
        Assert.assertEquals(expectedShortRating, resultShortRating);
        Mockito.verify(ratingService, Mockito.times(1)).getShortRating(workId);
    }

    @Test
    public void testCreateRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID workId = UUID.randomUUID();

        Mockito.when(ratingService.createRating(workId, create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/creative-works/{workId}/ratings", workId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).createRating(workId, create, userDetails);
    }

    @Test
    public void testCreateRatingWrongRatingAlreadyExists() throws Exception {

        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID workId = UUID.randomUUID();

        AlreadyExistsException exception = new AlreadyExistsException(CreativeWork.class, workId,
                userDetails.getId());

        Mockito.when(ratingService.createRating(workId, create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/creative-works/{workId}/ratings", workId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testPatchRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);
        UUID workId = UUID.randomUUID();

        Mockito.when(ratingService.patchRating(workId, expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/creative-works/{workId}/ratings/{id}", workId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).patchRating(workId, expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchRatingWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongWorkId = UUID.randomUUID();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(CreativeWorkRating.class, wrongId, wrongWorkId);

        Mockito.when(ratingService.patchRating(wrongWorkId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/creative-works/{workId}/ratings/{id}", wrongWorkId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateRating() throws Exception {

        UUID workId = UUID.randomUUID();
        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        Mockito.when(ratingService.updateRating(workId, expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/creative-works/{workId}/ratings/{id}", workId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).updateRating(workId, expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testDeleteRating() throws Exception {
        UUID workId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/creative-works/{workId}/ratings/{id}", workId, id)).andExpect(status().isOk());
        Mockito.verify(ratingService).deleteRating(workId, id);
    }

    @Test
    @WithMockUser
    public void testCreateRatingValidationFailed() throws Exception {

        UUID workId = UUID.randomUUID();
        RatingCreateDTO create = new RatingCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/creative-works/{workId}/ratings", workId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).createRating(ArgumentMatchers.eq(workId), ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateRatingValidationFailed() throws Exception {

        UUID workId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RatingPutDTO put = new RatingPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/creative-works/{workId}/ratings/{id}", workId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).updateRating(ArgumentMatchers.eq(workId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }
}