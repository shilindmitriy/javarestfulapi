package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePutDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadExtendedDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.LinkDuplicatedException;
import com.mycompany.backend.movierating.repository.MovieRepository;

public class MovieServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private TranslationService translationService;
    
    @Test
    public void testGetMovieExtended() {

        Movie movie = generator.generatePersistentMovie();

        MovieRating rating1 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating1.setParentEntity(movie);
        rating1.setUser(generator.generatePersistentUser());

        MovieRating rating2 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating2.setParentEntity(movie);
        rating2.setUser(generator.generatePersistentUser());

        List<MovieRating> ratings = new ArrayList<>();
        ratings.add(rating1);
        ratings.add(rating2);

        movie.setMovieRatings(ratings);
        Movie resultMovie = movieRepository.save(movie);

        inTransaction(() -> {
            MovieReadExtendedDTO readExtended = movieService.getMovieExtended(resultMovie.getId());
            Assertions.assertThat(readExtended).isEqualToIgnoringGivenFields(resultMovie, "urlPoster", "rating");
            Assert.assertTrue(
                    resultMovie.getProductionCountries().size() == readExtended.getProductionCountries().size());
            Assert.assertTrue(resultMovie.getProductionCountries().containsAll(readExtended.getProductionCountries()));
            Assert.assertTrue(resultMovie.getGenres().size() == readExtended.getGenres().size());
            Assert.assertTrue(resultMovie.getGenres().containsAll(readExtended.getGenres()));
            Assert.assertTrue(readExtended.getRating().getNumberOfAllVotes() == ratings.size());
            Assert.assertEquals(readExtended.getRating().getAverageRating(),
                    (double) (ratings.get(0).getRating() + ratings.get(1).getRating()) / ratings.size(), 0.01d);
        });
    }

    @Test
    public void testGetMovieExtendedEmptyRating() {

        Movie movie = generator.generatePersistentMovie();

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movie.getId()).get();
            MovieReadExtendedDTO readExtended = movieService.getMovieExtended(movie.getId());
            Assertions.assertThat(readExtended).isEqualToIgnoringGivenFields(resultMovie, "urlPoster", "rating");
            Assert.assertTrue(
                    resultMovie.getProductionCountries().size() == readExtended.getProductionCountries().size());
            Assert.assertTrue(resultMovie.getProductionCountries().containsAll(readExtended.getProductionCountries()));
            Assert.assertTrue(resultMovie.getGenres().size() == readExtended.getGenres().size());
            Assert.assertTrue(resultMovie.getGenres().containsAll(readExtended.getGenres()));
            Assert.assertTrue(readExtended.getRating().getNumberOfAllVotes() == 0);
            Assert.assertTrue(readExtended.getRating().getAverageRating() == null);
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieExtendedWrongIs() {
        movieService.getMovieExtended(UUID.randomUUID());
    }

    @Test
    public void testGetMoviesEmptyFilter() {

        Movie movie1 = generator.generatePersistentMovie();
        Movie movie2 = generator.generatePersistentMovie();
        Movie movie3 = generator.generatePersistentMovie();

        MovieFilter filter = new MovieFilter();

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie2.getId(), movie3.getId());
    }

    @Test
    public void testGetMoviesByTitle() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setTitle("title1");
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setTitle("title2");
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setTitle("title3");
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setTitle("title1");
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setTitle("title1");

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie4.getId());
    }

    @Test
    public void testGetMoviesByReleaseDateFrom() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setReleaseDate(LocalDate.of(1989, 11, 25));
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setReleaseDate(LocalDate.of(2019, 11, 25));
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setReleaseDate(LocalDate.of(2020, 01, 13));
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setReleaseDate(LocalDate.of(1995, 10, 12));
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setReleaseDateFrom(LocalDate.of(2019, 11, 25));

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie2.getId(), movie3.getId());
    }

    @Test
    public void testGetMoviesByReleaseDateTo() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setReleaseDate(LocalDate.of(1989, 11, 25));
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setReleaseDate(LocalDate.of(2019, 11, 25));
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setReleaseDate(LocalDate.of(2020, 01, 13));
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setReleaseDate(LocalDate.of(1995, 10, 12));
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setReleaseDateTo(LocalDate.of(1999, 11, 25));

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie4.getId());
    }

    @Test
    public void testGetMoviesByReleaseDate() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setReleaseDate(LocalDate.of(1989, 11, 25));
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setReleaseDate(LocalDate.of(2019, 11, 25));
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setReleaseDate(LocalDate.of(2020, 01, 13));
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setReleaseDate(LocalDate.of(1999, 11, 25));
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setReleaseDateFrom(LocalDate.of(1999, 11, 25));
        filter.setReleaseDateTo(LocalDate.of(2025, 11, 25));

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie2.getId(), movie3.getId(), movie4.getId());
    }

    @Test
    public void testGetMoviesByStatus() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setStatus(MovieStatus.NOT_RELEASED);
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setStatus(MovieStatus.RELEASED);
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setStatus(MovieStatus.RELEASED);
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setStatus(MovieStatus.RELEASED);
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setStatus(MovieStatus.NOT_RELEASED);

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId());
    }

    @Test
    public void testGetMoviesByRunningTime() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setRunningTime(LocalTime.of(2, 55));
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setRunningTime(LocalTime.of(0, 38));
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setRunningTime(LocalTime.of(2, 5));
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setRunningTime(LocalTime.of(1, 20));
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setRunningTimeFrom(LocalTime.of(2, 0));
        filter.setRunningTimeTo(LocalTime.of(3, 0));

        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie3.getId());
    }

    @Test
    public void testGetMoviesByProductionCountry() {

        Set<Country> productionCountry1 = new HashSet<>();
        productionCountry1.add(Country.UKRAINE);
        productionCountry1.add(Country.UNITED_STATES_OF_AMERICA);

        Set<Country> productionCountry2 = new HashSet<>();
        productionCountry2.add(Country.AFGHANISTAN);
        productionCountry2.add(Country.ALBANIA);

        Set<Country> productionCountry3 = new HashSet<>();
        productionCountry3.add(Country.BRAZIL);

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setProductionCountries(productionCountry1);
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setProductionCountries(productionCountry1);
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setProductionCountries(productionCountry2);
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setProductionCountries(productionCountry3);
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setProductionCountries(Set.of(Country.UKRAINE, Country.BRAZIL));
        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie2.getId(), movie4.getId());

        filter.setProductionCountries(Set.of());
        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie2.getId(), movie3.getId(), movie4.getId());
    }

    @Test
    public void testGetMoviesByGenres() {

        Set<Genre> productionGenre1 = new HashSet<>();
        productionGenre1.add(Genre.ACTION);
        productionGenre1.add(Genre.ADVENTURE);

        Set<Genre> productionGenre2 = new HashSet<>();
        productionGenre2.add(Genre.ANIMATION);

        Set<Genre> productionGenre3 = new HashSet<>();
        productionGenre3.add(Genre.COMEDY);

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setGenres(productionGenre1);
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setGenres(productionGenre1);
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setGenres(productionGenre2);
        movie3 = movieRepository.save(movie3);

        Movie movie4 = generator.generatePersistentMovie();
        movie4.setGenres(productionGenre3);
        movie4 = movieRepository.save(movie4);

        MovieFilter filter = new MovieFilter();
        filter.setGenres(Set.of(Genre.ACTION, Genre.COMEDY));
        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie2.getId(), movie4.getId());

        filter.setGenres(Set.of());
        Assertions.assertThat(movieService.getMovies(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(movie1.getId(), movie2.getId(), movie3.getId(), movie4.getId());
    }

    @Test
    public void testGetMoviesPagingAndSorting() {

        Movie movie1 = generator.generatePersistentMovie();
        movie1.setTitle("title1");
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.setTitle("title2");
        movie2 = movieRepository.save(movie2);

        Movie movie3 = generator.generatePersistentMovie();
        movie3.setTitle("title3");
        movie3 = movieRepository.save(movie3);

        MovieFilter filter = new MovieFilter();
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "title"));

        Assertions.assertThat(movieService.getMovies(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(movie1.getId(), movie2.getId(), movie3.getId()));
    }

    @Test
    public void testGetMoviesFull() {

        Movie movie1 = generator.generatePersistentMovie();
        Movie movie2 = generator.generatePersistentMovie();
        movie2.setTitle(movie1.getTitle());
        movie2.setReleaseDate(movie1.getReleaseDate());
        movie2.setStatus(movie1.getStatus());
        movie2.setRunningTime(movie1.getRunningTime());
        movie2.setStoryline(movie1.getStoryline());
        movie2.setProductionCountries(Set.copyOf(movie1.getProductionCountries()));
        movie2.setGenres(Set.copyOf(movie1.getGenres()));
        movie2 = movieRepository.save(movie2);

        Movie movie10 = generator.generatePersistentMovie();
        movie10 = movieRepository.save(movie10);

        MovieFilter filter = new MovieFilter();
        filter.setTitle(movie1.getTitle());
        filter.setReleaseDateFrom(movie1.getReleaseDate());
        filter.setReleaseDateTo(movie1.getReleaseDate().plusDays(1L));
        filter.setStatus(movie1.getStatus());
        filter.setRunningTimeFrom(LocalTime.of(0, 1));
        filter.setRunningTimeTo(LocalTime.of(23, 59));
        filter.setProductionCountries(Set.copyOf(movie1.getProductionCountries()));
        filter.setGenres(Set.copyOf(movie1.getGenres()));

        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "createdAt"));

        Assertions.assertThat(movieService.getMovies(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(movie1.getId(), movie2.getId()));
    }

    @Test
    public void testAddCompanyToMovie() {

        Movie movie = generator.generatePersistentMovie();

        ProductionCompany company = generator.generatePersistentProductionCompany();

        List<ProductionCompanyReadDTO> reads = movieService.addCompanyToMovie(movie.getId(), company.getId());
        ProductionCompanyReadDTO read = translationService.translate(company, ProductionCompanyReadDTO.class);
        Assertions.assertThat(reads).containsExactlyInAnyOrder(read);

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(resultMovie.getProductionCompanies()).extracting("id")
                    .containsExactlyInAnyOrder(company.getId());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        ProductionCompany company = generator.generatePersistentProductionCompany();
        movieService.addCompanyToMovie(UUID.randomUUID(), company.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCompanyId() {
        Movie movie = generator.generatePersistentMovie();
        movieService.addCompanyToMovie(movie.getId(), UUID.randomUUID());
    }

    @Test
    public void testDuplicatedCompany() {

        Movie movie = generator.generatePersistentMovie();
        ProductionCompany company = generator.generatePersistentProductionCompany();
        movieService.addCompanyToMovie(movie.getId(), company.getId());

        Assertions.assertThatThrownBy(() -> {
            movieService.addCompanyToMovie(movie.getId(), company.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test
    public void testAddCreativePersonToMovie() {

        Movie movie = generator.generatePersistentMovie();
        CreativePerson person = generator.generatePersistentCreativePerson();

        List<CreativePersonReadDTO> reads = movieService.addCreativePersonToMovie(movie.getId(), person.getId());
        CreativePersonReadDTO read = translationService.translate(person, CreativePersonReadDTO.class);
        Assertions.assertThat(reads).containsExactlyInAnyOrder(read);

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(resultMovie.getFilmCrew()).extracting("id").containsExactlyInAnyOrder(person.getId());
        });
    }

    @Test
    public void testDuplicatedCreativePerson() {

        Movie movie = generator.generatePersistentMovie();
        CreativePerson person = generator.generatePersistentCreativePerson();
        movieService.addCreativePersonToMovie(movie.getId(), person.getId());

        Assertions.assertThatThrownBy(() -> {
            movieService.addCreativePersonToMovie(movie.getId(), person.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test
    public void testCreateMovie() {

        MovieCreateDTO create = generator.generateObject(MovieCreateDTO.class);

        MovieReadDTO read = movieService.createMovie(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        inTransaction(() -> {
            Movie movie = movieRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(movie, "urlPoster");
            Assert.assertTrue(movie.getProductionCountries().size() == read.getProductionCountries().size());
            Assert.assertTrue(movie.getProductionCountries().containsAll(read.getProductionCountries()));
            Assert.assertTrue(movie.getGenres().size() == read.getGenres().size());
            Assert.assertTrue(movie.getGenres().containsAll(read.getGenres()));
        });
    }

    @Test
    public void testPatchMovie() {

        Movie movie = generator.generatePersistentMovie();

        MoviePatchDTO patch = generator.generateObject(MoviePatchDTO.class);

        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultMovie, "urlPoster");
            Assert.assertTrue(resultMovie.getProductionCountries().size() == read.getProductionCountries().size());
            Assert.assertTrue(resultMovie.getProductionCountries().containsAll(read.getProductionCountries()));
            Assert.assertTrue(resultMovie.getGenres().size() == read.getGenres().size());
            Assert.assertTrue(resultMovie.getGenres().containsAll(read.getGenres()));
        });
    }

    @Test
    public void testPatchMovieEmptyPatch() {

        Movie movie = generator.generatePersistentMovie();

        inTransaction(() -> {
            MovieReadDTO read = movieService.patchMovie(movie.getId(), new MoviePatchDTO());
            Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("urlPoster");
            Assert.assertFalse(read.getProductionCountries().isEmpty());
            Assert.assertFalse(read.getGenres().isEmpty());

            Movie resultMovie = movieRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultMovie, "urlPoster");
            Assert.assertTrue(resultMovie.getProductionCountries().size() == read.getProductionCountries().size());
            Assert.assertTrue(resultMovie.getProductionCountries().containsAll(read.getProductionCountries()));
            Assert.assertTrue(resultMovie.getGenres().size() == read.getGenres().size());
            Assert.assertTrue(resultMovie.getGenres().containsAll(read.getGenres()));
        });
    }

    @Test
    public void testUpdateMovie() {

        Movie movie = generator.generatePersistentMovie();

        MoviePutDTO put = generator.generateObject(MoviePutDTO.class);

        MovieReadDTO read = movieService.updateMovie(movie.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultMovie, "urlPoster");
            Assert.assertTrue(resultMovie.getProductionCountries().size() == read.getProductionCountries().size());
            Assert.assertTrue(resultMovie.getProductionCountries().containsAll(read.getProductionCountries()));
            Assert.assertTrue(resultMovie.getGenres().size() == read.getGenres().size());
            Assert.assertTrue(resultMovie.getGenres().containsAll(read.getGenres()));
        });
    }

    @Test
    public void testDeleteMovie() {

        Movie movie = generator.generatePersistentMovie();
        Assert.assertTrue(movieRepository.existsById(movie.getId()));

        movieService.deleteMovie(movie.getId());
        Assert.assertFalse(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieNewsNotFound() {
        movieService.deleteMovie(UUID.randomUUID());
    }

    @Test
    public void testRemoveCompanyFromMovie() {

        Movie movie = generator.generatePersistentMovie();
        ProductionCompany company = generator.generatePersistentProductionCompany();

        List<ProductionCompanyReadDTO> addCompany = movieService.addCompanyToMovie(movie.getId(), company.getId());
        Assert.assertFalse(addCompany.isEmpty());

        List<ProductionCompanyReadDTO> removeCompany = movieService.removeCompanyFromMovie(movie.getId(),
                company.getId());
        Assert.assertTrue(removeCompany.isEmpty());

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertTrue(resultMovie.getProductionCompanies().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCompany() {
        Movie movie = generator.generatePersistentMovie();
        ProductionCompany company = generator.generatePersistentProductionCompany();
        movieService.removeCompanyFromMovie(movie.getId(), company.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedCompany() {
        Movie movie = generator.generatePersistentMovie();
        movieService.removeCompanyFromMovie(movie.getId(), UUID.randomUUID());
    }

    @Test
    public void testRemoveCreativePersonFromMovie() {

        Movie movie = generator.generatePersistentMovie();
        CreativePerson person = generator.generatePersistentCreativePerson();

        List<CreativePersonReadDTO> addPerson = movieService.addCreativePersonToMovie(movie.getId(), person.getId());
        Assert.assertFalse(addPerson.isEmpty());

        List<CreativePersonReadDTO> removePerson = movieService.removeCreativePersonFromMovie(movie.getId(),
                person.getId());
        Assert.assertTrue(removePerson.isEmpty());

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movie.getId()).get();
            Assert.assertTrue(resultMovie.getFilmCrew().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCreativePerson() {
        Movie movie = generator.generatePersistentMovie();
        CreativePerson person = generator.generatePersistentCreativePerson();
        movieService.removeCreativePersonFromMovie(movie.getId(), person.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedCreativePerson() {
        Movie movie = generator.generatePersistentMovie();
        movieService.removeCreativePersonFromMovie(movie.getId(), UUID.randomUUID());
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}