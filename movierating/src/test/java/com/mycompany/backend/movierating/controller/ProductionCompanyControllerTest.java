package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyCreateDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPatchDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPutDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.ProductionCompanyService;

@WebMvcTest(controllers = ProductionCompanyController.class)
public class ProductionCompanyControllerTest extends BaseControllerTest {

    @MockBean
    private ProductionCompanyService companyService;

    @Test
    public void testGetProductionCompany() throws Exception {

        ProductionCompanyReadDTO expectedRead = generator.generateObject(ProductionCompanyReadDTO.class);

        Mockito.when(companyService.getProductionCompany(expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/production-companies/{id}", expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO resultRead = objectMapper.readValue(resultJson, ProductionCompanyReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(companyService).getProductionCompany(expectedRead.getId());
    }

    @Test
    @WithMockUser
    public void testCreateProductionCompany() throws Exception {

        ProductionCompanyReadDTO expectedRead = generator.generateObject(ProductionCompanyReadDTO.class);
        ProductionCompanyCreateDTO create = generator.generateObject(ProductionCompanyCreateDTO.class);

        Mockito.when(companyService.createProductionCompany(create)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/production-companies").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO resultRead = objectMapper.readValue(resultJson, ProductionCompanyReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(companyService).createProductionCompany(create);
    }

    @Test
    @WithMockUser
    public void testCreateProductionCompanyValidationFailed() throws Exception {

        ProductionCompanyCreateDTO create = new ProductionCompanyCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/production-companies").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(companyService, Mockito.never()).createProductionCompany(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchProductionCompany() throws Exception {

        ProductionCompanyReadDTO expectedRead = generator.generateObject(ProductionCompanyReadDTO.class);
        ProductionCompanyPatchDTO patch = generator.generateObject(ProductionCompanyPatchDTO.class);

        Mockito.when(companyService.patchProductionCompany(expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{id}", expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO resultRead = objectMapper.readValue(resultJson, ProductionCompanyReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(companyService).patchProductionCompany(expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchProductionCompanyValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            name.append("1234567890");
        }

        ProductionCompanyPatchDTO patch = new ProductionCompanyPatchDTO();
        patch.setName(name.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(companyService, Mockito.never()).patchProductionCompany(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateProductionCompany() throws Exception {

        ProductionCompanyReadDTO expectedRead = generator.generateObject(ProductionCompanyReadDTO.class);
        ProductionCompanyPutDTO put = generator.generateObject(ProductionCompanyPutDTO.class);

        Mockito.when(companyService.updateProductionCompany(expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{id}", expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ProductionCompanyReadDTO resultRead = objectMapper.readValue(resultJson, ProductionCompanyReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(companyService).updateProductionCompany(expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testUpdateProductionCompanyValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        ProductionCompanyPutDTO put = new ProductionCompanyPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(companyService, Mockito.never()).updateProductionCompany(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteProductionCompany() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/production-companies/{id}", id)).andExpect(status().isOk());
        Mockito.verify(companyService).deleteProductionCompany(id);
    }

}
