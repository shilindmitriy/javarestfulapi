package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.service.BaseTest;

public class MovieReviewRepositoryTest extends BaseTest {

    @Autowired
    private MovieReviewRepository reviewRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        MovieReview review = new MovieReview();
        reviewRepository.save(review);
    }

    @Test
    public void testCreatedAtIsSet() {

        MovieReview review = generator.generatePersistentMovieReview();

        Instant createdAtBeforeReload = review.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        review = reviewRepository.findById(review.getId()).get();
        Instant createdAtAfterReload = review.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        MovieReview review = generator.generatePersistentMovieReview();

        Instant createdAtBeforeUpdate = review.getCreatedAt();
        Instant updatedAtBeforeUpdate = review.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        review.setText("updated");
        review = reviewRepository.save(review);

        Instant createdAtAfterUpdate = review.getCreatedAt();
        Instant updatedAtAfterUpdate = review.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}