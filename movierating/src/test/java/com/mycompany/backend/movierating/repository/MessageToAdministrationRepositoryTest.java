package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.service.BaseTest;

public class MessageToAdministrationRepositoryTest extends BaseTest {

    @Autowired
    private MessageToAdministrationRepository messageToAdministrationRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        MessageToAdministration message = new MessageToAdministration();
        messageToAdministrationRepository.save(message);
    }

    @Test
    public void testCreatedAtIsSet() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        Instant createdAtBeforeReload = message.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        message = messageToAdministrationRepository.findById(message.getId()).get();
        Instant createdAtAfterReload = message.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        Instant createdAtBeforeUpdate = message.getCreatedAt();
        Instant updatedAtBeforeUpdate = message.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        message.setMessage("Lemon");
        message = messageToAdministrationRepository.save(message);

        Instant createdAtAfterUpdate = message.getCreatedAt();
        Instant updatedAtAfterUpdate = message.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}
