package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;

public class CreativeWorkServiceTest extends BaseTest {

    @Autowired
    private CreativeWorkService workService;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Test
    public void testGetCreativeWorkForMovie() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        CreativeWorkReadDTO read = workService.getCreativeWork(work.getMovie().getId(), work.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(work, "personId", "movieId");
        Assert.assertTrue(work.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(work.getMovie().getId().equals(read.getMovieId()));
    }

    @Test
    public void testGetCreativeWorkForCreativePerson() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        CreativeWorkReadDTO read = workService.getCreativeWork(work.getCreativePerson().getId(), work.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(work, "personId", "movieId");
        Assert.assertTrue(work.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(work.getMovie().getId().equals(read.getMovieId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCreativeWorkNotFound() {
        workService.getCreativeWork(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testCreateCreativeWork() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        Movie movie = generator.generatePersistentMovie();

        CreativeWorkCreateDTO create = generator.generateObject(CreativeWorkCreateDTO.class);
        create.setPersonId(person.getId());

        CreativeWorkReadDTO read = workService.createCreativeWork(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(movie.getId().equals(read.getMovieId()));
        Assert.assertNotNull(read.getId());

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateCreativeWorkNotFound() {
        workService.createCreativeWork(UUID.randomUUID(), new CreativeWorkCreateDTO());
    }

    @Test
    public void testPatchCreativeWorkForMovie() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        CreativePerson newPerson = generator.generatePersistentCreativePerson();

        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);
        patch.setPersonId(newPerson.getId());
        patch.setMovieId(work.getMovie().getId());

        CreativeWorkReadDTO read = workService.patchCreativeWork(work.getMovie().getId(), work.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertTrue(newPerson.getId().equals(read.getPersonId()));
        Assert.assertTrue(work.getMovie().getId().equals(read.getMovieId()));

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPatchCreativeWorkNotFound() {
        workService.patchCreativeWork(UUID.randomUUID(), UUID.randomUUID(), new CreativeWorkPatchDTO());
    }

    @Test
    public void testPatchCreativeWorkForCreativePerson() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        Movie newMovie = generator.generatePersistentMovie();

        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);
        patch.setMovieId(newMovie.getId());
        patch.setPersonId(work.getCreativePerson().getId());

        CreativeWorkReadDTO read = workService.patchCreativeWork(work.getCreativePerson().getId(), work.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertTrue(work.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(newMovie.getId().equals(read.getMovieId()));

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test
    public void testPatchCreativeWorkEmptyPatch() {

        CreativeWork work = generator.generatePersistentCreativeWork();

        CreativeWorkReadDTO read = workService.patchCreativeWork(work.getMovie().getId(), work.getId(),
                new CreativeWorkPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(work, "personId", "movieId");
        Assert.assertTrue(work.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(work.getMovie().getId().equals(read.getMovieId()));

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test
    public void testUpdateCreativeWorkForMovie() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        CreativePerson newPerson = generator.generatePersistentCreativePerson();

        CreativeWorkPutDTO put = generator.generateObject(CreativeWorkPutDTO.class);
        put.setPersonId(newPerson.getId());
        put.setMovieId(work.getMovie().getId());

        CreativeWorkReadDTO read = workService.updateCreativeWork(work.getMovie().getId(), work.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertTrue(newPerson.getId().equals(read.getPersonId()));
        Assert.assertTrue(work.getMovie().getId().equals(read.getMovieId()));

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test
    public void testUpdateCreativeWorkForCreativePerson() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        Movie newMovie = generator.generatePersistentMovie();

        CreativeWorkPutDTO put = generator.generateObject(CreativeWorkPutDTO.class);
        put.setMovieId(newMovie.getId());
        put.setPersonId(work.getCreativePerson().getId());

        CreativeWorkReadDTO read = workService.updateCreativeWork(work.getCreativePerson().getId(), work.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertTrue(work.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(newMovie.getId().equals(read.getMovieId()));

        CreativeWork resultWork = workRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultWork, "personId", "movieId");
        Assert.assertTrue(resultWork.getCreativePerson().getId().equals(read.getPersonId()));
        Assert.assertTrue(resultWork.getMovie().getId().equals(read.getMovieId()));
    }

    @Test
    public void testDeleteCreativeWorkForMovie() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        Assert.assertTrue(workRepository.existsById(work.getId()));

        workService.deleteCreativeWork(work.getMovie().getId(), work.getId());
        Assert.assertFalse(workRepository.existsById(work.getId()));
    }

    @Test
    public void testDeleteCreativeWorkForCreativePerson() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        Assert.assertTrue(workRepository.existsById(work.getId()));

        workService.deleteCreativeWork(work.getCreativePerson().getId(), work.getId());
        Assert.assertFalse(workRepository.existsById(work.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCreativeWorkNotFound() {
        workService.deleteCreativeWork(UUID.randomUUID(), UUID.randomUUID());
    }

}