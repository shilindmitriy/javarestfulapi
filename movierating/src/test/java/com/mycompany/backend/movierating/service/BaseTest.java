package com.mycompany.backend.movierating.service;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from user_account", "delete from creative_person", "delete from movie",
        "delete from creative_work", "delete from movie", "delete from news", "delete from message_to_administration",
        "delete from production_company", "delete from external_system_import",
        "delete from review_like" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {

    @Autowired
    protected GeneratorDTOAndEntitiesForTest generator;

}
