package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.service.BaseTest;

public class NewsRepositoryTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        News news = new News();
        newsRepository.save(news);
    }

    @Test
    public void testCreatedAtIsSet() {

        News news = generator.generatePersistentNews();

        Instant createdAtBeforeReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        news = newsRepository.findById(news.getId()).get();
        Instant createdAtAfterReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        News news = generator.generatePersistentNews();

        Instant createdAtBeforeUpdate = news.getCreatedAt();
        Instant updatedAtBeforeUpdate = news.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        news.setText("updated");
        news = newsRepository.save(news);

        Instant createdAtAfterUpdate = news.getCreatedAt();
        Instant updatedAtAfterUpdate = news.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}