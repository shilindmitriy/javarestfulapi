package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewConfirmationRequest;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.CreativePersonReviewRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkReviewRepository;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyReviewRepository;

public class ReviewServiceTest extends BaseTest {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private CreativePersonReviewRepository personReviewRepository;

    @Autowired
    private CreativeWorkReviewRepository workReviewRepository;

    @Autowired
    private ProductionCompanyReviewRepository companyReviewRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Test
    public void testGetReviewsFromCreativePersonReview() {

        CreativePersonReview reviewTrue = generator.generatePersistentCreativePersonReview();
        reviewTrue.setEnabled(true);
        reviewTrue = personReviewRepository.save(reviewTrue);

        CreativePersonReview reviewFalse = generator.generatePersistentCreativePersonReview();
        reviewFalse.setEnabled(false);
        reviewFalse = personReviewRepository.save(reviewFalse);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(ReviewType.CREATIVE_PERSON_REVIEW, pageable);
        Assert.assertFalse(reviews.getData().isEmpty());
        Assert.assertTrue(reviews.getData().size() == 1);
        Assertions.assertThat(reviews.getData()).extracting("id").containsExactlyInAnyOrder(reviewFalse.getId());
    }

    @Test
    public void testGetReviewsFromCreativeWorkReview() {

        CreativeWorkReview reviewTrue = generator.generatePersistentCreativeWorkReview();
        reviewTrue.setEnabled(true);
        reviewTrue = workReviewRepository.save(reviewTrue);

        CreativeWorkReview reviewFalse = generator.generatePersistentCreativeWorkReview();
        reviewFalse.setEnabled(false);
        reviewFalse = workReviewRepository.save(reviewFalse);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(ReviewType.CREATIVE_WORK_REVIEW, pageable);
        Assert.assertFalse(reviews.getData().isEmpty());
        Assert.assertTrue(reviews.getData().size() == 1);
        Assertions.assertThat(reviews.getData()).extracting("id").containsExactlyInAnyOrder(reviewFalse.getId());
    }

    @Test
    public void testGetReviewsFromProductionCompanyReview() {

        ProductionCompanyReview reviewTrue = generator.generatePersistentProductionCompanyReview();
        reviewTrue.setEnabled(true);
        reviewTrue = companyReviewRepository.save(reviewTrue);

        ProductionCompanyReview reviewFalse = generator.generatePersistentProductionCompanyReview();
        reviewFalse.setEnabled(false);
        reviewFalse = companyReviewRepository.save(reviewFalse);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(ReviewType.PRODUCTION_COMPANY_REVIEW, pageable);
        Assert.assertFalse(reviews.getData().isEmpty());
        Assert.assertTrue(reviews.getData().size() == 1);
        Assertions.assertThat(reviews.getData()).extracting("id").containsExactlyInAnyOrder(reviewFalse.getId());
    }

    @Test
    public void testGetReviewsFromMovieReview() {

        MovieReview reviewTrue = generator.generatePersistentMovieReview();
        reviewTrue.setEnabled(true);
        reviewTrue = movieReviewRepository.save(reviewTrue);

        MovieReview reviewFalse = generator.generatePersistentMovieReview();
        reviewFalse.setEnabled(false);
        reviewFalse = movieReviewRepository.save(reviewFalse);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(ReviewType.MOVIE_REVIEW, pageable);
        Assert.assertFalse(reviews.getData().isEmpty());
        Assert.assertTrue(reviews.getData().size() == 1);
        Assertions.assertThat(reviews.getData()).extracting("id").containsExactlyInAnyOrder(reviewFalse.getId());
    }

    @Test
    public void testEnableReview() {

        MovieReview review = generator.generatePersistentMovieReview();
        review.setEnabled(false);
        review = movieReviewRepository.save(review);

        ReviewConfirmationRequest confirmationRequest = new ReviewConfirmationRequest();
        confirmationRequest.setReviewType(ReviewType.MOVIE_REVIEW);
        confirmationRequest.setParentId(review.getParentEntity().getId());
        confirmationRequest.setReviewId(review.getId());

        Boolean performed = reviewService.enableReview(confirmationRequest);
        Assert.assertTrue(performed);

        MovieReview resultReview = movieReviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assert.assertNotEquals(review.getEnabled(), resultReview.getEnabled());
        Assert.assertTrue(resultReview.getEnabled());
    }

    @Test
    public void testEnableReviewWrongId() {

        MovieReview review = generator.generatePersistentMovieReview();
        review.setEnabled(false);
        review = movieReviewRepository.save(review);

        UUID wrongId = UUID.randomUUID();

        ReviewConfirmationRequest confirmationRequest = new ReviewConfirmationRequest();
        confirmationRequest.setReviewType(ReviewType.MOVIE_REVIEW);
        confirmationRequest.setParentId(review.getParentEntity().getId());
        confirmationRequest.setReviewId(wrongId);

        Boolean performed = reviewService.enableReview(confirmationRequest);
        Assert.assertFalse(performed);

        MovieReview resultReview = movieReviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assert.assertEquals(review.getEnabled(), resultReview.getEnabled());
        Assert.assertFalse(resultReview.getEnabled());
    }
}
