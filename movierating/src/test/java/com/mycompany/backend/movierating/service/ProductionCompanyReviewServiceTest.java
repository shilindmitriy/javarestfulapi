package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyReviewRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class ProductionCompanyReviewServiceTest extends BaseTest {

    @Autowired
    private ProductionCompanyReviewService reviewService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private ProductionCompanyReviewRepository reviewRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetReview() {
        ProductionCompanyReview review = generator.generatePersistentProductionCompanyReview();
        ReviewReadDTO read = reviewService.getReview(review.getParentEntity().getId(), review.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewWrongId() {
        reviewService.getReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetReviews() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        UUID companyId = company.getId();
        UserAccount user1 = generator.generatePersistentUser();
        UserAccount user2 = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user2);

        ProductionCompanyReview review1 = generator.generateFlatEntityWithoutId(ProductionCompanyReview.class);
        review1.setEnabled(true);
        review1.setParentEntity(company);
        review1.setUser(user1);

        ProductionCompanyReview review2 = generator.generateFlatEntityWithoutId(ProductionCompanyReview.class);
        review2.setEnabled(false);
        review2.setParentEntity(company);
        review2.setUser(user2);

        company.getCompanyReviews().add(review1);
        company.getCompanyReviews().add(review2);
        company = companyRepository.save(company);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        inTransaction(() -> {
            ProductionCompany resultCompany = companyRepository.findById(companyId).get();
            PageResult<ReviewReadDTO> creviews = reviewService.getReviews(companyId, userDetails, pageable);
            Assert.assertTrue(creviews.getData().size() == resultCompany.getCompanyReviews().size());
            ProductionCompanyReview resultReview1 = resultCompany.getCompanyReviews().get(0);
            ProductionCompanyReview resultReview2 = resultCompany.getCompanyReviews().get(1);
            Assertions.assertThat(creviews.getData()).extracting("id").containsExactlyInAnyOrder(resultReview1.getId(),
                    resultReview2.getId());

        });
    }

    @Test
    public void testGetReviewsEmpty() {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(UUID.randomUUID(), null, pageable);
        Assert.assertTrue(reviews.getData().isEmpty());
    }

    @Test
    public void testCreateReviewWithSufficientTrust() {

        ProductionCompany comapany = generator.generatePersistentProductionCompany();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(100);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(comapany.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getEnabled());
        Assert.assertNotNull(read.getId());

        ProductionCompanyReview resultReview = reviewRepository.findByParentIdAndId(comapany.getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertTrue(resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test
    public void testCreateReviewWithoutSufficientTrust() {

        ProductionCompany comapany = generator.generatePersistentProductionCompany();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(9);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(comapany.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertFalse(read.getEnabled());
        Assert.assertNotNull(read.getId());

        ProductionCompanyReview resultReview = reviewRepository.findByParentIdAndId(comapany.getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertFalse(resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateReviewWithNegativeTrust() {

        ProductionCompany comapany = generator.generatePersistentProductionCompany();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(-1);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        reviewService.createReview(comapany.getId(), create, userDetails);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateReviewNotFoundNews() {
        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        reviewService.createReview(UUID.randomUUID(), new ReviewCreateDTO(), new UserDetailsImpl(user));
    }

    @Test
    public void testPatchReview() {

        ProductionCompanyReview Review = generator.generatePersistentProductionCompanyReview();
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        ReviewReadDTO read = reviewService.patchReview(Review.getParentEntity().getId(), Review.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProductionCompanyReview resultReview = reviewRepository
                .findByParentIdAndId(Review.getParentEntity().getId(), Review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

    }

    @Test
    public void testPatchReviewEmptyPatch() {

        ProductionCompanyReview review = generator.generatePersistentProductionCompanyReview();

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(),
                new ReviewPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        ProductionCompanyReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPatchReviewNotFoundReview() {
        reviewService.patchReview(UUID.randomUUID(), UUID.randomUUID(), new ReviewPatchDTO());
    }

    @Test
    public void testUpdateReview() {

        ProductionCompanyReview review = generator.generatePersistentProductionCompanyReview();
        ReviewPutDTO put = generator.generateObject(ReviewPutDTO.class);

        ReviewReadDTO read = reviewService.updateReview(review.getParentEntity().getId(), review.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProductionCompanyReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testDeleteReview() {

        ProductionCompanyReview review = generator.generatePersistentProductionCompanyReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));

    }

    @Test
    public void testDeleteReviewWithLike() {

        ProductionCompanyReview review = generator.generatePersistentProductionCompanyReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        ReviewLike likeForReview = new ReviewLike();
        likeForReview.setLike(true);
        likeForReview.setReviewType(ReviewType.PRODUCTION_COMPANY_REVIEW);
        likeForReview.setReviewId(review.getId());
        likeForReview.setUserId(UUID.randomUUID());
        likeForReview = reviewLikeRepository.save(likeForReview);

        ReviewLike like = new ReviewLike();
        like.setLike(true);
        like.setReviewType(ReviewType.CREATIVE_PERSON_REVIEW);
        like.setReviewId(UUID.randomUUID());
        like.setUserId(UUID.randomUUID());
        like = reviewLikeRepository.save(like);

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
        Assert.assertFalse(reviewLikeRepository.existsById(likeForReview.getId()));
        Assert.assertTrue(reviewLikeRepository.existsById(like.getId()));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}
