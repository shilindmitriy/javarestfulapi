package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewConfirmationRequest;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.dto.user.ModeratorsTrustDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.ReviewService;
import com.mycompany.backend.movierating.service.UserService;

@WebMvcTest(controllers = ModeratorController.class)
public class ModeratorControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private UserService userService;

    @Test
    @WithMockUser
    public void testGetReviews() throws Exception {

        UUID moderatorId = UUID.randomUUID();
        ReviewType reviewType = ReviewType.MOVIE_REVIEW;

        PageResult<ReviewReadDTO> expectedReviews = new PageResult<>();
        expectedReviews.getData().add(generator.generateObject(ReviewReadDTO.class));
        expectedReviews.setPage(0);
        expectedReviews.setPageSize(10);
        expectedReviews.setTotalPages(1);
        expectedReviews.setTotalElements(1);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        Mockito.when(reviewService.getReviews(reviewType, pageable)).thenReturn(expectedReviews);

        String resultJson = mvc
                .perform(get("/api/v1/moderators/{moderatorId}/reviews-type/{reviewType}", moderatorId, reviewType)
                        .param("page", "0").param("size", "10").param("sort", "createdAt,desc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ReviewReadDTO> resultReviews = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<ReviewReadDTO>>() {
                });

        Assert.assertTrue(resultReviews.getData().size() == expectedReviews.getData().size());
        Assert.assertTrue(resultReviews.getData().containsAll(expectedReviews.getData()));
        Assert.assertTrue(resultReviews.equals(expectedReviews));

        Mockito.verify(reviewService).getReviews(reviewType, pageable);
    }

    @Test
    @WithMockUser
    public void testConfirmReview() throws Exception {

        UUID moderatorId = UUID.randomUUID();
        Boolean performed = true;
        ReviewConfirmationRequest confirmationRequest = generator.generateObject(ReviewConfirmationRequest.class);

        Mockito.when(reviewService.enableReview(confirmationRequest)).thenReturn(performed);

        String resultJson = mvc
                .perform(post("/api/v1/moderators/{moderatorId}/confirmed-review", moderatorId)
                        .content(objectMapper.writeValueAsString(confirmationRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Boolean resultPerformed = objectMapper.readValue(resultJson, Boolean.class);
        Assert.assertEquals(performed, resultPerformed);

        Mockito.verify(reviewService).enableReview(confirmationRequest);
    }

    @Test
    @WithMockUser
    public void testConfirmReviewValidationFailed() throws Exception {

        UUID moderatorId = UUID.randomUUID();
        ReviewConfirmationRequest confirmationRequest = new ReviewConfirmationRequest();

        String resultJson = mvc
                .perform(post("/api/v1/moderators/{moderatorId}/confirmed-review/", moderatorId)
                        .content(objectMapper.writeValueAsString(confirmationRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);

        Mockito.verify(reviewService, Mockito.never()).enableReview(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testAddModeratorsTrust() throws Exception {

        UUID userId = UUID.randomUUID();
        UUID moderatorId = UUID.randomUUID();
        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);
        ModeratorsTrustDTO moderatorsTrust = generator.generateObject(ModeratorsTrustDTO.class);

        Mockito.when(userService.addModeratorsTrust(userId, moderatorsTrust)).thenReturn(read);

        String resultJson = mvc
                .perform(post("/api/v1/moderators/{moderatorId}/users/{userId}/moderators-trust", moderatorId, userId)
                        .content(objectMapper.writeValueAsString(moderatorsTrust))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assertions.assertThat(resultUser).isEqualToComparingFieldByField(read);

        Mockito.verify(userService).addModeratorsTrust(userId, moderatorsTrust);
    }

    @Test
    @WithMockUser
    public void testAddModeratorsTrustValidationFailed() throws Exception {

        UUID userId = UUID.randomUUID();
        UUID moderatorId = UUID.randomUUID();
        ModeratorsTrustDTO moderatorsTrust = new ModeratorsTrustDTO();

        String resultJson = mvc
                .perform(post("/api/v1/moderators/{moderatorId}/users/{userId}/moderators-trust", moderatorId, userId)
                        .content(objectMapper.writeValueAsString(moderatorsTrust))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(userService, Mockito.never()).addModeratorsTrust(ArgumentMatchers.eq(userId),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testBanUser() throws Exception {

        UUID userId = UUID.randomUUID();
        UUID moderatorId = UUID.randomUUID();
        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);

        Mockito.when(userService.banUser(userId)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/moderators/{moderatorId}/users/{userId}/banned-user", moderatorId, userId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assertions.assertThat(resultUser).isEqualToComparingFieldByField(read);

        Mockito.verify(userService).banUser(userId);
    }

    @Test
    @WithMockUser
    public void testBanUserIsAdmin() throws Exception {

        UUID userId = UUID.randomUUID();
        UUID moderatorId = UUID.randomUUID();

        AccessDeniedException exception = new AccessDeniedException("Admin cannot be banned.");

        Mockito.when(userService.banUser(userId)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/moderators/{moderatorId}/users/{userId}/banned-user", moderatorId, userId))
                .andExpect(status().isForbidden()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

}
