package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.ModeratorsTrustDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.UserAccountRepository;

public class UserServiceTest extends BaseTest {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private UserService userService;

    @Test
    public void testGetUser() {
        UserAccount user = generator.generatePersistentUser();
        UserAccountReadDTO userDTO = userService.getUser(user.getId());
        Assertions.assertThat(userDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongIs() {
        userService.getUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        UserAccountCreateDTO create = generator.generateObject(UserAccountCreateDTO.class);

        UserAccountReadDTO read = userService.createUser(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        UserAccount resultUser = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(resultUser);
    }

    @Test
    public void testPatchUser() {

        UserAccount user = generator.generatePersistentUser();
        UserAccountPatchDTO patch = generator.generateObject(UserAccountPatchDTO.class);

        UserAccountReadDTO read = userService.patchUser(user.getId(), patch);
        Assertions.assertThat(patch).isEqualToIgnoringGivenFields(read, "password");

        user = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUserEmptyPatch() {

        UserAccount user = generator.generatePersistentUser();

        UserAccountReadDTO read = userService.patchUser(user.getId(), new UserAccountPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);

        UserAccount userAfterUpdate = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(userAfterUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(userAfterUpdate);
    }

    @Test
    public void testChangeAuthority() {

        UserAccount user = generator.generatePersistentUser();
        AuthorityPatchDTO patch = generator.generateObject(AuthorityPatchDTO.class);

        UserAccountReadDTO read = userService.changeAuthority(user.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testChangeAuthorityEmptyPatch() {

        UserAccount user = generator.generatePersistentUser();

        UserAccountReadDTO read = userService.changeAuthority(user.getId(),
                new AuthorityPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);

        UserAccount userAfterUpdate = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(userAfterUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(userAfterUpdate);
    }

    @Test
    public void testUpdateUser() {

        UserAccount user = generator.generatePersistentUser();
        UserAccountPutDTO put = generator.generateObject(UserAccountPutDTO.class);

        UserAccountReadDTO read = userService.updateUser(user.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "password");

        user = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testDeleteUser() {
        UserAccount user = generator.generatePersistentUser();
        userService.deleteUser(user.getId());
        Assert.assertFalse(userAccountRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        userService.deleteUser(UUID.randomUUID());
    }

    @Test
    public void testAddModeratorsTrustWithNotNullRating() {

        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        user.setModeratorsTrust(10);
        user = userAccountRepository.save(user);

        ModeratorsTrustDTO moderatorsTrust = new ModeratorsTrustDTO();
        moderatorsTrust.setRating(10);

        UserAccountReadDTO read = userService.addModeratorsTrust(user.getId(), moderatorsTrust);
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(user, "moderatorsTrust", "updatedAt");
        Assert.assertTrue(moderatorsTrust.getRating() + user.getModeratorsTrust() == read.getModeratorsTrust());

        UserAccount resultUser = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(resultUser);
    }

    @Test
    public void testBanUser() {

        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        user.setAuthority(Authority.REGISTERED_USER);
        user = userAccountRepository.save(user);

        UserAccountReadDTO read = userService.banUser(user.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(user, "authority", "updatedAt");
        Assert.assertEquals(Authority.UNREGISTERED_USER, read.getAuthority());

        UserAccount resultUser = userAccountRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(resultUser);
    }

    @Test(expected = AccessDeniedException.class)
    public void testBanAdminUser() {

        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        user.setAuthority(Authority.ADMIN);
        user = userAccountRepository.save(user);

        userService.banUser(user.getId());
    }
}