package com.mycompany.backend.movierating.security;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.service.BaseTest;

public class UserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Test
    public void testLoadUserByUsername() {

        UserAccount user = generator.generatePersistentUser();

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        Assert.assertEquals(user.getEmail(), userDetails.getUsername());
        Assert.assertEquals(user.getPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities()).extracting("authority")
                .contains(user.getAuthority().name());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }

}
