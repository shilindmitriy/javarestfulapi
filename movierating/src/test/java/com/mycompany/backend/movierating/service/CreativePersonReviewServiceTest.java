package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.CreativePersonReviewRepository;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class CreativePersonReviewServiceTest extends BaseTest {

    @Autowired
    private CreativePersonReviewService reviewService;
    
    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private CreativePersonReviewRepository reviewRepository;

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetReview() {
        CreativePersonReview review = generator.generatePersistentCreativePersonReview();
        ReviewReadDTO read = reviewService.getReview(review.getParentEntity().getId(), review.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewWrongId() {
        reviewService.getReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetReviews() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        UUID companyId = person.getId();
        UserAccount user1 = generator.generatePersistentUser();
        UserAccount user2 = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user2);

        CreativePersonReview review1 = generator.generateFlatEntityWithoutId(CreativePersonReview.class);
        review1.setEnabled(true);
        review1.setParentEntity(person);
        review1.setUser(user1);

        CreativePersonReview review2 = generator.generateFlatEntityWithoutId(CreativePersonReview.class);
        review2.setEnabled(false);
        review2.setParentEntity(person);
        review2.setUser(user2);

        person.getCreativePersonReviews().add(review1);
        person.getCreativePersonReviews().add(review2);
        person = personRepository.save(person);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        inTransaction(() -> {
            CreativePerson resultPerson = personRepository.findById(companyId).get();
            PageResult<ReviewReadDTO> creviews = reviewService.getReviews(companyId, userDetails, pageable);
            Assert.assertTrue(creviews.getData().size() == resultPerson.getCreativePersonReviews().size());
            CreativePersonReview resultReview1 = resultPerson.getCreativePersonReviews().get(0);
            CreativePersonReview resultReview2 = resultPerson.getCreativePersonReviews().get(1);
            Assertions.assertThat(creviews.getData()).extracting("id").containsExactlyInAnyOrder(resultReview1.getId(),
                    resultReview2.getId());
        });
    }

    @Test
    public void testGetReviewsEmpty() {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(UUID.randomUUID(), null, pageable);
        Assert.assertTrue(reviews.getData().isEmpty());
    }

    @Test
    public void testCreateReviewWithSufficientTrust() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(100);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(person.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(true, read.getEnabled());
        Assert.assertNotNull(read.getId());

        CreativePersonReview resultReview = reviewRepository.findByParentIdAndId(person.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(true, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test
    public void testCreateReviewWithoutSufficientTrust() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(45);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(person.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(false, read.getEnabled());
        Assert.assertNotNull(read.getId());

        CreativePersonReview resultReview = reviewRepository.findByParentIdAndId(person.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(false, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateReviewWithNegativeTrust() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(-1);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        reviewService.createReview(person.getId(), create, userDetails);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateReviewNotFoundNews() {
        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        reviewService.createReview(UUID.randomUUID(), new ReviewCreateDTO(), new UserDetailsImpl(user));
    }

    @Test
    public void testPatchReview() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CreativePersonReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testPatchReviewEmptyPatch() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(),
                new ReviewPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());

        CreativePersonReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPatchReviewNotFoundReview() {
        reviewService.patchReview(UUID.randomUUID(), UUID.randomUUID(), new ReviewPatchDTO());
    }

    @Test
    public void testUpdateReview() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();
        ReviewPutDTO put = generator.generateObject(ReviewPutDTO.class);

        ReviewReadDTO read = reviewService.updateReview(review.getParentEntity().getId(), review.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CreativePersonReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testDeleteReview() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
    }

    @Test
    public void testDeleteReviewWithLike() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        ReviewLike likeForReview = new ReviewLike();
        likeForReview.setLike(true);
        likeForReview.setReviewType(ReviewType.CREATIVE_PERSON_REVIEW);
        likeForReview.setReviewId(review.getId());
        likeForReview.setUserId(UUID.randomUUID());
        likeForReview = reviewLikeRepository.save(likeForReview);

        ReviewLike like = new ReviewLike();
        like.setLike(true);
        like.setReviewType(ReviewType.CREATIVE_PERSON_REVIEW);
        like.setReviewId(UUID.randomUUID());
        like.setUserId(UUID.randomUUID());
        like = reviewLikeRepository.save(like);

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
        Assert.assertFalse(reviewLikeRepository.existsById(likeForReview.getId()));
        Assert.assertTrue(reviewLikeRepository.existsById(like.getId()));
    }
    
    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}