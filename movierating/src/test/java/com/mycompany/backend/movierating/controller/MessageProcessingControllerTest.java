package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationSolveRequest;
import com.mycompany.backend.movierating.exception.InvalidRequestException;
import com.mycompany.backend.movierating.exception.MisprintNotFoundException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.MessageProcessingService;

@WebMvcTest(controllers = MessageProcessingController.class)
public class MessageProcessingControllerTest extends BaseControllerTest {

    @MockBean
    private MessageProcessingService messageProcessingService;

    @Test
    public void testGetProcessedMessages() throws Exception {

        List<MessageToAdministrationReadDTO> expectedMessages = List
                .of(generator.generateObject(MessageToAdministrationReadDTO.class));

        MessageToAdministrationSolveRequest solved = generator.generateObject(MessageToAdministrationSolveRequest.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(messageProcessingService.getProcessedMessages(solved, userDetails)).thenReturn(expectedMessages);

        String resultJson = mvc
                .perform(post("/api/v1/processed-messages").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(solved)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<MessageToAdministrationReadDTO> resultMessages = objectMapper.readValue(resultJson,
                new TypeReference<List<MessageToAdministrationReadDTO>>() {
                });

        Assert.assertEquals(resultMessages, expectedMessages);

        Mockito.verify(messageProcessingService).getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesWrongMisprintNotFound() throws Exception {

        MessageToAdministrationSolveRequest wrong = generator.generateObject(MessageToAdministrationSolveRequest.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        MisprintNotFoundException e = new MisprintNotFoundException(
                "Messages have already been processed. Url=url. Misprints=misprints");

        Mockito.when(messageProcessingService.getProcessedMessages(wrong, userDetails)).thenThrow(e);

        String resultJson = mvc
                .perform(post("/api/v1/processed-messages").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(wrong)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(e.getMessage()));
    }

    @Test
    public void testGetProcessedMessagesWrongInvalidRequestException() throws Exception {

        MessageToAdministrationSolveRequest wrong = generator.generateObject(MessageToAdministrationSolveRequest.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        InvalidRequestException e = new InvalidRequestException("Entity entity does not contain this field=field");

        Mockito.when(messageProcessingService.getProcessedMessages(wrong, userDetails)).thenThrow(e);

        String resultJson = mvc
                .perform(post("/api/v1/processed-messages").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(wrong)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(e.getMessage()));
    }

    @Test
    @WithMockUser
    public void testGetProcessedMessagesValidationFailed() throws Exception {

        MessageToAdministrationSolveRequest solved = new MessageToAdministrationSolveRequest();

        String resultJson = mvc
                .perform(post("/api/v1/processed-messages").content(objectMapper.writeValueAsString(solved))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);

        Mockito.verify(messageProcessingService, Mockito.never()).getProcessedMessages(Mockito.any(), Mockito.any());
    }

}
