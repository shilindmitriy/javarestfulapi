package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.ProductionCompanyRatingRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class ProductionCompanyRatingServiceTest extends BaseTest {

    @Autowired
    private ProductionCompanyRatingService ratingService;

    @Autowired
    private ProductionCompanyRatingRepository ratingRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetRating() {
        ProductionCompanyRating rating = generator.generatePersistentProductionCompanyRating();
        RatingReadDTO read = ratingService.getRating(rating.getParentEntity().getId(), rating.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testGetShortRating() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        ProductionCompany companyNotFound = generator.generatePersistentProductionCompany();

        ProductionCompanyRating rating1 = generator.generateFlatEntityWithoutId(ProductionCompanyRating.class);
        rating1.setParentEntity(company);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = ratingRepository.save(rating1);

        ProductionCompanyRating rating2 = generator.generateFlatEntityWithoutId(ProductionCompanyRating.class);
        rating2.setParentEntity(company);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = ratingRepository.save(rating2);

        ProductionCompanyRating rating3 = generator.generateFlatEntityWithoutId(ProductionCompanyRating.class);
        rating3.setParentEntity(company);
        rating3.setUser(generator.generatePersistentUser());
        rating3 = ratingRepository.save(rating3);

        ProductionCompanyRating rating4 = generator.generateFlatEntityWithoutId(ProductionCompanyRating.class);
        rating4.setParentEntity(companyNotFound);
        rating4.setUser(generator.generatePersistentUser());
        rating4 = ratingRepository.save(rating4);

        ShortRatingReadDTO shortRating = ratingService.getShortRating(company.getId());
        Assert.assertTrue(shortRating.getNumberOfAllVotes() == 3L);
        Assert.assertEquals(shortRating.getAverageRating(),
                (double) (rating1.getRating() + rating2.getRating() + rating3.getRating()) / 3, 0.01d);
    }

    @Test
    public void testCreateRating() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);

        RatingReadDTO read = ratingService.createRating(company.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getId() != null);
        Assert.assertTrue(read.getAge() == Period.between(user.getBirthday(), LocalDate.now()).getYears());
        Assert.assertEquals(read.getCountryUser(), user.getCountry());
        Assert.assertEquals(read.getGender(), user.getGender());

        ProductionCompanyRating rating = ratingRepository.findByParentIdAndId(company.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(user.getId().equals(read.getUserId()));

        Assert.assertTrue(userRepository.existsByRatingIdAndEmail(rating.getId(), user.getEmail()));
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateRatingWrongRatingAlreadyExists() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        RatingCreateDTO firstCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(company.getId(), firstCreate, userDetails);

        RatingCreateDTO secondCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(company.getId(), secondCreate, userDetails);
    }

    @Test
    public void testPatchRating() {

        ProductionCompanyRating rating = generator.generatePersistentProductionCompanyRating();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(), patch);
        Assert.assertEquals(read.getRating(), patch.getRating());

        ProductionCompanyRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testPatchRatingEmptyPatch() {

        ProductionCompanyRating rating = generator.generatePersistentProductionCompanyRating();

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(),
                new RatingPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));

        ProductionCompanyRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testUpdateRating() {

        ProductionCompanyRating rating = generator.generatePersistentProductionCompanyRating();
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        RatingReadDTO read = ratingService.updateRating(rating.getParentEntity().getId(), rating.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        ProductionCompanyRating resultRating = ratingRepository
                .findByParentIdAndId(rating.getParentEntity().getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateRatingNotFoundRating() {
        ratingService.updateRating(UUID.randomUUID(), UUID.randomUUID(), new RatingPutDTO());
    }

    @Test
    public void testDeleteRating() {

        ProductionCompanyRating rating = generator.generatePersistentProductionCompanyRating();
        Assert.assertTrue(ratingRepository.existsById(rating.getId()));

        ratingService.deleteRating(rating.getParentEntity().getId(), rating.getId());
        Assert.assertFalse(ratingRepository.existsById(rating.getId()));
    }

}
