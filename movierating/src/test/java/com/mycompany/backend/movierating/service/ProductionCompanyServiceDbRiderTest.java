package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPutDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;

@DBRider
public class ProductionCompanyServiceDbRiderTest extends BaseTest {

    @Autowired
    private ProductionCompanyService companyService;

    @Test
    @DataSet("/datasets/testUpdateProductionCompany.xml")
    @ExpectedDataSet(value="/datasets/testUpdateProductionCompany_result.xml", ignoreCols = "UPDATED_AT")
    public void testUpdateProductionCompany() {

        UUID companyId = UUID.fromString("b02705fc-eb58-4624-8a1a-80da193b6238");
        ProductionCompanyPutDTO put = new ProductionCompanyPutDTO();
        put.setName("updated name");
        put.setHistory("updated history");

        ProductionCompanyReadDTO read = companyService.updateProductionCompany(companyId, put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
    }

}
