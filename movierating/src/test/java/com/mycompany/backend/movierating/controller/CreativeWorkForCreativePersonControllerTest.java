package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.CreativeWorkService;

@WebMvcTest(controllers = CreativeWorkForCreativePersonController.class)
public class CreativeWorkForCreativePersonControllerTest extends BaseControllerTest {

    @MockBean
    private CreativeWorkService workService;

    @Test
    public void testGetCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        UUID personId = expectedRead.getPersonId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.getCreativeWork(personId, id)).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(workService, Mockito.times(1)).getCreativeWork(personId, expectedRead.getId());
    }

    @Test
    @WithMockUser
    public void testPatchCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);
        UUID personId = expectedRead.getPersonId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.patchCreativeWork(personId, id, patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(workService).patchCreativeWork(personId, id, patch);
    }

    @Test
    @WithMockUser
    public void testPatchCreativeWorkValidationFailed() throws Exception {

        UUID personId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        StringBuilder character = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            character.append("1234567890");
        }

        CreativeWorkPatchDTO patch = new CreativeWorkPatchDTO();
        patch.setCharacter(character.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(workService, Mockito.never()).patchCreativeWork(ArgumentMatchers.eq(personId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchCreativeWorkWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongPersonId = UUID.randomUUID();
        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(CreativeWork.class, wrongId, wrongPersonId);

        Mockito.when(workService.patchCreativeWork(wrongPersonId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{personId}/creative-works/{id}", wrongPersonId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        CreativeWorkPutDTO put = generator.generateObject(CreativeWorkPutDTO.class);
        UUID personId = expectedRead.getPersonId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.updateCreativeWork(personId, id, put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(workService).updateCreativeWork(personId, id, put);
    }

    @Test
    @WithMockUser
    public void testUpdateCreativeWorkValidationFailed() throws Exception {

        UUID personId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        CreativeWorkPutDTO put = new CreativeWorkPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(workService, Mockito.never()).updateCreativeWork(ArgumentMatchers.eq(personId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteCreativeWork() throws Exception {
        UUID personId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/creative-persons/{personId}/creative-works/{id}", personId, id))
                .andExpect(status().isOk());
        Mockito.verify(workService).deleteCreativeWork(personId, id);
    }

}
