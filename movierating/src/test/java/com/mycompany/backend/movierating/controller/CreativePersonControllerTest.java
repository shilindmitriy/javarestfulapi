package com.mycompany.backend.movierating.controller;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPutDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.exception.ControllerValidationException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.CreativePersonService;

@WebMvcTest(controllers = CreativePersonController.class)
public class CreativePersonControllerTest extends BaseControllerTest {

    @MockBean
    private CreativePersonService personService;

    @Test
    public void testGetCreativePerson() throws Exception {

        CreativePersonReadDTO expectedPerson = generator.generateObject(CreativePersonReadDTO.class);

        Mockito.when(personService.getCreativePerson(expectedPerson.getId())).thenReturn(expectedPerson);

        String resultJson = mvc.perform(get("/api/v1/creative-persons/{id}", expectedPerson.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativePersonReadDTO resultPerson = objectMapper.readValue(resultJson, CreativePersonReadDTO.class);
        Assert.assertEquals(resultPerson, expectedPerson);

        Mockito.verify(personService).getCreativePerson(expectedPerson.getId());
    }

    @Test
    public void testGetAverageMoviesRating() throws Exception {

        UUID id = UUID.randomUUID();
        Double expectedRating = 8.5;

        Mockito.when(personService.getAverageMoviesRating(id)).thenReturn(expectedRating);

        String resultJson = mvc.perform(get("/api/v1/creative-persons/{id}/movies-rating", id))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Double resultRating = objectMapper.readValue(resultJson, Double.class);
        Assert.assertEquals(resultRating, expectedRating, 0.01);

        Mockito.verify(personService).getAverageMoviesRating(id);
    }

    @Test
    public void testGetAverageCreativeWorksRating() throws Exception {

        UUID id = UUID.randomUUID();
        Double expectedRating = 8.5;

        Mockito.when(personService.getAverageCreativeWorksRating(id)).thenReturn(expectedRating);

        String resultJson = mvc.perform(get("/api/v1/creative-persons/{id}/creative-works-rating", id))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Double resultRating = objectMapper.readValue(resultJson, Double.class);
        Assert.assertEquals(resultRating, expectedRating, 0.01);

        Mockito.verify(personService).getAverageCreativeWorksRating(id);
    }

    @Test
    @WithMockUser
    public void testCreateCreativePerson() throws Exception {

        CreativePersonReadDTO expectedPerson = generator.generateObject(CreativePersonReadDTO.class);
        CreativePersonCreateDTO create = generator.generateObject(CreativePersonCreateDTO.class);

        Mockito.when(personService.createCreativePerson(create)).thenReturn(expectedPerson);

        String resultJson = mvc
                .perform(post("/api/v1/creative-persons").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativePersonReadDTO resultPerson = objectMapper.readValue(resultJson, CreativePersonReadDTO.class);
        Assert.assertEquals(resultPerson, expectedPerson);

        Mockito.verify(personService).createCreativePerson(create);
    }

    @Test
    @WithMockUser
    public void testCreateCreativePersonValidationFailed() throws Exception {

        CreativePersonCreateDTO create = new CreativePersonCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/creative-persons").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(personService, Mockito.never()).createCreativePerson(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testCreateCreativePersonWrongDates() throws Exception {

        CreativePersonCreateDTO create = generator.generateObject(CreativePersonCreateDTO.class);
        create.setBorn(LocalDate.of(2018, 6, 12));
        create.setDeath(LocalDate.of(1958, 2, 16));

        String resultJson = mvc
                .perform(post("/api/v1/creative-persons").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("born"));
        Assert.assertTrue(errorInfo.getMessage().contains("death"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(personService, Mockito.never()).createCreativePerson(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchCreativePerson() throws Exception {

        CreativePersonReadDTO expectedPerson = generator.generateObject(CreativePersonReadDTO.class);
        CreativePersonPatchDTO patch = generator.generateObject(CreativePersonPatchDTO.class);

        Mockito.when(personService.patchCreativePerson(expectedPerson.getId(), patch)).thenReturn(expectedPerson);

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{id}", expectedPerson.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativePersonReadDTO resultUPerson = objectMapper.readValue(resultJson, CreativePersonReadDTO.class);
        Assert.assertEquals(expectedPerson, resultUPerson);

        Mockito.verify(personService).patchCreativePerson(expectedPerson.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchCreativePersonValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        CreativePersonPatchDTO patch = new CreativePersonPatchDTO();
        patch.setDeath(LocalDate.of(2999, 2, 16));

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(personService, Mockito.never()).patchCreativePerson(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchCreativePersonWrongDates() throws Exception {

        UUID id = UUID.randomUUID();

        CreativePersonPatchDTO patch = generator.generateObject(CreativePersonPatchDTO.class);
        patch.setBorn(LocalDate.of(2018, 6, 12));
        patch.setDeath(LocalDate.of(1950, 2, 16));

        String resultJson = mvc
                .perform(patch("/api/v1/creative-persons/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("born"));
        Assert.assertTrue(errorInfo.getMessage().contains("death"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(personService, Mockito.never()).patchCreativePerson(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateCreativePerson() throws Exception {

        CreativePersonReadDTO expectedPerson = generator.generateObject(CreativePersonReadDTO.class);
        CreativePersonPutDTO put = generator.generateObject(CreativePersonPutDTO.class);

        Mockito.when(personService.updateCreatePerson(expectedPerson.getId(), put)).thenReturn(expectedPerson);

        String resultJson = mvc
                .perform(put("/api/v1/creative-persons/{id}", expectedPerson.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativePersonReadDTO resultPerson = objectMapper.readValue(resultJson, CreativePersonReadDTO.class);
        Assert.assertEquals(expectedPerson, resultPerson);

        Mockito.verify(personService).updateCreatePerson(expectedPerson.getId(), put);
    }

    @Test
    @WithMockUser
    public void testUpdateCreativePersonValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        CreativePersonPutDTO put = new CreativePersonPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/creative-persons/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(personService, Mockito.never()).updateCreatePerson(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateCreativePersonWrongDates() throws Exception {

        UUID id = UUID.randomUUID();
        CreativePersonPutDTO put = generator.generateObject(CreativePersonPutDTO.class);
        put.setBorn(LocalDate.of(2018, 6, 12));
        put.setDeath(LocalDate.of(1500, 2, 16));

        String resultJson = mvc
                .perform(put("/api/v1/creative-persons/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("born"));
        Assert.assertTrue(errorInfo.getMessage().contains("death"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(personService, Mockito.never()).updateCreatePerson(ArgumentMatchers.eq(id),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteCreativePerson() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/creative-persons/{id}", id)).andExpect(status().isOk());
        Mockito.verify(personService).deleteCreativePerson(id);
    }

}