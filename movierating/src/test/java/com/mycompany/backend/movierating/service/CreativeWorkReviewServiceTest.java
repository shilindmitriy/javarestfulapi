package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkReviewRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class CreativeWorkReviewServiceTest extends BaseTest {

    @Autowired
    private CreativeWorkReviewService reviewService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private CreativeWorkReviewRepository reviewRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetReview() {
        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();
        ReviewReadDTO read = reviewService.getReview(review.getParentEntity().getId(), review.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewWrongId() {
        reviewService.getReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetReviews() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UUID workId = work.getId();
        UserAccount user1 = generator.generatePersistentUser();
        UserAccount user2 = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user2);

        CreativeWorkReview review1 = generator.generateFlatEntityWithoutId(CreativeWorkReview.class);
        review1.setEnabled(true);
        review1.setParentEntity(work);
        review1.setUser(user1);

        CreativeWorkReview review2 = generator.generateFlatEntityWithoutId(CreativeWorkReview.class);
        review2.setEnabled(false);
        review2.setParentEntity(work);
        review2.setUser(user2);

        work.getCreativeWorkReviews().add(review1);
        work.getCreativeWorkReviews().add(review2);
        work = workRepository.save(work);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        inTransaction(() -> {
            CreativeWork resultWork = workRepository.findById(workId).get();
            PageResult<ReviewReadDTO> creviews = reviewService.getReviews(workId, userDetails, pageable);
            Assert.assertTrue(creviews.getData().size() == resultWork.getCreativeWorkReviews().size());
            CreativeWorkReview resultReview1 = resultWork.getCreativeWorkReviews().get(0);
            CreativeWorkReview resultReview2 = resultWork.getCreativeWorkReviews().get(1);
            Assertions.assertThat(creviews.getData()).extracting("id").containsExactlyInAnyOrder(resultReview1.getId(),
                    resultReview2.getId());
        });
    }

    @Test
    public void testGetReviewsEmpty() {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(UUID.randomUUID(), null, pageable);
        Assert.assertTrue(reviews.getData().isEmpty());
    }

    @Test
    public void testCreateReviewWithSufficientTrust() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(100);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(work.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(true, read.getEnabled());
        Assert.assertNotNull(read.getId());

        CreativeWorkReview resultReview = reviewRepository.findByParentIdAndId(work.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(true, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test
    public void testCreateReviewWithoutSufficientTrust() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(99);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(work.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(false, read.getEnabled());
        Assert.assertNotNull(read.getId());

        CreativeWorkReview resultReview = reviewRepository.findByParentIdAndId(work.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(false, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateReviewWithNegativeTrust() {

        CreativeWork work = generator.generatePersistentCreativeWork();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(-10);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        reviewService.createReview(work.getId(), create, userDetails);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateReviewNotFoundNews() {
        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        reviewService.createReview(UUID.randomUUID(), new ReviewCreateDTO(), new UserDetailsImpl(user));
    }

    @Test
    public void testPatchReview() {

        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CreativeWorkReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testPatchReviewEmptyPatch() {

        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(),
                new ReviewPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        CreativeWorkReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPatchReviewNotFoundReview() {
        reviewService.patchReview(UUID.randomUUID(), UUID.randomUUID(), new ReviewPatchDTO());
    }

    @Test
    public void testUpdateReview() {

        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();
        ReviewPutDTO put = generator.generateObject(ReviewPutDTO.class);

        ReviewReadDTO read = reviewService.updateReview(review.getParentEntity().getId(), review.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CreativeWorkReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testDeleteReview() {

        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
    }

    @Test
    public void testDeleteReviewWithLike() {

        CreativeWorkReview review = generator.generatePersistentCreativeWorkReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        ReviewLike likeForReview = new ReviewLike();
        likeForReview.setLike(true);
        likeForReview.setReviewType(ReviewType.CREATIVE_WORK_REVIEW);
        likeForReview.setReviewId(review.getId());
        likeForReview.setUserId(UUID.randomUUID());
        likeForReview = reviewLikeRepository.save(likeForReview);

        ReviewLike like = new ReviewLike();
        like.setLike(true);
        like.setReviewType(ReviewType.CREATIVE_WORK_REVIEW);
        like.setReviewId(UUID.randomUUID());
        like.setUserId(UUID.randomUUID());
        like = reviewLikeRepository.save(like);

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
        Assert.assertFalse(reviewLikeRepository.existsById(likeForReview.getId()));
        Assert.assertTrue(reviewLikeRepository.existsById(like.getId()));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}