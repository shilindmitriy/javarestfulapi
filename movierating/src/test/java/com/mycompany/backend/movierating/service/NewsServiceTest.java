package com.mycompany.backend.movierating.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.news.DateFilter;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsPatchDTO;
import com.mycompany.backend.movierating.dto.news.NewsPutDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.NewsRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class NewsServiceTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    @Test
    public void testGetNews() {
        News news = generator.generatePersistentNews();
        NewsReadDTO read = newsService.getNews(news.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(news, "urlPhoto");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsWrongIs() {
        newsService.getNews(UUID.randomUUID());
    }

    @Test
    public void testGetNewsByAuthor() {

        News newsFound = generator.generatePersistentNews();
        News newsNotFound = generator.generatePersistentNews();
        newsNotFound.setAuthorId(UUID.randomUUID());
        newsRepository.save(newsNotFound);

        List<NewsReadDTO> reads = newsService.getNewsByAuthor(newsFound.getAuthorId(), new DateFilter());
        Assert.assertFalse(reads.isEmpty());
        Assert.assertFalse(reads.get(0).getAuthorId().equals(newsNotFound.getAuthorId()));
        Assertions.assertThat(reads.get(0)).isEqualToIgnoringGivenFields(newsFound, "urlPhoto");
    }

    @Test
    public void testGetNewsByAuthorWithDate() {

        News newsFound = generator.generatePersistentNews();
        News newsNotFound = generator.generatePersistentNews();

        DateFilter dateFilter = new DateFilter();
        dateFilter.setDateFrom(Instant.parse("2007-12-03T10:15:30.00Z"));
        dateFilter.setDateTo(Instant.now().truncatedTo(ChronoUnit.MICROS));

        List<NewsReadDTO> reads = newsService.getNewsByAuthor(newsFound.getAuthorId(), dateFilter);
        Assert.assertFalse(reads.isEmpty());
        Assert.assertFalse(reads.get(0).getText().equals(newsNotFound.getText()));
        Assertions.assertThat(reads.get(0)).isEqualToIgnoringGivenFields(newsFound, "urlPhoto");
    }

    @Test
    public void testCreateNews() {

        NewsCreateDTO create = generator.generateObject(NewsCreateDTO.class);
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        NewsReadDTO read = newsService.createNews(create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        News news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(news, "urlPhoto");
    }

    @Test
    public void testPatchNews() {

        News news = generator.generatePersistentNews();
        NewsPatchDTO patch = generator.generateObject(NewsPatchDTO.class);

        NewsReadDTO read = newsService.patchNews(news.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(news, "urlPhoto");
    }

    @Test
    public void testPatchNewsEmptyPatch() {

        News news = generator.generatePersistentNews();

        NewsReadDTO read = newsService.patchNews(news.getId(), new NewsPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("urlPhoto");
        Assert.assertNull(read.getUrlPhoto());

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(news, "urlPhoto");
    }

    @Test
    public void testUpdateNews() {

        News news = generator.generatePersistentNews();
        NewsPutDTO put = generator.generateObject(NewsPutDTO.class);

        NewsReadDTO read = newsService.updateNews(news.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(news, "urlPhoto");
    }

    @Test
    public void testDeleteNews() {

        News news = generator.generatePersistentNews();
        Assert.assertTrue(newsRepository.existsById(news.getId()));

        newsService.deleteNews(news.getId());
        Assert.assertFalse(newsRepository.existsById(news.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {
        newsService.deleteNews(UUID.randomUUID());
    }

}