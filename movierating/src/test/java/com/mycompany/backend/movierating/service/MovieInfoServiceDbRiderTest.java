package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;

@DBRider
public class MovieInfoServiceDbRiderTest extends BaseTest {

    @Autowired
    private MovieInfoService infoService;

    @Test
    @DataSet("/datasets/testGetCompanies.xml")
    public void testGetCompanies() {

        UUID movieId = UUID.fromString("b02705fc-eb58-4624-8a1a-80da193b6238");
        UUID company1Id = UUID.fromString("b20bb015-f87a-4675-b9eb-50325b05acd0");
        UUID company2Id = UUID.fromString("898a07f4-0e58-4d25-b6a2-d1019bd89147");
        UUID company3Id = UUID.fromString("641f03a5-a1f7-49dc-a712-aafd2937c432");
        UUID company4Id = UUID.fromString("0badbbd6-b044-4dbf-829f-9e61124c3195");
        UUID company5Id = UUID.fromString("f349db49-0736-45a4-883b-00b2b19e1b50");

        List<CompanyReadDTO> companies = infoService.getCompanies(movieId);
        Assert.assertFalse(companies.isEmpty());
        Assert.assertTrue(companies.size() == 4);
        Assertions.assertThat(companies).extracting("companyId")
                .containsExactlyInAnyOrder(company1Id, company2Id, company3Id, company4Id).doesNotContain(company5Id);
    }

    @Test
    @DataSet("/datasets/testGetCastAndCrew.xml")
    public void testGetCastAndCrew() {

        UUID movieId = UUID.fromString("b02705fc-eb58-4624-8a1a-80da193b6238");

        UUID work1Id = UUID.fromString("cc431afa-9301-47ee-bfc6-74b19a6ccb97");
        UUID work2Id = UUID.fromString("ad72008e-b862-4d92-b06f-91fed289e4b1");
        UUID work3Id = UUID.fromString("bdea29f8-031e-49cd-80d9-c25e50123d9c");
        UUID work4Id = UUID.fromString("19562bc4-b00e-44fd-8ad0-b6029f68aadc");
        UUID work5Id = UUID.fromString("3e47c340-7266-4bbc-9bd6-9fb78534c0a0");
        UUID work6Id = UUID.fromString("4a708173-0d73-49f9-b03f-bdf6e2433097");

        UUID person1Id = UUID.fromString("cd6f55d9-4b10-4505-8116-450fc18c9c56");
        UUID person2Id = UUID.fromString("6091563a-347e-4e6b-ba6d-a78683b8b641");
        UUID person3Id = UUID.fromString("a0c0da71-26e1-414b-a5d5-8db22e78054f");
        UUID person4Id = UUID.fromString("03908577-1983-422d-b9d2-8e7a1e2b1b63");
        UUID person5Id = UUID.fromString("b7206215-236d-4a48-a2f9-7e7ab0993fa8");

        List<CastAndCrewReadDTO> leadingActor = infoService.getCastAndCrew(movieId, Department.ACTING);
        Assert.assertFalse(leadingActor.isEmpty());
        Assert.assertTrue(leadingActor.size() == 2);
        Assertions.assertThat(leadingActor).extracting("personId").containsExactlyInAnyOrder(person1Id, person2Id)
                .doesNotContain(person5Id);
        Assertions.assertThat(leadingActor).extracting("workId").containsExactlyInAnyOrder(work1Id, work2Id)
                .doesNotContain(work5Id);
        Assertions.assertThat(leadingActor).extracting("department").containsExactly(Department.ACTING,
                Department.ACTING);

        List<CastAndCrewReadDTO> crowdScene = infoService.getCastAndCrew(movieId, Department.ART);
        Assert.assertFalse(crowdScene.isEmpty());
        Assert.assertTrue(crowdScene.size() == 2);
        Assertions.assertThat(crowdScene).extracting("personId").containsExactlyInAnyOrder(person3Id, person4Id)
                .doesNotContain(person5Id);
        Assertions.assertThat(crowdScene).extracting("workId").containsExactlyInAnyOrder(work3Id, work4Id)
                .doesNotContain(work5Id);
        Assertions.assertThat(crowdScene).extracting("department").containsExactly(Department.ART, Department.ART);

        List<CastAndCrewReadDTO> director = infoService.getCastAndCrew(movieId, Department.DIRECTING);
        Assert.assertFalse(director.isEmpty());
        Assert.assertTrue(director.size() == 1);
        Assertions.assertThat(director).extracting("personId").containsExactly(person1Id).doesNotContain(person5Id);
        Assertions.assertThat(director).extracting("workId").containsExactly(work6Id).doesNotContain(work5Id);
        Assertions.assertThat(director).extracting("department").containsExactly(Department.DIRECTING);
    }

}
