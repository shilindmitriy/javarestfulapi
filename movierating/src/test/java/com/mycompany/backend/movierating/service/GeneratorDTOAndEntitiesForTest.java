package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.bitbucket.brunneng.br.Randomizer;
import org.bitbucket.brunneng.br.Configuration.Bean.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.AbstractEntity;
import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.CreativePersonRating;
import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.ProductionCompanyRating;
import com.mycompany.backend.movierating.domain.Rating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPutDTO;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.repository.CreativeWorkReviewRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRatingRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.CreativePersonReviewRepository;
import com.mycompany.backend.movierating.repository.CreativePersonRatingRepository;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.MessageToAdministrationRepository;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.MovieRatingRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.NewsRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyReviewRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRatingRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;

@Service
public class GeneratorDTOAndEntitiesForTest {

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private CreativePersonReviewRepository personReviewRepository;

    @Autowired
    private CreativePersonRatingRepository personRatingRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Autowired
    private CreativeWorkReviewRepository workReviewRepository;

    @Autowired
    private CreativeWorkRatingRepository workRatingRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsRatingRepository newsRatingRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private MessageToAdministrationRepository messageToAdministrationRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private ProductionCompanyReviewRepository companyReviewRepository;

    @Autowired
    private ProductionCompanyRatingRepository companyRatingRepository;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    private RandomObjectGenerator generator;

    private RandomObjectGenerator flatGeneratorWithoutId;

    // Configuration for flatGeneratorWithoutId
    {
        Configuration c = new Configuration();

        // CreativePerson
        Configuration.Bean personBean = c.beanOfClass(CreativePerson.class);
        personBean.property("photo").skip();
        personBean.property("born").setMaxValue(LocalDate.of(1950, 03, 15));
        personBean.property("death").setMinValue(LocalDate.of(1950, 03, 16));
        personBean.property("death").setMaxValue(LocalDate.of(2020, 03, 15));

        // Movie
        Configuration.Bean movieBean = c.beanOfClass(Movie.class);
        movieBean.property("poster").skip();
        movieBean.property("releaseDate").setMaxValue(LocalDate.of(2020, 03, 15));
        movieBean.property("runningTime").setCustomRandomizer(new LocalTimeCustomRandomizer());

        // News
        Configuration.Bean newsBean = c.beanOfClass(News.class);
        newsBean.property("photo").skip();

        // UserAccount
        Configuration.Bean userBean = c.beanOfClass(UserAccount.class);
        userBean.property("birthday").setMaxValue(LocalDate.of(2002, 03, 15));
        userBean.property("birthday").setMinValue(LocalDate.of(1922, 03, 16));
        userBean.property("email").setCustomRandomizer(new EmailCustomRandomizer());

        // Rating
        Configuration.Bean ratingBean = c.beanOfClass(Rating.class);
        ratingBean.property("age").setMinValue(1);
        ratingBean.property("age").setMaxValue(100);

        c.setFlatMode(true);

        // for id
        Configuration.Bean entityBean = c.beanOfClass(AbstractEntity.class);
        entityBean.property("id").skip();

        flatGeneratorWithoutId = new RandomObjectGenerator(c);
    }

    // Configuration for generator
    {
        Configuration c = new Configuration();

        // CreativePersonCreateDTO
        Configuration.Bean personCreateBean = c.beanOfClass(CreativePersonCreateDTO.class);
        personCreateBean.property("born").setMaxValue(LocalDate.of(1950, 03, 15));
        personCreateBean.property("death").setMinValue(LocalDate.of(1950, 03, 16));
        personCreateBean.property("death").setMaxValue(LocalDate.of(2020, 03, 15));

        // CreativePersonPatchDTO
        Configuration.Bean personPatchBean = c.beanOfClass(CreativePersonPatchDTO.class);
        personPatchBean.property("born").setMaxValue(LocalDate.of(1950, 03, 15));
        personPatchBean.property("death").setMinValue(LocalDate.of(1950, 03, 16));
        personPatchBean.property("death").setMaxValue(LocalDate.of(2020, 03, 15));

        // CreativePersonPatchDTO
        Configuration.Bean personPutBean = c.beanOfClass(CreativePersonPutDTO.class);
        personPutBean.property("born").setMaxValue(LocalDate.of(1950, 03, 15));
        personPutBean.property("death").setMinValue(LocalDate.of(1950, 03, 16));
        personPutBean.property("death").setMaxValue(LocalDate.of(2020, 03, 15));

        // MovieCreateDTO
        Configuration.Bean movieCreateBean = c.beanOfClass(MovieCreateDTO.class);
        movieCreateBean.property("runningTime").setCustomRandomizer(new LocalTimeCustomRandomizer());

        // MoviePatchDTO
        Configuration.Bean moviePatchBean = c.beanOfClass(MoviePatchDTO.class);
        moviePatchBean.property("runningTime").setCustomRandomizer(new LocalTimeCustomRandomizer());

        // MovieCreateDTO
        Configuration.Bean moviePutBean = c.beanOfClass(MoviePutDTO.class);
        moviePutBean.property("runningTime").setCustomRandomizer(new LocalTimeCustomRandomizer());

        // RatingPutDTO
        Configuration.Bean ratingPutBean = c.beanOfClass(RatingPutDTO.class);
        ratingPutBean.property("age").setMinValue(1);
        ratingPutBean.property("age").setMaxValue(100);

        // UserAccountCreateDTO
        Configuration.Bean userCreateBean = c.beanOfClass(UserAccountCreateDTO.class);
        userCreateBean.property("birthday").setMaxValue(LocalDate.of(2002, 03, 15));
        userCreateBean.property("birthday").setMinValue(LocalDate.of(1922, 03, 16));
        userCreateBean.property("email").setCustomRandomizer(new EmailCustomRandomizer());

        // UserAccountPatchDTO
        Configuration.Bean userPatchBean = c.beanOfClass(UserAccountPatchDTO.class);
        userPatchBean.property("birthday").setMaxValue(LocalDate.of(2002, 03, 15));
        userPatchBean.property("birthday").setMinValue(LocalDate.of(1922, 03, 16));
        userPatchBean.property("email").setCustomRandomizer(new EmailCustomRandomizer());

        // UserAccountPutDTO
        Configuration.Bean userPutBean = c.beanOfClass(UserAccountPutDTO.class);
        userPutBean.property("birthday").setMaxValue(LocalDate.of(2002, 03, 15));
        userPutBean.property("birthday").setMinValue(LocalDate.of(1922, 03, 16));
        userPutBean.property("email").setCustomRandomizer(new EmailCustomRandomizer());

        generator = new RandomObjectGenerator(c);
    }

    /*
     * CreativePerson
     */
    public CreativePerson generatePersistentCreativePerson() {
        CreativePerson person = generateFlatEntityWithoutId(CreativePerson.class);
        return personRepository.save(person);
    }

    public CreativePersonReview generatePersistentCreativePersonReview() {
        CreativePerson person = generatePersistentCreativePerson();
        UserAccount user = generatePersistentUser();
        CreativePersonReview review = generateFlatEntityWithoutId(CreativePersonReview.class);
        review.setParentEntity(person);
        review.setUser(user);
        return personReviewRepository.save(review);
    }

    public CreativePersonRating generatePersistentCreativePersonRating() {
        CreativePerson person = generatePersistentCreativePerson();
        UserAccount user = generatePersistentUser();
        CreativePersonRating rating = generateFlatEntityWithoutId(CreativePersonRating.class);
        rating.setParentEntity(person);
        rating.setUser(user);
        return personRatingRepository.save(rating);
    }

    /*
     * CreativeWork
     */
    public CreativeWork generatePersistentCreativeWork() {
        CreativePerson person = generatePersistentCreativePerson();
        Movie movie = generatePersistentMovie();
        CreativeWork work = generateFlatEntityWithoutId(CreativeWork.class);
        work.setCreativePerson(person);
        work.setMovie(movie);
        return workRepository.save(work);
    }

    public CreativeWorkReview generatePersistentCreativeWorkReview() {
        CreativeWork work = generatePersistentCreativeWork();
        UserAccount user = generatePersistentUser();
        CreativeWorkReview review = generateFlatEntityWithoutId(CreativeWorkReview.class);
        review.setParentEntity(work);
        review.setUser(user);
        return workReviewRepository.save(review);
    }

    public CreativeWorkRating generatePersistentCreativeWorkRating() {
        CreativeWork work = generatePersistentCreativeWork();
        UserAccount user = generatePersistentUser();
        CreativeWorkRating rating = generateFlatEntityWithoutId(CreativeWorkRating.class);
        rating.setParentEntity(work);
        rating.setUser(user);
        return workRatingRepository.save(rating);
    }

    /*
     * Movie
     */
    public Movie generatePersistentMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.getProductionCountries().add(Country.UKRAINE);
        movie.getGenres().add(Genre.ACTION);
        return movieRepository.save(movie);
    }

    public MovieReview generatePersistentMovieReview() {
        Movie movie = generatePersistentMovie();
        UserAccount user = generatePersistentUser();
        MovieReview review = generateFlatEntityWithoutId(MovieReview.class);
        review.setParentEntity(movie);
        review.setUser(user);
        return movieReviewRepository.save(review);
    }

    public MovieRating generatePersistentMovieRating() {
        Movie movie = generatePersistentMovie();
        UserAccount user = generatePersistentUser();
        MovieRating rating = generateFlatEntityWithoutId(MovieRating.class);
        rating.setParentEntity(movie);
        rating.setUser(user);
        return movieRatingRepository.save(rating);
    }

    /*
     * News
     */
    public News generatePersistentNews() {
        News news = generateFlatEntityWithoutId(News.class);
        return newsRepository.save(news);
    }

    public NewsRating generatePersistentNewsRating() {
        News news = generatePersistentNews();
        UserAccount user = generatePersistentUser();
        NewsRating rating = generateFlatEntityWithoutId(NewsRating.class);
        rating.setParentEntity(news);
        rating.setUser(user);
        return newsRatingRepository.save(rating);
    }

    /*
     * UserAccount
     */
    public UserAccount generatePersistentUser() {
        UserAccount user = generateFlatEntityWithoutId(UserAccount.class);
        return userAccountRepository.save(user);
    }

    /*
     * MessageToAdministration
     */
    public MessageToAdministration generatePersistentMessageToAdministration() {
        MessageToAdministration message = generateFlatEntityWithoutId(MessageToAdministration.class);
        return messageToAdministrationRepository.save(message);
    }

    /*
     * ProductionCompany
     */
    public ProductionCompany generatePersistentProductionCompany() {
        ProductionCompany company = generateFlatEntityWithoutId(ProductionCompany.class);
        return companyRepository.save(company);
    }

    public ProductionCompanyReview generatePersistentProductionCompanyReview() {
        ProductionCompany company = generatePersistentProductionCompany();
        UserAccount user = generatePersistentUser();
        ProductionCompanyReview review = generateFlatEntityWithoutId(ProductionCompanyReview.class);
        review.setParentEntity(company);
        review.setUser(user);
        return companyReviewRepository.save(review);
    }

    public ProductionCompanyRating generatePersistentProductionCompanyRating() {
        ProductionCompany company = generatePersistentProductionCompany();
        UserAccount user = generatePersistentUser();
        ProductionCompanyRating rating = generateFlatEntityWithoutId(ProductionCompanyRating.class);
        rating.setUser(user);
        rating.setParentEntity(company);
        return companyRatingRepository.save(rating);
    }

    /*
     * ReviewLike
     */
    public ReviewLike generatePersistentReviewLike() {
        ReviewLike like = generateFlatEntityWithoutId(ReviewLike.class);
        return reviewLikeRepository.save(like);
    }

    /*
     * Generators
     */
    public <T> T generateObject(Class<T> objectClass) {
        return generator.generateRandomObject(objectClass);
    }

    public <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGeneratorWithoutId.generateRandomObject(entityClass);
        return entity;
    }

    private class LocalTimeCustomRandomizer implements Randomizer {

        @Override
        public Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                Property property) {
            return LocalTime.of(random.nextInt(24), random.nextInt(59) + 1);
        }

        @Override
        public boolean isMatched(Class<?> clazz, Property property) {
            return clazz.equals(LocalTime.class);
        }

    }

    private class EmailCustomRandomizer implements Randomizer {

        @Override
        public Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                Property property) {
            return String.format("email%d@gmail.com", random.nextInt(10000));
        }

        @Override
        public boolean isMatched(Class<?> clazz, Property property) {
            return clazz.equals(String.class);
        }

    }
}