package com.mycompany.backend.movierating.client;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MoviesPageDTO;
import com.mycompany.backend.movierating.service.BaseTest;

public class TheMovieDbClientTest extends BaseTest {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Test
    public void testGetMovieRu() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovieWhithLanguage(movieId, "ru");
        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals("Терминатор 2: Судный день", movie.getTitle());
    }

    @Test
    public void testGetMovieDefaultLanguage() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId);
        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals(movie.getOriginalTitle(), movie.getTitle());
    }

    @Test
    public void testGetTopRatedMovies() {

        MoviesPageDTO moviesPage = theMovieDbClient.getTopRatedMovies();

        Assert.assertTrue(moviesPage.getTotalPages() > 0);
        Assert.assertTrue(moviesPage.getTotalResults() > 0);
        Assert.assertTrue(moviesPage.getResults().size() > 0);

        moviesPage.getResults().forEach(m -> {
            Assert.assertNotNull(m.getId());
            Assert.assertNotNull(m.getTitle());
        });
    }

}
