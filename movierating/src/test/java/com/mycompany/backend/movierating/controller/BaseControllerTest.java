package com.mycompany.backend.movierating.controller;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.backend.movierating.security.UserDetailsServiceImpl;
import com.mycompany.backend.movierating.service.GeneratorDTOAndEntitiesForTest;

@RunWith(SpringRunner.class)
public abstract class BaseControllerTest {

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected GeneratorDTOAndEntitiesForTest generator = new GeneratorDTOAndEntitiesForTest();

    @Value("${spring.data.web.pageable.default-page-size}")
    protected int defaultPageSize;

    @Value("${spring.data.web.pageable.max-page-size}")
    protected int maxPageSize;

}
