package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MvcResult;

import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.service.NewsService;

@WebMvcTest(controllers = NewsController.class)
public class NoSessionTest extends BaseControllerTest {

    @MockBean
    private NewsService newsService;

    @Test
    public void testNoSession() throws Exception {

        UUID id = UUID.randomUUID();

        Mockito.when(newsService.getNews(id)).thenReturn(new NewsReadDTO());

        MvcResult mvcResult = mvc.perform(get("/api/v1/news/{id}", id)).andExpect(status().isOk()).andReturn();

        Assert.assertNull(mvcResult.getRequest().getSession(false));
    }

}
