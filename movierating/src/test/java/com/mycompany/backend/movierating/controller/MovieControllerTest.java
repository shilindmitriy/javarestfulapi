package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePutDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadExtendedDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.LinkDuplicatedException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.MovieService;

@WebMvcTest(controllers = MovieController.class)
public class MovieControllerTest extends BaseControllerTest {

    @MockBean
    private MovieService movieService;

    @Test
    public void testGetMovieExtended() throws Exception {

        MovieReadExtendedDTO expectedRead = generator.generateObject(MovieReadExtendedDTO.class);

        Mockito.when(movieService.getMovieExtended(expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", expectedRead.getId())).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadExtendedDTO resultRead = objectMapper.readValue(resultJson, MovieReadExtendedDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).getMovieExtended(expectedRead.getId());
    }

    @Test
    @WithMockUser
    public void testAddCompanyToMovie() throws Exception {

        UUID id = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();
        ProductionCompanyReadDTO read = generator.generateObject(ProductionCompanyReadDTO.class);
        List<ProductionCompanyReadDTO> reads = List.of(read);
        Mockito.when(movieService.addCompanyToMovie(id, companyId)).thenReturn(reads);

        String resultJson = mvc.perform(post("/api/v1/movies/{id}/production-companies/{companyId}", id, companyId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> resultReads = objectMapper.readValue(resultJson,
                new TypeReference<List<ProductionCompanyReadDTO>>() {
                });
        Assert.assertEquals(reads, resultReads);

        Mockito.verify(movieService).addCompanyToMovie(id, companyId);
    }

    @Test
    @WithMockUser
    public void testDuplicatedCompany() throws Exception {

        UUID id = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        LinkDuplicatedException exc = new LinkDuplicatedException(
                String.format("Movie %s already has ProductionCompany %s", id, companyId));

        Mockito.when(movieService.addCompanyToMovie(id, companyId)).thenThrow(exc);

        String resultJson = mvc.perform(post("/api/v1/movies/{id}/production-companies/{companyId}", id, companyId))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        LinkDuplicatedException resultExc = objectMapper.readValue(resultJson, LinkDuplicatedException.class);
        Assert.assertTrue(resultExc.getMessage().contains(id.toString()));
        Assert.assertTrue(resultExc.getMessage().contains(companyId.toString()));

        Mockito.verify(movieService).addCompanyToMovie(id, companyId);
    }

    @Test
    @WithMockUser
    public void testAddCreativePersonToMovie() throws Exception {

        UUID id = UUID.randomUUID();
        UUID personId = UUID.randomUUID();

        CreativePersonReadDTO read = generator.generateObject(CreativePersonReadDTO.class);
        List<CreativePersonReadDTO> reads = List.of(read);
        Mockito.when(movieService.addCreativePersonToMovie(id, personId)).thenReturn(reads);

        String resultJson = mvc.perform(post("/api/v1/movies/{id}/creative-persons/{personId}", id, personId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<CreativePersonReadDTO> resultReads = objectMapper.readValue(resultJson,
                new TypeReference<List<CreativePersonReadDTO>>() {
                });
        Assert.assertEquals(reads, resultReads);

        Mockito.verify(movieService).addCreativePersonToMovie(id, personId);
    }

    @Test
    @WithMockUser
    public void testDuplicatedCreativePerson() throws Exception {

        UUID id = UUID.randomUUID();
        UUID personId = UUID.randomUUID();

        LinkDuplicatedException exc = new LinkDuplicatedException(
                String.format("Movie %s already has CreativePerson %s", id, personId));

        Mockito.when(movieService.addCreativePersonToMovie(id, personId)).thenThrow(exc);

        String resultJson = mvc.perform(post("/api/v1/movies/{id}/creative-persons/{personId}", id, personId))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        LinkDuplicatedException resultExc = objectMapper.readValue(resultJson, LinkDuplicatedException.class);
        Assert.assertTrue(resultExc.getMessage().contains(id.toString()));
        Assert.assertTrue(resultExc.getMessage().contains(personId.toString()));

        Mockito.verify(movieService).addCreativePersonToMovie(id, personId);
    }

    @Test
    @WithMockUser
    public void testCreateMovie() throws Exception {

        MovieReadDTO expectedRead = generator.generateObject(MovieReadDTO.class);
        MovieCreateDTO create = generator.generateObject(MovieCreateDTO.class);

        Mockito.when(movieService.createMovie(create)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/movies").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MovieReadDTO resultRead = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).createMovie(create);
    }

    @Test
    @WithMockUser
    public void testCreateMovieValidationFailed() throws Exception {

        MovieCreateDTO create = new MovieCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/movies").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(movieService, Mockito.never()).createMovie(ArgumentMatchers.any());
    }

    @Test
    public void testGetMovies() throws Exception {

        MovieFilter filter = createFilter();

        PageResult<MovieReadDTO> expectedRead = new PageResult<>();
        expectedRead.setData(List.of(generator.generateObject(MovieReadDTO.class)));

        Mockito.when(movieService.getMovies(filter, PageRequest.of(0, defaultPageSize))).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/movies").param("title", filter.getTitle()).param("releaseDateFrom", "2009-06-13")
                        .param("releaseDateTo", "2019-07-13").param("status", filter.getStatus().name())
                        .param("runningTimeFrom", "01:45:00.000").param("runningTimeTo", "02:15:00.000")
                        .param("productionCountries", "UNITED_STATES_OF_AMERICA", "UKRAINE")
                        .param("genres", "ACTION", "SCI_FI"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> resultRead = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<MovieReadDTO>>() {
                });
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).getMovies(filter, PageRequest.of(0, defaultPageSize));
    }

    @Test
    public void testGetMoviesPagingAndSorting() throws Exception {

        MovieFilter filter = createFilter();

        int page = 1;
        int size = 25;

        PageResult<MovieReadDTO> expectedRead = new PageResult<>();
        expectedRead.setData(List.of(generator.generateObject(MovieReadDTO.class)));
        expectedRead.setPage(page);
        expectedRead.setPageSize(size);
        expectedRead.setTotalElements(100);
        expectedRead.setTotalPages(4);

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "title"));

        Mockito.when(movieService.getMovies(filter, pageRequest)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/movies").param("title", filter.getTitle()).param("releaseDateFrom", "2009-06-13")
                        .param("releaseDateTo", "2019-07-13").param("status", filter.getStatus().name())
                        .param("runningTimeFrom", "01:45:00.000").param("runningTimeTo", "02:15:00.000")
                        .param("productionCountries", "UNITED_STATES_OF_AMERICA", "UKRAINE")
                        .param("genres", "ACTION", "SCI_FI").param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)).param("sort", "title,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> resultRead = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<MovieReadDTO>>() {
                });
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).getMovies(filter, pageRequest);
    }

    @Test
    public void testGetMoviesWhithBigPage() throws Exception {

        MovieFilter filter = createFilter();

        int page = 0;

        PageResult<MovieReadDTO> expectedRead = new PageResult<>();
        expectedRead.setData(List.of(generator.generateObject(MovieReadDTO.class)));
        expectedRead.setPage(page);
        expectedRead.setPageSize(maxPageSize);
        expectedRead.setTotalElements(100);
        expectedRead.setTotalPages(4);

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.ASC, "title"));

        Mockito.when(movieService.getMovies(filter, pageRequest)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/movies").param("title", filter.getTitle()).param("releaseDateFrom", "2009-06-13")
                        .param("releaseDateTo", "2019-07-13").param("status", filter.getStatus().name())
                        .param("runningTimeFrom", "01:45:00.000").param("runningTimeTo", "02:15:00.000")
                        .param("productionCountries", "UNITED_STATES_OF_AMERICA", "UKRAINE")
                        .param("genres", "ACTION", "SCI_FI").param("page", String.valueOf(page))
                        .param("size", String.valueOf(maxPageSize)).param("sort", "title,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> resultRead = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<MovieReadDTO>>() {
                });
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).getMovies(filter, pageRequest);
    }

    @Test
    @WithMockUser
    public void testPatchMovie() throws Exception {

        MovieReadDTO expectedRead = generator.generateObject(MovieReadDTO.class);
        MoviePatchDTO patch = generator.generateObject(MoviePatchDTO.class);

        Mockito.when(movieService.patchMovie(expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{id}", expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MovieReadDTO resultRead = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).patchMovie(expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchMovieValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();

        MoviePatchDTO patch = new MoviePatchDTO();
        patch.setProductionCountries(Set.of());
        patch.setGenres(Set.of());

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(movieService, Mockito.never()).patchMovie(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateMovie() throws Exception {

        MovieReadDTO expectedRead = generator.generateObject(MovieReadDTO.class);
        MoviePutDTO put = generator.generateObject(MoviePutDTO.class);

        Mockito.when(movieService.updateMovie(expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/movies/{id}", expectedRead.getId()).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MovieReadDTO resultRead = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(movieService).updateMovie(expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testUpdateMovieValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        MoviePutDTO put = new MoviePutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/movies/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(movieService, Mockito.never()).updateMovie(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteMovie() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/movies/{id}", id)).andExpect(status().isOk());
        Mockito.verify(movieService).deleteMovie(id);
    }

    @Test
    @WithMockUser
    public void testRemoveCompanyFromMovie() throws Exception {

        UUID id = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        List<ProductionCompanyReadDTO> reads = List.of();
        Mockito.when(movieService.removeCompanyFromMovie(id, companyId)).thenReturn(reads);

        String resultJson = mvc.perform(delete("/api/v1/movies/{id}/production-companies/{companyId}", id, companyId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<ProductionCompanyReadDTO> resultReads = objectMapper.readValue(resultJson,
                new TypeReference<List<ProductionCompanyReadDTO>>() {
                });
        Assert.assertEquals(reads, resultReads);

        Mockito.verify(movieService).removeCompanyFromMovie(id, companyId);
    }

    @Test
    @WithMockUser
    public void testRemoveNotAddedCompany() throws Exception {

        UUID id = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        EntityNotFoundException exc = new EntityNotFoundException(
                String.format("Movie %s has no ProductionCompany %s", id, companyId));

        Mockito.when(movieService.removeCompanyFromMovie(id, companyId)).thenThrow(exc);

        String resultJson = mvc.perform(delete("/api/v1/movies/{id}/production-companies/{companyId}", id, companyId))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        EntityNotFoundException resultExc = objectMapper.readValue(resultJson, EntityNotFoundException.class);
        Assert.assertTrue(resultExc.getMessage().contains(id.toString()));
        Assert.assertTrue(resultExc.getMessage().contains(companyId.toString()));

        Mockito.verify(movieService).removeCompanyFromMovie(id, companyId);
    }

    @Test
    @WithMockUser
    public void testRemoveCreativePersonFromMovie() throws Exception {

        UUID id = UUID.randomUUID();
        UUID personId = UUID.randomUUID();

        List<CreativePersonReadDTO> reads = List.of();
        Mockito.when(movieService.removeCreativePersonFromMovie(id, personId)).thenReturn(reads);

        String resultJson = mvc.perform(delete("/api/v1/movies/{id}/creative-persons/{personId}", id, personId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<CreativePersonReadDTO> resultReads = objectMapper.readValue(resultJson,
                new TypeReference<List<CreativePersonReadDTO>>() {
                });
        Assert.assertEquals(reads, resultReads);

        Mockito.verify(movieService).removeCreativePersonFromMovie(id, personId);
    }

    @Test
    @WithMockUser
    public void testRemoveNotAddedCreativePerson() throws Exception {

        UUID id = UUID.randomUUID();
        UUID personId = UUID.randomUUID();

        EntityNotFoundException exc = new EntityNotFoundException(
                String.format("Movie %s has no CreativePerson %s", id, personId));

        Mockito.when(movieService.removeCreativePersonFromMovie(id, personId)).thenThrow(exc);

        String resultJson = mvc.perform(delete("/api/v1/movies/{id}/creative-persons/{personId}", id, personId))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        EntityNotFoundException resultExc = objectMapper.readValue(resultJson, EntityNotFoundException.class);
        Assert.assertTrue(resultExc.getMessage().contains(id.toString()));
        Assert.assertTrue(resultExc.getMessage().contains(personId.toString()));

        Mockito.verify(movieService).removeCreativePersonFromMovie(id, personId);
    }

    private MovieFilter createFilter() throws Exception {
        MovieFilter filter = new MovieFilter();
        filter.setTitle("title");
        filter.setReleaseDateFrom(LocalDate.of(2009, 06, 13));
        filter.setReleaseDateTo(LocalDate.of(2019, 07, 13));
        filter.setStatus(MovieStatus.RELEASED);
        filter.setRunningTimeFrom(LocalTime.of(1, 45));
        filter.setRunningTimeTo(LocalTime.of(2, 15));
        filter.setProductionCountries(Set.of(Country.UNITED_STATES_OF_AMERICA, Country.UKRAINE));
        filter.setGenres(Set.of(Genre.ACTION, Genre.SCI_FI));
        return filter;
    }
}
