package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import org.hibernate.LazyInitializationException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.service.BaseTest;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Test
    public void testGetReferenceIfExist() {
        News news = generator.generatePersistentNews();
        News newsReference = repositoryHelper.getReferenceIfExist(News.class, news.getId());
        Assert.assertTrue(news.getId().equals(newsReference.getId()));
    }

    @Test(expected = LazyInitializationException.class)
    public void testGetReferenceIfExistExactlyReference() {
        News news = generator.generatePersistentNews();
        News newsReference = repositoryHelper.getReferenceIfExist(News.class, news.getId());
        Assert.assertTrue(news.getId().equals(newsReference.getId()));
        Assert.assertTrue(news.getText() != null);
        newsReference.getText();
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReferenceIfExistWrongId() {
        repositoryHelper.getReferenceIfExist(News.class, UUID.randomUUID());
    }

}
