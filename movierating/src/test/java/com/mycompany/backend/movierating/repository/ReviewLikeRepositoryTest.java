package com.mycompany.backend.movierating.repository;

import java.time.Instant;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.service.BaseTest;

public class ReviewLikeRepositoryTest extends BaseTest {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        ReviewLike like = new ReviewLike();
        reviewLikeRepository.save(like);
    }

    @Test
    public void testCreatedAtIsSet() {

        ReviewLike like = generator.generatePersistentReviewLike();

        Instant createdAtBeforeReload = like.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        like = reviewLikeRepository.findById(like.getId()).get();
        Instant createdAtAfterReload = like.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        ReviewLike like = generator.generatePersistentReviewLike();

        Instant createdAtBeforeUpdate = like.getCreatedAt();
        Instant updatedAtBeforeUpdate = like.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        like.setUserId(UUID.randomUUID());
        like = reviewLikeRepository.save(like);

        Instant createdAtAfterUpdate = like.getCreatedAt();
        Instant updatedAtAfterUpdate = like.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}
