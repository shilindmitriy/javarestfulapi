package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO;
import com.mycompany.backend.movierating.service.FilmographyService;

@WebMvcTest(controllers = FilmographyController.class)
public class FilmographyControllerTest extends BaseControllerTest {

    @MockBean
    private FilmographyService filmographyService;

    @Test
    public void testGetFilmography() throws Exception {

        UUID personId = UUID.randomUUID();
        FilmographyReadDTO filmography = generator.generateObject(FilmographyReadDTO.class);

        List<FilmographyReadDTO> expectedFilmography = List.of(filmography);

        Mockito.when(filmographyService.getFilmography(personId, filmography.getDepartment()))
                .thenReturn(expectedFilmography);

        String resultJson = mvc
                .perform(get("/api/v1/creative-persons/{personId}/filmography/{department}", personId,
                        filmography.getDepartment()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<FilmographyReadDTO> resultFilmography = objectMapper.readValue(resultJson,
                new TypeReference<List<FilmographyReadDTO>>() {
                });
        Assertions.assertThat(expectedFilmography).isEqualTo(resultFilmography);
        Assert.assertEquals(expectedFilmography, resultFilmography);

        Mockito.verify(filmographyService, Mockito.times(1)).getFilmography(personId, filmography.getDepartment());
    }
}