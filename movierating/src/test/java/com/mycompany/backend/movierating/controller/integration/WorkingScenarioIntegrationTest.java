package com.mycompany.backend.movierating.controller.integration;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.impl.client.HttpClients;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.domain.constants.EntityType;
import com.mycompany.backend.movierating.domain.constants.Gender;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationCreateDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationSolveRequest;
import com.mycompany.backend.movierating.dto.message.constants.Operation;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadExtendedDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyCreateDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.dto.review.ReviewConfirmationRequest;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeCreateDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeReadDTO;
import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.ModeratorsTrustDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.service.BaseTest;
import com.mycompany.backend.movierating.service.TranslationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ActiveProfiles({ "test", "integration-test" })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class WorkingScenarioIntegrationTest extends BaseTest {

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TranslationService translationService;

    private RestTemplate restTemplate;

    private final String BEGIN_URL = "http://localhost:8080/api/v1/";

    {
        restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault()));
    }

    @Test
    public void testWorkingScenario() throws InterruptedException {

        // Create an account a1
        log.info("FINAL_0");
        UserAccount a1 = generator.generateFlatEntityWithoutId(UserAccount.class);
        String a1Password = "passwordForA1";
        a1.setAuthority(Authority.ADMIN);
        a1.setPassword(passwordEncoder.encode(a1Password));
        a1 = userRepository.save(a1);

        // FINAL_1
        // Create an account m1
        log.info("FINAL_1");
        String m1Password = "passwordForM1";
        UserAccountReadDTO m1 = adminCreatesUserAcount(a1.getEmail(), a1Password, m1Password);

        // FINAL_2
        // User m1 gets the role of a MODERATOR
        log.info("FINAL_2");
        m1 = changeAuthority(a1.getEmail(), a1Password, a1.getId(), m1.getId(), Authority.MODERATOR);
        Assert.assertEquals(Authority.MODERATOR, m1.getAuthority());

        // FINAL_3
        // Create an account c1
        log.info("FINAL_3");
        String c1Password = "passwordForC1";
        UserAccountReadDTO c1 = adminCreatesUserAcount(a1.getEmail(), a1Password, c1Password);

        // FINAL_4
        // User c1 gets the role of a CONTENT_MANAGER
        log.info("FINAL_4");
        c1 = changeAuthority(a1.getEmail(), a1Password, a1.getId(), c1.getId(), Authority.CONTENT_MANAGER);
        Assert.assertEquals(Authority.CONTENT_MANAGER, c1.getAuthority());

        // FINAL_5
        // Create accounts u1, u2 and u3
        log.info("FINAL_5");
        String u1Password = "passwordForU1";
        UserAccountReadDTO u1 = anonymousCreatesUserAcount(Gender.MALE, u1Password);
        Assert.assertEquals(Authority.REGISTERED_USER, u1.getAuthority());
        Assert.assertEquals(Gender.MALE, u1.getGender());

        String u2Password = "passwordForU2";
        UserAccountReadDTO u2 = anonymousCreatesUserAcount(Gender.MALE, u2Password);
        Assert.assertEquals(Authority.REGISTERED_USER, u2.getAuthority());
        Assert.assertEquals(Gender.MALE, u2.getGender());

        String u3Password = "passwordForU3";
        UserAccountReadDTO u3 = anonymousCreatesUserAcount(Gender.FEMALE, u3Password);
        Assert.assertEquals(Authority.REGISTERED_USER, u3.getAuthority());
        Assert.assertEquals(Gender.FEMALE, u3.getGender());

        // FINAL_6
        // User u2 corrects the field of firstName
        log.info("FINAL_6");
        UserAccountPatchDTO patch = new UserAccountPatchDTO();
        patch.setFirstName("Bob");
        u2 = patchUser(u2, u2Password, patch);
        Assert.assertEquals("Bob", u2.getFirstName());

        // FINAL_7
        // User c1 creates a movie and related entities
        log.info("FINAL_7");
        MovieReadDTO movieLebowski = setMovieTheBigLebowski(c1.getEmail(), c1Password);

        ProductionCompanyReadDTO companyWorkingTitleFilms = setCompanyWorkingTitleFilms(c1.getEmail(), c1Password);
        addProductionCompanyToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), companyWorkingTitleFilms.getId());

        CreativePersonReadDTO personJeffBridges = setPersonJeffBridges(c1.getEmail(), c1Password);
        setWorkTheDude(c1.getEmail(), c1Password, movieLebowski.getId(), personJeffBridges.getId());
        addCreativePersonToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), personJeffBridges.getId());

        CreativePersonReadDTO personJohnGoodman = setPersonJohnGoodman(c1.getEmail(), c1Password);
        setWorkWalterSobchak(c1.getEmail(), c1Password, movieLebowski.getId(), personJohnGoodman.getId());
        addCreativePersonToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), personJohnGoodman.getId());

        CreativePersonReadDTO personJulianneMoore = setPersonJulianneMoore(c1.getEmail(), c1Password);
        setWorkMaudeLebowski(c1.getEmail(), c1Password, movieLebowski.getId(), personJulianneMoore.getId());
        addCreativePersonToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), personJulianneMoore.getId());

        CreativePersonReadDTO personJoelCoen = setPersonJoelCoen(c1.getEmail(), c1Password);
        setWorkDirectedByTheBigLebowski(c1.getEmail(), c1Password, movieLebowski.getId(), personJoelCoen.getId());
        setWorkWroteScriptForTheBigLebowski(c1.getEmail(), c1Password, movieLebowski.getId(), personJoelCoen.getId());
        addCreativePersonToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), personJoelCoen.getId());

        CreativePersonReadDTO personEthanCoen = setPersonEthanCoen(c1.getEmail(), c1Password);
        setWorkWroteScriptForTheBigLebowski(c1.getEmail(), c1Password, movieLebowski.getId(), personEthanCoen.getId());
        addCreativePersonToMovie(c1.getEmail(), c1Password, movieLebowski.getId(), personEthanCoen.getId());

        // FINAL_8
        // User c1 creates a news about The Big Lebowski
        log.info("FINAL_8");
        NewsReadDTO newsAboutLebowski = setNewsAboutTheBigLebowski(c1.getEmail(), c1Password);

        // FINAL_9
        // User u1 is getting the news about The Big Lebowski
        log.info("FINAL_9");
        NewsReadDTO viewNewsAboutLebowski = getNews(u1.getEmail(), u1Password, newsAboutLebowski.getId());
        Assert.assertEquals(newsAboutLebowski, viewNewsAboutLebowski);

        // FINAL_10
        // Users u1 and u2 like the news about The Big Lebowski
        log.info("FINAL_10");
        RatingReadDTO newsRatingFromU1 = setRatingForNews(u1.getEmail(), u1Password, 1, newsAboutLebowski.getId());
        Assert.assertTrue(newsRatingFromU1.getRating() == 1);

        SimpleRatingReadDTO simpleRatingU1 = getSimpleRating(u1.getEmail(), u1Password, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRatingU1.getLike() == 1L);
        Assert.assertTrue(simpleRatingU1.getDislike() == 0);

        RatingReadDTO newsRatingFromU2 = setRatingForNews(u2.getEmail(), u2Password, 1, newsAboutLebowski.getId());
        Assert.assertTrue(newsRatingFromU2.getRating() == 1);

        SimpleRatingReadDTO simpleRatingU2 = getSimpleRating(u2.getEmail(), u2Password, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRatingU2.getLike() == 2L);
        Assert.assertTrue(simpleRatingU2.getDislike() == 0);

        // FINAL_11
        // User u3 mistakenly likes the news about The Big Lebowski
        log.info("FINAL_11");
        RatingReadDTO wrongRating = setRatingForNews(u3.getEmail(), u3Password, 1, newsAboutLebowski.getId());
        Assert.assertTrue(wrongRating.getRating() == 1);

        SimpleRatingReadDTO simpleRatingU3 = getSimpleRating(u3.getEmail(), u3Password, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRatingU3.getLike() == 3L);
        Assert.assertTrue(simpleRatingU3.getDislike() == 0);

        // User u3 deleted own wrong like
        deleteRatingForNews(u3.getEmail(), u3Password, newsAboutLebowski.getId(), wrongRating.getId());
        simpleRatingU3 = getSimpleRating(u3.getEmail(), u3Password, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRatingU3.getLike() == 2L);
        Assert.assertTrue(simpleRatingU3.getDislike() == 0);

        // FINAL_12
        // User u3 dislikes the news about The Big Lebowski
        log.info("FINAL_12");
        RatingReadDTO newsRatingFromU3 = setRatingForNews(u3.getEmail(), u3Password, -1, newsAboutLebowski.getId());
        Assert.assertTrue(newsRatingFromU3.getRating() == -1);

        simpleRatingU3 = getSimpleRating(u3.getEmail(), u3Password, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRatingU3.getLike() == 2L);
        Assert.assertTrue(simpleRatingU3.getDislike() == 1L);

        // FINAL_13
        // User u3 found a misprint in the news about The Big Lebowski. User u3 sent a
        // message to content manager.
        log.info("FINAL_13");
        MessageToAdministrationReadDTO sentMessage = sentMisprintInNewsAboutTheBigLebowski(u3.getEmail(), u3Password,
                newsAboutLebowski.getId());

        // FINAL_14
        // User c1 is getting messages.
        log.info("FINAL_14");
        PageResult<MessageToAdministrationReadDTO> messages = getMessages(c1.getEmail(), c1Password, EntityType.NEWS,
                Administration.CONTENT_MANAGER);
        MessageToAdministrationReadDTO gotMessage = messages.getData().get(0);
        Assert.assertEquals(sentMessage, gotMessage);

        // FINAL_15
        // User c1 responds to the messages and fixes a misprint
        log.info("FINAL_15");
        List<MessageToAdministrationReadDTO> solvedMessages = solvedMessages(c1.getEmail(), c1Password, gotMessage);
        MessageToAdministrationReadDTO solvedMessage = solvedMessages.get(0);
        Assert.assertEquals(sentMessage.getId(), solvedMessage.getId());
        Assert.assertNotEquals(sentMessage.getSolved(), solvedMessage.getSolved());

        // FINAL_16
        // User c1 is getting messages. User c1 sees that there are no messages.
        log.info("FINAL_16");
        PageResult<MessageToAdministrationReadDTO> messagesIsEmpty = getMessages(c1.getEmail(), c1Password,
                EntityType.NEWS, Administration.CONTENT_MANAGER);
        Assert.assertTrue(messagesIsEmpty.getData().isEmpty());

        // FINAL_17
        // Anonimus is getting news and news' rating.
        log.info("FINAL_17");
        NewsReadDTO newsForAnonimus = getNews(null, null, newsAboutLebowski.getId());
        Assert.assertEquals(newsAboutLebowski.getId(), newsForAnonimus.getId());

        SimpleRatingReadDTO simpleRating = getSimpleRating(null, null, newsAboutLebowski.getId());
        Assert.assertTrue(simpleRating.getLike() == 2L);
        Assert.assertTrue(simpleRating.getDislike() == 1L);

        // FINAL_18
        // User u1 is getting the movie The Big Lebowski.
        log.info("FINAL_18");
        MovieReadExtendedDTO movieLebowskiForU1 = getMovieExtended(u1.getEmail(), u1Password, movieLebowski.getId());
        Assertions.assertThat(movieLebowski).isEqualToIgnoringGivenFields(movieLebowskiForU1, "averageRating",
                "numberOfAllVotes", "updatedAt");

        // FINAL_19
        // User u1 writes review and rates on movie The Big Lebowski.
        log.info("FINAL_19");
        ReviewReadDTO reviewOnLebowski = setReviewOnTheBigLebowski(u1.getEmail(), u1Password, movieLebowski.getId());
        Assert.assertEquals(false, reviewOnLebowski.getEnabled());
        Assert.assertEquals(movieLebowski.getId(), reviewOnLebowski.getParentId());
        Assert.assertEquals(u1.getId(), reviewOnLebowski.getUserId());

        // User u1 is viewing his review on movie The Big Lebowski. The review not yet
        // confirmed.
        PageResult<ReviewReadDTO> reviewsForU1 = getConfirmedReviews(u1.getEmail(), u1Password, movieLebowski.getId(),
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertTrue(reviewsForU1.getData().size() == 1);
        ReviewReadDTO reviewForU1 = reviewsForU1.getData().get(0);
        Assert.assertEquals(false, reviewForU1.getEnabled());
        Assert.assertEquals(reviewOnLebowski, reviewForU1);

        // User u1 writes rates on movie The Big Lebowski.
        int ratingU1 = 10;
        RatingReadDTO ratingMovieLebowskiU1 = setRatingForMovie(u1.getEmail(), u1Password, ratingU1,
                movieLebowski.getId());
        Assert.assertTrue(ratingMovieLebowskiU1.getRating() == ratingU1);

        movieLebowskiForU1 = getMovieExtended(u1.getEmail(), u1Password, movieLebowski.getId());
        ShortRatingReadDTO shortRatingU1 = movieLebowskiForU1.getRating();
        Assert.assertTrue(shortRatingU1.getAverageRating() == ratingU1);
        Assert.assertTrue(shortRatingU1.getNumberOfAllVotes() == 1L);

        // FINAL_20
        // User u2 is getting the movie The Big Lebowski and reviews.
        log.info("FINAL_20");
        MovieReadExtendedDTO movieLebowskiForU2 = getMovieExtended(u2.getEmail(), u2Password, movieLebowski.getId());
        Assertions.assertThat(movieLebowski).isEqualToIgnoringGivenFields(movieLebowskiForU2, "averageRating",
                "numberOfAllVotes", "updatedAt");

        PageResult<ReviewReadDTO> reviewsForU2 = getConfirmedReviews(u2.getEmail(), u2Password, movieLebowski.getId(),
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertTrue(reviewsForU2.getData().isEmpty());

        // FINAL_21
        // User u2 rates movie The Big Lebowski.
        log.info("FINAL_21");
        int ratingU2 = 10;
        RatingReadDTO ratingMovieLebowskiU2 = setRatingForMovie(u2.getEmail(), u2Password, ratingU2,
                movieLebowski.getId());
        Assert.assertTrue(ratingMovieLebowskiU2.getRating() == ratingU2);

        movieLebowskiForU2 = getMovieExtended(u2.getEmail(), u2Password, movieLebowski.getId());
        ShortRatingReadDTO shortRatingU2 = movieLebowskiForU2.getRating();
        Assert.assertTrue(shortRatingU2.getAverageRating() == (double) (ratingU1 + ratingU2) / 2);
        Assert.assertTrue(shortRatingU2.getNumberOfAllVotes() == 2L);

        // FINAL_22
        // User m1 is getting unconfirmed movie's reviews.
        log.info("FINAL_22");
        PageResult<ReviewReadDTO> unconfirmedMovieReviews = getUnconfirmedReviews(m1.getEmail(), m1Password, m1.getId(),
                ReviewType.MOVIE_REVIEW, PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertFalse(unconfirmedMovieReviews.getData().isEmpty());
        Assert.assertTrue(unconfirmedMovieReviews.getData().size() == 1);

        ReviewReadDTO unconfirmedMovieReview = unconfirmedMovieReviews.getData().get(0);
        Assert.assertEquals(reviewOnLebowski, unconfirmedMovieReview);

        // FINAL_23
        // User m1 confirms movie's review and makes trusted user u1.
        log.info("FINAL_23");
        Boolean confirmation = confirmReview(m1.getEmail(), m1Password, ReviewType.MOVIE_REVIEW, m1.getId(),
                unconfirmedMovieReview.getParentId(), unconfirmedMovieReview.getId());
        Assert.assertTrue(confirmation);

        PageResult<ReviewReadDTO> reviewsForM1 = getConfirmedReviews(m1.getEmail(), m1Password, movieLebowski.getId(),
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertTrue(reviewsForM1.getData().size() == 1);

        ReviewReadDTO reviewForM1 = reviewsForM1.getData().get(0);
        Assertions.assertThat(reviewForM1).isEqualToIgnoringGivenFields(reviewOnLebowski, "enabled", "updatedAt");
        Assert.assertTrue(reviewForM1.getEnabled());

        ModeratorsTrustDTO trust = new ModeratorsTrustDTO();
        trust.setRating(100);
        u1 = addModeratorsTrust(m1.getEmail(), m1Password, m1.getId(), u1.getId(), trust);
        Assert.assertEquals(trust.getRating(), u1.getModeratorsTrust());

        // FINAL_24
        // User u3 is getting the movie The Big Lebowski and reviews. User u3 rates
        // movie The Big Lebowski.
        log.info("FINAL_24");
        MovieReadExtendedDTO movieLebowskiForU3 = getMovieExtended(u3.getEmail(), u3Password, movieLebowski.getId());
        Assertions.assertThat(movieLebowski).isEqualToIgnoringGivenFields(movieLebowskiForU3, "averageRating",
                "numberOfAllVotes", "updatedAt");

        PageResult<ReviewReadDTO> reviewsForU3 = getConfirmedReviews(u3.getEmail(), u3Password, movieLebowski.getId(),
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertTrue(reviewsForU3.getData().size() == 1);

        ReviewReadDTO reviewForU3 = reviewsForU3.getData().get(0);
        Assert.assertEquals(reviewForM1, reviewForU3);

        int ratingU3 = 8;
        RatingReadDTO ratingMovieLebowskiU3 = setRatingForMovie(u3.getEmail(), u3Password, ratingU3,
                movieLebowski.getId());
        Assert.assertTrue(ratingMovieLebowskiU3.getRating() == ratingU3);

        movieLebowskiForU3 = getMovieExtended(u3.getEmail(), u3Password, movieLebowski.getId());
        ShortRatingReadDTO shortRatingU3 = movieLebowskiForU3.getRating();
        Assert.assertEquals((double) (ratingU1 + ratingU2 + ratingU3) / 3, shortRatingU3.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU3.getNumberOfAllVotes() == 3L);

        // FINAL_25
        // Users u3 likes u1's review.
        log.info("FINAL_25");
        ReviewLikeReadDTO likeReviewWrittenByU1 = setReviewLikeForReviewWrittenByU1(u3.getEmail(), u3Password, true,
                reviewOnLebowski.getId());
        Assert.assertTrue(likeReviewWrittenByU1.getLike());

        // FINAL_26
        // Anonimus is getting movie The Big Lebowski and its rating.
        log.info("FINAL_26");
        MovieReadExtendedDTO movieLebowskiForAnonimus = getMovieExtended(null, null, movieLebowski.getId());
        Assertions.assertThat(movieLebowski).isEqualToIgnoringGivenFields(movieLebowskiForAnonimus, "averageRating",
                "numberOfAllVotes", "updatedAt");

        DetailedRatingReadDTO detailedRating = getDetailedRating(movieLebowski.getId());
        Assert.assertEquals((double) (ratingMovieLebowskiU1.getRating() + ratingMovieLebowskiU2.getRating()
                + ratingMovieLebowskiU3.getRating()) / 3, detailedRating.getAverageRating(), 0.01d);
        Assert.assertTrue(detailedRating.getNumberOfAllVotes() == 3L);

        // FINAL_27
        // User c1 imports a movie O Brother, Where Art Thou? .
        log.info("FINAL_27");
        String movieExternalId = "134";
        UUID movieBrotherId = importMovie(c1.getEmail(), c1Password, c1.getId(), movieExternalId);
        Assert.assertTrue(movieBrotherId != null);

        // FINAL_28
        // User u1 writes review and rates on movie O Brother, Where Art Thou?.
        log.info("FINAL_28");
        MovieReadExtendedDTO movieBrotherForU1 = getMovieExtended(u1.getEmail(), u1Password, movieBrotherId);

        ReviewReadDTO reviewOnBrother = setReviewOnBrotherWhereArtThou(u1.getEmail(), u1Password, movieBrotherId);
        Assert.assertEquals(true, reviewOnBrother.getEnabled());
        Assert.assertEquals(movieBrotherId, reviewOnBrother.getParentId());
        Assert.assertEquals(u1.getId(), reviewOnBrother.getUserId());

        ratingU1 = 1;
        RatingReadDTO ratingMovieBrotherU1 = setRatingForMovie(u1.getEmail(), u1Password, ratingU1, movieBrotherId);
        Assert.assertTrue(ratingMovieBrotherU1.getRating() == 1);

        movieBrotherForU1 = getMovieExtended(u1.getEmail(), u1Password, movieBrotherId);
        shortRatingU1 = movieBrotherForU1.getRating();
        Assert.assertTrue(shortRatingU1.getAverageRating() == ratingU1);
        Assert.assertTrue(shortRatingU1.getNumberOfAllVotes() == 1L);

        // FINAL_29
        // Users u2 and u3 are getting the movie O Brother, Where Art Thou? and
        // reviews.
        // They rate movie and send a message to moderator.
        log.info("FINAL_29");
        MovieReadExtendedDTO movieBrotherForU2 = getMovieExtended(u2.getEmail(), u2Password, movieBrotherId);
        Assert.assertEquals(movieBrotherId, movieBrotherForU2.getId());

        reviewsForU2 = getConfirmedReviews(u2.getEmail(), u2Password, movieBrotherId,
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertFalse(reviewsForU2.getData().isEmpty());

        ratingU2 = 7;
        RatingReadDTO ratingMovieBrotherU2 = setRatingForMovie(u2.getEmail(), u2Password, ratingU2, movieBrotherId);
        Assert.assertTrue(ratingMovieBrotherU2.getRating() == ratingU2);

        movieBrotherForU2 = getMovieExtended(u2.getEmail(), u2Password, movieBrotherId);
        shortRatingU2 = movieBrotherForU2.getRating();
        Assert.assertTrue(shortRatingU2.getAverageRating() == (double) (ratingU1 + ratingU2) / 2);
        Assert.assertTrue(shortRatingU2.getNumberOfAllVotes() == 2L);

        MessageToAdministrationReadDTO sentMessageFromU2 = complaintOnReviewAboutBrotherWhereArtThou(u2.getEmail(),
                u2Password, movieBrotherId, reviewOnBrother.getId());
        Assert.assertEquals(u2.getId(), sentMessageFromU2.getCreatedById());

        MovieReadExtendedDTO movieBrotherForU3 = getMovieExtended(u3.getEmail(), u3Password, movieBrotherId);
        Assert.assertEquals(movieBrotherId, movieBrotherForU3.getId());

        reviewsForU3 = getConfirmedReviews(u3.getEmail(), u3Password, movieBrotherId,
                PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt")));
        Assert.assertFalse(reviewsForU3.getData().isEmpty());

        ratingU3 = 9;
        RatingReadDTO ratingMovieBrotherU3 = setRatingForMovie(u3.getEmail(), u3Password, ratingU3, movieBrotherId);
        Assert.assertTrue(ratingMovieBrotherU3.getRating() == ratingU3);

        movieBrotherForU3 = getMovieExtended(u3.getEmail(), u3Password, movieBrotherId);
        shortRatingU3 = movieBrotherForU3.getRating();
        Assert.assertEquals((double) (ratingU1 + ratingU2 + ratingU3) / 3, shortRatingU3.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU3.getNumberOfAllVotes() == 3L);

        MessageToAdministrationReadDTO sentMessageFromU3 = complaintOnReviewAboutBrotherWhereArtThou(u3.getEmail(),
                u3Password, movieBrotherId, reviewOnBrother.getId());
        Assert.assertEquals(u3.getId(), sentMessageFromU3.getCreatedById());

        // FINAL_30
        // User m1 is getting messages.
        log.info("FINAL_30");
        PageResult<MessageToAdministrationReadDTO> getMessagesForM1 = getMessages(m1.getEmail(), m1Password,
                EntityType.MOVIE_REVIEW, Administration.MODERATOR);
        Assert.assertFalse(getMessagesForM1.getData().isEmpty());
        Assert.assertTrue(getMessagesForM1.getData().size() == 2);
        Assertions.assertThat(getMessagesForM1.getData()).extracting("id")
                .containsExactlyInAnyOrder(sentMessageFromU3.getId(), sentMessageFromU2.getId());

        // FINAL_31
        // User m1 delete last u1's review and ban u1. User m1 notes that the messages
        // are resolved.
        log.info("FINAL_31");
        MessageToAdministrationReadDTO messageForM1 = getMessagesForM1.getData().get(0);
        deleteReview(m1.getEmail(), m1Password, messageForM1.getUrl());

        u1 = setBanUser(m1.getEmail(), m1Password, m1.getId(), u1.getId());
        Assert.assertEquals(Authority.UNREGISTERED_USER, u1.getAuthority());

        MessageToAdministrationPatchDTO messagePatch = new MessageToAdministrationPatchDTO();
        messagePatch.setNote("Review has been deleted.");
        messagePatch.setSolved(true);
        sentMessageFromU2 = patchMessage(m1.getEmail(), m1Password, sentMessageFromU2.getId(), messagePatch);
        Assert.assertTrue(sentMessageFromU2.getSolved());
        sentMessageFromU3 = patchMessage(m1.getEmail(), m1Password, sentMessageFromU3.getId(), messagePatch);
        Assert.assertTrue(sentMessageFromU3.getSolved());

        // FINAL_32
        // User m1 is getting messages.
        log.info("FINAL_32");
        getMessagesForM1 = getMessages(m1.getEmail(), m1Password, EntityType.MOVIE_REVIEW, Administration.MODERATOR);
        Assert.assertTrue(getMessagesForM1.getData().isEmpty());

        // FINAL_33
        // "Users u2 and u3 is getting movies' cast. They rate John Goodman's acting.
        log.info("FINAL_33");
        List<CastAndCrewReadDTO> movieLebowskisCastForU2 = getCastAndCrew(u2.getEmail(), u2Password,
                movieLebowski.getId(), Department.ACTING);
        UUID walterSobchakId = null;
        for (CastAndCrewReadDTO c : movieLebowskisCastForU2) {
            if (c.getCharacter().equalsIgnoreCase("Walter Sobchak")) {
                walterSobchakId = c.getWorkId();
            }
        }
        int ratingWalterU2 = 9;
        RatingReadDTO ratingWalterSobchakU2 = setRatingForCreativeWork(u2.getEmail(), u2Password, ratingWalterU2,
                walterSobchakId);
        Assert.assertTrue(ratingWalterSobchakU2.getRating() == ratingWalterU2);

        shortRatingU2 = getShortRatingForCreativeWork(u2.getEmail(), u2Password, walterSobchakId);
        Assert.assertEquals(ratingWalterU2, shortRatingU2.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU2.getNumberOfAllVotes() == 1L);

        List<CastAndCrewReadDTO> movieBrotherCastForU2 = getCastAndCrew(u2.getEmail(), u2Password, movieBrotherId,
                Department.ACTING);
        UUID bigDanId = null;
        for (CastAndCrewReadDTO c : movieBrotherCastForU2) {
            if (c.getCharacter().equalsIgnoreCase("Daniel \"Big Dan\" Teague")) {
                bigDanId = c.getWorkId();
            }
        }
        int ratingDanU2 = 5;
        RatingReadDTO ratingBigDanU2 = setRatingForCreativeWork(u2.getEmail(), u2Password, ratingDanU2, bigDanId);
        Assert.assertTrue(ratingBigDanU2.getRating() == ratingDanU2);

        shortRatingU2 = getShortRatingForCreativeWork(u2.getEmail(), u2Password, bigDanId);
        Assert.assertEquals(ratingDanU2, shortRatingU2.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU2.getNumberOfAllVotes() == 1L);

        List<CastAndCrewReadDTO> movieLebowskisCastForU3 = getCastAndCrew(u3.getEmail(), u3Password,
                movieLebowski.getId(), Department.ACTING);
        walterSobchakId = null;
        for (CastAndCrewReadDTO c : movieLebowskisCastForU3) {
            if (c.getCharacter().equalsIgnoreCase("Walter Sobchak")) {
                walterSobchakId = c.getWorkId();
            }
        }
        int ratingWalterU3 = 8;
        RatingReadDTO ratingWalterSobchakU3 = setRatingForCreativeWork(u3.getEmail(), u3Password, ratingWalterU3,
                walterSobchakId);
        Assert.assertTrue(ratingWalterSobchakU3.getRating() == ratingWalterU3);

        shortRatingU3 = getShortRatingForCreativeWork(u3.getEmail(), u3Password, walterSobchakId);
        Assert.assertEquals((double) (ratingWalterU2 + ratingWalterU3) / 2, shortRatingU3.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU3.getNumberOfAllVotes() == 2L);

        List<CastAndCrewReadDTO> movieBrotherCastForU3 = getCastAndCrew(u3.getEmail(), u3Password, movieBrotherId,
                Department.ACTING);
        bigDanId = null;
        for (CastAndCrewReadDTO c : movieBrotherCastForU3) {
            if (c.getCharacter().equalsIgnoreCase("Daniel \"Big Dan\" Teague")) {
                bigDanId = c.getWorkId();
            }
        }
        int ratingDanU3 = 7;
        RatingReadDTO ratingBigDanU3 = setRatingForCreativeWork(u3.getEmail(), u3Password, ratingDanU3, bigDanId);
        Assert.assertTrue(ratingBigDanU3.getRating() == ratingDanU3);

        shortRatingU3 = getShortRatingForCreativeWork(u3.getEmail(), u3Password, bigDanId);
        Assert.assertEquals((double) (ratingDanU2 + ratingDanU3) / 2, shortRatingU3.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU3.getNumberOfAllVotes() == 2L);

        // FINAL_34
        // User u1 is getting movies' cast. He tries to rate John Goodman's acting.
        log.info("FINAL_34");
        List<CastAndCrewReadDTO> movieLebowskisCastForU1 = getCastAndCrew(u1.getEmail(), u1Password,
                movieLebowski.getId(), Department.ACTING);
        walterSobchakId = null;
        for (CastAndCrewReadDTO c : movieLebowskisCastForU1) {
            if (c.getCharacter().equalsIgnoreCase("Walter Sobchak")) {
                walterSobchakId = c.getWorkId();
            }
        }

        RatingCreateDTO ratingCreate = new RatingCreateDTO();
        ratingCreate.setRating(1);

        HttpEntity<?> httpEntity = createHttpEntity(u1.getEmail(), u1Password, ratingCreate);
        String url = BEGIN_URL + "creative-works/" + walterSobchakId + "/ratings";
        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(url, HttpMethod.POST, httpEntity, RatingReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);

        shortRatingU1 = getShortRatingForCreativeWork(u1.getEmail(), u1Password, walterSobchakId);
        Assert.assertEquals((double) (ratingWalterU2 + ratingWalterU3) / 2, shortRatingU1.getAverageRating(), 0.01);
        Assert.assertTrue(shortRatingU1.getNumberOfAllVotes() == 2L);

        // FINAL_35
        // Anonimus is getting a creative person John Goodman.
        log.info("FINAL_35");
        CreativePersonReadDTO personJohnGoodmanForAnonimus = getCreativePerson(personJohnGoodman.getId());
        Assert.assertEquals(personJohnGoodman, personJohnGoodmanForAnonimus);

        Thread.sleep(2000);

        Double avgMoviesRating = getAverageMoviesRating(personJohnGoodmanForAnonimus.getId());
        Double avgRatingMovieLebowski = (double) (ratingMovieLebowskiU1.getRating() + ratingMovieLebowskiU2.getRating()
                + ratingMovieLebowskiU3.getRating()) / 3;
        Double avgRatingMovieBrother = (double) (ratingMovieBrotherU1.getRating() + ratingMovieBrotherU2.getRating()
                + ratingMovieBrotherU3.getRating()) / 3;
        Assert.assertEquals((avgRatingMovieLebowski + avgRatingMovieBrother) / 2, avgMoviesRating, 0.01);
        Double avgCreativeWorksRating = getAverageCreativeWorksRating(personJohnGoodmanForAnonimus.getId());
        Double avgRatingWalterSobchak = (double) (ratingWalterSobchakU2.getRating() + ratingWalterSobchakU3.getRating())
                / 2;
        Double avgRatingBigDan = (double) (ratingBigDanU2.getRating() + ratingBigDanU3.getRating()) / 2;
        Assert.assertEquals((avgRatingWalterSobchak + avgRatingBigDan) / 2, avgCreativeWorksRating, 0.01);
    }

    // The Big Lebowski.
    private MovieReadDTO setMovieTheBigLebowski(String email, String password) {
        MovieCreateDTO create = new MovieCreateDTO();
        create.setTitle("The Big Lebowski.");
        create.setReleaseDate(LocalDate.of(1998, 3, 6));
        create.setStatus(MovieStatus.RELEASED);
        create.setRunningTime(LocalTime.of(1, 57));
        create.setStoryline("When \"the dude\" Lebowski is mistaken for a millionaire Lebowski, two thugs urinate"
                + " on his rug to coerce him into paying a debt he knows nothing about. While attempting to gain "
                + "recompense for the ruined rug from his wealthy counterpart, he accepts a one-time job with high "
                + "pay-off. He enlists the help of his bowling buddy, Walter, a gun-toting Jewish-convert with anger "
                + "issues. Deception leads to more trouble, and it soon seems that everyone from porn empire tycoons "
                + "to nihilists want something from The Dude.");
        create.getProductionCountries().add(Country.UNITED_STATES_OF_AMERICA);
        create.getProductionCountries().add(Country.UNITED_KINGDOM);
        create.getGenres().add(Genre.COMEDY);
        create.getGenres().add(Genre.CRIME);
        create.getGenres().add(Genre.SPOTR);
        return createMovie(email, password, create);
    }

    private ProductionCompanyReadDTO setCompanyWorkingTitleFilms(String email, String password) {
        ProductionCompanyCreateDTO create = new ProductionCompanyCreateDTO();
        create.setName("Working Title Films");
        create.setHistory("Working Title is a British film and television production company owned by "
                + "Universal Pictures. The company was founded by Tim Bevan and Sarah Radclyffe in 1983. It produces "
                + "feature films and several television productions. Eric Fellner and Tim Bevan are now the co "
                + "chairmen of the company.");
        return createProductionCompany(email, password, create);
    }

    private CreativePersonReadDTO setPersonJeffBridges(String email, String password) {
        CreativePersonCreateDTO create = new CreativePersonCreateDTO();
        create.setName("Jeff Bridges");
        create.setBorn(LocalDate.of(1949, 12, 4));
        return createCreativePerson(email, password, create);
    }

    private CreativeWorkReadDTO setWorkTheDude(String email, String password, UUID movieId, UUID personId) {
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();
        create.setCharacter("The Dude");
        create.setDepartment(Department.ACTING);
        create.setPost("Leading actor");
        create.setPersonId(personId);
        return createCreativeWork(email, password, movieId, create);
    }

    private CreativePersonReadDTO setPersonJohnGoodman(String email, String password) {
        CreativePersonCreateDTO create = new CreativePersonCreateDTO();
        create.setName("John Goodman");
        create.setBorn(LocalDate.of(1952, 6, 20));
        create.setBiography("John Stephen Goodman is a U.S. film, television, and stage actor. "
                + "He was born in St. Louis, Missouri, to Virginia Roos (Loosmore), a waitress and saleswoman,"
                + " and Leslie Francis Goodman, a postal worker who died when John was a small child. He is "
                + "of English, Welsh, and German ancestry. John is best known for his role as Dan Conner on "
                + "the television series Roseanne (1988), which ran until 1997, and for which he won a Best "
                + "Actor Golden Globe award in 1993. Goodman is also noted for appearances in the films of the "
                + "Coen brothers, with prominent roles in Raising Arizona (1987), as an escaped convict, in "
                + "Barton Fink (1991), as a congenial murderer, in The Big Lebowski (1998), as a volatile bowler, "
                + "and in O Brother, Where Art Thou? (2000), as a cultured thief. Additionally, Goodman's voice work "
                + "has appeared in numerous Disney films, including the voice for \"Sulley\" in "
                + "Monsters, Inc. (2001). Having contributed to more than 50 films, Goodman has also won two "
                + "American Comedy Awards and hosted Saturday Night Live (1975) fourteen times.");
        return createCreativePerson(email, password, create);
    }

    private CreativeWorkReadDTO setWorkWalterSobchak(String email, String password, UUID movieId, UUID personId) {
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();
        create.setCharacter("Walter Sobchak");
        create.setDepartment(Department.ACTING);
        create.setPost("Supporting Actor");
        create.setPersonId(personId);
        return createCreativeWork(email, password, movieId, create);
    }

    private CreativePersonReadDTO setPersonJulianneMoore(String email, String password) {
        CreativePersonCreateDTO create = new CreativePersonCreateDTO();
        create.setName("Julianne Moore");
        create.setBorn(LocalDate.of(1960, 12, 3));
        return createCreativePerson(email, password, create);
    }

    private CreativeWorkReadDTO setWorkMaudeLebowski(String email, String password, UUID movieId, UUID personId) {
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();
        create.setCharacter("Maude Lebowski");
        create.setDepartment(Department.ACTING);
        create.setPost("Bit part");
        create.setPersonId(personId);
        return createCreativeWork(email, password, movieId, create);
    }

    private CreativePersonReadDTO setPersonJoelCoen(String email, String password) {
        CreativePersonCreateDTO create = new CreativePersonCreateDTO();
        create.setName("Joel Coen");
        create.setBorn(LocalDate.of(1954, 11, 29));
        create.setBiography("Joel Coen was born on November 29, 1954 in Minneapolis, Minnesota, USA as "
                + "Joel Daniel Coen. He is a producer and writer, known for The Ballad of Buster Scruggs (2018),"
                + " Fargo (1996) and A Serious Man (2009). He has been married to Frances McDormand since "
                + "April 1, 1984. They have one child.");
        return createCreativePerson(email, password, create);
    }

    private CreativeWorkReadDTO setWorkDirectedByTheBigLebowski(String email, String password, UUID movieId,
            UUID personId) {
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();
        create.setDepartment(Department.DIRECTING);
        create.setPost("Director");
        create.setPersonId(personId);
        return createCreativeWork(email, password, movieId, create);
    }

    private CreativeWorkReadDTO setWorkWroteScriptForTheBigLebowski(String email, String password, UUID movieId,
            UUID personId) {
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();
        create.setDepartment(Department.WRITING);
        create.setPost("Writer");
        create.setPersonId(personId);
        return createCreativeWork(email, password, movieId, create);
    }

    private CreativePersonReadDTO setPersonEthanCoen(String email, String password) {
        CreativePersonCreateDTO create = new CreativePersonCreateDTO();
        create.setName("Ethan Coen");
        create.setBorn(LocalDate.of(1957, 9, 21));
        create.setBiography("Ethan Coen was born on September 21, 1957 in Minneapolis, Minnesota, USA as "
                + "Ethan Jesse Coen. He is a producer and writer, known for The Ballad of Buster Scruggs (2018),"
                + " A Serious Man (2009) and Inside Llewyn Davis (2013). He has been married to Tricia Cooke since "
                + "October 2, 1990. They have two children.");
        return createCreativePerson(email, password, create);
    }

    // News about The Big Lebowski.
    private NewsReadDTO setNewsAboutTheBigLebowski(String email, String password) {
        NewsCreateDTO create = new NewsCreateDTO();
        create.setText("<h1>Is The Big Lebowski On Ntflix, Hulu Or Prime? Where To Watch Online</h1>\r\n"
                + "Cult classic The Big Lebowski is one of the Coen Brothers’ best films but where can "
                + "fans watch the movie online? Is it on Netflix, Hulu or Prime?\r\n"
                + "Here’s how to watch The Big Lebowski online, including whether the Coen Brothers comedy"
                + " is on Netflix, Hulu, Prime or other major streaming platforms. It’s hard to believe it’s"
                + " been over 20 years since the Coen Brothers released their cult classic The Big Lebowski "
                + "back in 1998. While the movie is probably now older than Jeffrey Lebowski’s trophy wife Bunny,"
                + " its legacy lives on today through its fans who’ve dedicated everything from festivals "
                + "(the annual, aptly titled Lebowski Fest) to bars (in Reykjavik, Glasgow, and Edinburgh) in "
                + "honor of their favorite movie. It’s even spawned its own religion with The Church of the "
                + "Latter-Day Dude.\r\n"
                + "The Big Lebowski recently got a spin-off in the form of The Jesus Rolls, directed by and "
                + "starring John Turturro who played Jesus Quintana in the original film. The movie – which is "
                + "also partly a remake of the 1974 French comedy-drama Going Places – sees Turturro reprise his "
                + "character and follows the fresh-out-of-jail Jesus as he joins fellow misfits Petey "
                + "(Bobby Cannavale) and Marie (Audrey Tatou) on a petty crime spree that takes them across upstate"
                + " New York’s backroads.\r\n"
                + "As it’s part spin-off, part remake of an entirely unrelated French movie, The Jesus Rolls isn’t "
                + "required viewing to understand or enjoy The Big Lebowski. Those looking to relive The Big Lebowski"
                + " before seeing its spin-off, however, have several ways to watch it online at their fingertips. "
                + "Unfortunately, The Big Lebowski isn’t currently on Netflix. It left the streaming platform’s "
                + "library in 2019 but has been available on and off over the years so there’s a good chance it will "
                + "return to Netflix eventually.\r\n"
                + "The Big Lebowski is currently available for Hulu subscribers although they’ll need the Starz "
                + "add-on to watch the film, which costs an additional $8.99 per month. Likewise, The Big Lebowski"
                + " is on Amazon Prime, but members need a Starz add-on to watch it, which is also priced at $8.99"
                + " per month. Otherwise, it’ll cost between $3.99 and $14.99 to rent or buy the movie on Prime.\r\n"
                + "If Hulu or Prime aren't options, there are still plenty of other ways to watch the Coen Brothers "
                + "classic online without having to purchase a physical copy on Blu-ray or DVD. It’s currently "
                + "available for FandangoNOW and Vudu subscribers and can be rented or bought via iTunes for $4.99 "
                + "or $14.99. A slightly cheaper option is renting or buying The Big Lebowski on the Microsoft Store,"
                + " which costs from $3.99 and $12.99.");
        return createNews(email, password, create);
    }

    // MessageToAdministration operations
    private MessageToAdministrationReadDTO sentMisprintInNewsAboutTheBigLebowski(String email, String password,
            UUID newsId) {

        MessageToAdministrationCreateDTO create = new MessageToAdministrationCreateDTO();
        create.setMisprint("Ntflix");
        create.setBeginIndex(27);
        create.setMessage("Misprint in the text.");
        create.setUrl(BEGIN_URL + "news/" + newsId);
        create.setEntityId(newsId);
        create.setEntityType(EntityType.NEWS);
        create.setEntityField("text");
        create.setAdministration(Administration.CONTENT_MANAGER);

        return createMessage(email, password, create);
    }

    private MessageToAdministrationReadDTO complaintOnReviewAboutBrotherWhereArtThou(String email, String password,
            UUID movieId, UUID reviewId) {

        MessageToAdministrationCreateDTO create = new MessageToAdministrationCreateDTO();
        create.setMisprint("Fucking idiot.");
        create.setMessage("Please pay attention. [i]George clooney didn't sing one note in this movie. In addition, "
                + "he is a disgusting actor. [b]Fucking idiot.[/b][/i]");
        create.setUrl(BEGIN_URL + "movies/" + movieId + "/reviews/" + reviewId);
        create.setEntityId(reviewId);
        create.setEntityType(EntityType.MOVIE_REVIEW);
        create.setEntityField("text");
        create.setAdministration(Administration.MODERATOR);

        return createMessage(email, password, create);
    }

    private List<MessageToAdministrationReadDTO> solvedMessages(String email, String password,
            MessageToAdministrationReadDTO message) {

        MessageToAdministrationSolveRequest solved = translationService.translate(message,
                MessageToAdministrationSolveRequest.class);
        solved.getMisprints().add(message.getMisprint());
        solved.setCorrection("Netflix");
        solved.setOperation(Operation.REPLACE);

        return getProcessedMessages(email, password, solved);
    }

    // review
    private ReviewReadDTO setReviewOnBrotherWhereArtThou(String email, String password, UUID movieId) {

        ReviewCreateDTO create = new ReviewCreateDTO();
        create.setText("George clooney didn't sing one note in this movie. In addition, he is a disgusting actor. "
                + "Fucking idiot.");

        return createReviewForMovie(email, password, movieId, create);
    }

    private ReviewReadDTO setReviewOnTheBigLebowski(String email, String password, UUID movieId) {

        ReviewCreateDTO create = new ReviewCreateDTO();
        create.setText("Well, maybe not all comedies, but ones that would be categorized as cult classics. "
                + "It never has been fully understood why the movie became so popular after its theatrical release. "
                + "[spoiler] Why would a movie about bowling, restitution for a rug, a kidnapping, Jeff Bridges as "
                + "the Dude, an insane twisted plot rise to the top of the comedy charts?[/spoiler] "
                + "I think because it was written and directed by the Coen brothers. Joel and Ethan know how to "
                + "make a comedy that will leave you laughing at all the wrong spots. "
                + "The absurdity is what makes their movies so enjoyable to watch. Think of Fargo, O Brother, "
                + "Where Art Thou?, Raising Arizona, and you get the picture.  Their brand of comedy is very unique "
                + "and often leaves the audience questioning their judgement.");

        return createReviewForMovie(email, password, movieId, create);
    }

    // rating
    private RatingReadDTO setRatingForNews(String email, String password, int rating, UUID newsId) {

        RatingCreateDTO create = new RatingCreateDTO();
        create.setRating(rating);

        return createRatingForNews(email, password, create, newsId);
    }

    private RatingReadDTO setRatingForMovie(String email, String password, int rating, UUID movieId) {

        RatingCreateDTO create = new RatingCreateDTO();
        create.setRating(rating);

        return createRatingForMovie(email, password, create, movieId);
    }

    private RatingReadDTO setRatingForCreativeWork(String email, String password, int rating, UUID workId) {

        RatingCreateDTO create = new RatingCreateDTO();
        create.setRating(rating);

        return createRatingForCreativeWork(email, password, create, workId);
    }

    // like
    private ReviewLikeReadDTO setReviewLikeForReviewWrittenByU1(String email, String password, Boolean like,
            UUID reviewId) {
        ReviewLikeCreateDTO create = new ReviewLikeCreateDTO();
        create.setLike(like);
        create.setReviewType(ReviewType.MOVIE_REVIEW);
        create.setReviewId(reviewId);

        return createReviewLike(email, password, create);
    }

    // Endpoints
    // http://localhost:8080/api/v1/creative-persons
    private Double getAverageMoviesRating(UUID personId) {

        ResponseEntity<Double> response = restTemplate.exchange(
                BEGIN_URL + "creative-persons/" + personId + "/movies-rating", HttpMethod.GET, null, Double.class);

        Double avg = response.getBody();

        Assert.assertNotNull(avg);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return avg;
    }

    private Double getAverageCreativeWorksRating(UUID personId) {

        ResponseEntity<Double> response = restTemplate.exchange(
                BEGIN_URL + "creative-persons/" + personId + "/creative-works-rating", HttpMethod.GET, null,
                Double.class);

        Double avg = response.getBody();

        Assert.assertNotNull(avg);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return avg;
    }

    private CreativePersonReadDTO getCreativePerson(UUID personId) {

        ResponseEntity<CreativePersonReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "creative-persons/" + personId, HttpMethod.GET, null, CreativePersonReadDTO.class);

        CreativePersonReadDTO person = response.getBody();

        Assert.assertNotNull(person.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return person;
    }

    private CreativePersonReadDTO createCreativePerson(String email, String password, CreativePersonCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<CreativePersonReadDTO> response = restTemplate.exchange(BEGIN_URL + "creative-persons",
                HttpMethod.POST, httpEntity, CreativePersonReadDTO.class);

        CreativePersonReadDTO person = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(person);
        Assert.assertNotNull(person.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return person;
    }

    // http://localhost:8080/api/v1/creative-works
    private RatingReadDTO createRatingForCreativeWork(String email, String password, RatingCreateDTO create,
            UUID workId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<RatingReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "creative-works/" + workId + "/ratings", HttpMethod.POST, httpEntity, RatingReadDTO.class);

        RatingReadDTO rating = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(rating);
        Assert.assertNotNull(rating.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    private ShortRatingReadDTO getShortRatingForCreativeWork(String email, String password, UUID workId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<ShortRatingReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "creative-works/" + workId + "/ratings", HttpMethod.GET, httpEntity,
                ShortRatingReadDTO.class);

        ShortRatingReadDTO rating = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    // http://localhost:8080/api/v1/movies
    private List<CastAndCrewReadDTO> getCastAndCrew(String email, String password, UUID movieId,
            Department department) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<List<CastAndCrewReadDTO>> response = restTemplate.exchange(
                BEGIN_URL + "movies/" + movieId + "/cast-and-crew/" + department, HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<CastAndCrewReadDTO>>() {
                });

        List<CastAndCrewReadDTO> cast = response.getBody();
        Assert.assertNotNull(cast);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return cast;
    }

    private DetailedRatingReadDTO getDetailedRating(UUID movieId) {

        ResponseEntity<DetailedRatingReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "movies/" + movieId + "/ratings", HttpMethod.GET, null, DetailedRatingReadDTO.class);

        DetailedRatingReadDTO rating = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    private RatingReadDTO createRatingForMovie(String email, String password, RatingCreateDTO create, UUID movieId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<RatingReadDTO> response = restTemplate.exchange(BEGIN_URL + "movies/" + movieId + "/ratings",
                HttpMethod.POST, httpEntity, RatingReadDTO.class);

        RatingReadDTO rating = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(rating);
        Assert.assertNotNull(rating.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    private MovieReadExtendedDTO getMovieExtended(String email, String password, UUID movieId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<MovieReadExtendedDTO> response = restTemplate.exchange(BEGIN_URL + "movies/" + movieId,
                HttpMethod.GET, httpEntity, MovieReadExtendedDTO.class);

        MovieReadExtendedDTO movie = response.getBody();

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));
        Assert.assertEquals(movie.getId(), movieId);

        return movie;
    }

    private MovieReadDTO createMovie(String email, String password, MovieCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);
        ResponseEntity<MovieReadDTO> response = restTemplate.exchange(BEGIN_URL + "movies", HttpMethod.POST, httpEntity,
                MovieReadDTO.class);

        MovieReadDTO movie = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(movie);
        Assert.assertNotNull(movie.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return movie;
    }

    private List<ProductionCompanyReadDTO> addProductionCompanyToMovie(String email, String password, UUID movieId,
            UUID companyId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);
        ResponseEntity<ProductionCompanyReadDTO[]> response = restTemplate.exchange(
                BEGIN_URL + "movies/" + movieId + "/production-companies/" + companyId, HttpMethod.POST, httpEntity,
                ProductionCompanyReadDTO[].class);

        ProductionCompanyReadDTO[] company = response.getBody();

        Assert.assertTrue(company.length > 0);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return Arrays.asList(company);
    }

    private List<CreativePersonReadDTO> addCreativePersonToMovie(String email, String password, UUID movieId,
            UUID personId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);
        ResponseEntity<CreativePersonReadDTO[]> response = restTemplate.exchange(
                BEGIN_URL + "movies/" + movieId + "/creative-persons/" + personId, HttpMethod.POST, httpEntity,
                CreativePersonReadDTO[].class);

        CreativePersonReadDTO[] persons = response.getBody();

        Assert.assertTrue(persons.length > 0);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return Arrays.asList(persons);
    }

    private CreativeWorkReadDTO createCreativeWork(String email, String password, UUID movieId,
            CreativeWorkCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);
        ResponseEntity<CreativeWorkReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "movies/" + movieId + "/creative-works", HttpMethod.POST, httpEntity,
                CreativeWorkReadDTO.class);

        CreativeWorkReadDTO work = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(work);
        Assert.assertNotNull(work.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return work;
    }

    private ReviewReadDTO createReviewForMovie(String email, String password, UUID movieId, ReviewCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<ReviewReadDTO> response = restTemplate.exchange(BEGIN_URL + "movies/" + movieId + "/reviews",
                HttpMethod.POST, httpEntity, ReviewReadDTO.class);

        ReviewReadDTO review = response.getBody();

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));
        Assert.assertNotNull(review.getId());
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(review, "enabled");

        return review;
    }

    private void deleteReview(String email, String password, String url) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<Void> response = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));
    }

    private PageResult<ReviewReadDTO> getConfirmedReviews(String email, String password, UUID movieId,
            Pageable pageable) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        String url = BEGIN_URL + "movies/" + movieId + "/reviews/";

        UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(url);
        uri.queryParam("page", pageable.getPageNumber());
        uri.queryParam("size", pageable.getPageSize());
        uri.queryParam("sort", sortToQueryParam(pageable.getSort()));

        ResponseEntity<PageResult<ReviewReadDTO>> response = restTemplate.exchange(uri.toUriString(), HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<PageResult<ReviewReadDTO>>() {
                });

        PageResult<ReviewReadDTO> reviews = response.getBody();
        Assert.assertNotNull(reviews);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return reviews;
    }

    // http://localhost:8080/api/v1/message-to-administrations
    private MessageToAdministrationReadDTO patchMessage(String email, String password, UUID messageId,
            MessageToAdministrationPatchDTO patch) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, patch);

        ResponseEntity<MessageToAdministrationReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "message-to-administrations/" + messageId, HttpMethod.PATCH, httpEntity,
                MessageToAdministrationReadDTO.class);

        MessageToAdministrationReadDTO message = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return message;
    }

    private PageResult<MessageToAdministrationReadDTO> getMessages(String email, String password, EntityType entityType,
            Administration administration) {

        MessageToAdministrationFilter filter = new MessageToAdministrationFilter();
        filter.setAdministration(administration);
        filter.setSolved(false);
        filter.setEntityType(entityType);

        Pageable pageable = PageRequest.of(0, 50, Sort.by(Sort.Direction.ASC, "createdAt"));

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        String url = BEGIN_URL + "message-to-administrations";

        UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(url);
        uri.queryParam("administration", filter.getAdministration());
        uri.queryParam("solved", filter.getSolved());
        uri.queryParam("entityType", filter.getEntityType());
        uri.queryParam("page", pageable.getPageNumber());
        uri.queryParam("size", pageable.getPageSize());
        uri.queryParam("sort", sortToQueryParam(pageable.getSort()));

        ResponseEntity<PageResult<MessageToAdministrationReadDTO>> response = restTemplate.exchange(uri.toUriString(),
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<MessageToAdministrationReadDTO>>() {
                });

        PageResult<MessageToAdministrationReadDTO> messages = response.getBody();
        Assert.assertNotNull(messages);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return messages;
    }

    private MessageToAdministrationReadDTO createMessage(String email, String password,
            MessageToAdministrationCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<MessageToAdministrationReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "message-to-administrations", HttpMethod.POST, httpEntity,
                MessageToAdministrationReadDTO.class);

        MessageToAdministrationReadDTO rating = response.getBody();
        Assertions.assertThat(create).isEqualToComparingFieldByField(rating);
        Assert.assertNotNull(rating.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    // http://localhost:8080/api/v1/moderators
    private UserAccountReadDTO setBanUser(String email, String password, UUID moderatorId, UUID userId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "moderators/" + moderatorId + "/users/" + userId + "/banned-user", HttpMethod.PATCH,
                httpEntity, UserAccountReadDTO.class);

        UserAccountReadDTO user = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return user;
    }

    private UserAccountReadDTO addModeratorsTrust(String email, String password, UUID moderatorId, UUID userId,
            ModeratorsTrustDTO moderatorsTrust) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, moderatorsTrust);

        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "moderators/" + moderatorId + "/users/" + userId + "/moderators-trust", HttpMethod.POST,
                httpEntity, UserAccountReadDTO.class);

        UserAccountReadDTO user = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return user;
    }

    private PageResult<ReviewReadDTO> getUnconfirmedReviews(String email, String password, UUID moderatorId,
            ReviewType reviewType, Pageable pageable) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        String url = BEGIN_URL + "moderators/" + moderatorId + "/reviews-type/" + reviewType;

        UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(url);
        uri.queryParam("page", pageable.getPageNumber());
        uri.queryParam("size", pageable.getPageSize());
        uri.queryParam("sort", sortToQueryParam(pageable.getSort()));

        ResponseEntity<PageResult<ReviewReadDTO>> response = restTemplate.exchange(uri.toUriString(), HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<PageResult<ReviewReadDTO>>() {
                });

        PageResult<ReviewReadDTO> reviews = response.getBody();
        Assert.assertNotNull(reviews);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return reviews;
    }

    private Boolean confirmReview(String email, String password, ReviewType reviewType, UUID moderatorId, UUID movieId,
            UUID reviewId) {

        ReviewConfirmationRequest confirmationRequest = new ReviewConfirmationRequest();
        confirmationRequest.setReviewType(reviewType);
        confirmationRequest.setParentId(movieId);
        confirmationRequest.setReviewId(reviewId);

        HttpEntity<?> httpEntity = createHttpEntity(email, password, confirmationRequest);

        ResponseEntity<Boolean> response = restTemplate.exchange(
                BEGIN_URL + "moderators/" + moderatorId + "/confirmed-review", HttpMethod.POST, httpEntity,
                Boolean.class);

        Boolean review = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return review;
    }

    // http://localhost:8080/api/v1/content-managers
    private UUID importMovie(String email, String password, UUID managerId, String movieExternalId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<UUID> response = restTemplate.exchange(
                BEGIN_URL + "content-managers/" + managerId + "/movies-external/" + movieExternalId + "/import",
                HttpMethod.POST, httpEntity, UUID.class);

        UUID movieId = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return movieId;
    }

    // http://localhost:8080/api/v1/review-likes
    private ReviewLikeReadDTO createReviewLike(String email, String password, ReviewLikeCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<ReviewLikeReadDTO> response = restTemplate.exchange(BEGIN_URL + "review-likes", HttpMethod.POST,
                httpEntity, ReviewLikeReadDTO.class);

        ReviewLikeReadDTO like = response.getBody();
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return like;
    }

    // http://localhost:8080/api/v1/news/
    private NewsReadDTO getNews(String email, String password, UUID newsId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<NewsReadDTO> response = restTemplate.exchange(BEGIN_URL + "news/" + newsId, HttpMethod.GET,
                httpEntity, NewsReadDTO.class);

        NewsReadDTO news = response.getBody();

        Assert.assertEquals(news.getId(), newsId);
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return news;
    }

    private NewsReadDTO createNews(String email, String password, NewsCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<NewsReadDTO> response = restTemplate.exchange(BEGIN_URL + "news", HttpMethod.POST, httpEntity,
                NewsReadDTO.class);

        NewsReadDTO news = response.getBody();
        Assertions.assertThat(create).isEqualToComparingFieldByField(news);
        Assert.assertNotNull(news.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return news;
    }

    private RatingReadDTO createRatingForNews(String email, String password, RatingCreateDTO create, UUID newsId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);

        ResponseEntity<RatingReadDTO> response = restTemplate.exchange(BEGIN_URL + "news/" + newsId + "/ratings",
                HttpMethod.POST, httpEntity, RatingReadDTO.class);

        RatingReadDTO rating = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(rating);
        Assert.assertNotNull(rating.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    private void deleteRatingForNews(String email, String password, UUID newsId, UUID ratingId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<Void> response = restTemplate.exchange(BEGIN_URL + "news/" + newsId + "/ratings/" + ratingId,
                HttpMethod.DELETE, httpEntity, Void.class);

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));
    }

    private SimpleRatingReadDTO getSimpleRating(String email, String password, UUID newsId) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, null);

        ResponseEntity<SimpleRatingReadDTO> response = restTemplate.exchange(BEGIN_URL + "news/" + newsId + "/ratings",
                HttpMethod.GET, httpEntity, SimpleRatingReadDTO.class);

        SimpleRatingReadDTO rating = response.getBody();

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return rating;
    }

    // http://localhost:8080/api/v1/processed-messages
    private List<MessageToAdministrationReadDTO> getProcessedMessages(String email, String password,
            MessageToAdministrationSolveRequest solved) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, solved);

        ResponseEntity<MessageToAdministrationReadDTO[]> response = restTemplate.exchange(
                BEGIN_URL + "processed-messages", HttpMethod.POST, httpEntity, MessageToAdministrationReadDTO[].class);

        MessageToAdministrationReadDTO[] messages = response.getBody();

        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return Arrays.asList(messages);
    }

    // http://localhost:8080/api/v1/production-companies
    private ProductionCompanyReadDTO createProductionCompany(String email, String password,
            ProductionCompanyCreateDTO create) {

        HttpEntity<?> httpEntity = createHttpEntity(email, password, create);
        ResponseEntity<ProductionCompanyReadDTO> response = restTemplate.exchange(BEGIN_URL + "production-companies",
                HttpMethod.POST, httpEntity, ProductionCompanyReadDTO.class);

        ProductionCompanyReadDTO company = response.getBody();

        Assertions.assertThat(create).isEqualToComparingFieldByField(company);
        Assert.assertNotNull(company.getId());
        Assert.assertTrue(response.getStatusCode().equals(HttpStatus.OK));

        return company;
    }

    // http://localhost:8080/api/v1/users
    private UserAccountReadDTO patchUser(UserAccountReadDTO user, String password, UserAccountPatchDTO patch) {

        HttpEntity<?> httpEntity = createHttpEntity(user.getEmail(), password, patch);
        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(BEGIN_URL + "users/" + user.getId(),
                HttpMethod.PATCH, httpEntity, UserAccountReadDTO.class);

        user = response.getBody();

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(patch.getFirstName(), user.getFirstName());

        return user;
    }

    private UserAccountReadDTO createUserAcount(String adminsEmail, String adminsPassword, Gender gender,
            String createdUsersPassword) {

        UserAccountCreateDTO create = generator.generateObject(UserAccountCreateDTO.class);
        create.setPassword(passwordEncoder.encode(createdUsersPassword));
        if (gender != null) {
            create.setGender(gender);
        }

        HttpEntity<?> httpEntity = createHttpEntity(adminsEmail, adminsPassword, create);
        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(BEGIN_URL + "users", HttpMethod.POST,
                httpEntity, UserAccountReadDTO.class);

        UserAccountReadDTO user = response.getBody();

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(user, "password");
        Assert.assertNotNull(user.getId());

        return user;
    }

    private UserAccountReadDTO adminCreatesUserAcount(String adminsEmail, String adminsPassword,
            String createdUsersPassword) {
        return createUserAcount(adminsEmail, adminsPassword, null, createdUsersPassword);
    }

    private UserAccountReadDTO anonymousCreatesUserAcount(Gender gender, String createdUsersPassword) {
        return createUserAcount(null, null, gender, createdUsersPassword);
    }

    // http://localhost:8080/api/v1/admins
    private UserAccountReadDTO changeAuthority(String email, String password, UUID adminId, UUID id,
            Authority authority) {

        AuthorityPatchDTO patch = new AuthorityPatchDTO();
        patch.setAuthority(authority);

        HttpEntity<?> httpEntity = createHttpEntity(email, password, patch);
        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(
                BEGIN_URL + "admins/" + adminId + "/users/" + id + "/authority", HttpMethod.PATCH, httpEntity,
                UserAccountReadDTO.class);

        UserAccountReadDTO user = response.getBody();

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(authority, user.getAuthority());

        return user;
    }

    // support methods
    private HttpEntity<?> createHttpEntity(String email, String password, Object body) {

        HttpHeaders headers = new HttpHeaders();
        if (email != null && password != null) {
            headers.add(HttpHeaders.AUTHORIZATION, getyBasicAuthorizationHeaderValue(email, password));
        }

        Map<String, Object> map = null;
        if (body != null) {
            headers.setContentType(MediaType.APPLICATION_JSON);
            map = objectMapper.convertValue(body, new TypeReference<Map<String, Object>>() {
            });
            return new HttpEntity<>(map, headers);
        }

        return new HttpEntity<>(headers);
    }

    private String getyBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.getEncoder().encode(String.format("%s:%s", userName, password).getBytes()));
    }

    private String sortToQueryParam(Sort sort) {
        return sort.toString().replace(": ", ",");
    }

}
