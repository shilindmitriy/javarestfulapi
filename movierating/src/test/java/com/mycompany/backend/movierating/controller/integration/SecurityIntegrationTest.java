package com.mycompany.backend.movierating.controller.integration;

import java.util.Base64;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.service.BaseTest;

@ActiveProfiles({ "test", "integration-test" })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private MovieReviewRepository reviewRepository;

    @Autowired
    private NewsRatingRepository ratingRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testHealthNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:8080/health", Void.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testGetMessageToAdministrationNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(
                        "http://localhost:8080/api/v1/message-to-administrations/" + UUID.randomUUID(), HttpMethod.GET,
                        HttpEntity.EMPTY, MessageToAdministrationReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMessageToAdministration() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);
        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<MessageToAdministrationReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/message-to-administrations/" + message.getId(), HttpMethod.GET,
                httpEntity, MessageToAdministrationReadDTO.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMessageToAdministrationWrongPassword() {

        UserAccount user = getUserAccountWithPassword("qwerty");
        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), "wrong password"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(
                        "http://localhost:8080/api/v1/message-to-administrations/" + message.getId(), HttpMethod.GET,
                        httpEntity, MessageToAdministrationReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMessageToAdministrationWrongUser() {

        String password = "qwerty";

        getUserAccountWithPassword(password);
        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue("wrong user", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(
                        "http://localhost:8080/api/v1/message-to-administrations/" + message.getId(), HttpMethod.GET,
                        httpEntity, MessageToAdministrationReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMessageToAdministrationNoSession() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);
        MessageToAdministration message = generator.generatePersistentMessageToAdministration();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<MessageToAdministrationReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/message-to-administrations/" + message.getId(), HttpMethod.GET,
                httpEntity, MessageToAdministrationReadDTO.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNull(response.getHeaders().get("Set-Cookie"));
    }

    @Test
    public void testCreateNewsRoleIsNotContentManager() {

        String password = "qwerty";

        UserAccount user = generator.generatePersistentUser();
        user.setPassword(passwordEncoder.encode(password));
        user.setAuthority(Authority.REGISTERED_USER);
        user = userRepository.save(user);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        NewsCreateDTO create = generator.generateObject(NewsCreateDTO.class);
        Map<String, Object> map = objectMapper.convertValue(create, new TypeReference<Map<String, Object>>() {
        });

        HttpEntity<?> httpEntity = new HttpEntity<>(map, headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange("http://localhost:8080/api/v1/news/", HttpMethod.POST,
                        httpEntity, NewsReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testCreateNewsRoleIsContentManager() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        NewsCreateDTO create = generator.generateObject(NewsCreateDTO.class);
        Map<String, Object> map = objectMapper.convertValue(create, new TypeReference<Map<String, Object>>() {
        });

        HttpEntity<?> httpEntity = new HttpEntity<>(map, headers);
        ResponseEntity<NewsReadDTO> response = restTemplate.exchange("http://localhost:8080/api/v1/news/",
                HttpMethod.POST, httpEntity, NewsReadDTO.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testPutUserWrongUser() {

        String password = "qwerty";

        UserAccount user = generator.generatePersistentUser();
        user.setPassword(passwordEncoder.encode("123456"));
        user.setAuthority(Authority.CONTENT_MANAGER);
        user = userRepository.save(user);
        UUID wrongId = user.getId();

        UserAccount currentUser = getUserAccountWithPassword(password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(currentUser.getEmail(), password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        UserAccountPutDTO update = generator.generateObject(UserAccountPutDTO.class);
        Map<String, Object> map = objectMapper.convertValue(update, new TypeReference<Map<String, Object>>() {
        });

        HttpEntity<?> httpEntity = new HttpEntity<>(map, headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange("http://localhost:8080/api/v1/users/" + wrongId,
                        HttpMethod.PUT, httpEntity, UserAccountReadDTO.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testPutUserCurrentUser() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));
        headers.setContentType(MediaType.APPLICATION_JSON);

        UserAccountPutDTO update = generator.generateObject(UserAccountPutDTO.class);
        Map<String, Object> map = objectMapper.convertValue(update, new TypeReference<Map<String, Object>>() {
        });

        HttpEntity<?> httpEntity = new HttpEntity<>(map, headers);
        ResponseEntity<UserAccountReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + user.getId(), HttpMethod.PUT, httpEntity,
                UserAccountReadDTO.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteReviewWrongUser() {

        String password = "qwerty";

        UserAccount wrongUser = getUserAccountWithPassword("123456");
        UserAccount currentUser = getUserAccountWithPassword(password);

        MovieReview review = generator.generatePersistentMovieReview();
        review.setUser(wrongUser);
        review = reviewRepository.save(review);
        UUID reviewId = review.getId();
        UUID movieId = review.getParentEntity().getId();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(currentUser.getEmail(), password));

        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(
                        "http://localhost:8080/api/v1/movies/" + movieId + "/reviews/" + reviewId, HttpMethod.DELETE,
                        httpEntity, Void.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testDeleteReviewCurrentUser() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);

        MovieReview review = generator.generatePersistentMovieReview();
        review.setUser(user);
        review = reviewRepository.save(review);
        UUID reviewId = review.getId();
        UUID movieId = review.getParentEntity().getId();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));

        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<Void> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/movies/" + movieId + "/reviews/" + reviewId, HttpMethod.DELETE, httpEntity,
                Void.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteRatingWrongUser() {

        String password = "qwerty";

        UserAccount wrongUser = getUserAccountWithPassword("123456");
        UserAccount currentUser = getUserAccountWithPassword(password);

        NewsRating rating = generator.generatePersistentNewsRating();
        rating.setUser(wrongUser);
        rating = ratingRepository.save(rating);
        UUID ratingId = rating.getId();
        UUID newsId = rating.getParentEntity().getId();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(currentUser.getEmail(), password));

        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions
                .assertThatThrownBy(() -> restTemplate.exchange(
                        "http://localhost:8080/api/v1/news/" + newsId + "/ratings/" + ratingId, HttpMethod.DELETE,
                        httpEntity, Void.class))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testDeleteRatingCurrentUser() {

        String password = "qwerty";

        UserAccount user = getUserAccountWithPassword(password);

        NewsRating rating = generator.generatePersistentNewsRating();
        rating.setUser(user);
        rating = ratingRepository.save(rating);
        UUID ratingId = rating.getId();
        UUID newsId = rating.getParentEntity().getId();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getyBasicAuthorizationHeaderValue(user.getEmail(), password));

        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<Void> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/news/" + newsId + "/ratings/" + ratingId, HttpMethod.DELETE, httpEntity,
                Void.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String getyBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.getEncoder().encode(String.format("%s:%s", userName, password).getBytes()));
    }

    private UserAccount getUserAccountWithPassword(String password) {
        UserAccount user = generator.generatePersistentUser();
        user.setPassword(passwordEncoder.encode(password));
        user.setAuthority(Authority.CONTENT_MANAGER);
        return userRepository.save(user);
    }

}
