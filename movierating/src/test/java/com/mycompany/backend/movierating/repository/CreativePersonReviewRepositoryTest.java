package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.service.BaseTest;

public class CreativePersonReviewRepositoryTest extends BaseTest {

    @Autowired
    private CreativePersonReviewRepository creviewRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        CreativePersonReview rating = new CreativePersonReview();
        creviewRepository.save(rating);
    }

    @Test
    public void testCreatedAtIsSet() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();

        Instant createdAtBeforeReload = review.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        review = creviewRepository.findById(review.getId()).get();
        Instant createdAtAfterReload = review.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        CreativePersonReview review = generator.generatePersistentCreativePersonReview();

        Instant createdAtBeforeUpdate = review.getCreatedAt();
        Instant updatedAtBeforeUpdate = review.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        review.setText("updated");
        review = creviewRepository.save(review);

        Instant createdAtAfterUpdate = review.getCreatedAt();
        Instant updatedAtAfterUpdate = review.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}