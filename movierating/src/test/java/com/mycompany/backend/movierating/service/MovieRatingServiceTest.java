package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.InappropriateEntityException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.MovieRatingRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class MovieRatingServiceTest extends BaseTest {

    @Autowired
    private MovieRatingService ratingService;

    @Autowired
    private MovieRatingRepository ratingRepository;

    @Autowired
    private UserAccountRepository userRepository;
    
    @Autowired
    private MovieRepository movieRepository;

    @Test
    public void testGetRating() {
        MovieRating rating = generator.generatePersistentMovieRating();
        RatingReadDTO read = ratingService.getRating(rating.getParentEntity().getId(), rating.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testGetShortRating() {

        Movie movie = generator.generatePersistentMovie();
        Movie movieNotFound = generator.generatePersistentMovie();

        MovieRating rating1 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating1.setRating(3);
        rating1.setParentEntity(movie);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = ratingRepository.save(rating1);

        MovieRating rating2 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating2.setRating(10);
        rating2.setParentEntity(movie);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = ratingRepository.save(rating2);

        MovieRating rating3 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating3.setRating(6);
        rating3.setParentEntity(movie);
        rating3.setUser(generator.generatePersistentUser());
        rating3 = ratingRepository.save(rating3);

        MovieRating rating4 = generator.generateFlatEntityWithoutId(MovieRating.class);
        rating4.setParentEntity(movieNotFound);
        rating4.setUser(generator.generatePersistentUser());
        rating4 = ratingRepository.save(rating4);

        ShortRatingReadDTO shortRating = ratingService.getShortRating(movie.getId());
        Assert.assertTrue(shortRating.getNumberOfAllVotes() == 3L);
        Assert.assertEquals(shortRating.getAverageRating(),
                (double) (rating1.getRating() + rating2.getRating() + rating3.getRating()) / 3, 0.01d);
    }

    @Test
    public void testGetDetailedRating() {

        Movie movie = generator.generatePersistentMovie();
        Movie movieNotFound = generator.generatePersistentMovie();
        List<MovieRating> ratings = new ArrayList<>();

        MovieRating rating1 = new MovieRating();
        rating1.setRating(1);
        rating1.setAge(10);
        rating1.setGender(Gender.MALE);
        rating1.setCountryUser(Country.UKRAINE);
        rating1.setParentEntity(movie);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = ratingRepository.save(rating1);
        ratings.add(rating1);

        MovieRating rating2 = new MovieRating();
        rating2.setRating(9);
        rating2.setAge(15);
        rating2.setGender(Gender.FEMALE);
        rating2.setCountryUser(Country.UKRAINE);
        rating2.setParentEntity(movie);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = ratingRepository.save(rating2);
        ratings.add(rating2);

        MovieRating rating3 = new MovieRating();
        rating3.setRating(2);
        rating3.setAge(16);
        rating3.setGender(Gender.MALE);
        rating3.setCountryUser(Country.UKRAINE);
        rating3.setParentEntity(movie);
        rating3.setUser(generator.generatePersistentUser());
        rating3 = ratingRepository.save(rating3);
        ratings.add(rating3);

        MovieRating rating4 = new MovieRating();
        rating4.setRating(3);
        rating4.setAge(18);
        rating4.setGender(Gender.MALE);
        rating4.setCountryUser(Country.GREECE);
        rating4.setParentEntity(movie);
        rating4.setUser(generator.generatePersistentUser());
        rating4 = ratingRepository.save(rating4);
        ratings.add(rating4);

        MovieRating rating5 = new MovieRating();
        rating5.setRating(4);
        rating5.setAge(19);
        rating5.setGender(Gender.MALE);
        rating5.setCountryUser(Country.CHINA);
        rating5.setParentEntity(movie);
        rating5.setUser(generator.generatePersistentUser());
        rating5 = ratingRepository.save(rating5);
        ratings.add(rating5);

        MovieRating rating6 = new MovieRating();
        rating6.setRating(4);
        rating6.setAge(19);
        rating6.setGender(Gender.FEMALE);
        rating6.setCountryUser(Country.CUBA);
        rating6.setParentEntity(movie);
        rating6.setUser(generator.generatePersistentUser());
        rating6 = ratingRepository.save(rating6);
        ratings.add(rating6);

        MovieRating rating7 = new MovieRating();
        rating7.setRating(5);
        rating7.setAge(25);
        rating7.setGender(Gender.MALE);
        rating7.setCountryUser(Country.ARMENIA);
        rating7.setParentEntity(movie);
        rating7.setUser(generator.generatePersistentUser());
        rating7 = ratingRepository.save(rating7);
        ratings.add(rating7);

        MovieRating rating8 = new MovieRating();
        rating8.setRating(6);
        rating8.setAge(29);
        rating8.setGender(Gender.FEMALE);
        rating8.setCountryUser(Country.BELARUS);
        rating8.setParentEntity(movie);
        rating8.setUser(generator.generatePersistentUser());
        rating8 = ratingRepository.save(rating8);
        ratings.add(rating8);

        MovieRating rating9 = new MovieRating();
        rating9.setRating(7);
        rating9.setAge(30);
        rating9.setGender(Gender.FEMALE);
        rating9.setCountryUser(Country.FRANCE);
        rating9.setParentEntity(movie);
        rating9.setUser(generator.generatePersistentUser());
        rating9 = ratingRepository.save(rating9);
        ratings.add(rating9);

        MovieRating rating10 = new MovieRating();
        rating10.setRating(8);
        rating10.setAge(40);
        rating10.setGender(Gender.FEMALE);
        rating10.setCountryUser(Country.UNITED_STATES_OF_AMERICA);
        rating10.setParentEntity(movie);
        rating10.setUser(generator.generatePersistentUser());
        rating10 = ratingRepository.save(rating10);
        ratings.add(rating10);

        MovieRating rating11 = new MovieRating();
        rating11.setRating(7);
        rating11.setAge(40);
        rating11.setGender(Gender.MALE);
        rating11.setCountryUser(Country.UNITED_STATES_OF_AMERICA);
        rating11.setParentEntity(movie);
        rating11.setUser(generator.generatePersistentUser());
        rating11 = ratingRepository.save(rating11);
        ratings.add(rating11);

        MovieRating rating12 = new MovieRating();
        rating12.setRating(9);
        rating12.setAge(44);
        rating12.setGender(Gender.FEMALE);
        rating12.setCountryUser(Country.UNITED_STATES_OF_AMERICA);
        rating12.setParentEntity(movie);
        rating12.setUser(generator.generatePersistentUser());
        rating12 = ratingRepository.save(rating12);
        ratings.add(rating12);

        MovieRating rating13 = new MovieRating();
        rating13.setRating(10);
        rating13.setAge(45);
        rating13.setGender(Gender.FEMALE);
        rating13.setCountryUser(Country.UNITED_STATES_OF_AMERICA);
        rating13.setParentEntity(movie);
        rating13.setUser(generator.generatePersistentUser());
        rating13 = ratingRepository.save(rating13);
        ratings.add(rating13);

        MovieRating rating14 = new MovieRating();
        rating14.setRating(10);
        rating14.setAge(45);
        rating14.setGender(Gender.MALE);
        rating14.setCountryUser(Country.UNITED_STATES_OF_AMERICA);
        rating14.setParentEntity(movie);
        rating14.setUser(generator.generatePersistentUser());
        rating14 = ratingRepository.save(rating14);
        ratings.add(rating14);

        MovieRating rating15 = new MovieRating();
        rating15.setRating(10);
        rating15.setAge(15);
        rating15.setGender(Gender.MALE);
        rating15.setCountryUser(Country.UKRAINE);
        rating15.setParentEntity(movieNotFound);
        rating15.setUser(generator.generatePersistentUser());
        rating15 = ratingRepository.save(rating15);

        DetailedRatingReadDTO result;
        DetailedRatingReadDTO expect;
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                expect = createDetaileRating(ratings, Country.UKRAINE);
                result = ratingService.getDetailedRating(movie.getId(), Country.UKRAINE);
            } else {
                expect = createDetaileRating(ratings, null);
                result = ratingService.getDetailedRating(movie.getId(), null);
            }
            Assert.assertEquals(result.getNumberOfAllVotes(), expect.getNumberOfAllVotes());
            Assert.assertEquals(result.getAverageRating(), expect.getAverageRating(), 0.01D);
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToOne(),
                    expect.getNumberOfVotesWhereRatingEqualToOne());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToTwo(),
                    expect.getNumberOfVotesWhereRatingEqualToTwo());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToThree(),
                    expect.getNumberOfVotesWhereRatingEqualToThree());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToFour(),
                    expect.getNumberOfVotesWhereRatingEqualToFour());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToFive(),
                    expect.getNumberOfVotesWhereRatingEqualToFive());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToSix(),
                    expect.getNumberOfVotesWhereRatingEqualToSix());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToSeven(),
                    expect.getNumberOfVotesWhereRatingEqualToSeven());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToEight(),
                    expect.getNumberOfVotesWhereRatingEqualToEight());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToNine(),
                    expect.getNumberOfVotesWhereRatingEqualToNine());
            Assert.assertEquals(result.getNumberOfVotesWhereRatingEqualToTen(),
                    expect.getNumberOfVotesWhereRatingEqualToTen());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeLessThan18(),
                    expect.getNumberOfVotesWhereAgeLessThan18());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween18And29(),
                    expect.getNumberOfVotesWhereAgeBetween18And29());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween30And44(),
                    expect.getNumberOfVotesWhereAgeBetween30And44());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeOver45(), expect.getNumberOfVotesWhereAgeOver45());
            Assert.assertEquals(result.getAverageRatingWhereAgeLessThan18(),
                    expect.getAverageRatingWhereAgeLessThan18(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween18And29(),
                    expect.getAverageRatingWhereAgeBetween18And29(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween30And44(),
                    expect.getAverageRatingWhereAgeBetween30And44(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeOver45(), expect.getAverageRatingWhereAgeOver45(),
                    0.01D);
            Assert.assertEquals(result.getNumberOfVotesWhereAgeLessThan18ForMales(),
                    expect.getNumberOfVotesWhereAgeLessThan18ForMales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween18And29ForMales(),
                    expect.getNumberOfVotesWhereAgeBetween18And29ForMales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween30And44ForMales(),
                    expect.getNumberOfVotesWhereAgeBetween30And44ForMales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeOver45ForMales(),
                    expect.getNumberOfVotesWhereAgeOver45ForMales());
            Assert.assertEquals(result.getAverageRatingWhereAgeLessThan18ForMales(),
                    expect.getAverageRatingWhereAgeLessThan18ForMales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween18And29ForMales(),
                    expect.getAverageRatingWhereAgeBetween18And29ForMales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween30And44ForMales(),
                    expect.getAverageRatingWhereAgeBetween30And44ForMales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeOver45ForMales(),
                    expect.getAverageRatingWhereAgeOver45ForMales(), 0.01D);
            Assert.assertEquals(result.getNumberOfVotesWhereAgeLessThan18ForFemales(),
                    expect.getNumberOfVotesWhereAgeLessThan18ForFemales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween18And29ForFemales(),
                    expect.getNumberOfVotesWhereAgeBetween18And29ForFemales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeBetween30And44ForFemales(),
                    expect.getNumberOfVotesWhereAgeBetween30And44ForFemales());
            Assert.assertEquals(result.getNumberOfVotesWhereAgeOver45ForFemales(),
                    expect.getNumberOfVotesWhereAgeOver45ForFemales());
            Assert.assertEquals(result.getAverageRatingWhereAgeLessThan18ForFemales(),
                    expect.getAverageRatingWhereAgeLessThan18ForFemales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween18And29ForFemales(),
                    expect.getAverageRatingWhereAgeBetween18And29ForFemales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeBetween30And44ForFemales(),
                    expect.getAverageRatingWhereAgeBetween30And44ForFemales(), 0.01D);
            Assert.assertEquals(result.getAverageRatingWhereAgeOver45ForFemales(),
                    expect.getAverageRatingWhereAgeOver45ForFemales(), 0.01D);
            Assert.assertEquals(result.getNumberOfVotesAccordingToCountry(),
                    expect.getNumberOfVotesAccordingToCountry());
            Assert.assertEquals(result.getAverageRatingAccordingToCountry(),
                    expect.getAverageRatingAccordingToCountry(), 0.01D);
            Assert.assertEquals(result.getNumberOfVotesExceptCountry(), expect.getNumberOfVotesExceptCountry());
            Assert.assertEquals(result.getAverageRatingExceptCountry(), expect.getAverageRatingExceptCountry(), 0.01D);
        }
    }

    @Test
    public void testCreateRating() {

        Movie movie = generator.generateFlatEntityWithoutId(Movie.class);
        movie.getProductionCountries().add(Country.UKRAINE);
        movie.getGenres().add(Genre.ACTION);
        movie.setStatus(MovieStatus.RELEASED);
        movie = movieRepository.save(movie);
        
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);

        RatingReadDTO read = ratingService.createRating(movie.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getId() != null);
        Assert.assertTrue(read.getAge() == Period.between(user.getBirthday(), LocalDate.now()).getYears());
        Assert.assertEquals(read.getCountryUser(), user.getCountry());
        Assert.assertEquals(read.getGender(), user.getGender());

        MovieRating rating = ratingRepository.findByParentIdAndId(movie.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(user.getId().equals(read.getUserId()));

        Assert.assertTrue(userRepository.existsByRatingIdAndEmail(rating.getId(), user.getEmail()));
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateRatingWrongRatingAlreadyExists() {

        Movie movie = generator.generateFlatEntityWithoutId(Movie.class);
        movie.getProductionCountries().add(Country.UKRAINE);
        movie.getGenres().add(Genre.ACTION);
        movie.setStatus(MovieStatus.RELEASED);
        movie = movieRepository.save(movie);
        
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        RatingCreateDTO firstCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(movie.getId(), firstCreate, userDetails);

        RatingCreateDTO secondCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(movie.getId(), secondCreate, userDetails);
    }
    
    @Test(expected = InappropriateEntityException.class)
    public void testCreateRatingInappropriateEntity() {

        Movie movie = generator.generateFlatEntityWithoutId(Movie.class);
        movie.getProductionCountries().add(Country.UKRAINE);
        movie.getGenres().add(Genre.ACTION);
        movie.setStatus(MovieStatus.NOT_RELEASED);
        movie = movieRepository.save(movie);
        
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(movie.getId(), create, userDetails);
    }

    @Test
    public void testPatchRating() {

        MovieRating rating = generator.generatePersistentMovieRating();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(), patch);
        Assert.assertEquals(read.getRating(), patch.getRating());

        MovieRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testPatchRatingEmptyPatch() {

        MovieRating rating = generator.generatePersistentMovieRating();

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(),
                new RatingPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));

        MovieRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testUpdateRating() {

        MovieRating rating = generator.generatePersistentMovieRating();
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        RatingReadDTO read = ratingService.updateRating(rating.getParentEntity().getId(), rating.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        MovieRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateRatingNotFoundRating() {
        ratingService.updateRating(UUID.randomUUID(), UUID.randomUUID(), new RatingPutDTO());
    }

    @Test
    public void testDeleteRating() {

        MovieRating rating = generator.generatePersistentMovieRating();
        Assert.assertTrue(ratingRepository.existsById(rating.getId()));

        ratingService.deleteRating(rating.getParentEntity().getId(), rating.getId());
        Assert.assertFalse(ratingRepository.existsById(rating.getId()));
    }

    private DetailedRatingReadDTO createDetaileRating(List<MovieRating> ratings, Country country) {
        if (country == null) {
            country = Country.UNITED_STATES_OF_AMERICA;
        }
        DetailedRatingReadDTO det = new DetailedRatingReadDTO();
        det.setNumberOfAllVotes((long) ratings.size());
        det.setAverageRating(0.0);
        det.setNumberOfVotesWhereRatingEqualToOne(0L);
        det.setNumberOfVotesWhereRatingEqualToTwo(0L);
        det.setNumberOfVotesWhereRatingEqualToThree(0L);
        det.setNumberOfVotesWhereRatingEqualToFour(0L);
        det.setNumberOfVotesWhereRatingEqualToFive(0L);
        det.setNumberOfVotesWhereRatingEqualToSix(0L);
        det.setNumberOfVotesWhereRatingEqualToSeven(0L);
        det.setNumberOfVotesWhereRatingEqualToEight(0L);
        det.setNumberOfVotesWhereRatingEqualToNine(0L);
        det.setNumberOfVotesWhereRatingEqualToTen(0L);
        det.setNumberOfVotesWhereAgeLessThan18(0L);
        det.setNumberOfVotesWhereAgeBetween18And29(0L);
        det.setNumberOfVotesWhereAgeBetween30And44(0L);
        det.setNumberOfVotesWhereAgeOver45(0L);
        det.setAverageRatingWhereAgeLessThan18(0.0);
        det.setAverageRatingWhereAgeBetween18And29(0.0);
        det.setAverageRatingWhereAgeBetween30And44(0.0);
        det.setAverageRatingWhereAgeOver45(0.0);
        det.setNumberOfVotesWhereAgeLessThan18ForMales(0L);
        det.setNumberOfVotesWhereAgeBetween18And29ForMales(0L);
        det.setNumberOfVotesWhereAgeBetween30And44ForMales(0L);
        det.setNumberOfVotesWhereAgeOver45ForMales(0L);
        det.setAverageRatingWhereAgeLessThan18ForMales(0.0);
        det.setAverageRatingWhereAgeBetween18And29ForMales(0.0);
        det.setAverageRatingWhereAgeBetween30And44ForMales(0.0);
        det.setAverageRatingWhereAgeOver45ForMales(0.0);
        det.setNumberOfVotesWhereAgeLessThan18ForFemales(0L);
        det.setNumberOfVotesWhereAgeBetween18And29ForFemales(0L);
        det.setNumberOfVotesWhereAgeBetween30And44ForFemales(0L);
        det.setNumberOfVotesWhereAgeOver45ForFemales(0L);
        det.setAverageRatingWhereAgeLessThan18ForFemales(0.0);
        det.setAverageRatingWhereAgeBetween18And29ForFemales(0.0);
        det.setAverageRatingWhereAgeBetween30And44ForFemales(0.0);
        det.setAverageRatingWhereAgeOver45ForFemales(0.0);
        det.setNumberOfVotesAccordingToCountry(0L);
        det.setAverageRatingAccordingToCountry(0.0);
        det.setNumberOfVotesExceptCountry(0L);
        det.setAverageRatingExceptCountry(0.0);

        for (MovieRating rating : ratings) {
            det.setAverageRating(det.getAverageRating() + rating.getRating());
            if (rating.getRating() == 1) {
                det.setNumberOfVotesWhereRatingEqualToOne(det.getNumberOfVotesWhereRatingEqualToOne() + 1L);
            }
            if (rating.getRating() == 2) {
                det.setNumberOfVotesWhereRatingEqualToTwo(det.getNumberOfVotesWhereRatingEqualToTwo() + 1L);
            }
            if (rating.getRating() == 3) {
                det.setNumberOfVotesWhereRatingEqualToThree(det.getNumberOfVotesWhereRatingEqualToThree() + 1L);
            }
            if (rating.getRating() == 4) {
                det.setNumberOfVotesWhereRatingEqualToFour(det.getNumberOfVotesWhereRatingEqualToFour() + 1L);
            }
            if (rating.getRating() == 5) {
                det.setNumberOfVotesWhereRatingEqualToFive(det.getNumberOfVotesWhereRatingEqualToFive() + 1L);
            }
            if (rating.getRating() == 6) {
                det.setNumberOfVotesWhereRatingEqualToSix(det.getNumberOfVotesWhereRatingEqualToSix() + 1L);
            }
            if (rating.getRating() == 7) {
                det.setNumberOfVotesWhereRatingEqualToSeven(det.getNumberOfVotesWhereRatingEqualToSeven() + 1L);
            }
            if (rating.getRating() == 8) {
                det.setNumberOfVotesWhereRatingEqualToEight(det.getNumberOfVotesWhereRatingEqualToEight() + 1L);
            }
            if (rating.getRating() == 9) {
                det.setNumberOfVotesWhereRatingEqualToNine(det.getNumberOfVotesWhereRatingEqualToNine() + 1L);
            }
            if (rating.getRating() == 10) {
                det.setNumberOfVotesWhereRatingEqualToTen(det.getNumberOfVotesWhereRatingEqualToTen() + 1L);
            }
            if (rating.getAge() < 18) {
                det.setNumberOfVotesWhereAgeLessThan18(det.getNumberOfVotesWhereAgeLessThan18() + 1L);
                det.setAverageRatingWhereAgeLessThan18(det.getAverageRatingWhereAgeLessThan18() + rating.getRating());
            }
            if (rating.getAge() >= 18 && rating.getAge() < 30) {
                det.setNumberOfVotesWhereAgeBetween18And29(det.getNumberOfVotesWhereAgeBetween18And29() + 1L);
                det.setAverageRatingWhereAgeBetween18And29(
                        det.getAverageRatingWhereAgeBetween18And29() + rating.getRating());
            }
            if (rating.getAge() >= 30 && rating.getAge() < 45) {
                det.setNumberOfVotesWhereAgeBetween30And44(det.getNumberOfVotesWhereAgeBetween30And44() + 1L);
                det.setAverageRatingWhereAgeBetween30And44(
                        det.getAverageRatingWhereAgeBetween30And44() + rating.getRating());
            }
            if (rating.getAge() >= 45) {
                det.setNumberOfVotesWhereAgeOver45(det.getNumberOfVotesWhereAgeOver45() + 1L);
                det.setAverageRatingWhereAgeOver45(det.getAverageRatingWhereAgeOver45() + rating.getRating());
            }
            if (rating.getAge() < 18 && rating.getGender() == Gender.MALE) {
                det.setNumberOfVotesWhereAgeLessThan18ForMales(det.getNumberOfVotesWhereAgeLessThan18ForMales() + 1L);
                det.setAverageRatingWhereAgeLessThan18ForMales(
                        det.getAverageRatingWhereAgeLessThan18ForMales() + rating.getRating());
            }
            if (rating.getAge() >= 18 && rating.getAge() < 30 && rating.getGender() == Gender.MALE) {
                det.setNumberOfVotesWhereAgeBetween18And29ForMales(
                        det.getNumberOfVotesWhereAgeBetween18And29ForMales() + 1L);
                det.setAverageRatingWhereAgeBetween18And29ForMales(
                        det.getAverageRatingWhereAgeBetween18And29ForMales() + rating.getRating());
            }
            if (rating.getAge() >= 30 && rating.getAge() < 45 && rating.getGender() == Gender.MALE) {
                det.setNumberOfVotesWhereAgeBetween30And44ForMales(
                        det.getNumberOfVotesWhereAgeBetween30And44ForMales() + 1L);
                det.setAverageRatingWhereAgeBetween30And44ForMales(
                        det.getAverageRatingWhereAgeBetween30And44ForMales() + rating.getRating());
            }
            if (rating.getAge() >= 45 && rating.getGender() == Gender.MALE) {
                det.setNumberOfVotesWhereAgeOver45ForMales(det.getNumberOfVotesWhereAgeOver45ForMales() + 1L);
                det.setAverageRatingWhereAgeOver45ForMales(
                        det.getAverageRatingWhereAgeOver45ForMales() + rating.getRating());
            }
            if (rating.getAge() < 18 && rating.getGender() == Gender.FEMALE) {
                det.setNumberOfVotesWhereAgeLessThan18ForFemales(
                        det.getNumberOfVotesWhereAgeLessThan18ForFemales() + 1L);
                det.setAverageRatingWhereAgeLessThan18ForFemales(
                        det.getAverageRatingWhereAgeLessThan18ForFemales() + rating.getRating());
            }
            if (rating.getAge() >= 18 && rating.getAge() < 30 && rating.getGender() == Gender.FEMALE) {
                det.setNumberOfVotesWhereAgeBetween18And29ForFemales(
                        det.getNumberOfVotesWhereAgeBetween18And29ForFemales() + 1L);
                det.setAverageRatingWhereAgeBetween18And29ForFemales(
                        det.getAverageRatingWhereAgeBetween18And29ForFemales() + rating.getRating());
            }
            if (rating.getAge() >= 30 && rating.getAge() < 45 && rating.getGender() == Gender.FEMALE) {
                det.setNumberOfVotesWhereAgeBetween30And44ForFemales(
                        det.getNumberOfVotesWhereAgeBetween30And44ForFemales() + 1L);
                det.setAverageRatingWhereAgeBetween30And44ForFemales(
                        det.getAverageRatingWhereAgeBetween30And44ForFemales() + rating.getRating());
            }
            if (rating.getAge() >= 45 && rating.getGender() == Gender.FEMALE) {
                det.setNumberOfVotesWhereAgeOver45ForFemales(det.getNumberOfVotesWhereAgeOver45ForFemales() + 1L);
                det.setAverageRatingWhereAgeOver45ForFemales(
                        det.getAverageRatingWhereAgeOver45ForFemales() + rating.getRating());
            }
            if (rating.getCountryUser() == country) {
                det.setNumberOfVotesAccordingToCountry(det.getNumberOfVotesAccordingToCountry() + 1L);
                det.setAverageRatingAccordingToCountry(det.getAverageRatingAccordingToCountry() + rating.getRating());
            }
            if (rating.getCountryUser() != country) {
                det.setNumberOfVotesExceptCountry(det.getNumberOfVotesExceptCountry() + 1L);
                det.setAverageRatingExceptCountry(det.getAverageRatingExceptCountry() + rating.getRating());
            }
        }
        det.setAverageRating(det.getAverageRating() / det.getNumberOfAllVotes());
        det.setAverageRatingWhereAgeLessThan18(
                det.getAverageRatingWhereAgeLessThan18() / det.getNumberOfVotesWhereAgeLessThan18());
        det.setAverageRatingWhereAgeBetween18And29(
                det.getAverageRatingWhereAgeBetween18And29() / det.getNumberOfVotesWhereAgeBetween18And29());
        det.setAverageRatingWhereAgeBetween30And44(
                det.getAverageRatingWhereAgeBetween30And44() / det.getNumberOfVotesWhereAgeBetween30And44());
        det.setAverageRatingWhereAgeOver45(det.getAverageRatingWhereAgeOver45() / det.getNumberOfVotesWhereAgeOver45());
        det.setAverageRatingWhereAgeLessThan18ForMales(
                det.getAverageRatingWhereAgeLessThan18ForMales() / det.getNumberOfVotesWhereAgeLessThan18ForMales());
        det.setAverageRatingWhereAgeBetween18And29ForMales(det.getAverageRatingWhereAgeBetween18And29ForMales()
                / det.getNumberOfVotesWhereAgeBetween18And29ForMales());
        det.setAverageRatingWhereAgeBetween30And44ForMales(det.getAverageRatingWhereAgeBetween30And44ForMales()
                / det.getNumberOfVotesWhereAgeBetween30And44ForMales());
        det.setAverageRatingWhereAgeOver45ForMales(
                det.getAverageRatingWhereAgeOver45ForMales() / det.getNumberOfVotesWhereAgeOver45ForMales());
        det.setAverageRatingWhereAgeLessThan18ForFemales(det.getAverageRatingWhereAgeLessThan18ForFemales()
                / det.getNumberOfVotesWhereAgeLessThan18ForFemales());
        det.setAverageRatingWhereAgeBetween18And29ForFemales(det.getAverageRatingWhereAgeBetween18And29ForFemales()
                / det.getNumberOfVotesWhereAgeBetween18And29ForFemales());
        det.setAverageRatingWhereAgeBetween30And44ForFemales(det.getAverageRatingWhereAgeBetween30And44ForFemales()
                / det.getNumberOfVotesWhereAgeBetween30And44ForFemales());
        det.setAverageRatingWhereAgeOver45ForFemales(
                det.getAverageRatingWhereAgeOver45ForFemales() / det.getNumberOfVotesWhereAgeOver45ForFemales());
        det.setAverageRatingAccordingToCountry(
                det.getAverageRatingAccordingToCountry() / det.getNumberOfVotesAccordingToCountry());
        det.setAverageRatingExceptCountry(det.getAverageRatingExceptCountry() / det.getNumberOfVotesExceptCountry());
        return det;
    }

}