package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class NewsRatingServiceTest extends BaseTest {

    @Autowired
    private NewsRatingService ratingService;

    @Autowired
    private NewsRatingRepository ratingRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Test
    public void testGetRating() {
        NewsRating rating = generator.generatePersistentNewsRating();
        RatingReadDTO read = ratingService.getRating(rating.getParentEntity().getId(), rating.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testGetSimpleRating() {

        News news = generator.generatePersistentNews();
        News newsNotFound = generator.generatePersistentNews();

        NewsRating rating1 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating1.setRating(1);
        rating1.setParentEntity(news);
        rating1.setUser(generator.generatePersistentUser());
        rating1 = ratingRepository.save(rating1);

        NewsRating rating2 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating2.setRating(1);
        rating2.setParentEntity(news);
        rating2.setUser(generator.generatePersistentUser());
        rating2 = ratingRepository.save(rating2);

        NewsRating rating3 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating3.setRating(-1);
        rating3.setParentEntity(news);
        rating3.setUser(generator.generatePersistentUser());
        rating3 = ratingRepository.save(rating3);

        NewsRating rating4 = generator.generateFlatEntityWithoutId(NewsRating.class);
        rating4.setParentEntity(newsNotFound);
        rating4.setUser(generator.generatePersistentUser());
        rating4 = ratingRepository.save(rating2);

        SimpleRatingReadDTO simpleRating = ratingService.getSimpleRating(news.getId());
        Assert.assertTrue(simpleRating.getLike() == 2L);
        Assert.assertTrue(simpleRating.getDislike() == 1L);
    }

    @Test
    public void testCreateRating() {

        News news = generator.generatePersistentNews();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);

        RatingReadDTO read = ratingService.createRating(news.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getId() != null);
        Assert.assertTrue(read.getAge() == Period.between(user.getBirthday(), LocalDate.now()).getYears());
        Assert.assertEquals(read.getCountryUser(), user.getCountry());
        Assert.assertEquals(read.getGender(), user.getGender());

        NewsRating rating = ratingRepository.findByParentIdAndId(news.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(user.getId().equals(read.getUserId()));

        Assert.assertTrue(userRepository.existsByRatingIdAndEmail(rating.getId(), user.getEmail()));
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateRatingWrongRatingAlreadyExists() {

        News news = generator.generatePersistentNews();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        RatingCreateDTO firstCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(news.getId(), firstCreate, userDetails);

        RatingCreateDTO secondCreate = generator.generateObject(RatingCreateDTO.class);
        ratingService.createRating(news.getId(), secondCreate, userDetails);
    }

    @Test
    public void testPatchRating() {

        NewsRating rating = generator.generatePersistentNewsRating();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(), patch);
        Assert.assertEquals(read.getRating(), patch.getRating());

        NewsRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testPatchRatingEmptyPatch() {

        NewsRating rating = generator.generatePersistentNewsRating();

        RatingReadDTO read = ratingService.patchRating(rating.getParentEntity().getId(), rating.getId(),
                new RatingPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(rating, "userId");
        Assert.assertTrue(rating.getUser().getId().equals(read.getUserId()));

        NewsRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test
    public void testUpdateRating() {

        NewsRating rating = generator.generatePersistentNewsRating();
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        RatingReadDTO read = ratingService.updateRating(rating.getParentEntity().getId(), rating.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        NewsRating resultRating = ratingRepository.findByParentIdAndId(rating.getParentEntity().getId(), read.getId())
                .get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultRating, "userId");
        Assert.assertTrue(resultRating.getUser().getId().equals(read.getUserId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateRatingNotFoundRating() {
        ratingService.updateRating(UUID.randomUUID(), UUID.randomUUID(), new RatingPutDTO());
    }

    @Test
    public void testDeleteRating() {

        NewsRating rating = generator.generatePersistentNewsRating();
        Assert.assertTrue(ratingRepository.existsById(rating.getId()));

        ratingService.deleteRating(rating.getParentEntity().getId(), rating.getId());
        Assert.assertFalse(ratingRepository.existsById(rating.getId()));
    }

}