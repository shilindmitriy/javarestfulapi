package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.EntityType;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationCreateDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPutDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.MessageToAdministrationRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class MessageToAdministrationServiceTest extends BaseTest {

    @Autowired
    private MessageToAdministrationService messageService;

    @Autowired
    private MessageToAdministrationRepository messageRepository;

    @Test
    public void testGeMessage() {
        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        MessageToAdministrationReadDTO read = messageService.getMessage(message.getId());
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGeMessageWrongIs() {
        messageService.getMessage(UUID.randomUUID());
    }

    @Test
    public void testGeMessagesByAdministration() {

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setAdministration(Administration.CONTENT_MANAGER);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setAdministration(Administration.CONTENT_MANAGER);
        message2 = messageRepository.save(message2);

        MessageToAdministration message3 = generator.generatePersistentMessageToAdministration();
        message3.setAdministration(Administration.MODERATOR);
        message3 = messageRepository.save(message3);

        MessageToAdministrationFilter filter = new MessageToAdministrationFilter();
        filter.setAdministration(Administration.CONTENT_MANAGER);

        Assertions.assertThat(messageService.getMessages(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());
    }

    @Test
    public void testGeMessagesBySolved() {

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        MessageToAdministration message3 = generator.generatePersistentMessageToAdministration();
        message3.setSolved(true);
        message3 = messageRepository.save(message3);

        MessageToAdministrationFilter filter = new MessageToAdministrationFilter();
        filter.setSolved(false);

        Assertions.assertThat(messageService.getMessages(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());
    }

    @Test
    public void testGeMessagesPagingAndSorting() {

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        MessageToAdministration message3 = generator.generatePersistentMessageToAdministration();

        MessageToAdministrationFilter filter = new MessageToAdministrationFilter();
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "createdAt"));

        Assertions.assertThat(messageService.getMessages(filter, pageRequest).getData()).extracting("id")
                .containsExactly(message1.getId(), message2.getId(), message3.getId());
    }

    @Test
    public void testGeMessagesFull() {

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setSolved(false);
        message1.setAdministration(Administration.CONTENT_MANAGER);
        message1.setEntityType(EntityType.CREATIVE_PERSON);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setSolved(false);
        message2.setAdministration(Administration.CONTENT_MANAGER);
        message2.setEntityType(EntityType.CREATIVE_PERSON);
        message2 = messageRepository.save(message2);

        MessageToAdministration message3 = generator.generatePersistentMessageToAdministration();
        message3.setSolved(true);
        message3.setAdministration(Administration.MODERATOR);
        message3.setEntityType(EntityType.MOVIE);
        message3 = messageRepository.save(message3);

        MessageToAdministrationFilter filter = new MessageToAdministrationFilter();
        filter.setSolved(false);
        filter.setAdministration(Administration.CONTENT_MANAGER);
        filter.setEntityType(EntityType.CREATIVE_PERSON);

        Assertions.assertThat(messageService.getMessages(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());
    }

    @Test
    public void testCreateMessage() {

        MessageToAdministrationCreateDTO create = generator.generateObject(MessageToAdministrationCreateDTO.class);
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministrationReadDTO read = messageService.createMessage(create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MessageToAdministration message = messageRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);
    }

    @Test
    public void testPatchMessage() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        MessageToAdministrationPatchDTO patch = generator.generateObject(MessageToAdministrationPatchDTO.class);

        MessageToAdministrationReadDTO read = messageService.patchMessage(message.getId(), patch, userDetails);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        message = messageRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);
    }

    @Test
    public void testPatchMessageEmptyPatch() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        user.setId(message.getLastModifiedById());
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministrationReadDTO read = messageService.patchMessage(message.getId(),
                new MessageToAdministrationPatchDTO(), userDetails);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);

        message = messageRepository.findById(read.getId()).get();
        Assertions.assertThat(message).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);
    }

    @Test
    public void testUpdateMessage() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        MessageToAdministrationPutDTO put = generator.generateObject(MessageToAdministrationPutDTO.class);

        MessageToAdministrationReadDTO read = messageService.updateMessage(message.getId(), put, userDetails);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        message = messageRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(message);
    }

    @Test
    public void testDeleteMessage() {

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        Assert.assertTrue(messageRepository.existsById(message.getId()));

        messageService.deleteMessage(message.getId());
        Assert.assertFalse(messageRepository.existsById(message.getId()));
    }

}
