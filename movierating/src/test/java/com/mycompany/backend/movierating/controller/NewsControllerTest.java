package com.mycompany.backend.movierating.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.news.DateFilter;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsPatchDTO;
import com.mycompany.backend.movierating.dto.news.NewsPutDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.NewsService;

@WebMvcTest(controllers = NewsController.class)
public class NewsControllerTest extends BaseControllerTest {

    @MockBean
    private NewsService newsService;

    @Test
    public void testGetNews() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);

        Mockito.when(newsService.getNews(expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", expectedRead.getId())).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO resultRead = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(newsService, Mockito.times(1)).getNews(expectedRead.getId());
    }

    @Test
    public void testGetNewsByAuthor() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);
        List<NewsReadDTO> expectedReads = new ArrayList<>();
        expectedReads.add(expectedRead);

        Mockito.when(newsService.getNewsByAuthor(expectedRead.getAuthorId(), new DateFilter()))
                .thenReturn(expectedReads);

        String resultJson = mvc.perform(get("/api/v1/news/list/{author}", expectedRead.getAuthorId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<NewsReadDTO> resultReads = objectMapper.readValue(resultJson, new TypeReference<List<NewsReadDTO>>() {
        });
        Assert.assertEquals(expectedReads, resultReads);

        Mockito.verify(newsService, Mockito.times(1)).getNewsByAuthor(expectedRead.getAuthorId(), new DateFilter());
    }

    @Test
    public void testGetNewsByAuthorWithDate() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);
        List<NewsReadDTO> expectedReads = new ArrayList<>();
        expectedReads.add(expectedRead);

        DateFilter dateFilter = new DateFilter();
        dateFilter.setDateFrom(Instant.parse("2007-12-03T10:15:30.00Z"));
        dateFilter.setDateTo(Instant.parse("2025-12-03T10:15:30.00Z"));

        Mockito.when(newsService.getNewsByAuthor(expectedRead.getAuthorId(), dateFilter)).thenReturn(expectedReads);

        String resultJson = mvc
                .perform(get("/api/v1/news/list/{author}", expectedRead.getAuthorId())
                        .param("dateFrom", dateFilter.getDateFrom().toString())
                        .param("dateTo", dateFilter.getDateTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<NewsReadDTO> resultReads = objectMapper.readValue(resultJson, new TypeReference<List<NewsReadDTO>>() {
        });
        Assert.assertEquals(expectedReads, resultReads);

        Mockito.verify(newsService, Mockito.times(1)).getNewsByAuthor(expectedRead.getAuthorId(), dateFilter);
    }

    @Test
    public void testCreateNews() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);
        NewsCreateDTO create = generator.generateObject(NewsCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(newsService.createNews(create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/news").with(user(userDetails)).content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        NewsReadDTO resultRead = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualToComparingFieldByField(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(newsService, Mockito.times(1)).createNews(create, userDetails);
    }

    @Test
    @WithMockUser
    public void testCreateNewsValidationFailed() throws Exception {

        NewsCreateDTO create = new NewsCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/news").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(newsService, Mockito.never()).createNews(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchNews() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);
        NewsPatchDTO patch = generator.generateObject(NewsPatchDTO.class);

        Mockito.when(newsService.patchNews(expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/news/{id}", expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        NewsReadDTO resultRead = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualToComparingFieldByField(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(newsService, Mockito.times(1)).patchNews(expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchNewsValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < 1001; i++) {
            text.append("1234567890");
        }

        NewsPatchDTO patch = new NewsPatchDTO();
        patch.setText(text.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/news/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(newsService, Mockito.never()).patchNews(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateNews() throws Exception {

        NewsReadDTO expectedRead = generator.generateObject(NewsReadDTO.class);
        NewsPutDTO put = generator.generateObject(NewsPutDTO.class);

        Mockito.when(newsService.updateNews(expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/news/{id}", expectedRead.getId()).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        NewsReadDTO resultRead = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualToComparingFieldByField(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(newsService, Mockito.times(1)).updateNews(expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testUpdateNewsValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        NewsPutDTO put = new NewsPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/news/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(newsService, Mockito.never()).updateNews(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteNews() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/news/{id}", id)).andExpect(status().isOk());
        Mockito.verify(newsService).deleteNews(id);
    }
}
