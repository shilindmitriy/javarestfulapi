package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.service.BaseTest;

public class ProductionCompanyRepositoryTest extends BaseTest {

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        ProductionCompany company = new ProductionCompany();
        companyRepository.save(company);
    }

    @Test
    public void testCreatedAtIsSet() {

        ProductionCompany company = generator.generatePersistentProductionCompany();

        Instant createdAtBeforeReload = company.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        company = companyRepository.findById(company.getId()).get();
        Instant createdAtAfterReload = company.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        ProductionCompany company = generator.generatePersistentProductionCompany();

        Instant createdAtBeforeUpdate = company.getCreatedAt();
        Instant updatedAtBeforeUpdate = company.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        company.setHistory("updated");
        company = companyRepository.save(company);

        Instant createdAtAfterUpdate = company.getCreatedAt();
        Instant updatedAtAfterUpdate = company.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}
