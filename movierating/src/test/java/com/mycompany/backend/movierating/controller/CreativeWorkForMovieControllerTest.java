package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.CreativeWorkService;

@WithMockUser
@WebMvcTest(controllers = CreativeWorkForMovieController.class)
public class CreativeWorkForMovieControllerTest extends BaseControllerTest {

    @MockBean
    private CreativeWorkService workService;

    @Test
    public void testGetCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        UUID movieId = expectedRead.getMovieId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.getCreativeWork(movieId, id)).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assertions.assertThat(expectedRead).isEqualTo(resultRead);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(workService, Mockito.times(1)).getCreativeWork(movieId, expectedRead.getId());
    }

    @Test
    public void testCreateCreativeWork() throws Exception {

        UUID movieId = UUID.randomUUID();
        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        CreativeWorkCreateDTO create = generator.generateObject(CreativeWorkCreateDTO.class);

        Mockito.when(workService.createCreativeWork(movieId, create)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/creative-works", movieId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(workService).createCreativeWork(movieId, create);

    }

    @Test
    public void testCreateCreativeWorkValidationFailed() throws Exception {

        UUID movieId = UUID.randomUUID();
        CreativeWorkCreateDTO create = new CreativeWorkCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/creative-works", movieId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(workService, Mockito.never()).createCreativeWork(ArgumentMatchers.eq(movieId),
                ArgumentMatchers.any());
    }

    @Test
    public void testPatchCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);
        UUID movieId = expectedRead.getMovieId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.patchCreativeWork(movieId, id, patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(workService).patchCreativeWork(movieId, id, patch);
    }

    @Test
    public void testPatchCreativeWorkValidationFailed() throws Exception {

        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        StringBuilder character = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            character.append("1234567890");
        }

        CreativeWorkPatchDTO patch = new CreativeWorkPatchDTO();
        patch.setCharacter(character.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(workService, Mockito.never()).patchCreativeWork(ArgumentMatchers.eq(movieId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    public void testPatchCreativeWorkWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongMovieId = UUID.randomUUID();
        CreativeWorkPatchDTO patch = generator.generateObject(CreativeWorkPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(CreativeWork.class, wrongId, wrongMovieId);

        Mockito.when(workService.patchCreativeWork(wrongMovieId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{movieId}/creative-works/{id}", wrongMovieId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testUpdateCreativeWork() throws Exception {

        CreativeWorkReadDTO expectedRead = generator.generateObject(CreativeWorkReadDTO.class);
        CreativeWorkPutDTO put = generator.generateObject(CreativeWorkPutDTO.class);
        UUID movieId = expectedRead.getPersonId();
        UUID id = expectedRead.getId();

        Mockito.when(workService.updateCreativeWork(movieId, id, put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        CreativeWorkReadDTO resultRead = objectMapper.readValue(resultJson, CreativeWorkReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);

        Mockito.verify(workService).updateCreativeWork(movieId, id, put);
    }

    @Test
    public void testUpdateCreativeWorkValidationFailed() throws Exception {

        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        CreativeWorkPutDTO put = new CreativeWorkPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(workService, Mockito.never()).updateCreativeWork(ArgumentMatchers.eq(movieId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    public void testDeleteCreativeWork() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/movies/{movieId}/creative-works/{id}", movieId, id)).andExpect(status().isOk());
        Mockito.verify(workService).deleteCreativeWork(movieId, id);
    }

}
