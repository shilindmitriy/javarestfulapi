package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.service.UserService;

@WebMvcTest(controllers = AdminController.class)
public class AdminControllerTest extends BaseControllerTest {

    @MockBean
    private UserService userService;

    @Test
    @WithMockUser
    public void testChangeAuthority() throws Exception {

        UUID adminId = UUID.randomUUID();
        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);
        AuthorityPatchDTO patch = generator.generateObject(AuthorityPatchDTO.class);

        Mockito.when(userService.changeAuthority(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/admins/{adminId}/users/{userId}/authority", adminId, read.getId().toString())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assert.assertEquals(read, resultUser);

        Mockito.verify(userService).changeAuthority(read.getId(), patch);
    }

}
