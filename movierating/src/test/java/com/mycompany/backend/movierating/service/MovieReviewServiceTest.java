package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class MovieReviewServiceTest extends BaseTest {

    @Autowired
    private MovieReviewService reviewService;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private MovieReviewRepository reviewRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetReview() {
        MovieReview review = generator.generatePersistentMovieReview();
        ReviewReadDTO read = reviewService.getReview(review.getParentEntity().getId(), review.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewWrongId() {
        reviewService.getReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetReviews() {

        Movie movie = generator.generatePersistentMovie();
        UUID movieId = movie.getId();
        UserAccount user1 = generator.generatePersistentUser();
        UserAccount user2 = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user2);

        MovieReview review1 = generator.generateFlatEntityWithoutId(MovieReview.class);
        review1.setEnabled(true);
        review1.setParentEntity(movie);
        review1.setUser(user1);

        MovieReview review2 = generator.generateFlatEntityWithoutId(MovieReview.class);
        review2.setEnabled(false);
        review2.setParentEntity(movie);
        review2.setUser(user2);

        movie.getMovieReviews().add(review1);
        movie.getMovieReviews().add(review2);
        movie = movieRepository.save(movie);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        inTransaction(() -> {
            Movie resultMovie = movieRepository.findById(movieId).get();
            PageResult<ReviewReadDTO> creviews = reviewService.getReviews(movieId, userDetails, pageable);
            Assert.assertTrue(creviews.getData().size() == resultMovie.getMovieReviews().size());
            MovieReview resultReview1 = resultMovie.getMovieReviews().get(0);
            MovieReview resultReview2 = resultMovie.getMovieReviews().get(1);
            Assertions.assertThat(creviews.getData()).extracting("id").containsExactlyInAnyOrder(resultReview1.getId(),
                    resultReview2.getId());

        });
    }

    @Test
    public void testGetReviewsEmpty() {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        PageResult<ReviewReadDTO> reviews = reviewService.getReviews(UUID.randomUUID(), null, pageable);
        Assert.assertTrue(reviews.getData().isEmpty());
    }

    @Test
    public void testCreateReviewWithSufficientTrust() {

        Movie movie = generator.generatePersistentMovie();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(100);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(movie.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(true, read.getEnabled());
        Assert.assertNotNull(read.getId());

        MovieReview resultReview = reviewRepository.findByParentIdAndId(movie.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(true, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test
    public void testCreateReviewWithoutSufficientTrust() {

        Movie movie = generator.generatePersistentMovie();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(15);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        ReviewReadDTO read = reviewService.createReview(movie.getId(), create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertEquals(false, read.getEnabled());
        Assert.assertNotNull(read.getId());

        MovieReview resultReview = reviewRepository.findByParentIdAndId(movie.getId(), read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(false, resultReview.getEnabled());
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());

        Assert.assertTrue(userRepository.existsByReviewIdAndEmail(resultReview.getId(), user.getEmail()));
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateReviewWithNegativeTrust() {

        Movie movie = generator.generatePersistentMovie();
        UserAccount user = generator.generatePersistentUser();
        user.setModeratorsTrust(-5);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);

        reviewService.createReview(movie.getId(), create, userDetails);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateReviewNotFoundNews() {
        UserAccount user = generator.generateFlatEntityWithoutId(UserAccount.class);
        reviewService.createReview(UUID.randomUUID(), new ReviewCreateDTO(), new UserDetailsImpl(user));
    }

    @Test
    public void testPatchReview() {

        MovieReview review = generator.generatePersistentMovieReview();
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MovieReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testPatchReviewEmptyPatch() {

        MovieReview review = generator.generatePersistentMovieReview();

        ReviewReadDTO read = reviewService.patchReview(review.getParentEntity().getId(), review.getId(),
                new ReviewPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), review.getUser().getId());
        Assert.assertEquals(read.getParentId(), review.getParentEntity().getId());

        MovieReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPatchReviewNotFoundReview() {
        reviewService.patchReview(UUID.randomUUID(), UUID.randomUUID(), new ReviewPatchDTO());
    }

    @Test
    public void testUpdateReview() {

        MovieReview review = generator.generatePersistentMovieReview();
        ReviewPutDTO put = generator.generateObject(ReviewPutDTO.class);

        ReviewReadDTO read = reviewService.updateReview(review.getParentEntity().getId(), review.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MovieReview resultReview = reviewRepository
                .findByParentIdAndId(review.getParentEntity().getId(), review.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultReview, "userId", "parentId");
        Assert.assertEquals(read.getUserId(), resultReview.getUser().getId());
        Assert.assertEquals(read.getParentId(), resultReview.getParentEntity().getId());
    }

    @Test
    public void testDeleteReview() {

        MovieReview review = generator.generatePersistentMovieReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
    }

    @Test
    public void testDeleteReviewWithLike() {

        MovieReview review = generator.generatePersistentMovieReview();
        Assert.assertTrue(reviewRepository.existsById(review.getId()));

        ReviewLike likeForReview = new ReviewLike();
        likeForReview.setLike(true);
        likeForReview.setReviewType(ReviewType.MOVIE_REVIEW);
        likeForReview.setReviewId(review.getId());
        likeForReview.setUserId(UUID.randomUUID());
        likeForReview = reviewLikeRepository.save(likeForReview);

        ReviewLike like = new ReviewLike();
        like.setLike(true);
        like.setReviewType(ReviewType.CREATIVE_PERSON_REVIEW);
        like.setReviewId(UUID.randomUUID());
        like.setUserId(UUID.randomUUID());
        like = reviewLikeRepository.save(like);

        reviewService.deleteReview(review.getParentEntity().getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
        Assert.assertFalse(reviewLikeRepository.existsById(likeForReview.getId()));
        Assert.assertTrue(reviewLikeRepository.existsById(like.getId()));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}