package com.mycompany.backend.movierating.controller;

import java.time.LocalDate;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.service.UserService;

@WebMvcTest(controllers = UserController.class)
public class UserControllerTest extends BaseControllerTest {

    @MockBean
    private UserService userService;

    @Test
    public void testGetUser() throws Exception {

        UserAccountReadDTO expectedUser = generator.generateObject(UserAccountReadDTO.class);

        Mockito.when(userService.getUser(expectedUser.getId())).thenReturn(expectedUser);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", expectedUser.getId())).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assertions.assertThat(resultUser).isEqualToComparingFieldByField(expectedUser);

        Mockito.verify(userService).getUser(expectedUser.getId());
    }

    @Test
    public void testGetUserWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(UserAccount.class, wrongId);
        Mockito.when(userService.getUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongId)).andExpect(status().isNotFound()).andReturn()
                .getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserWrongIsBadRequest() throws Exception {

        String wrongId = "555362Wer";

        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongId)).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(wrongId));
    }

    @Test
    public void testCreateUser() throws Exception {

        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);
        UserAccountCreateDTO create = generator.generateObject(UserAccountCreateDTO.class);

        Mockito.when(userService.createUser(create)).thenReturn(read);

        String resultJson = mvc
                .perform(post("/api/v1/users").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assertions.assertThat(resultUser).isEqualToComparingFieldByField(read);

        Mockito.verify(userService).createUser(create);
    }

    @Test
    public void testCreateUserValidationFailed() throws Exception {

        UserAccountCreateDTO create = new UserAccountCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/users").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(userService, Mockito.never()).createUser(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchUser() throws Exception {

        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);
        UserAccountPatchDTO patch = generator.generateObject(UserAccountPatchDTO.class);

        Mockito.when(userService.patchUser(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/users/{id}", read.getId().toString())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assert.assertEquals(read, resultUser);

        Mockito.verify(userService).patchUser(read.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchUserValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        UserAccountPatchDTO patch = new UserAccountPatchDTO();
        patch.setBirthday(LocalDate.of(2999, 5, 1));

        String resultJson = mvc
                .perform(patch("/api/v1/users/{id}", id).content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(userService, Mockito.never()).patchUser(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateUser() throws Exception {

        UserAccountReadDTO read = generator.generateObject(UserAccountReadDTO.class);
        UserAccountPutDTO put = generator.generateObject(UserAccountPutDTO.class);

        Mockito.when(userService.updateUser(read.getId(), put)).thenReturn(read);

        String resultJson = mvc
                .perform(put("/api/v1/users/{id}", read.getId().toString())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UserAccountReadDTO resultUser = objectMapper.readValue(resultJson, UserAccountReadDTO.class);
        Assert.assertEquals(read, resultUser);

        Mockito.verify(userService).updateUser(read.getId(), put);
    }

    @Test
    @WithMockUser
    public void testUpdateUserValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        UserAccountPutDTO put = new UserAccountPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/users/{id}", id).content(objectMapper.writeValueAsString(put))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(userService, Mockito.never()).updateUser(ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteUser() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/users/{id}", id.toString())).andExpect(status().isOk());
        Mockito.verify(userService).deleteUser(id);
    }
}
