package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationCreateDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPutDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.MessageToAdministrationService;

@WebMvcTest(controllers = MessageToAdministrationController.class)
public class MessageToAdministrationControllerTest extends BaseControllerTest {

    @MockBean
    private MessageToAdministrationService messageService;

    @Test
    @WithMockUser
    public void testGetMessage() throws Exception {

        MessageToAdministrationReadDTO expectedMessage = generator.generateObject(MessageToAdministrationReadDTO.class);

        Mockito.when(messageService.getMessage(expectedMessage.getId())).thenReturn(expectedMessage);

        String resultJson = mvc.perform(get("/api/v1/message-to-administrations/{id}", expectedMessage.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MessageToAdministrationReadDTO resultMessage = objectMapper.readValue(resultJson,
                MessageToAdministrationReadDTO.class);
        Assert.assertEquals(resultMessage, expectedMessage);

        Mockito.verify(messageService).getMessage(expectedMessage.getId());
    }

    @Test
    @WithMockUser
    public void testGetMessages() throws Exception {

        MessageToAdministrationFilter filter = generator.generateObject(MessageToAdministrationFilter.class);

        PageResult<MessageToAdministrationReadDTO> expectedMessages = new PageResult<>();
        expectedMessages.getData().add(generator.generateObject(MessageToAdministrationReadDTO.class));

        Mockito.when(messageService.getMessages(filter, PageRequest.of(0, defaultPageSize)))
                .thenReturn(expectedMessages);

        String resultJson = mvc
                .perform(get("/api/v1/message-to-administrations")
                        .param("administration", filter.getAdministration().name())
                        .param("solved", filter.getSolved().toString())
                        .param("entityType", filter.getEntityType().name()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MessageToAdministrationReadDTO> resultMessages = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<MessageToAdministrationReadDTO>>() {
                });
        Assert.assertEquals(resultMessages, expectedMessages);

        Mockito.verify(messageService).getMessages(filter, PageRequest.of(0, defaultPageSize));
    }

    @Test
    @WithMockUser
    public void testGetMessagesPagingAndSorting() throws Exception {

        MessageToAdministrationFilter filter = generator.generateObject(MessageToAdministrationFilter.class);

        int page = 1;
        int size = 25;

        PageResult<MessageToAdministrationReadDTO> expectedMessages = new PageResult<>();
        expectedMessages.getData().add(generator.generateObject(MessageToAdministrationReadDTO.class));
        expectedMessages.setPage(page);
        expectedMessages.setPageSize(size);
        expectedMessages.setTotalPages(4);
        expectedMessages.setTotalElements(50);

        Mockito.when(messageService.getMessages(filter,
                PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "created_at")))).thenReturn(expectedMessages);

        String resultJson = mvc
                .perform(get("/api/v1/message-to-administrations")
                        .param("administration", filter.getAdministration().name())
                        .param("solved", filter.getSolved().toString())
                        .param("entityType", filter.getEntityType().name()).param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)).param("sort", "created_at,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MessageToAdministrationReadDTO> resultMessages = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<MessageToAdministrationReadDTO>>() {
                });
        Assert.assertEquals(resultMessages, expectedMessages);

        Mockito.verify(messageService).getMessages(filter,
                PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "created_at")));
    }

    @Test
    public void testCreateMessage() throws Exception {

        MessageToAdministrationReadDTO expectedMessage = generator.generateObject(MessageToAdministrationReadDTO.class);
        MessageToAdministrationCreateDTO create = generator.generateObject(MessageToAdministrationCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(messageService.createMessage(create, userDetails)).thenReturn(expectedMessage);

        String resultJson = mvc
                .perform(post("/api/v1/message-to-administrations").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MessageToAdministrationReadDTO resultMessage = objectMapper.readValue(resultJson,
                MessageToAdministrationReadDTO.class);
        Assert.assertEquals(resultMessage, expectedMessage);

        Mockito.verify(messageService).createMessage(create, userDetails);

    }

    @Test
    @WithMockUser
    public void testCreateMessageValidationFailed() throws Exception {

        MessageToAdministrationCreateDTO create = new MessageToAdministrationCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/message-to-administrations").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(messageService, Mockito.never()).createMessage(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testPatchMessage() throws Exception {

        MessageToAdministrationReadDTO expectedMessage = generator.generateObject(MessageToAdministrationReadDTO.class);
        MessageToAdministrationPatchDTO patch = generator.generateObject(MessageToAdministrationPatchDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(messageService.patchMessage(expectedMessage.getId(), patch, userDetails))
                .thenReturn(expectedMessage);

        String resultJson = mvc
                .perform(patch("/api/v1/message-to-administrations/{id}", expectedMessage.getId())
                        .content(objectMapper.writeValueAsString(patch)).with(user(userDetails))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MessageToAdministrationReadDTO resultMessage = objectMapper.readValue(resultJson,
                MessageToAdministrationReadDTO.class);
        Assert.assertEquals(expectedMessage, resultMessage);

        Mockito.verify(messageService).patchMessage(expectedMessage.getId(), patch, userDetails);
    }

    @Test
    @WithMockUser
    public void testPatchMessageValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        StringBuilder note = new StringBuilder();
        for (int i = 0; i < 51; i++) {
            note.append("1234567890");
        }

        MessageToAdministrationPatchDTO patch = new MessageToAdministrationPatchDTO();
        patch.setNote(note.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/message-to-administrations/{id}", id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(messageService, Mockito.never()).patchMessage(ArgumentMatchers.eq(id), ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }

    @Test
    public void testUpdateMessage() throws Exception {

        MessageToAdministrationReadDTO expectedMessage = generator.generateObject(MessageToAdministrationReadDTO.class);
        MessageToAdministrationPutDTO put = generator.generateObject(MessageToAdministrationPutDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(messageService.updateMessage(expectedMessage.getId(), put, userDetails))
                .thenReturn(expectedMessage);

        String resultJson = mvc
                .perform(put("/api/v1/message-to-administrations/{id}", expectedMessage.getId())
                        .content(objectMapper.writeValueAsString(put)).with(user(userDetails))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MessageToAdministrationReadDTO resultMessage = objectMapper.readValue(resultJson,
                MessageToAdministrationReadDTO.class);
        Assert.assertEquals(expectedMessage, resultMessage);

        Mockito.verify(messageService).updateMessage(expectedMessage.getId(), put, userDetails);
    }

    @Test
    @WithMockUser
    public void testUpdateMessageValidationFailed() throws Exception {

        UUID id = UUID.randomUUID();
        MessageToAdministrationPutDTO put = new MessageToAdministrationPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/message-to-administrations/{id}", id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);

        Mockito.verify(messageService, Mockito.never()).updateMessage(ArgumentMatchers.eq(id), ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testDeleteMessage() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/message-to-administrations/{id}", id)).andExpect(status().isOk());
        Mockito.verify(messageService).deleteMessage(id);
    }

}
