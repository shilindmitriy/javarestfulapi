package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.LocalTime;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.EntityType;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationSolveRequest;
import com.mycompany.backend.movierating.dto.message.constants.Operation;
import com.mycompany.backend.movierating.exception.InvalidRequestException;
import com.mycompany.backend.movierating.exception.MisprintNotFoundException;
import com.mycompany.backend.movierating.repository.MessageToAdministrationRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class MessageProcessingServiceTest extends BaseTest {

    @Autowired
    private MessageProcessingService processingService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MessageToAdministrationRepository messageRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesWrongMessageNotFound() {
        MessageToAdministrationSolveRequest solved = generator.generateObject(MessageToAdministrationSolveRequest.class);
        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesWrongField() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("misprint");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("wrong");
        solved.setOperation(Operation.REPLACE);
        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesFieldIsNull() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("misprint");
        message.setSolved(false);
        message = messageRepository.save(message);

        ProductionCompany company = generator.generatePersistentProductionCompany();
        company.setHistory(null);
        company = companyRepository.save(company);

        MessageToAdministrationSolveRequest solved = new MessageToAdministrationSolveRequest();
        solved.setUrl(message.getUrl());
        solved.setEntityId(company.getId());
        solved.setEntityType(EntityType.PRODUCTION_COMPANY);
        solved.setEntityField("history");
        solved.setCorrection("correction");
        solved.setMisprint(message.getMisprint());
        solved.getMisprints().add(message.getMisprint());
        solved.setBeginIndex(5);
        solved.setOperation(Operation.REPLACE);
        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesReplaceString() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("misprint");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("misprint ");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setTitle("text misprint text misprint ");
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("title");
        solved.setCorrection("correction");
        solved.setBeginIndex(5);
        solved.setOperation(Operation.REPLACE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getTitle().equals("text correction text misprint "));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceStringMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("misprint");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setTitle("text text misprint ");
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("title");
        solved.setCorrection("correction");
        solved.setBeginIndex(5);
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesAppendString() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("misprint");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("misprint ");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setTitle("text misprint text misprint ");
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("title");
        solved.setCorrection("correction_");
        solved.setBeginIndex(5);
        solved.setOperation(Operation.APPEND);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getTitle().equals("text correction_misprint text misprint "));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesAppendStringMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("misprint");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setTitle("text correction_misprint text misprint ");
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("title");
        solved.setCorrection("correction_");
        solved.setBeginIndex(5);
        solved.setOperation(Operation.APPEND);
        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesRemoveString() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("misprint");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("misprint ");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setTitle("text misprint text misprint ");
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("title");
        solved.setBeginIndex(5);
        solved.setOperation(Operation.REMOVE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getTitle().equals("text  text misprint "));
    }

    @Test
    public void testGetProcessedMessagesReplaceEnum() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("NOT_RELEASED");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("NOT_RELEASED");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setStatus(MovieStatus.NOT_RELEASED);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("status");
        solved.setCorrection("RELEASED");
        solved.setOperation(Operation.REPLACE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getStatus().equals(MovieStatus.RELEASED));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceEnumMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("NOT_RELEASED");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setStatus(MovieStatus.RELEASED);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("status");
        solved.setCorrection("RELEASED");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesAppendEnum() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("NOT_RELEASED");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setStatus(MovieStatus.RELEASED);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("status");
        solved.setCorrection("RELEASED");
        solved.setOperation(Operation.APPEND);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesRemoveEnum() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("NOT_RELEASED");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setStatus(MovieStatus.RELEASED);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("status");
        solved.setCorrection("RELEASED");
        solved.setOperation(Operation.REMOVE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesReplaceLocalDate() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("2019-10-03");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("2019-10-03");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setReleaseDate(LocalDate.parse(message1.getMisprint()));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("releaseDate");
        solved.setCorrection("1992-10-03");
        solved.setOperation(Operation.REPLACE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getReleaseDate().equals(LocalDate.parse(solved.getCorrection())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceLocalDateMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("2019-10-03");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setReleaseDate(LocalDate.parse("1992-10-03"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("releaseDate");
        solved.setCorrection("1992-10-03");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesRemoveLocalDate() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("2019-10-03");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("2019-10-03");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setReleaseDate(LocalDate.parse("2019-10-03"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("releaseDate");
        solved.setMisprint("2019-10-03");
        solved.setOperation(Operation.REMOVE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getReleaseDate() == null);
    }

    @Test
    public void testGetProcessedMessagesAppendLocalDate() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("Field is empty");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint(message1.getMisprint());
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setReleaseDate(null);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("releaseDate");
        solved.setCorrection("1992-10-03");
        solved.setOperation(Operation.APPEND);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getReleaseDate().equals(LocalDate.parse(solved.getCorrection())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesAppendLocalDateMisprintNot() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("Field is empty");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("releaseDate");
        solved.setCorrection("1992-10-03");
        solved.setOperation(Operation.APPEND);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesReplaceLocalTime() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("02:02:00");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("02:02:00");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setRunningTime(LocalTime.parse("02:02:00"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("runningTime");
        solved.setCorrection("01:43:00");
        solved.setOperation(Operation.REPLACE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getRunningTime().equals(LocalTime.parse(solved.getCorrection())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceLocalTimeMisprintNot() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("02:02:00");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setRunningTime(LocalTime.parse("01:43:00"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("runningTime");
        solved.setCorrection("01:43:00");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesRemoveLocalTime() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("02:02:00");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint("02:02:00");
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setRunningTime(LocalTime.parse("02:02:00"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("runningTime");
        solved.setOperation(Operation.REMOVE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getRunningTime() == null);
    }

    @Test
    public void testGetProcessedMessagesAppendLocalTime() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("Field is empty");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint(message1.getMisprint());
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.setRunningTime(null);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("runningTime");
        solved.setCorrection("01:43:00");
        solved.setOperation(Operation.APPEND);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getRunningTime().equals(LocalTime.parse(solved.getCorrection())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesAppendLocalTimeMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("Field is empty");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.setRunningTime(LocalTime.parse("02:02:00"));
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("runningTime");
        solved.setCorrection("01:43:00");
        solved.setOperation(Operation.APPEND);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesColectionElementWrongFieldType() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("ACTION");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("movieRatings");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesReplaceColectionElement() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("ACTION");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint(message1.getMisprint());
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.ACTION);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("genres");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.REPLACE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getGenres().contains(Genre.valueOf(solved.getCorrection())));
        Assert.assertFalse(movie.getGenres().contains(Genre.valueOf(solved.getMisprint())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceColectionElementMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("ACTION");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.FANTASY);
        movie.getGenres().remove(Genre.ACTION);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("genres");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesReplaceColectionElementCorrectionFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("ACTION");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.HISTORY);
        movie.getGenres().add(Genre.ACTION);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("genres");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.REPLACE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesRemoveColectionElement() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("ACTION");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint(message1.getMisprint());
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.ACTION);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("genres");
        solved.setOperation(Operation.REMOVE);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertFalse(movie.getGenres().contains(Genre.valueOf(solved.getMisprint())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesRemoveColectionElementMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("ACTION");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.DRAMA);
        movie.getGenres().remove(Genre.ACTION);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("genres");
        solved.setOperation(Operation.REMOVE);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test
    public void testGetProcessedMessagesAppendColectionElement() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message1 = generator.generatePersistentMessageToAdministration();
        message1.setMisprint("Append colection element");
        message1.setSolved(false);
        message1 = messageRepository.save(message1);

        MessageToAdministration message2 = generator.generatePersistentMessageToAdministration();
        message2.setUrl(message1.getUrl());
        message2.setMisprint(message1.getMisprint());
        message2.setSolved(false);
        message2 = messageRepository.save(message2);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.ACTION);
        movie.getGenres().remove(Genre.HISTORY);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message1, message2, movie);
        solved.setEntityField("genres");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.APPEND);

        Assertions.assertThat(processingService.getProcessedMessages(solved, userDetails)).extracting("id")
                .containsExactlyInAnyOrder(message1.getId(), message2.getId());

        movie = movieRepository.findById(movie.getId()).get();
        Assert.assertTrue(movie.getGenres().contains(Genre.valueOf(solved.getCorrection())));
    }

    @Test(expected = MisprintNotFoundException.class)
    public void testGetProcessedMessagesAppendColectionElementMisprintNotFound() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("Append colection element");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();
        movie.getGenres().add(Genre.HISTORY);
        movie = movieRepository.save(movie);

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("genres");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.APPEND);

        processingService.getProcessedMessages(solved, userDetails);
    }

    @Test(expected = InvalidRequestException.class)
    public void testGetProcessedMessagesWrongFieldType() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        MessageToAdministration message = generator.generatePersistentMessageToAdministration();
        message.setMisprint("wrong");
        message.setSolved(false);
        message = messageRepository.save(message);

        Movie movie = generator.generatePersistentMovie();

        MessageToAdministrationSolveRequest solved = getSolved(message, message, movie);
        solved.setEntityField("averageRating");
        solved.setCorrection("HISTORY");
        solved.setOperation(Operation.APPEND);

        processingService.getProcessedMessages(solved, userDetails);
    }

    private MessageToAdministrationSolveRequest getSolved(MessageToAdministration message1, MessageToAdministration message2,
            Movie movie) {
        MessageToAdministrationSolveRequest solved = new MessageToAdministrationSolveRequest();
        solved.setEntityId(movie.getId());
        solved.setUrl(message1.getUrl());
        solved.setEntityType(EntityType.MOVIE);
        solved.setMisprint(message1.getMisprint());
        solved.getMisprints().add(message1.getMisprint());
        solved.getMisprints().add(message2.getMisprint());
        return solved;
    }

}
