package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.NewsRatingService;

@WebMvcTest(controllers = NewsRatingController.class)
public class NewsRatingControllerTest extends BaseControllerTest {

    @MockBean
    private NewsRatingService ratingService;

    @Test
    public void testGetRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        UUID newsId = UUID.randomUUID();

        Mockito.when(ratingService.getRating(newsId, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/news/{newsId}/ratings/{id}", newsId, expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(ratingService, Mockito.times(1)).getRating(newsId, expectedRead.getId());
    }

    @Test
    public void testGetSimpleRating() throws Exception {

        SimpleRatingReadDTO expectedSimpleRating = generator.generateObject(SimpleRatingReadDTO.class);
        UUID newsId = UUID.randomUUID();

        Mockito.when(ratingService.getSimpleRating(newsId)).thenReturn(expectedSimpleRating);

        String resultJson = mvc.perform(get("/api/v1/news/{newsId}/ratings", newsId)).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SimpleRatingReadDTO resultSimpleRating = objectMapper.readValue(resultJson, SimpleRatingReadDTO.class);
        Assert.assertEquals(expectedSimpleRating, resultSimpleRating);
        Mockito.verify(ratingService, Mockito.times(1)).getSimpleRating(newsId);
    }

    @Test
    public void testCreateRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID newsId = UUID.randomUUID();

        Mockito.when(ratingService.createRating(newsId, create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/news/{newsId}/ratings", newsId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).createRating(newsId, create, userDetails);
    }

    @Test
    public void testCreateRatingWrongRatingAlreadyExists() throws Exception {

        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID newsId = UUID.randomUUID();

        AlreadyExistsException exception = new AlreadyExistsException(News.class, newsId,
                userDetails.getId());

        Mockito.when(ratingService.createRating(newsId, create, userDetails)).thenThrow(exception);
        String resultJson = mvc
                .perform(post("/api/v1/news/{newsId}/ratings", newsId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testPatchRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);
        UUID newsId = UUID.randomUUID();

        Mockito.when(ratingService.patchRating(newsId, expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/news/{newsId}/ratings/{id}", newsId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).patchRating(newsId, expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchRatingWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongNewsId = UUID.randomUUID();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(NewsRating.class, wrongId, wrongNewsId);

        Mockito.when(ratingService.patchRating(wrongNewsId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/news/{newsId}/ratings/{id}", wrongNewsId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateRating() throws Exception {

        UUID newsId = UUID.randomUUID();
        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        Mockito.when(ratingService.updateRating(newsId, expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/news/{newsId}/ratings/{id}", newsId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).updateRating(newsId, expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testDeleteRating() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/news/{newsId}/ratings/{id}", newsId, id)).andExpect(status().isOk());
        Mockito.verify(ratingService).deleteRating(newsId, id);
    }

    @Test
    @WithMockUser
    public void testCreateRatingValidationFailed() throws Exception {

        UUID newsId = UUID.randomUUID();
        RatingCreateDTO create = new RatingCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/news/{newsId}/ratings", newsId).content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).createRating(ArgumentMatchers.eq(newsId), ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateRatingValidationFailed() throws Exception {

        UUID newsId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RatingPutDTO put = new RatingPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/news/{newsId}/ratings/{id}", newsId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).updateRating(ArgumentMatchers.eq(newsId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

}
