package com.mycompany.backend.movierating.security;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.service.BaseTest;

public class CurrentUserValidatorTest extends BaseTest {
    
    @Autowired
    private BelongsToCurrentUserValidator currentUserValidator;

    @MockBean
    private AuthenticationResolver authenticationResolver;

    @Test
    public void testReviewBelongsToCurrentUser() {

        MovieReview review = generator.generatePersistentMovieReview();
        UserAccount user = review.getUser();

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertTrue(currentUserValidator.reviewBelongsToCurrentUser(review.getId()));
    }
    
    @Test
    public void testReviewBelongsToDifferentUser() {

        MovieReview review = generator.generatePersistentMovieReview();
        UserAccount user = generator.generatePersistentUser();

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertFalse(currentUserValidator.reviewBelongsToCurrentUser(review.getId()));
    }    
    
    @Test
    public void testRatingBelongsToCurrentUser() {

        NewsRating rating = generator.generatePersistentNewsRating();
        UserAccount user = rating.getUser();

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertTrue(currentUserValidator.ratingBelongsToCurrentUser(rating.getId()));
    }
    
    @Test
    public void testRatingBelongsToDifferentUser() {

        NewsRating rating = generator.generatePersistentNewsRating();
        UserAccount user = generator.generatePersistentUser();

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertFalse(currentUserValidator.ratingBelongsToCurrentUser(rating.getId()));
    }    

}
