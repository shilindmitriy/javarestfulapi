package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.ProductionCompanyRatingService;

@WebMvcTest(controllers = ProductionCompanyRatingController.class)
public class ProductionCompanyRatingControllerTest extends BaseControllerTest {

    @MockBean
    private ProductionCompanyRatingService ratingService;

    @Test
    public void testGetRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        UUID companyId = UUID.randomUUID();

        Mockito.when(ratingService.getRating(companyId, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/production-companies/{companyId}/ratings/{id}", companyId, expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(ratingService, Mockito.times(1)).getRating(companyId, expectedRead.getId());
    }

    @Test
    public void testGetShortRating() throws Exception {

        ShortRatingReadDTO expectedShortRating = generator.generateObject(ShortRatingReadDTO.class);
        UUID companyId = UUID.randomUUID();

        Mockito.when(ratingService.getShortRating(companyId)).thenReturn(expectedShortRating);

        String resultJson = mvc.perform(get("/api/v1/production-companies/{companyId}/ratings", companyId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ShortRatingReadDTO resultShortRating = objectMapper.readValue(resultJson, ShortRatingReadDTO.class);
        Assert.assertEquals(expectedShortRating, resultShortRating);
        Mockito.verify(ratingService, Mockito.times(1)).getShortRating(companyId);
    }

    @Test
    public void testCreateRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID companyId = UUID.randomUUID();

        Mockito.when(ratingService.createRating(companyId, create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/ratings", companyId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).createRating(companyId, create, userDetails);
    }

    @Test
    public void testCreateRatingWrongRatingAlreadyExists() throws Exception {

        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        UUID companyId = UUID.randomUUID();

        AlreadyExistsException exception = new AlreadyExistsException(ProductionCompany.class, companyId,
                userDetails.getId());

        Mockito.when(ratingService.createRating(companyId, create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/ratings", companyId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testPatchRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);
        UUID companyId = UUID.randomUUID();

        Mockito.when(ratingService.patchRating(companyId, expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{companyId}/ratings/{id}", companyId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).patchRating(companyId, expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchRatingWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongCompanyId = UUID.randomUUID();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(ProductionCompanyRating.class, wrongId,
                wrongCompanyId);

        Mockito.when(ratingService.patchRating(wrongCompanyId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{companyId}/ratings/{id}", wrongCompanyId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);
        UUID companyId = UUID.randomUUID();

        Mockito.when(ratingService.updateRating(companyId, expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{companyId}/ratings/{id}", companyId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).updateRating(companyId, expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testDeleteRating() throws Exception {
        UUID companyId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/production-companies/{companyId}/ratings/{id}", companyId, id))
                .andExpect(status().isOk());
        Mockito.verify(ratingService).deleteRating(companyId, id);
    }

    @Test
    @WithMockUser
    public void testCreateRatingValidationFailed() throws Exception {

        UUID companyId = UUID.randomUUID();
        RatingCreateDTO create = new RatingCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/ratings", companyId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).createRating(ArgumentMatchers.eq(companyId),
                ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateRatingValidationFailed() throws Exception {

        UUID companyId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RatingPutDTO put = new RatingPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{companyId}/ratings/{id}", companyId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).updateRating(ArgumentMatchers.eq(companyId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

}
