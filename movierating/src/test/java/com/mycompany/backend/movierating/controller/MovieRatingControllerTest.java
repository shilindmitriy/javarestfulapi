package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.InappropriateEntityException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.MovieRatingService;

@WebMvcTest(controllers = MovieRatingController.class)
public class MovieRatingControllerTest extends BaseControllerTest {

    @MockBean
    private MovieRatingService ratingService;

    @Test
    public void testGetRating() throws Exception {

        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        UUID movieId = UUID.randomUUID();

        Mockito.when(ratingService.getRating(movieId, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/ratings/{id}", movieId, expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(ratingService, Mockito.times(1)).getRating(movieId, expectedRead.getId());
    }

    @Test
    public void testGetDetailedRatingWithCountry() throws Exception {

        UUID movieId = UUID.randomUUID();
        DetailedRatingReadDTO expectedDetailedRating = generator.generateObject(DetailedRatingReadDTO.class);

        Mockito.when(ratingService.getDetailedRating(movieId, Country.AUSTRIA)).thenReturn(expectedDetailedRating);

        String resultJson = mvc
                .perform(get("/api/v1/movies/{movieId}/ratings", movieId).param("country", Country.AUSTRIA.name()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        DetailedRatingReadDTO resultDetailedRating = objectMapper.readValue(resultJson, DetailedRatingReadDTO.class);
        Assert.assertEquals(expectedDetailedRating, resultDetailedRating);
        Mockito.verify(ratingService, Mockito.times(1)).getDetailedRating(movieId, Country.AUSTRIA);
    }

    @Test
    public void testGetDetailedRatingWithoutCountry() throws Exception {

        UUID movieId = UUID.randomUUID();
        DetailedRatingReadDTO expectedDetailedRating = generator.generateObject(DetailedRatingReadDTO.class);

        Mockito.when(ratingService.getDetailedRating(movieId, null)).thenReturn(expectedDetailedRating);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/ratings", movieId)).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        DetailedRatingReadDTO resultDetailedRating = objectMapper.readValue(resultJson, DetailedRatingReadDTO.class);
        Assert.assertEquals(expectedDetailedRating, resultDetailedRating);
        Mockito.verify(ratingService, Mockito.times(1)).getDetailedRating(movieId, null);
    }

    @Test
    public void testCreateRating() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(ratingService.createRating(movieId, create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/ratings", movieId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).createRating(movieId, create, userDetails);
    }

    @Test
    public void testCreateRatingWrongRatingAlreadyExists() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        AlreadyExistsException exception = new AlreadyExistsException(Movie.class, movieId, userDetails.getId());

        Mockito.when(ratingService.createRating(movieId, create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/ratings", movieId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRatingWrongInappropriateEntity() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingCreateDTO create = generator.generateObject(RatingCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        InappropriateEntityException exception = new InappropriateEntityException(
                "Movie with id=" + movieId + " connot be rated. It has a status " + MovieStatus.NOT_RELEASED);

        Mockito.when(ratingService.createRating(movieId, create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/ratings", movieId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testPatchRating() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        Mockito.when(ratingService.patchRating(movieId, expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{newsId}/ratings/{id}", movieId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).patchRating(movieId, expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchRatingWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongMovieId = UUID.randomUUID();
        RatingPatchDTO patch = generator.generateObject(RatingPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(MovieRating.class, wrongId, wrongMovieId);

        Mockito.when(ratingService.patchRating(wrongMovieId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/movies/{newsId}/ratings/{id}", wrongMovieId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateRating() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingReadDTO expectedRead = generator.generateObject(RatingReadDTO.class);
        RatingPutDTO put = generator.generateObject(RatingPutDTO.class);

        Mockito.when(ratingService.updateRating(movieId, expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/movies/{movieId}/ratings/{id}", movieId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        RatingReadDTO resultRead = objectMapper.readValue(resultJson, RatingReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(ratingService, Mockito.times(1)).updateRating(movieId, expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testDeleteRating() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/movies/{movieId}/ratings/{id}", movieId, id)).andExpect(status().isOk());
        Mockito.verify(ratingService).deleteRating(movieId, id);
    }

    @Test
    @WithMockUser
    public void testCreateRatingValidationFailed() throws Exception {

        UUID movieId = UUID.randomUUID();
        RatingCreateDTO create = new RatingCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/movies/{movieId}/ratings", movieId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).createRating(ArgumentMatchers.eq(movieId),
                ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateRatingValidationFailed() throws Exception {

        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        RatingPutDTO put = new RatingPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/movies/{movieId}/ratings/{id}", movieId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingService, Mockito.never()).updateRating(ArgumentMatchers.eq(movieId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }
}