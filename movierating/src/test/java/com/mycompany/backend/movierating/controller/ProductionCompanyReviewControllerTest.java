package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.ProductionCompanyReviewService;

@WebMvcTest(controllers = ProductionCompanyReviewController.class)
public class ProductionCompanyReviewControllerTest extends BaseControllerTest {

    @MockBean
    private ProductionCompanyReviewService reviewService;

    @Test
    public void testGetReview() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewReadDTO expectedRead = generator.generateObject(ReviewReadDTO.class);

        Mockito.when(reviewService.getReview(companyId, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(get("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewReadDTO resultRead = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(reviewService, Mockito.times(1)).getReview(companyId, expectedRead.getId());
    }

    @Test
    public void testGetReviews() throws Exception {

        PageResult<ReviewReadDTO> expectedReviews = new PageResult<>();
        expectedReviews.getData().add(generator.generateObject(ReviewReadDTO.class));
        expectedReviews.setPage(0);
        expectedReviews.setPageSize(10);
        expectedReviews.setTotalPages(1);
        expectedReviews.setTotalElements(1);

        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        UUID companyId = UUID.randomUUID();

        Mockito.when(reviewService.getReviews(companyId, userDetails, pageable)).thenReturn(expectedReviews);

        String resultJson = mvc
                .perform(get("/api/v1/production-companies/{companyId}/reviews", companyId).param("page", "0")
                        .param("size", "10").param("sort", "createdAt,desc").with(user(userDetails)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ReviewReadDTO> resultReviews = objectMapper.readValue(resultJson,
                new TypeReference<PageResult<ReviewReadDTO>>() {
                });

        Assert.assertTrue(resultReviews.getData().size() == expectedReviews.getData().size());
        Assert.assertTrue(resultReviews.getData().containsAll(expectedReviews.getData()));
        Assert.assertTrue(resultReviews.equals(expectedReviews));

        Mockito.verify(reviewService, Mockito.times(1)).getReviews(companyId, userDetails, pageable);
    }

    @Test
    public void testCreateReview() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewReadDTO expectedRead = generator.generateObject(ReviewReadDTO.class);
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(reviewService.createReview(companyId, create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/reviews", companyId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewReadDTO resultRead = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(reviewService, Mockito.times(1)).createReview(companyId, create, userDetails);
    }

    @Test
    public void testCreateReviewWithNegativeTrust() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewCreateDTO create = generator.generateObject(ReviewCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        AccessDeniedException exception = new AccessDeniedException(
                "The user with id=" + userDetails.getId() + " is not allowed to write a review.");

        Mockito.when(reviewService.createReview(companyId, create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/reviews", companyId).with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testPatchReview() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewReadDTO expectedRead = generator.generateObject(ReviewReadDTO.class);
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        Mockito.when(reviewService.patchReview(companyId, expectedRead.getId(), patch)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewReadDTO resultRead = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(reviewService, Mockito.times(1)).patchReview(companyId, expectedRead.getId(), patch);
    }

    @Test
    @WithMockUser
    public void testPatchReviewWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        UUID wrongCompanyId = UUID.randomUUID();
        ReviewPatchDTO patch = generator.generateObject(ReviewPatchDTO.class);

        EntityNotFoundException exception = new EntityNotFoundException(ProductionCompanyReview.class, wrongId,
                wrongCompanyId);

        Mockito.when(reviewService.patchReview(wrongCompanyId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{companyId}/reviews/{id}", wrongCompanyId, wrongId)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testUpdateReview() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewReadDTO expectedRead = generator.generateObject(ReviewReadDTO.class);
        ReviewPutDTO put = generator.generateObject(ReviewPutDTO.class);

        Mockito.when(reviewService.updateReview(companyId, expectedRead.getId(), put)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, expectedRead.getId())
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewReadDTO resultRead = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(resultRead, expectedRead);
        Mockito.verify(reviewService, Mockito.times(1)).updateReview(companyId, expectedRead.getId(), put);
    }

    @Test
    @WithMockUser
    public void testDeleteReview() throws Exception {
        UUID companyId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, id))
                .andExpect(status().isOk());
        Mockito.verify(reviewService).deleteReview(companyId, id);
    }

    @Test
    @WithMockUser
    public void testCreateReviewValidationFailed() throws Exception {

        UUID companyId = UUID.randomUUID();
        ReviewCreateDTO create = new ReviewCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/production-companies/{companyId}/reviews", companyId)
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewService, Mockito.never()).createReview(ArgumentMatchers.eq(companyId),
                ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchReviewValidationFailed() throws Exception {

        UUID companyId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        StringBuilder text = new StringBuilder();
        for (int i = 0; i < 151; i++) {
            text.append("1234567890");
        }

        ReviewPatchDTO patch = new ReviewPatchDTO();
        patch.setText(text.toString());

        String resultJson = mvc
                .perform(patch("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, id)
                        .content(objectMapper.writeValueAsString(patch)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewService, Mockito.never()).patchReview(ArgumentMatchers.eq(companyId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testUpdateReviewValidationFailed() throws Exception {

        UUID companyId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        ReviewPutDTO put = new ReviewPutDTO();

        String resultJson = mvc
                .perform(put("/api/v1/production-companies/{companyId}/reviews/{id}", companyId, id)
                        .content(objectMapper.writeValueAsString(put)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewService, Mockito.never()).updateReview(ArgumentMatchers.eq(companyId),
                ArgumentMatchers.eq(id), ArgumentMatchers.any());
    }

}
