package com.mycompany.backend.movierating.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeCreateDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikePatchDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.handler.ErrorInfo;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.ReviewLikeService;

@WebMvcTest(controllers = ReviewLikeController.class)
public class ReviewLikeControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewLikeService reviewLikeService;

    @Test
    public void testGetReviewLike() throws Exception {

        ReviewLikeReadDTO expectedRead = generator.generateObject(ReviewLikeReadDTO.class);

        Mockito.when(reviewLikeService.getReviewLike(expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/review-likes/{id}", expectedRead.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewLikeReadDTO resultRead = objectMapper.readValue(resultJson, ReviewLikeReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(reviewLikeService).getReviewLike(expectedRead.getId());
    }

    @Test
    public void testGetReviewLikeWrongIsNotFound() throws Exception {

        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(ReviewLike.class, wrongId);

        Mockito.when(reviewLikeService.getReviewLike(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/review-likes/{id}", wrongId)).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetReviewLikes() throws Exception {

        ReviewLikesReadDTO expectedRead = generator.generateObject(ReviewLikesReadDTO.class);
        ReviewType reviewType = ReviewType.CREATIVE_PERSON_REVIEW;
        UUID reviewId = UUID.randomUUID();

        Mockito.when(reviewLikeService.getReviewLikes(reviewType, reviewId)).thenReturn(expectedRead);

        String resultJson = mvc.perform(get("/api/v1/review-likes/{reviewType}/{reviewId}", reviewType, reviewId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewLikesReadDTO resultRead = objectMapper.readValue(resultJson, ReviewLikesReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(reviewLikeService).getReviewLikes(reviewType, reviewId);
    }

    @Test
    public void testCreateReviewLike() throws Exception {

        ReviewLikeReadDTO expectedRead = generator.generateObject(ReviewLikeReadDTO.class);
        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        Mockito.when(reviewLikeService.createReviewLike(create, userDetails)).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(post("/api/v1/review-likes").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewLikeReadDTO resultRead = objectMapper.readValue(resultJson, ReviewLikeReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(reviewLikeService).createReviewLike(create, userDetails);
    }

    @Test
    public void testCreateReviewLikeWrongRatingAlreadyExists() throws Exception {

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        UserDetailsImpl userDetails = new UserDetailsImpl(generator.generateFlatEntityWithoutId(UserAccount.class));

        AlreadyExistsException exception = new AlreadyExistsException(MovieReview.class,
                create.getReviewId(), userDetails.getId());

        Mockito.when(reviewLikeService.createReviewLike(create, userDetails)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/review-likes").with(user(userDetails))
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    @WithMockUser
    public void testCreateReviewLikeValidationFailed() throws Exception {

        ReviewLikeCreateDTO create = new ReviewLikeCreateDTO();

        String resultJson = mvc
                .perform(post("/api/v1/review-likes").content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewLikeService, Mockito.never()).createReviewLike(ArgumentMatchers.eq(create),
                ArgumentMatchers.any());
    }

    @Test
    @WithMockUser
    public void testPatchReviewLike() throws Exception {

        ReviewLikeReadDTO expectedRead = generator.generateObject(ReviewLikeReadDTO.class);
        ReviewLikePatchDTO create = generator.generateObject(ReviewLikePatchDTO.class);

        Mockito.when(reviewLikeService.patchReviewLike(create, expectedRead.getId())).thenReturn(expectedRead);

        String resultJson = mvc
                .perform(patch("/api/v1/review-likes/{id}", expectedRead.getId())
                        .content(objectMapper.writeValueAsString(create)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ReviewLikeReadDTO resultRead = objectMapper.readValue(resultJson, ReviewLikeReadDTO.class);
        Assert.assertEquals(expectedRead, resultRead);

        Mockito.verify(reviewLikeService).patchReviewLike(create, expectedRead.getId());
    }

    @Test
    @WithMockUser
    public void testDeleteReviewLike() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/review-likes/{id}", id)).andExpect(status().isOk());
        Mockito.verify(reviewLikeService).deleteReviewLike(id);
    }

}
