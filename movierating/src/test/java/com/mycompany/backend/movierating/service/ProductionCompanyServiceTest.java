package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyCreateDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPatchDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPutDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;

public class ProductionCompanyServiceTest extends BaseTest {

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private ProductionCompanyService companyService;

    @Test
    public void testGetProductionCompany() {
        ProductionCompany company = generator.generatePersistentProductionCompany();
        ProductionCompanyReadDTO read = companyService.getProductionCompany(company.getId());
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetProductionCompanyWrongIs() {
        companyService.getProductionCompany(UUID.randomUUID());
    }

    @Test
    public void testCreateProductionCompany() {

        ProductionCompanyCreateDTO create = generator.generateObject(ProductionCompanyCreateDTO.class);

        ProductionCompanyReadDTO read = companyService.createProductionCompany(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProductionCompany company = companyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testPatchProductionCompany() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        ProductionCompanyPatchDTO patch = generator.generateObject(ProductionCompanyPatchDTO.class);

        ProductionCompanyReadDTO read = companyService.patchProductionCompany(company.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        company = companyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testPatchProductionCompanyEmptyPatch() {

        ProductionCompany company = generator.generatePersistentProductionCompany();

        ProductionCompanyReadDTO read = companyService.patchProductionCompany(company.getId(),
                new ProductionCompanyPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        company = companyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testUpdateProductionCompany() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        ProductionCompanyPutDTO put = generator.generateObject(ProductionCompanyPutDTO.class);

        ProductionCompanyReadDTO read = companyService.updateProductionCompany(company.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        company = companyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testDeleteProductionCompany() {

        ProductionCompany company = generator.generatePersistentProductionCompany();
        Assert.assertTrue(companyRepository.existsById(company.getId()));

        companyService.deleteProductionCompany(company.getId());
        Assert.assertFalse(companyRepository.existsById(company.getId()));
    }

}
