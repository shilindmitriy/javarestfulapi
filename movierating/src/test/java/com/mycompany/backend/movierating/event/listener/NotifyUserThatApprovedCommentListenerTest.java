package com.mycompany.backend.movierating.event.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mycompany.backend.movierating.event.ReviewEnabledEvent;
import com.mycompany.backend.movierating.service.UserNotificationService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyUserThatApprovedCommentListenerTest {

    @MockBean
    private UserNotificationService userNotificationService;

    @SpyBean
    private NotifyUserThatApprovedReviewListener notifyUserThatApprovedCommentListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEvent() {
        ReviewEnabledEvent event = new ReviewEnabledEvent();
        event.setReviewId(UUID.randomUUID());
        event.setEnabled(true);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserThatApprovedCommentListener, Mockito.timeout(500)).onEvent(event);
        Mockito.verify(userNotificationService, Mockito.timeout(500)).notifyUserReviewIsEnabled(event.getReviewId());
    }

    @Test
    public void testOnEventNotEnabled() {
        ReviewEnabledEvent event = new ReviewEnabledEvent();
        event.setReviewId(UUID.randomUUID());
        event.setEnabled(false);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserThatApprovedCommentListener, Mockito.never()).onEvent(Mockito.any());
        Mockito.verify(userNotificationService, Mockito.never()).notifyUserReviewIsEnabled(Mockito.any());
    }

    @Test
    public void testOnEventAsync() throws InterruptedException {
        ReviewEnabledEvent event = new ReviewEnabledEvent();
        event.setReviewId(UUID.randomUUID());
        event.setEnabled(true);

        List<Integer> checks = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            checks.add(2);
            latch.countDown();
            return null;
        }).when(userNotificationService).notifyUserReviewIsEnabled(event.getReviewId());

        applicationEventPublisher.publishEvent(event);
        checks.add(1);

        latch.await();
        Mockito.verify(notifyUserThatApprovedCommentListener).onEvent(event);
        Mockito.verify(userNotificationService).notifyUserReviewIsEnabled(event.getReviewId());
        Assert.assertEquals(Arrays.asList(1, 2), checks);
    }

}
