package com.mycompany.backend.movierating.repository;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import com.mycompany.backend.movierating.service.BaseTest;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private CreativePersonReviewRepository personReviewRepository;

    @Autowired
    private CreativePersonRatingRepository personRatingRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Autowired
    private CreativeWorkReviewRepository workReviewRepository;

    @Autowired
    private CreativeWorkRatingRepository workRatingRepository;

    @Autowired
    private MessageToAdministrationRepository administrationRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsRatingRepository newsRatingRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private ProductionCompanyReviewRepository companyReviewRepository;

    @Autowired
    private ProductionCompanyRatingRepository companyRatingRepository;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;
    
    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(userRepository.count() > 0);
        Assert.assertTrue(personRepository.count() > 0);
        Assert.assertTrue(personReviewRepository.count() > 0);
        Assert.assertTrue(personRatingRepository.count() > 0);
        Assert.assertTrue(workRepository.count() > 0);
        Assert.assertTrue(workReviewRepository.count() > 0);
        Assert.assertTrue(workRatingRepository.count() > 0);
        Assert.assertTrue(administrationRepository.count() > 0);
        Assert.assertTrue(movieRepository.count() > 0);
        Assert.assertTrue(movieReviewRepository.count() > 0);
        Assert.assertTrue(movieRatingRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(newsRatingRepository.count() > 0);
        Assert.assertTrue(companyRepository.count() > 0);
        Assert.assertTrue(companyReviewRepository.count() > 0);
        Assert.assertTrue(companyRatingRepository.count() > 0);
        Assert.assertTrue(externalSystemImportRepository.count() > 0);
        Assert.assertTrue(reviewLikeRepository.count() > 0);
    }
}
