package com.mycompany.backend.movierating.repository;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.service.BaseTest;

public class MovieRepositoryTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        Movie movie = new Movie();
        movieRepository.save(movie);
    }

    @Test
    public void testCreatedAtIsSet() {

        Movie movie = generator.generatePersistentMovie();

        Instant createdAtBeforeReload = movie.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        movie = movieRepository.findById(movie.getId()).get();
        Instant createdAtAfterReload = movie.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        Movie movie = generator.generatePersistentMovie();

        Instant createdAtBeforeUpdate = movie.getCreatedAt();
        Instant updatedAtBeforeUpdate = movie.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        movie.setStoryline("updated");
        movie = movieRepository.save(movie);

        Instant createdAtAfterUpdate = movie.getCreatedAt();
        Instant updatedAtAfterUpdate = movie.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

    @Test
    public void testGetIdsOfMovies() {
        Set<UUID> expectedIdsOfMovies = new HashSet<>();
        expectedIdsOfMovies.add(generator.generatePersistentMovie().getId());
        expectedIdsOfMovies.add(generator.generatePersistentMovie().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMovies, movieRepository.getIdsOfMovies().collect(Collectors.toSet()));
        });
    }

}