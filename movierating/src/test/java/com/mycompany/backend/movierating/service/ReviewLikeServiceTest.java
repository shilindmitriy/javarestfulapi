package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeCreateDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikePatchDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

public class ReviewLikeServiceTest extends BaseTest {

    @Autowired
    private ReviewLikeService reviewLikeService;

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Test
    public void testGetReviewLike() {
        ReviewLike like = generator.generatePersistentReviewLike();
        ReviewLikeReadDTO read = reviewLikeService.getReviewLike(like.getId());
        Assertions.assertThat(read).isEqualToComparingFieldByField(like);
        Assert.assertEquals(like.getId(), read.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewLikeWrongId() {
        reviewLikeService.getReviewLike(UUID.randomUUID());
    }

    @Test
    public void testGetReviewLikes() {

        UUID creviewId = UUID.randomUUID();
        ReviewType reviewType = ReviewType.MOVIE_REVIEW;

        ReviewLike like1 = new ReviewLike();
        like1.setLike(true);
        like1.setReviewType(reviewType);
        like1.setReviewId(creviewId);
        like1.setUserId(UUID.randomUUID());
        like1 = reviewLikeRepository.save(like1);

        ReviewLike like2 = new ReviewLike();
        like2.setLike(true);
        like2.setReviewType(reviewType);
        like2.setReviewId(creviewId);
        like2.setUserId(UUID.randomUUID());
        like2 = reviewLikeRepository.save(like2);

        ReviewLike like3 = new ReviewLike();
        like3.setLike(false);
        like3.setReviewType(reviewType);
        like3.setReviewId(creviewId);
        like3.setUserId(UUID.randomUUID());
        like3 = reviewLikeRepository.save(like3);

        ReviewLikesReadDTO likes = reviewLikeService.getReviewLikes(reviewType, creviewId);
        Assert.assertTrue(likes.getLike() == 2L);
        Assert.assertTrue(likes.getDislike() == 1L);
    }

    @Test
    public void testCreateReviewLike() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);

        ReviewLikeReadDTO read = reviewLikeService.createReviewLike(create, userDetails);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertTrue(read.getId() != null);

        ReviewLike like = reviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(like);

        Assert.assertTrue(reviewLikeRepository.existsByReviewIdAndUserId(create.getReviewId(), user.getId()));
        Assert.assertTrue(reviewLikeRepository.existsByIdAndUserId(like.getId(), user.getId()));
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateReviewLikeForNewsReviewWrongRatingAlreadyExists() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        create.setReviewType(ReviewType.MOVIE_REVIEW);
        reviewLikeService.createReviewLike(create, userDetails);

        create.setLike(!create.getLike());
        reviewLikeService.createReviewLike(create, userDetails);
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateReviewLikeForMovieReviewWrongRatingAlreadyExists() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        create.setReviewType(ReviewType.MOVIE_REVIEW);
        reviewLikeService.createReviewLike(create, userDetails);

        create.setLike(!create.getLike());
        reviewLikeService.createReviewLike(create, userDetails);
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateReviewLikeForProductionCompanyReviewWrongRatingAlreadyExists() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        create.setReviewType(ReviewType.PRODUCTION_COMPANY_REVIEW);
        reviewLikeService.createReviewLike(create, userDetails);

        create.setLike(!create.getLike());
        reviewLikeService.createReviewLike(create, userDetails);
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateReviewLikeForCreativePersonReviewWrongRatingAlreadyExists() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        create.setReviewType(ReviewType.CREATIVE_PERSON_REVIEW);
        reviewLikeService.createReviewLike(create, userDetails);

        create.setLike(!create.getLike());
        reviewLikeService.createReviewLike(create, userDetails);
    }

    @Test(expected = AlreadyExistsException.class)
    public void testCreateReviewLikeForCreativeWorkReviewWrongRatingAlreadyExists() {

        UserAccount user = generator.generatePersistentUser();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);

        ReviewLikeCreateDTO create = generator.generateObject(ReviewLikeCreateDTO.class);
        create.setReviewType(ReviewType.CREATIVE_WORK_REVIEW);
        reviewLikeService.createReviewLike(create, userDetails);

        create.setLike(!create.getLike());
        reviewLikeService.createReviewLike(create, userDetails);
    }

    @Test
    public void testPatchReviewLike() {

        ReviewLike like = generator.generatePersistentReviewLike();
        ReviewLikePatchDTO patch = generator.generateObject(ReviewLikePatchDTO.class);

        ReviewLikeReadDTO read = reviewLikeService.patchReviewLike(patch, like.getId());
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        ReviewLike resultLike = reviewLikeRepository.findById(like.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(resultLike);
    }

    @Test
    public void testPatchReviewLikeEmptyPatch() {

        ReviewLike like = generator.generatePersistentReviewLike();
        ReviewLikePatchDTO patch = new ReviewLikePatchDTO();

        ReviewLikeReadDTO read = reviewLikeService.patchReviewLike(patch, like.getId());
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).isEqualToComparingFieldByField(like);

        ReviewLike resultLike = reviewLikeRepository.findById(like.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(resultLike);
    }

    @Test
    public void testDeleteReviewLike() {

        ReviewLike like = generator.generatePersistentReviewLike();
        Assert.assertTrue(reviewLikeRepository.existsById(like.getId()));

        reviewLikeService.deleteReviewLike(like.getId());
        Assert.assertFalse(reviewLikeRepository.existsById(like.getId()));
    }

}
