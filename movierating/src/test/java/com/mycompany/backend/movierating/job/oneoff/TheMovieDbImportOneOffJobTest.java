package com.mycompany.backend.movierating.job.oneoff;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.mycompany.backend.movierating.client.TheMovieDbClient;
import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadShortDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MoviesPageDTO;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.exception.ImportedEntityAlreadyExistException;
import com.mycompany.backend.movierating.service.BaseTest;
import com.mycompany.backend.movierating.service.importer.MovieImporterService;

public class TheMovieDbImportOneOffJobTest extends BaseTest {

    @Autowired
    private TheMovieDbImportOneOffJob job;

    @MockBean
    private TheMovieDbClient client;

    @MockBean
    private MovieImporterService movieImporterService;

    @Test
    public void testDoImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWhith2Result();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    @Test
    public void testDoImportNoExceptionIfGetPageFailed() {
        Mockito.when(client.getTopRatedMovies()).thenThrow(RuntimeException.class);

        job.doImport();

        Mockito.verifyNoInteractions(movieImporterService);
    }

    @Test
    public void testDoImportFirstFailedAndSecondSuccess()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWhith2Result();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);
        Mockito.when(movieImporterService.importMovie(page.getResults().get(0).getId()))
                .thenThrow(RuntimeException.class);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    private MoviesPageDTO generatePageWhith2Result() {
        MoviesPageDTO page = generator.generateObject(MoviesPageDTO.class);
        page.getResults().add(generator.generateObject(MovieReadShortDTO.class));
        Assert.assertEquals(page.getResults().size(), 2);

        return page;
    }

}
