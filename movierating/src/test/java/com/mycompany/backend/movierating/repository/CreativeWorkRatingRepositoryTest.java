package com.mycompany.backend.movierating.repository;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.service.BaseTest;

public class CreativeWorkRatingRepositoryTest extends BaseTest {

    @Autowired
    private CreativeWorkRatingRepository ratingRepository;

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        CreativeWorkRating rating = new CreativeWorkRating();
        ratingRepository.save(rating);
    }

    @Test
    public void testCreatedAtIsSet() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();

        Instant createdAtBeforeReload = rating.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        rating = ratingRepository.findById(rating.getId()).get();
        Instant createdAtAfterReload = rating.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        CreativeWorkRating rating = generator.generatePersistentCreativeWorkRating();

        Instant createdAtBeforeUpdate = rating.getCreatedAt();
        Instant updatedAtBeforeUpdate = rating.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        rating.setRating(9999);
        rating = ratingRepository.save(rating);

        Instant createdAtAfterUpdate = rating.getCreatedAt();
        Instant updatedAtAfterUpdate = rating.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }
}
