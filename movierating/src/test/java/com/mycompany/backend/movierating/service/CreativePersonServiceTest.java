package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPutDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;

public class CreativePersonServiceTest extends BaseTest {

    @Autowired
    private CreativePersonService personService;

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Test
    public void testGetCreativePerson() {
        CreativePerson person = generator.generatePersistentCreativePerson();
        CreativePersonReadDTO read = personService.getCreativePerson(person.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(person, "urlPhoto");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCreativePersonWrongIs() {
        personService.getCreativePerson(UUID.randomUUID());
    }

    @Test
    public void testGetMoviesRating() {

        CreativePerson person = generator.generatePersistentCreativePerson();

        Movie movie1 = generator.generatePersistentMovie();
        movie1.getFilmCrew().add(person);
        movie1 = movieRepository.save(movie1);

        Movie movie2 = generator.generatePersistentMovie();
        movie2.getFilmCrew().add(person);
        movie2 = movieRepository.save(movie2);

        Double averageMoviesRating = personService.getAverageMoviesRating(person.getId());
        Assert.assertEquals((movie1.getAverageRating() + movie2.getAverageRating()) / 2, averageMoviesRating, 0.01);
    }

    @Test
    public void testGetAverageCreativeWorksRating() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        Movie movie1 = generator.generatePersistentMovie();
        Movie movie2 = generator.generatePersistentMovie();

        CreativeWork work1 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work1.setMovie(movie1);
        work1.setCreativePerson(person);
        work1 = workRepository.save(work1);

        CreativeWork work2 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        work2.setMovie(movie2);
        work2.setCreativePerson(person);
        work2 = workRepository.save(work2);

        Double averageCreativeWorkRating = personService.getAverageCreativeWorksRating(person.getId());
        Assert.assertEquals((work1.getAverageRating() + work2.getAverageRating()) / 2, averageCreativeWorkRating, 0.01);
    }

    @Test
    public void testCreateCreativePerson() {

        CreativePersonCreateDTO create = generator.generateObject(CreativePersonCreateDTO.class);

        CreativePersonReadDTO read = personService.createCreativePerson(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CreativePerson resultPerson = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultPerson, "urlPhoto");
    }

    @Test
    public void testPatchCreativePerson() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        CreativePersonPatchDTO patch = generator.generateObject(CreativePersonPatchDTO.class);

        CreativePersonReadDTO read = personService.patchCreativePerson(person.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(person, "urlPhoto");
    }

    @Test
    public void testPatchCreativePersonEmptyPatch() {

        CreativePerson expectPerson = generator.generatePersistentCreativePerson();

        CreativePersonReadDTO read = personService.patchCreativePerson(expectPerson.getId(),
                new CreativePersonPatchDTO());
        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("urlPhoto");

        CreativePerson resultPerson = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(resultPerson, "urlPhoto");
    }

    @Test
    public void testUpdateCreativePerson() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        CreativePersonPutDTO put = generator.generateObject(CreativePersonPutDTO.class);

        CreativePersonReadDTO read = personService.updateCreatePerson(person.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(person, "urlPhoto");
    }

    @Test
    public void testDeleteCreativePerson() {

        CreativePerson person = generator.generatePersistentCreativePerson();
        Assert.assertTrue(personRepository.existsById(person.getId()));

        personService.deleteCreativePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCreativePersonNotFound() {
        personService.deleteCreativePerson(UUID.randomUUID());
    }

}