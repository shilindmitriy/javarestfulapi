package com.mycompany.backend.movierating.service.importer;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.ExternalSystemImport;
import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.constants.ImportedEntityType;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.repository.ExternalSystemImportRepository;
import com.mycompany.backend.movierating.service.BaseTest;

public class ExternalSystemImportServiceTest extends BaseTest {

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Test
    public void testValidateNotImportedFromExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Movie.class, "some-id");
    }

    @Test
    public void testExceptionWhenAlreadyImported() {
        ExternalSystemImport esi = generator.generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.MOVIE);
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Movie.class, idInExternalSystem),
                ImportAlreadyPerformedException.class);

        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = generator.generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.CREATIVE_WORK);
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Movie.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testCreateExternalSystemImport() {
        Movie movie = generator.generatePersistentMovie();
        String idInExternalSystem = "id280";

        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);

        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());
    }

    @Test
    public void testGetExternalSystemImportWithNotSupportedEntity() {
        IllegalArgumentException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.getExternalSystemImport(MessageToAdministration.class, "123"),
                IllegalArgumentException.class);
        Assertions.assertThat(ex.getMessage().equals(MessageToAdministration.class.toString()));
    }

}
