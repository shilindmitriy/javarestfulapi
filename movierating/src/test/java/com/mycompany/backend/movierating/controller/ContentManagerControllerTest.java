package com.mycompany.backend.movierating.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.service.importer.MovieImporterService;

@WebMvcTest(controllers = ContentManagerController.class)
public class ContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private MovieImporterService movieImporterService;

    @Test
    @WithMockUser
    public void testImportMovie() throws Exception {

        UUID managerId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();
        String movieExternalId = "1234";

        Mockito.when(movieImporterService.importMovieFromWebPage(movieExternalId)).thenReturn(movieId);

        String resultJson = mvc
                .perform(post("/api/v1/content-managers/{managerId}/movies-external/{movieExternalId}/import",
                        managerId, movieExternalId))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        UUID resultId = objectMapper.readValue(resultJson, UUID.class);
        Assert.assertEquals(movieId, resultId);

        Mockito.verify(movieImporterService).importMovieFromWebPage(movieExternalId);
    }

    @Test
    @WithMockUser
    public void testImportMovieAlreadyExists() throws Exception {

        UUID managerId = UUID.randomUUID();
        String movieExternalId = "1234";

        AlreadyExistsException exception = new AlreadyExistsException(
                String.format("Can't import movie id=%s : %s", movieExternalId, "message"));

        Mockito.when(movieImporterService.importMovieFromWebPage(movieExternalId)).thenThrow(exception);

        String resultJson = mvc
                .perform(post("/api/v1/content-managers/{managerId}/movies-external/{movieExternalId}/import",
                        managerId, movieExternalId))
                .andExpect(status().isConflict()).andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

}
