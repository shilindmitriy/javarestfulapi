package com.mycompany.backend.movierating.repository;

import static org.junit.Assert.*;

import java.time.Instant;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;
import com.mycompany.backend.movierating.service.BaseTest;

public class UseruserAccountRepositoryTest extends BaseTest {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Test
    public void testSave() {

        UserAccount expectUser = new UserAccount();
        expectUser.setGender(Gender.MALE);
        expectUser.setBirthday(LocalDate.of(2001, 03, 31));
        expectUser.setNickName("Neo");
        expectUser.setCountry(Country.UNITED_STATES_OF_AMERICA);
        expectUser.setEmail("mail@mail.com");
        expectUser.setPassword("123456789");
        expectUser.setModeratorsTrust(0);
        expectUser.setEnabled(true);
        expectUser.setAuthority(Authority.REGISTERED_USER);

        UserAccount resultUser = userAccountRepository.save(expectUser);

        String expect[] = { expectUser.getGender().name(), expectUser.getBirthday().toString(),
                expectUser.getNickName(), expectUser.getCountry().name(), expectUser.getEmail(),
                expectUser.getPassword(), expectUser.getAuthority().name() };

        String result[] = { resultUser.getGender().name(), resultUser.getBirthday().toString(),
                resultUser.getNickName(), resultUser.getCountry().name(), resultUser.getEmail(),
                resultUser.getPassword(), resultUser.getAuthority().name() };

        assertNotNull(resultUser.getId());
        assertTrue(userAccountRepository.existsById(resultUser.getId()));
        assertArrayEquals(expect, result);
        assertEquals(expectUser.getModeratorsTrust(), resultUser.getModeratorsTrust());
        assertTrue(expectUser.getEnabled() == resultUser.getEnabled());
        assertTrue(resultUser.getFirstName() == null);
        assertTrue(resultUser.getLastName() == null);
        assertTrue(resultUser.getUsersRating() == null);
        assertTrue(resultUser.getCreatedAt().equals(expectUser.getCreatedAt()));
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveValidation() {
        UserAccount user = new UserAccount();
        userAccountRepository.save(user);
    }

    @Test
    public void testCreatedAtIsSet() {

        UserAccount user = generator.generatePersistentUser();

        Instant createdAtBeforeReload = user.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        user = userAccountRepository.findById(user.getId()).get();
        Instant createdAtAfterReload = user.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtAfterReload, createdAtBeforeReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        UserAccount user = generator.generatePersistentUser();

        Instant createdAtBeforeUpdate = user.getCreatedAt();
        Instant updatedAtBeforeUpdate = user.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeUpdate);
        Assert.assertEquals(updatedAtBeforeUpdate, createdAtBeforeUpdate);

        user.setNickName("Lemon");
        user = userAccountRepository.save(user);

        Instant createdAtAfterUpdate = user.getCreatedAt();
        Instant updatedAtAfterUpdate = user.getUpdatedAt();
        Assert.assertNotNull(createdAtAfterUpdate);
        Assert.assertEquals(createdAtAfterUpdate, createdAtBeforeUpdate);
        Assert.assertTrue(updatedAtAfterUpdate.isAfter(createdAtBeforeUpdate));
    }

}