package com.mycompany.backend.movierating.service;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;

public class MovieInfoServiceTest extends BaseTest {

    @Autowired
    private MovieInfoService infoService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Test
    public void testGetCompanies() {

        Movie movie = generator.generatePersistentMovie();
        Movie movieNotFound = generator.generatePersistentMovie();

        ProductionCompany company1 = generator.generatePersistentProductionCompany();
        company1.setName("company1");
        company1 = companyRepository.save(company1);
        ProductionCompany company2 = generator.generatePersistentProductionCompany();
        company2.setName("company2");
        company2 = companyRepository.save(company2);
        ProductionCompany company3 = generator.generatePersistentProductionCompany();
        company3.setName("company3");
        company3 = companyRepository.save(company3);
        ProductionCompany company4 = generator.generatePersistentProductionCompany();
        company4.setName("company4");
        company4 = companyRepository.save(company4);
        ProductionCompany company5 = generator.generatePersistentProductionCompany();
        company5.setName("companyNotFound");
        company5 = companyRepository.save(company5);

        movie.getProductionCompanies().add(company1);
        movie.getProductionCompanies().add(company2);
        movie.getProductionCompanies().add(company3);
        movie.getProductionCompanies().add(company4);
        movieNotFound.getProductionCompanies().add(company5);
        movieNotFound.getProductionCompanies().add(company4);

        movie = movieRepository.save(movie);
        movieNotFound = movieRepository.save(movieNotFound);

        List<CompanyReadDTO> companies = infoService.getCompanies(movie.getId());
        Assert.assertFalse(companies.isEmpty());
        Assert.assertTrue(companies.size() == 4);
        Assertions.assertThat(companies).extracting("companyId").containsExactlyInAnyOrder(company1.getId(),
                company2.getId(), company3.getId(), company4.getId());
        Assertions.assertThat(companies).extracting("name").containsExactlyInAnyOrder(company1.getName(),
                company2.getName(), company3.getName(), company4.getName());
        Assertions.assertThat(companies).extracting("companyId").doesNotContain(company5.getId());
        Assertions.assertThat(companies).extracting("name").doesNotContain(company5.getName());
    }

    @Test
    public void testGetCastAndCrew() {

        Movie movie = generator.generatePersistentMovie();
        Movie movieNotFound = generator.generatePersistentMovie();

        CreativeWork work1 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        CreativeWork work2 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        CreativeWork work3 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        CreativeWork work4 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        CreativeWork work5 = generator.generateFlatEntityWithoutId(CreativeWork.class);
        CreativeWork work6 = generator.generateFlatEntityWithoutId(CreativeWork.class);

        CreativePerson person1 = generator.generatePersistentCreativePerson();
        CreativePerson person2 = generator.generatePersistentCreativePerson();
        CreativePerson person3 = generator.generatePersistentCreativePerson();
        CreativePerson person4 = generator.generatePersistentCreativePerson();
        CreativePerson person5 = generator.generatePersistentCreativePerson();

        work1.setDepartment(Department.ACTING);
        work1.setMovie(movie);
        work1.setCreativePerson(person1);
        work1 = workRepository.save(work1);

        work2.setDepartment(Department.ACTING);
        work2.setMovie(movie);
        work2.setCreativePerson(person2);
        work2 = workRepository.save(work2);

        work3.setDepartment(Department.ART);
        work3.setMovie(movie);
        work3.setCreativePerson(person3);
        work3 = workRepository.save(work3);

        work4.setDepartment(Department.ART);
        work4.setMovie(movie);
        work4.setCreativePerson(person4);
        work4 = workRepository.save(work4);

        work5.setDepartment(Department.CAMERA);
        work5.setMovie(movieNotFound);
        work5.setCreativePerson(person5);
        work5 = workRepository.save(work5);

        work6.setDepartment(Department.PRODUCTION);
        work6.setMovie(movie);
        work6.setCreativePerson(person1);
        work6 = workRepository.save(work6);

        List<CastAndCrewReadDTO> leadingActor = infoService.getCastAndCrew(movie.getId(), Department.ACTING);
        Assert.assertFalse(leadingActor.isEmpty());
        Assert.assertTrue(leadingActor.size() == 2);
        Assertions.assertThat(leadingActor).extracting("personId").containsExactlyInAnyOrder(person1.getId(),
                person2.getId());
        Assertions.assertThat(leadingActor).extracting("workId").containsExactlyInAnyOrder(work1.getId(),
                work2.getId());
        Assertions.assertThat(leadingActor).extracting("department").containsExactlyInAnyOrder(Department.ACTING,
                Department.ACTING);

        List<CastAndCrewReadDTO> crowdScene = infoService.getCastAndCrew(movie.getId(), Department.ART);
        Assert.assertFalse(crowdScene.isEmpty());
        Assert.assertTrue(crowdScene.size() == 2);
        Assertions.assertThat(crowdScene).extracting("personId").containsExactlyInAnyOrder(person3.getId(),
                person4.getId());
        Assertions.assertThat(crowdScene).extracting("workId").containsExactlyInAnyOrder(work3.getId(), work4.getId());
        Assertions.assertThat(crowdScene).extracting("department").containsExactlyInAnyOrder(Department.ART,
                Department.ART);

        List<CastAndCrewReadDTO> director = infoService.getCastAndCrew(movie.getId(), Department.PRODUCTION);
        Assert.assertFalse(director.isEmpty());
        Assert.assertTrue(director.size() == 1);
        Assertions.assertThat(director).extracting("personId").containsExactly(person1.getId());
        Assertions.assertThat(director).extracting("workId").containsExactly(work6.getId());
        Assertions.assertThat(director).extracting("department").containsExactly(Department.PRODUCTION);
    }
}
