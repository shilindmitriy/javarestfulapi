package com.mycompany.backend.movierating.dto.creativework;

import java.time.Instant;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Department;

import lombok.Data;

@Data
public class CreativeWorkReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String character;

    private Department department;

    private String post;

    private String notes;
    
    private Double averageRating;

    private Long numberOfAllVotes;

    private UUID personId;

    private UUID movieId;
}
