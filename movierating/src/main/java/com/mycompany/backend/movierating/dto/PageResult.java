package com.mycompany.backend.movierating.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PageResult<T> {

    private List<T> data = new ArrayList<>();

    private int page;

    private int pageSize;

    private int totalPages;

    private long totalElements;
}
