package com.mycompany.backend.movierating.dto.review;

import java.time.Instant;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String text;

    private Boolean enabled;

    private UUID userId;
    
    private UUID parentId;
}
