package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.exception.EntityNotFoundException;

@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager em;
    
    public <E> E getById(Class<E> entityClass, UUID id) {        
        return em.find(entityClass, id);
    } 

    public <E> E getByIdRequired(Class<E> entityClass, UUID id) {
        E res = em.find(entityClass, id);
        if (res == null) {
            throw new EntityNotFoundException(entityClass, id);
        }
        return res;
    }

    // For Review and Rating
    public <E> E getByParentIdAndIdRequired(Class<E> entityClass, UUID id, UUID parentId) {

        // Review and Rating are checking
        validateExists(entityClass, id);

        TypedQuery<E> query = em.createQuery("select e from " + entityClass.getSimpleName()
                + " e where e.parentEntity.id = :parentId and e.id = :id", entityClass);
        query.setParameter("id", id);
        query.setParameter("parentId", parentId);

        E res;
        try {
            res = query.getSingleResult();
        } catch (NoResultException ex) {
            throw new EntityNotFoundException(entityClass, id, parentId);
        }
        return res;
    }
    
    @Transactional
    public <E> E updateEntity(E entity) {
        return em.merge(entity);
    }

    public <E> E getReferenceIfExist(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return em.getReference(entityClass, id);
    }

    private <E> void validateExists(Class<E> entityClass, UUID id) {
        Query query = em.createQuery("select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }
}