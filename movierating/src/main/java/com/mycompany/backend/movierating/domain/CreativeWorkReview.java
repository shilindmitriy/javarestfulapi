package com.mycompany.backend.movierating.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class CreativeWorkReview extends Review {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "creative_work_id", referencedColumnName = "id")
    private CreativeWork parentEntity;
}