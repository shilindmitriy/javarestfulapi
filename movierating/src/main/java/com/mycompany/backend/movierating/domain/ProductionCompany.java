package com.mycompany.backend.movierating.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class ProductionCompany extends Auditable {

    @NotBlank
    @Size(max = 100)
    private String name;

    @Size(max = 10000)
    private String history;

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<ProductionCompanyReview> companyReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<ProductionCompanyRating> companyRatings = new ArrayList<>();

    @ManyToMany(mappedBy = "productionCompanies")
    private List<Movie> movies = new ArrayList<>();
}
