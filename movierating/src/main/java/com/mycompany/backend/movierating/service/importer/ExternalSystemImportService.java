package com.mycompany.backend.movierating.service.importer;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.AbstractEntity;
import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.ExternalSystemImport;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.constants.ImportedEntityType;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.repository.ExternalSystemImportRepository;

@Service
public class ExternalSystemImportService {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    public void validateNotImported(Class<? extends AbstractEntity> entityToImport, String idInExternalSystem)
            throws ImportAlreadyPerformedException {
        ImportedEntityType entityType = getImportedEntityType(entityToImport);
        ExternalSystemImport esi = externalSystemImportRepository
                .findByIdInExternalSystemAndEntityType(idInExternalSystem, entityType);

        if (esi != null) {
            throw new ImportAlreadyPerformedException(esi);
        }
    }

    public <T extends AbstractEntity> UUID createExternalSystemImport(T entity, String idInExternalSystem) {
        ImportedEntityType entityType = getImportedEntityType(entity.getClass());
        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setEntityId(entity.getId());
        esi.setEntityType(entityType);
        esi.setIdInExternalSystem(idInExternalSystem);
        esi = externalSystemImportRepository.save(esi);
        return esi.getId();
    }

    public ExternalSystemImport getExternalSystemImport(Class<? extends AbstractEntity> entityClass,
            String idInExternalSystem) {
        ImportedEntityType entityType = getImportedEntityType(entityClass);
        return externalSystemImportRepository.findByIdInExternalSystemAndEntityType(idInExternalSystem, entityType);
    }

    public <T extends AbstractEntity> ExternalSystemImport updateExternalSystemImport(T entity,
            ExternalSystemImport esi) {
        esi.setEntityId(entity.getId());
        return externalSystemImportRepository.save(esi);
    }

    private ImportedEntityType getImportedEntityType(Class<? extends AbstractEntity> entityToImport) {

        if (Movie.class.equals(entityToImport)) {
            return ImportedEntityType.MOVIE;
        }
        if (CreativePerson.class.equals(entityToImport)) {
            return ImportedEntityType.CREATIVE_PERSON;
        }
        if (CreativeWork.class.equals(entityToImport)) {
            return ImportedEntityType.CREATIVE_WORK;
        }
        if (ProductionCompany.class.equals(entityToImport)) {
            return ImportedEntityType.PRODUCTION_COMPANY;
        }

        throw new IllegalArgumentException("Importing of entities " + entityToImport + " is not supported");
    }

}
