package com.mycompany.backend.movierating.dto.creativework;

import java.util.UUID;

import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Department;

import lombok.Data;

@Data
public class CreativeWorkPatchDTO {

    @Size(max = 100)
    private String character;

    private Department department;

    private String post;

    @Size(max = 1500)
    private String notes;

    private UUID personId;

    private UUID movieId;
}
