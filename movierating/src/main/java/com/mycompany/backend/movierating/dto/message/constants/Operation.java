package com.mycompany.backend.movierating.dto.message.constants;

public enum Operation {

    APPEND, REMOVE, REPLACE;

}
