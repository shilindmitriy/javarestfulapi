package com.mycompany.backend.movierating.dto.user;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Data;

@Data
public class UserAccountReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String firstName;

    private String lastName;

    private Gender gender;

    private LocalDate birthday;

    private String nickName;

    private Country country;

    private Integer moderatorsTrust;

    private Integer usersRating;

    private String email;

    private Boolean enabled;

    private Authority authority;
}
