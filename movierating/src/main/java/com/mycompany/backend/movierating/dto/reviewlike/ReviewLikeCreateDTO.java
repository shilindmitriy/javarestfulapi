package com.mycompany.backend.movierating.dto.reviewlike;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.mycompany.backend.movierating.domain.constants.ReviewType;

import lombok.Data;

@Data
public class ReviewLikeCreateDTO {

    @NotNull
    private Boolean like;

    @NotNull
    private ReviewType reviewType;

    @NotNull
    private UUID reviewId;

}
