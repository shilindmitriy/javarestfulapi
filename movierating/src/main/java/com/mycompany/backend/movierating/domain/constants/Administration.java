package com.mycompany.backend.movierating.domain.constants;

public enum Administration {
    MODERATOR, CONTENT_MANAGER
}
