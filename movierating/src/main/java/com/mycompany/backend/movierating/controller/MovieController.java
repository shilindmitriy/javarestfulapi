package com.mycompany.backend.movierating.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.documentation.ApiPageable;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;
import com.mycompany.backend.movierating.dto.movie.MovieReadExtendedDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePutDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadDTO;
import com.mycompany.backend.movierating.service.MovieService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public MovieReadExtendedDTO getMovieExtended(@PathVariable UUID id) {
        return movieService.getMovieExtended(id);
    }

    @PreAuthorize("permitAll")
    @ApiPageable
    @GetMapping
    public PageResult<MovieReadDTO> getMovies(MovieFilter filter, @ApiIgnore Pageable pageable) {
        return movieService.getMovies(filter, pageable);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping
    public MovieReadDTO createMovie(@RequestBody @Valid MovieCreateDTO create) {
        return movieService.createMovie(create);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping("/{id}/production-companies/{companyId}")
    public List<ProductionCompanyReadDTO> addCompanyToMovie(@PathVariable UUID id, @PathVariable UUID companyId) {
        return movieService.addCompanyToMovie(id, companyId);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping("/{id}/creative-persons/{personId}")
    public List<CreativePersonReadDTO> addCreativePersonToMovie(@PathVariable UUID id, @PathVariable UUID personId) {
        return movieService.addCreativePersonToMovie(id, personId);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public MovieReadDTO patchMovie(@PathVariable UUID id, @RequestBody @Valid MoviePatchDTO patch) {
        return movieService.patchMovie(id, patch);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public MovieReadDTO updateMovie(@PathVariable UUID id, @RequestBody @Valid MoviePutDTO put) {
        return movieService.updateMovie(id, put);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable UUID id) {
        movieService.deleteMovie(id);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @DeleteMapping("/{id}/production-companies/{companyId}")
    public List<ProductionCompanyReadDTO> removeCompanyFromMovie(@PathVariable UUID id, @PathVariable UUID companyId) {
        return movieService.removeCompanyFromMovie(id, companyId);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @DeleteMapping("/{id}/creative-persons/{personId}")
    public List<CreativePersonReadDTO> removeCreativePersonFromMovie(@PathVariable UUID id,
            @PathVariable UUID personId) {
        return movieService.removeCreativePersonFromMovie(id, personId);
    }
}
