package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.ModeratorsTrustDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.repository.UserAccountRepository;

@Service
public class UserService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private UserAccountRepository userAccountRepository;

    public UserAccountReadDTO getUser(UUID id) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public UserAccountReadDTO createUser(UserAccountCreateDTO create) {

        UserAccount user = translationService.translate(create, UserAccount.class);
        user.setModeratorsTrust(0);
        user.setEnabled(true);
        user.setAuthority(Authority.REGISTERED_USER);

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public UserAccountReadDTO patchUser(UUID id, UserAccountPatchDTO patch) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        translationService.map(patch, user);

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public UserAccountReadDTO changeAuthority(UUID id, AuthorityPatchDTO patch) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        translationService.map(patch, user);

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public UserAccountReadDTO banUser(UUID id) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        if (!user.getAuthority().equals(Authority.ADMIN)) {
            user.setAuthority(Authority.UNREGISTERED_USER);
        } else {
            throw new AccessDeniedException("Admin cannot be banned.");
        }

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public UserAccountReadDTO updateUser(UUID id, UserAccountPutDTO put) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        translationService.map(put, user);

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }

    public void deleteUser(UUID id) {
        userAccountRepository.delete(repositoryHelper.getByIdRequired(UserAccount.class, id));
    }

    public UserAccountReadDTO addModeratorsTrust(UUID id, ModeratorsTrustDTO moderatorsTrust) {
        UserAccount user = repositoryHelper.getByIdRequired(UserAccount.class, id);
        user.setModeratorsTrust(user.getModeratorsTrust() + moderatorsTrust.getRating());

        user = userAccountRepository.save(user);
        return translationService.translate(user, UserAccountReadDTO.class);
    }
}
