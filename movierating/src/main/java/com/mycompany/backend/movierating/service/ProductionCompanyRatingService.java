package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.ProductionCompanyRatingRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class ProductionCompanyRatingService {

    @Autowired
    private ProductionCompanyRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingReadDTO getRating(UUID companyId, UUID id) {
        ProductionCompanyRating rating = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyRating.class, id,
                companyId);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public ShortRatingReadDTO getShortRating(UUID companyId) {
        return ratingRepository.retrieveShortRating(companyId);
    }

    public RatingReadDTO createRating(UUID companyId, RatingCreateDTO create, UserDetailsImpl user) {

        if (ratingRepository.existsByParentIdAndUserId(companyId, user.getId())) {
            throw new AlreadyExistsException(ProductionCompany.class, companyId, user.getId());
        }

        ProductionCompanyRating rating = translationService.translate(create, ProductionCompanyRating.class);
        rating.setGender(user.getGender());
        rating.setCountryUser(user.getCountry());
        rating.setAge(Period.between(user.getBirthday(), LocalDate.now()).getYears());
        rating.setParentEntity(repositoryHelper.getReferenceIfExist(ProductionCompany.class, companyId));
        rating.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO patchRating(UUID companyId, UUID id, RatingPatchDTO patch) {

        ProductionCompanyRating rating = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyRating.class, id,
                companyId);
        translationService.map(patch, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO updateRating(UUID companyId, UUID id, RatingPutDTO put) {

        ProductionCompanyRating rating = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyRating.class, id,
                companyId);
        translationService.map(put, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public void deleteRating(UUID companyId, UUID id) {
        ratingRepository
                .delete(repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyRating.class, id, companyId));
    }

}
