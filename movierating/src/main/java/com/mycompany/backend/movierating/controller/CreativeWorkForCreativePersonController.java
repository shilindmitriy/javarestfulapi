package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.service.CreativeWorkService;

@RestController
@RequestMapping("/api/v1/creative-persons/{personId}/creative-works")
public class CreativeWorkForCreativePersonController {

    @Autowired
    private CreativeWorkService workService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public CreativeWorkReadDTO getCreativeWork(@PathVariable UUID personId, @PathVariable UUID id) {
        return workService.getCreativeWork(personId, id);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public CreativeWorkReadDTO patchCreativeWork(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid CreativeWorkPatchDTO patch) {
        return workService.patchCreativeWork(personId, id, patch);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public CreativeWorkReadDTO updateCreativeWork(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid CreativeWorkPutDTO put) {
        return workService.updateCreativeWork(personId, id, put);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteCreativeWork(@PathVariable UUID personId, @PathVariable UUID id) {
        workService.deleteCreativeWork(personId, id);
    }
}
