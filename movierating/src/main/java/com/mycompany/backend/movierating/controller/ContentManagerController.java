package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.service.importer.MovieImporterService;

@RestController
@RequestMapping("/api/v1/content-managers")
public class ContentManagerController {

    @Autowired
    private MovieImporterService movieImporterService;

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping("/{managerId}/movies-external/{movieExternalId}/import")
    public UUID importMovie(@PathVariable String movieExternalId) {
        return movieImporterService.importMovieFromWebPage(movieExternalId);
    }

}
