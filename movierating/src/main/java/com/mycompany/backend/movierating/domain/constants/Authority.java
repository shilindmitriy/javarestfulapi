package com.mycompany.backend.movierating.domain.constants;

public enum Authority {
    ADMIN, MODERATOR, CONTENT_MANAGER, REGISTERED_USER, UNREGISTERED_USER
}
