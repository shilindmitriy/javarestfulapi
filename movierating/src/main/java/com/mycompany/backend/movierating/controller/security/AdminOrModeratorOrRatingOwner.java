package com.mycompany.backend.movierating.controller.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR') or (hasAnyAuthority('CONTENT_MANAGER', 'REGISTERED_USER') "
        + "and @belongsToCurrentUserValidator.ratingBelongsToCurrentUser(#id))")
public @interface AdminOrModeratorOrRatingOwner {

}
