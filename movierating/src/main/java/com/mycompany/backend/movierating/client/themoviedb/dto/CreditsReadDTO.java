package com.mycompany.backend.movierating.client.themoviedb.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CreditsReadDTO {

    private List<CastReadDTO> cast = new ArrayList<>();

    private List<CrewReadDTO> crew = new ArrayList<>();

}
