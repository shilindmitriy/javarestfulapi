package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.AbstractEntity;
import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeCreateDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikePatchDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class ReviewLikeService {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewLikeReadDTO getReviewLike(UUID id) {
        ReviewLike like = repositoryHelper.getByIdRequired(ReviewLike.class, id);
        return translationService.translate(like, ReviewLikeReadDTO.class);
    }

    public ReviewLikesReadDTO getReviewLikes(ReviewType reviewType, UUID reviewId) {
        return reviewLikeRepository.retrieveReviewLikes(reviewType, reviewId);
    }

    public ReviewLikeReadDTO createReviewLike(ReviewLikeCreateDTO create, UserDetailsImpl user) {

        if (reviewLikeRepository.existsByReviewIdAndUserId(create.getReviewId(), user.getId())) {
            throw new AlreadyExistsException(getReviewClass(create.getReviewType()), create.getReviewId(),
                    user.getId());
        }

        ReviewLike like = translationService.translate(create, ReviewLike.class);
        like.setUserId(user.getId());

        like = reviewLikeRepository.save(like);
        return translationService.translate(like, ReviewLikeReadDTO.class);
    }

    public ReviewLikeReadDTO patchReviewLike(ReviewLikePatchDTO patch, UUID id) {

        ReviewLike like = repositoryHelper.getById(ReviewLike.class, id);
        translationService.map(patch, like);

        like = reviewLikeRepository.save(like);
        return translationService.translate(like, ReviewLikeReadDTO.class);
    }

    public void deleteReviewLike(UUID id) {
        reviewLikeRepository.delete(repositoryHelper.getById(ReviewLike.class, id));
    }

    private Class<? extends AbstractEntity> getReviewClass(ReviewType reviewType) {
        switch (reviewType) {
          case CREATIVE_PERSON_REVIEW:
              return CreativePersonReview.class;
          case CREATIVE_WORK_REVIEW:
              return CreativeWorkReview.class;
          case MOVIE_REVIEW:
              return MovieReview.class;
          case PRODUCTION_COMPANY_REVIEW:
              return ProductionCompanyReview.class;
          default:
              throw new IllegalArgumentException("Entity " + reviewType + " is not supported for liking.");
        }
    }

}
