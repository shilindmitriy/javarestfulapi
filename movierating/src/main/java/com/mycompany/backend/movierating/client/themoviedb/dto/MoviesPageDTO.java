package com.mycompany.backend.movierating.client.themoviedb.dto;

import java.util.List;

import lombok.Data;

@Data
public class MoviesPageDTO {

    private Integer page;

    private Integer totalResults;

    private Integer totalPages;

    private List<MovieReadShortDTO> results;

}
