package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.CreativePersonRatingRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class CreativePersonRatingService {

    @Autowired
    private CreativePersonRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingReadDTO getRating(UUID personId, UUID id) {
        CreativePersonRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativePersonRating.class, id,
                personId);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public ShortRatingReadDTO getShortRating(UUID personId) {
        return ratingRepository.retrieveShortRating(personId);
    }

    public RatingReadDTO createRating(UUID personId, RatingCreateDTO create, UserDetailsImpl user) {

        if (ratingRepository.existsByParentIdAndUserId(personId, user.getId())) {
            throw new AlreadyExistsException(CreativePerson.class, personId, user.getId());
        }

        CreativePersonRating rating = translationService.translate(create, CreativePersonRating.class);
        rating.setGender(user.getGender());
        rating.setCountryUser(user.getCountry());
        rating.setAge(Period.between(user.getBirthday(), LocalDate.now()).getYears());
        rating.setParentEntity(repositoryHelper.getReferenceIfExist(CreativePerson.class, personId));
        rating.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO patchRating(UUID personId, UUID id, RatingPatchDTO patch) {

        CreativePersonRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativePersonRating.class, id,
                personId);
        translationService.map(patch, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO updateRating(UUID personId, UUID id, RatingPutDTO put) {

        CreativePersonRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativePersonRating.class, id,
                personId);
        translationService.map(put, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public void deleteRating(UUID personId, UUID id) {
        ratingRepository.delete(repositoryHelper.getByParentIdAndIdRequired(CreativePersonRating.class, id, personId));
    }

}