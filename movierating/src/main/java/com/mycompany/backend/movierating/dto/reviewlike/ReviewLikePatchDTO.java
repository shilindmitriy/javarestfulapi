package com.mycompany.backend.movierating.dto.reviewlike;

import lombok.Data;

@Data
public class ReviewLikePatchDTO {

    private Boolean like;

}
