package com.mycompany.backend.movierating.domain.constants;

public enum ReviewType {

    CREATIVE_PERSON_REVIEW, CREATIVE_WORK_REVIEW, MOVIE_REVIEW, PRODUCTION_COMPANY_REVIEW

}
