package com.mycompany.backend.movierating.domain.constants;

public enum Department {

    ACTING, ART, CAMERA, COSTUME_AND_MAKE_UP, CREW, DIRECTING, EDITING, LIGHTING, PRODUCTION, SOUND, VISUAL_EFFECTS,
    WRITING

}
