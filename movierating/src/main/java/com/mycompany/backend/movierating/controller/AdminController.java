package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.service.UserService;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PatchMapping("/{adminId}/users/{userId}/authority")
    public UserAccountReadDTO changeAuthority(@PathVariable UUID userId, @RequestBody AuthorityPatchDTO patch) {
        return userService.changeAuthority(userId, patch);
    }

}
