package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class MovieReviewService {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private MovieReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewReadDTO getReview(UUID movieId, UUID id) {
        MovieReview review = repositoryHelper.getByParentIdAndIdRequired(MovieReview.class, id, movieId);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public PageResult<ReviewReadDTO> getReviews(UUID movieId, UserDetailsImpl user, Pageable pageable) {
        UUID userId = null;
        if (user != null) {
            userId = user.getId();
        }
        Page<ReviewReadDTO> reviews = reviewRepository.findAllByParentIdAndEnabledOrUserId(movieId, userId, true,
                pageable);
        return translationService.toPageResult(reviews);
    }

    public ReviewReadDTO createReview(UUID movieId, ReviewCreateDTO create, UserDetailsImpl user) {

        if (user.getModeratorsTrust() < 0) {
            throw new AccessDeniedException("The user with id=" + user.getId() + " is not allowed to write a review.");
        }

        MovieReview review = translationService.translate(create, MovieReview.class);
        review.setParentEntity(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        review.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        if (user.getModeratorsTrust() > 99) {
            review.setEnabled(true);
        } else {
            review.setEnabled(false);
        }

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchReview(UUID movieId, UUID id, ReviewPatchDTO patch) {

        MovieReview review = repositoryHelper.getByParentIdAndIdRequired(MovieReview.class, id, movieId);
        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateReview(UUID movieId, UUID id, ReviewPutDTO put) {

        MovieReview review = repositoryHelper.getByParentIdAndIdRequired(MovieReview.class, id, movieId);
        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    @Transactional
    public void deleteReview(UUID movieId, UUID id) {
        reviewLikeRepository.deleteByReviewTypeAndReviewId(ReviewType.MOVIE_REVIEW, id);
        reviewRepository.delete(repositoryHelper.getByParentIdAndIdRequired(MovieReview.class, id, movieId));
    }
}
