package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.security.AdminOrModeratorOrRatingOwner;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.CreativePersonRatingService;

@RestController
@RequestMapping("/api/v1/creative-persons/{personId}/ratings")
public class CreativePersonRatingController {

    @Autowired
    private CreativePersonRatingService ratingService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public RatingReadDTO getRating(@PathVariable UUID personId, @PathVariable UUID id) {
        return ratingService.getRating(personId, id);
    }

    @PreAuthorize("permitAll")
    @GetMapping
    public ShortRatingReadDTO getShortRating(@PathVariable UUID personId) {
        return ratingService.getShortRating(personId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER', 'REGISTERED_USER')")
    @PostMapping
    public RatingReadDTO createRating(@PathVariable UUID personId, @RequestBody @Valid RatingCreateDTO create,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return ratingService.createRating(personId, create, user);
    }

    @AdminOrModeratorOrRatingOwner
    @PatchMapping("/{id}")
    public RatingReadDTO patchRating(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid RatingPatchDTO patch) {
        return ratingService.patchRating(personId, id, patch);
    }

    @AdminOrModeratorOrRatingOwner
    @PutMapping("/{id}")
    public RatingReadDTO updateRating(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid RatingPutDTO put) {
        return ratingService.updateRating(personId, id, put);
    }

    @AdminOrModeratorOrRatingOwner
    @DeleteMapping("/{id}")
    public void deleteRating(@PathVariable UUID personId, @PathVariable UUID id) {
        ratingService.deleteRating(personId, id);
    }
}
