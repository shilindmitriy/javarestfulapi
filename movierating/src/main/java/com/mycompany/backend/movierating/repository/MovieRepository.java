package com.mycompany.backend.movierating.repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;

@Repository
public interface MovieRepository extends CrudRepository<Movie, UUID>, MovieRepositoryCustom {

    @Query("select new com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO(c.id, c.name) "
            + "from Movie m join m.productionCompanies c where m.id = :movieId order by c.name")
    List<CompanyReadDTO> findCompanies(UUID movieId);

    @Query("select m.id from Movie m")
    Stream<UUID> getIdsOfMovies();
    
    @Query("select m.status from Movie m where m.id = :movieId")
    MovieStatus getMovieStatusOfMovie(UUID movieId);

    Movie findByTitle(String title); 
}
