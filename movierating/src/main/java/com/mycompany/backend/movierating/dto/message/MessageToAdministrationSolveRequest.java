package com.mycompany.backend.movierating.dto.message;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.EntityType;
import com.mycompany.backend.movierating.dto.message.constants.Operation;

import lombok.Data;

@Data
public class MessageToAdministrationSolveRequest {

    @NotEmpty
    private List<String> misprints = new ArrayList<>();

    @NotBlank
    @Size(max = 500)
    private String url;

    @Size(max = 500)
    private String note;

    private UUID entityId;

    private EntityType entityType;

    private String entityField;

    private String correction;

    @NotBlank
    private String misprint;

    private Integer beginIndex;

    @NotNull
    private Operation operation;

}
