package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.validation.ControllerValidationUtil;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPutDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.service.CreativePersonService;

@RestController
@RequestMapping("/api/v1/creative-persons")
public class CreativePersonController {

    @Autowired
    private CreativePersonService personService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public CreativePersonReadDTO getCreativePerson(@PathVariable UUID id) {
        return personService.getCreativePerson(id);
    }
    
    @PreAuthorize("permitAll")
    @GetMapping("/{id}/movies-rating")
    public Double getAverageMoviesRating(@PathVariable UUID id) {
        return personService.getAverageMoviesRating(id);
    }
    
    @PreAuthorize("permitAll")
    @GetMapping("/{id}/creative-works-rating")
    public Double getAverageCreativeWorksRating(@PathVariable UUID id) {
        return personService.getAverageCreativeWorksRating(id);
    }    

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping
    public CreativePersonReadDTO createCreativePerson(@RequestBody @Valid CreativePersonCreateDTO create) {
        ControllerValidationUtil.validateLessThan(create.getBorn(), create.getDeath(), "born", "death");
        return personService.createCreativePerson(create);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public CreativePersonReadDTO patchCreativePerson(@PathVariable UUID id,
            @RequestBody @Valid CreativePersonPatchDTO patch) {
        ControllerValidationUtil.validateLessThan(patch.getBorn(), patch.getDeath(), "born", "death");
        return personService.patchCreativePerson(id, patch);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public CreativePersonReadDTO updateCreativePerson(@PathVariable UUID id,
            @RequestBody @Valid CreativePersonPutDTO put) {
        ControllerValidationUtil.validateLessThan(put.getBorn(), put.getDeath(), "born", "death");
        return personService.updateCreatePerson(id, put);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteCreativePerson(@PathVariable UUID id) {
        personService.deleteCreativePerson(id);
    }
}
