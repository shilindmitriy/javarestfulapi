package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;

@Service
public class FilmographyService {

    @Autowired
    private CreativeWorkRepository workRepository;

    public List<FilmographyReadDTO> getFilmography(UUID personId, Department department) {
        return workRepository.findFilmography(personId, department);
    }

}