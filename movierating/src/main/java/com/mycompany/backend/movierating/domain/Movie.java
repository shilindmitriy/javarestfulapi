package com.mycompany.backend.movierating.domain;

import java.sql.Blob;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class Movie extends Auditable {

    private Blob poster;

    @NotBlank
    @Size(max = 100)
    private String title;

    private LocalDate releaseDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MovieStatus status;

    private LocalTime runningTime;

    @Size(max = 10000)
    private String storyline;

    private Double averageRating;

    private Long numberOfAllVotes;

    @NotEmpty
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<Country> productionCountries = new HashSet<>();

    @NotEmpty
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<Genre> genres = new HashSet<>();

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<MovieRating> movieRatings = new ArrayList<>();

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<MovieReview> movieReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "movie")
    private List<CreativeWork> creativeWorks = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "movie_production_company", joinColumns = {
            @JoinColumn(name = "movie_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "production_company_id", referencedColumnName = "id") })
    private List<ProductionCompany> productionCompanies = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "movie_creative_person", joinColumns = {
            @JoinColumn(name = "movie_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "creative_person_id", referencedColumnName = "id") })
    private List<CreativePerson> filmCrew = new ArrayList<>();
}