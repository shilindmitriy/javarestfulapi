package com.mycompany.backend.movierating.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.NewsRepository;
import com.mycompany.backend.movierating.service.CreativePersonService;
import com.mycompany.backend.movierating.service.CreativeWorkService;
import com.mycompany.backend.movierating.service.MovieService;
import com.mycompany.backend.movierating.service.NewsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UpdateRatingsJob {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CreativePersonService personService;

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private CreativeWorkService workService;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsRepository newsRepository;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.ratings.of.movies.job.cron}")
    public void updateRatingOfMovies() {
        log.info("Job started");

        movieRepository.getIdsOfMovies().forEach(id -> {
            try {
                movieService.updateRating(id);
            } catch (Exception e) {
                log.error("Failed to update rating for movie: {}", id, e);
            }
        });

        log.info("Job finished");
    }

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.ratings.of.creativepersons.job.cron}")
    public void updateRatingOfCreativePersons() {
        log.info("Job started");

        personRepository.getIdsOfCreativePersons().forEach(id -> {
            try {
                personService.updateRating(id);
            } catch (Exception e) {
                log.error("Failed to update rating for creative person: {}", id, e);
            }
        });

        log.info("Job finished");
    }

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.ratings.of.news.job.cron}")
    public void updateRatingOfNews() {
        log.info("Job started");

        newsRepository.getIdsOfNews().forEach(id -> {
            try {
                newsService.updateRating(id);
            } catch (Exception e) {
                log.error("Failed to update rating for news: {}", id, e);
            }
        });

        log.info("Job finished");
    }

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.ratings.of.creativeworks.job.cron}")
    public void updateRatingOfCreativeWorks() {
        log.info("Job started");

        workRepository.getIdsOfCreativeWorks().forEach(id -> {
            try {
                workService.updateRating(id);
            } catch (Exception e) {
                log.error("Failed to update rating for creative work: {}", id, e);
            }
        });

        log.info("Job finished");
    }
}
