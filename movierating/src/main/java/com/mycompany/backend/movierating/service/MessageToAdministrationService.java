package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationCreateDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPutDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.repository.MessageToAdministrationRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class MessageToAdministrationService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MessageToAdministrationRepository messageRepository;

    public MessageToAdministrationReadDTO getMessage(UUID id) {
        MessageToAdministration message = repositoryHelper.getByIdRequired(MessageToAdministration.class, id);
        return translationService.translate(message, MessageToAdministrationReadDTO.class);
    }

    public PageResult<MessageToAdministrationReadDTO> getMessages(MessageToAdministrationFilter filter,
            Pageable pageable) {
        Page<MessageToAdministration> messages = messageRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(messages, MessageToAdministrationReadDTO.class);
    }

    public MessageToAdministrationReadDTO createMessage(MessageToAdministrationCreateDTO create,
            UserDetailsImpl user) {
        MessageToAdministration message = translationService.translate(create, MessageToAdministration.class);
        message.setSolved(false);
        message.setCreatedById(user.getId());
        message.setLastModifiedById(user.getId());
        message = messageRepository.save(message);
        return translationService.translate(message, MessageToAdministrationReadDTO.class);
    }

    public MessageToAdministrationReadDTO patchMessage(UUID id, MessageToAdministrationPatchDTO patch,
            UserDetailsImpl user) {

        MessageToAdministration message = repositoryHelper.getByIdRequired(MessageToAdministration.class, id);
        translationService.map(patch, message);
        message.setLastModifiedById(user.getId());

        message = messageRepository.save(message);
        return translationService.translate(message, MessageToAdministrationReadDTO.class);
    }

    public MessageToAdministrationReadDTO updateMessage(UUID id, MessageToAdministrationPutDTO put,
            UserDetailsImpl user) {

        MessageToAdministration message = repositoryHelper.getByIdRequired(MessageToAdministration.class, id);
        translationService.map(put, message);
        message.setLastModifiedById(user.getId());

        message = messageRepository.save(message);
        return translationService.translate(message, MessageToAdministrationReadDTO.class);
    }

    public void deleteMessage(UUID id) {
        messageRepository.delete(repositoryHelper.getByIdRequired(MessageToAdministration.class, id));
    }

}
