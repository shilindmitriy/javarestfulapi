package com.mycompany.backend.movierating.dto.user;

import com.mycompany.backend.movierating.domain.constants.Authority;

import lombok.Data;

@Data
public class AuthorityPatchDTO {

    private Authority authority;
}
