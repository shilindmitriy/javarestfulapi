package com.mycompany.backend.movierating.dto.movie;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;

import lombok.Data;

@Data
public class MovieFilter {

    @Size(max = 100)
    private String title;

    @DateTimeFormat(iso = ISO.DATE)
    private LocalDate releaseDateFrom;

    @DateTimeFormat(iso = ISO.DATE)
    private LocalDate releaseDateTo;

    private MovieStatus status;

    @DateTimeFormat(iso = ISO.TIME)
    private LocalTime runningTimeFrom;

    @DateTimeFormat(iso = ISO.TIME)
    private LocalTime runningTimeTo;

    private Set<Country> productionCountries = new HashSet<>();

    private Set<Genre> genres = new HashSet<>();
}
