package com.mycompany.backend.movierating.exception;

import java.util.UUID;

import com.mycompany.backend.movierating.domain.AbstractEntity;

import lombok.Getter;

@Getter
public class ImportedEntityAlreadyExistException extends Exception {

    private Class<? extends AbstractEntity> entityClass;
    private UUID entityId;

    public ImportedEntityAlreadyExistException(Class<? extends AbstractEntity> entityClass, UUID entityId,
            String message) {
        super(message);
        this.entityClass = entityClass;
        this.entityId = entityId;
    }

}
