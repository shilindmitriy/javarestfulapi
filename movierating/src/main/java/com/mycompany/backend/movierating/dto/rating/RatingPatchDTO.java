package com.mycompany.backend.movierating.dto.rating;

import lombok.Data;

@Data
public class RatingPatchDTO {

    private Integer rating;

}
