package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.security.AdminrOrReviewLikeOwner;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeCreateDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikePatchDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikeReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.ReviewLikeService;

@RestController
@RequestMapping("/api/v1/review-likes")
public class ReviewLikeController {

    @Autowired
    private ReviewLikeService reviewLikeService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public ReviewLikeReadDTO getReviewLike(@PathVariable UUID id) {
        return reviewLikeService.getReviewLike(id);
    }

    @PreAuthorize("permitAll")
    @GetMapping("/{reviewType}/{reviewId}")
    public ReviewLikesReadDTO getReviewLikes(@PathVariable ReviewType reviewType, @PathVariable UUID reviewId) {
        return reviewLikeService.getReviewLikes(reviewType, reviewId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER', 'REGISTERED_USER')")
    @PostMapping
    public ReviewLikeReadDTO createReviewLike(@RequestBody @Valid ReviewLikeCreateDTO create,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return reviewLikeService.createReviewLike(create, user);
    }

    @AdminrOrReviewLikeOwner
    @PatchMapping("/{id}")
    public ReviewLikeReadDTO patchReviewLike(@PathVariable UUID id, @RequestBody @Valid ReviewLikePatchDTO patch) {
        return reviewLikeService.patchReviewLike(patch, id);
    }

    @AdminrOrReviewLikeOwner
    @DeleteMapping("/{id}")
    public void deleteReviewLike(@PathVariable UUID id) {
        reviewLikeService.deleteReviewLike(id);
    }

}
