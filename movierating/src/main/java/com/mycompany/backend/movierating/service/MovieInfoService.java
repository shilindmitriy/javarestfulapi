package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;

@Service
public class MovieInfoService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    public List<CompanyReadDTO> getCompanies(UUID movieId) {
        return movieRepository.findCompanies(movieId);
    }

    public List<CastAndCrewReadDTO> getCastAndCrew(UUID movieId, Department department) {
        return workRepository.findCastAndCrew(movieId, department);
    }

}
