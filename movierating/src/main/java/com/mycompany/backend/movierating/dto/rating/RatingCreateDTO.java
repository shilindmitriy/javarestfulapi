package com.mycompany.backend.movierating.dto.rating;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RatingCreateDTO {

    @NotNull
    private Integer rating;

}
