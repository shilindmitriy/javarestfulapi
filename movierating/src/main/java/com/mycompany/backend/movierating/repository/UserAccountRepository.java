package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.UserAccount;

@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, UUID> {

    UserAccount findByEmail(String email);

    @Query("select case when (count(*) > 0)  then true else false end " 
            + "from UserAccount u "
            + "left join u.movieReviews mReviews on mReviews.id = :reviewId "
            + "left join u.creativeWorkReviews wReviews on wReviews.id = :reviewId " 
            + "left join u.creativityPersonReviews pReviews on pReviews.id = :reviewId "
            + "left join u.companyReviews cReviews on cReviews.id = :reviewId " 
            + "where u.email = :email and "
            + "(mReviews.id = :reviewId or wReviews.id = :reviewId or "
            + "pReviews.id = :reviewId or cReviews.id = :reviewId)")
    boolean existsByReviewIdAndEmail(UUID reviewId, String email);

    @Query("select case when (count(*) > 0)  then true else false end " 
            + "from UserAccount u "
            + "left join u.newsRatings nRatings on nRatings.id = :ratingId " 
            + "left join u.movieRatings mRatings on mRatings.id = :ratingId "
            + "left join u.creativeWorkRatings wRatings on wRatings.id = :ratingId " 
            + "left join u.creativityPersonRatings pRatings on pRatings.id = :ratingId "
            + "left join u.companyRatings cRatings on cRatings.id = :ratingId " 
            + "where u.email = :email and "
            + "(nRatings.id = :ratingId or mRatings.id = :ratingId or wRatings.id = :ratingId or "
            + "pRatings.id = :ratingId or cRatings.id = :ratingId)")
    boolean existsByRatingIdAndEmail(UUID ratingId, String email);

}
