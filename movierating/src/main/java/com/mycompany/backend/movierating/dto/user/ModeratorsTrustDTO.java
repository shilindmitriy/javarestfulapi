package com.mycompany.backend.movierating.dto.user;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ModeratorsTrustDTO {
    
    @NotNull
    private Integer rating;

}
