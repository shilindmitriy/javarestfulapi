package com.mycompany.backend.movierating.controller.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAuthority('ADMIN') or (hasAnyAuthority('MODERATOR', 'CONTENT_MANAGER', 'REGISTERED_USER') "
        + "and @belongsToCurrentUserValidator.reviewLikeBelongsToCurrentUser(#id))")
public @interface AdminrOrReviewLikeOwner {

}
