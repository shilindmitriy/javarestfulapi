package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.security.AdminOrModeratorOrRatingOwner;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.NewsRatingService;

@RestController
@RequestMapping("/api/v1/news/{newsId}/ratings")
public class NewsRatingController {

    @Autowired
    private NewsRatingService ratingService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public RatingReadDTO getRating(@PathVariable UUID newsId, @PathVariable UUID id) {
        return ratingService.getRating(newsId, id);
    }

    @PreAuthorize("permitAll")
    @GetMapping
    public SimpleRatingReadDTO getSimpleRating(@PathVariable UUID newsId) {
        return ratingService.getSimpleRating(newsId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER', 'REGISTERED_USER')")
    @PostMapping
    public RatingReadDTO createRating(@PathVariable UUID newsId, @RequestBody @Valid RatingCreateDTO create,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return ratingService.createRating(newsId, create, user);
    }

    @AdminOrModeratorOrRatingOwner
    @PatchMapping("/{id}")
    public RatingReadDTO patchRating(@PathVariable UUID newsId, @PathVariable UUID id,
            @RequestBody @Valid RatingPatchDTO patch) {
        return ratingService.patchRating(newsId, id, patch);
    }

    @AdminOrModeratorOrRatingOwner
    @PutMapping("/{id}")
    public RatingReadDTO updateRating(@PathVariable UUID newsId, @PathVariable UUID id,
            @RequestBody @Valid RatingPutDTO put) {
        return ratingService.updateRating(newsId, id, put);
    }

    @AdminOrModeratorOrRatingOwner
    @DeleteMapping("/{id}")
    public void deleteRating(@PathVariable UUID newsId, @PathVariable UUID id) {
        ratingService.deleteRating(newsId, id);
    }

}
