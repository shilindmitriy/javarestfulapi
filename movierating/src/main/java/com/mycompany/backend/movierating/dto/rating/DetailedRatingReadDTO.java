package com.mycompany.backend.movierating.dto.rating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailedRatingReadDTO {

    private Long numberOfAllVotes;

    private Double averageRating;

    private Long numberOfVotesWhereRatingEqualToOne;

    private Long numberOfVotesWhereRatingEqualToTwo;

    private Long numberOfVotesWhereRatingEqualToThree;

    private Long numberOfVotesWhereRatingEqualToFour;

    private Long numberOfVotesWhereRatingEqualToFive;

    private Long numberOfVotesWhereRatingEqualToSix;

    private Long numberOfVotesWhereRatingEqualToSeven;

    private Long numberOfVotesWhereRatingEqualToEight;

    private Long numberOfVotesWhereRatingEqualToNine;

    private Long numberOfVotesWhereRatingEqualToTen;

    private Long numberOfVotesWhereAgeLessThan18;

    private Long numberOfVotesWhereAgeBetween18And29;

    private Long numberOfVotesWhereAgeBetween30And44;

    private Long numberOfVotesWhereAgeOver45;

    private Double averageRatingWhereAgeLessThan18;

    private Double averageRatingWhereAgeBetween18And29;

    private Double averageRatingWhereAgeBetween30And44;

    private Double averageRatingWhereAgeOver45;

    private Long numberOfVotesWhereAgeLessThan18ForMales;

    private Long numberOfVotesWhereAgeBetween18And29ForMales;

    private Long numberOfVotesWhereAgeBetween30And44ForMales;

    private Long numberOfVotesWhereAgeOver45ForMales;

    private Double averageRatingWhereAgeLessThan18ForMales;

    private Double averageRatingWhereAgeBetween18And29ForMales;

    private Double averageRatingWhereAgeBetween30And44ForMales;

    private Double averageRatingWhereAgeOver45ForMales;

    private Long numberOfVotesWhereAgeLessThan18ForFemales;

    private Long numberOfVotesWhereAgeBetween18And29ForFemales;

    private Long numberOfVotesWhereAgeBetween30And44ForFemales;

    private Long numberOfVotesWhereAgeOver45ForFemales;

    private Double averageRatingWhereAgeLessThan18ForFemales;

    private Double averageRatingWhereAgeBetween18And29ForFemales;

    private Double averageRatingWhereAgeBetween30And44ForFemales;

    private Double averageRatingWhereAgeOver45ForFemales;

    private Long numberOfVotesAccordingToCountry;

    private Double averageRatingAccordingToCountry;

    private Long numberOfVotesExceptCountry;

    private Double averageRatingExceptCountry;
}
