package com.mycompany.backend.movierating.exception;

import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AlreadyExistsException extends RuntimeException {

    public AlreadyExistsException(Class resourceClass, UUID resourceId, UUID userId) {
        super(String.format("User id=%s has already rated %s id=%s.", userId, resourceClass.getSimpleName(),
                resourceId));
    }
    
    public AlreadyExistsException(String message) {
        super(message);
    }

}
