package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.documentation.ApiPageable;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationCreateDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPutDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.MessageToAdministrationService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/message-to-administrations")
public class MessageToAdministrationController {

    @Autowired
    private MessageToAdministrationService messageService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @GetMapping("/{id}")
    public MessageToAdministrationReadDTO getMessage(@PathVariable UUID id) {
        return messageService.getMessage(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @ApiPageable
    @GetMapping
    public PageResult<MessageToAdministrationReadDTO> getMessages(MessageToAdministrationFilter filter,
            @ApiIgnore Pageable pageable) {
        return messageService.getMessages(filter, pageable);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping
    public MessageToAdministrationReadDTO createMessage(@RequestBody @Valid MessageToAdministrationCreateDTO create,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return messageService.createMessage(create, user);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public MessageToAdministrationReadDTO patchMessage(@PathVariable UUID id,
            @RequestBody @Valid MessageToAdministrationPatchDTO patch, @AuthenticationPrincipal UserDetailsImpl user) {
        return messageService.patchMessage(id, patch, user);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public MessageToAdministrationReadDTO updateMessage(@PathVariable UUID id,
            @RequestBody @Valid MessageToAdministrationPutDTO put, @AuthenticationPrincipal UserDetailsImpl user) {
        return messageService.updateMessage(id, put, user);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteMessage(@PathVariable UUID id) {
        messageService.deleteMessage(id);
    }

}
