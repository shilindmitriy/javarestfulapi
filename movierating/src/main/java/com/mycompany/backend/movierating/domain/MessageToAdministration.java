package com.mycompany.backend.movierating.domain;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.EntityType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class MessageToAdministration extends Auditable {

    @Size(max = 500)
    private String misprint;

    private Integer beginIndex;

    @NotBlank
    @Size(max = 500)
    private String message;

    @NotBlank
    @Size(max = 500)
    private String url;

    private UUID entityId;

    @Enumerated(EnumType.STRING)
    private EntityType entityType;

    private String entityField;

    @Size(max = 500)
    private String note;

    @NotNull
    private UUID createdById;

    @NotNull
    private UUID lastModifiedById;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Administration administration;

    @NotNull
    private Boolean solved;

}