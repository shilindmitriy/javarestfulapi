package com.mycompany.backend.movierating.domain.constants;

public enum MovieStatus {

    RELEASED, NOT_RELEASED

}
