package com.mycompany.backend.movierating.dto.rating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleRatingReadDTO {

    private Long like;

    private Long dislike;

}
