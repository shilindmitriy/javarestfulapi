package com.mycompany.backend.movierating.dto.review;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.mycompany.backend.movierating.domain.constants.ReviewType;

import lombok.Data;

@Data
public class ReviewConfirmationRequest {

    @NotNull
    private ReviewType reviewType;

    @NotNull
    private UUID parentId;

    @NotNull
    private UUID reviewId;
}
