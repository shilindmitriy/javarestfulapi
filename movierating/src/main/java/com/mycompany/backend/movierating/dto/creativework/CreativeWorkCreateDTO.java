package com.mycompany.backend.movierating.dto.creativework;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Department;

import lombok.Data;

@Data
public class CreativeWorkCreateDTO {

    @Size(max = 100)
    private String character;

    @NotNull
    private Department department;

    @NotBlank
    private String post;

    @Size(max = 1500)
    private String notes;

    @NotNull
    private UUID personId;
}
