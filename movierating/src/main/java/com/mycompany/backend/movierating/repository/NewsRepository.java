package com.mycompany.backend.movierating.repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.News;

@Repository
public interface NewsRepository extends CrudRepository<News, UUID> {

    List<News> findAllByAuthorId(UUID authorId);

    @Query("select n from News n where n.authorId = ?1 and n.createdAt >= ?2 and n.createdAt < ?3 "
            + "order by n.createdAt desc")
    List<News> findAllByAuthorIdAndDate(UUID authorId, Instant dateFrom, Instant dateTo);

    @Query("select n.id from News n")
    Stream<UUID> getIdsOfNews();
}
