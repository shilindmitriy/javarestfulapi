package com.mycompany.backend.movierating.dto.user;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Data;

@Data
public class UserAccountPatchDTO {

    @Size(max = 30)
    private String firstName;

    @Size(max = 30)
    private String lastName;

    private Gender gender;

    @Past
    private LocalDate birthday;

    @Size(max = 30)
    private String nickName;

    private Country country;

    @Email
    @Size(max = 50)
    private String email;

    @Size(max = 100)
    private String password;
}