package com.mycompany.backend.movierating.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class NewsRating extends Rating {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "news_id", referencedColumnName = "id")
    private News parentEntity;
}
