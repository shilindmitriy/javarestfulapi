package com.mycompany.backend.movierating.dto.productioncompany;

import java.time.Instant;
import java.util.UUID;

import lombok.Data;

@Data
public class ProductionCompanyReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String name;

    private String history;

}
