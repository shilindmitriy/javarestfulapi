package com.mycompany.backend.movierating.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;

public class MovieRepositoryCustomImpl implements MovieRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Movie> findByFilter(MovieFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(em);
        qb.append("select distinct m from Movie m join m.productionCountries country join m.genres genre where 1=1");
        qb.append("and m.title = :title", filter.getTitle());
        qb.append("and m.releaseDate >= :releaseDateFrom", filter.getReleaseDateFrom());
        qb.append("and m.releaseDate < :releaseDateTo", filter.getReleaseDateTo());
        qb.append("and m.status = :status", filter.getStatus());
        qb.append("and m.runningTime >= :runningTimeFrom", filter.getRunningTimeFrom());
        qb.append("and m.runningTime < :runningTimeTo", filter.getRunningTimeTo());
        qb.appendIn("and country in (:productionCountries)", filter.getProductionCountries());
        qb.appendIn("and genre in (:genres)", filter.getGenres());
        
        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}