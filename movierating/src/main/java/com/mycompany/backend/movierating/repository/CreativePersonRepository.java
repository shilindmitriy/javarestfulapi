package com.mycompany.backend.movierating.repository;

import java.time.LocalDate;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativePerson;

@Repository
public interface CreativePersonRepository extends CrudRepository<CreativePerson, UUID> {

    CreativePerson findByNameAndBorn(String name, LocalDate born);

    @Query("select p.id from CreativePerson p")
    Stream<UUID> getIdsOfCreativePersons();

    @Query("select avg(m.averageRating) from CreativePerson p join p.movies m where p.id = :id")
    Double getAverageMoviesRating(UUID id);

    @Query("select avg(w.averageRating) from CreativePerson p join p.creativeWorks w where p.id = :id")
    Double getAverageCreativeWorksRating(UUID id);

}