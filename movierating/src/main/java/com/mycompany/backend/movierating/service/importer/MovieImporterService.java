package com.mycompany.backend.movierating.service.importer;

import java.time.LocalTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.client.TheMovieDbClient;
import com.mycompany.backend.movierating.client.themoviedb.dto.CastReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CountryReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditsReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CrewReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.GenreReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.PersonReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.domain.AbstractEntity;
import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.ExternalSystemImport;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.exception.ImportedEntityAlreadyExistException;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private CreativePersonRepository personRepository;

    @Autowired
    private CreativeWorkRepository workRepository;

    @Transactional
    public UUID importMovieFromWebPage(String movieExternalId) {
        UUID id = null;
        try {
            id = importMovie(movieExternalId);
        } catch (ImportedEntityAlreadyExistException | ImportAlreadyPerformedException e) {
            throw new AlreadyExistsException(
                    String.format("Can't import movie id=%s : %s", movieExternalId, e.getMessage()));
        }
        return id;
    }

    @Transactional
    public UUID importMovie(String movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        log.info("Importing movie with external id={}", movieExternalId);

        externalSystemImportService.validateNotImported(Movie.class, movieExternalId);

        MovieReadDTO read = movieDbClient.getMovie(movieExternalId);

        Movie existingMovie = movieRepository.findByTitle(read.getTitle());
        if (existingMovie != null) {
            throw new ImportedEntityAlreadyExistException(Movie.class, existingMovie.getId(),
                    "Movie with name=" + read.getTitle() + " already exist.");
        }

        // get movie
        Movie movie = new Movie();
        movie.setTitle(read.getTitle());
        movie.setReleaseDate(read.getReleaseDate());
        movie.setStatus(MovieStatus.valueOf(toUpperCaseSnakeCase(read.getStatus())));
        if (read.getRuntime() != null) {
            movie.setRunningTime(LocalTime.ofSecondOfDay(read.getRuntime() * 60));
        }
        movie.setStoryline(read.getOverview());
        for (CountryReadDTO c : read.getProductionCountries()) {
            movie.getProductionCountries().add(Country.valueOf(toUpperCaseSnakeCase(c.getName())));
        }
        for (GenreReadDTO g : read.getGenres()) {
            movie.getGenres().add(Genre.valueOf(toUpperCaseSnakeCase(g.getName())));
        }
        for (ProductionCompanyReadDTO c : read.getProductionCompanies()) {
            movie.getProductionCompanies().add(importCompany(c));
        }
        movie = movieRepository.save(movie);

        // get movie's creative_persons and creative_works
        CreditsReadDTO credits = movieDbClient.getCredits(movieExternalId);
        for (CrewReadDTO c : credits.getCrew()) {
            CreativeWork work = importCrew(c);
            addPersonAndMovieToCreativeWork(movie, work, c.getId(), c.getCreditId());
        }
        for (CastReadDTO c : credits.getCast()) {
            CreativeWork work = importCast(c);
            addPersonAndMovieToCreativeWork(movie, work, c.getId(), c.getCreditId());
        }

        externalSystemImportService.createExternalSystemImport(movie, movieExternalId);
        log.info("Movie with external id={} imported", movieExternalId);
        return movie.getId();
    }

    private ProductionCompany importCompany(ProductionCompanyReadDTO c) {

        // Was there an import before?
        ProductionCompany company = getImportedEntity(ProductionCompany.class, c.getId());
        if (company == null) {
            // Find in repository
            company = companyRepository.findByName(c.getName());
            // if ProductionCompany dos't exist, then create new ProductionCompany
            if (company == null) {
                company = new ProductionCompany();
                company.setName(c.getName());
                company = companyRepository.save(company);
                createOrUpdateExternalSystemImport(company, c.getId());
            }
        }

        return company;
    }

    private CreativePerson importPerson(PersonReadDTO p) {

        // Was there an import before?
        CreativePerson person = getImportedEntity(CreativePerson.class, p.getId());
        if (person == null) {
            // Find in repository
            person = personRepository.findByNameAndBorn(p.getName(), p.getBirthday());
            // if CreativePerson dos't exist, then create new CreativePerson
            if (person == null) {
                person = new CreativePerson();
                person.setName(p.getName());
                person.setBiography(p.getBiography());
                person.setBorn(p.getBirthday());
                person.setDeath(p.getDeathday());
                person = personRepository.save(person);
                createOrUpdateExternalSystemImport(person, p.getId());
            }
        }

        return person;
    }

    private CreativeWork importCrew(CrewReadDTO c) {

        CreativeWork work = new CreativeWork();
        work.setDepartment(Department.valueOf(toUpperCaseSnakeCase(c.getDepartment())));
        work.setPost(c.getJob());

        return work;
    }

    private CreativeWork importCast(CastReadDTO c) {

        CreditReadDTO credit = movieDbClient.getCredit(c.getCreditId());
        CreativeWork work = new CreativeWork();
        work.setDepartment(Department.valueOf(toUpperCaseSnakeCase(credit.getDepartment())));
        work.setPost(credit.getJob());
        work.setCharacter(c.getCharacter());

        return work;
    }

    private void addPersonAndMovieToCreativeWork(Movie movie, CreativeWork work, String personId, String creditId) {
        CreativePerson person = importPerson(movieDbClient.getPerson(personId));
        movie.getFilmCrew().add(person);
        work.setCreativePerson(person);
        work.setMovie(movie);
        work = workRepository.save(work);
        externalSystemImportService.createExternalSystemImport(work, creditId);
    }

    private <T extends AbstractEntity> T getImportedEntity(Class<T> entityClass, String idInExternalSystem) {

        // try to get ExternalSystemImport
        ExternalSystemImport esi = externalSystemImportService.getExternalSystemImport(entityClass, idInExternalSystem);

        T entity = null;
        // if ExternalSystemImport exists, then get a entity by entityId.
        if (esi != null) {
            entity = repositoryHelper.getById(entityClass, esi.getEntityId());
        }

        return entity;
    }

    private <T extends AbstractEntity> void createOrUpdateExternalSystemImport(T entity, String idInExternalSystem) {
        ExternalSystemImport esi = externalSystemImportService.getExternalSystemImport(entity.getClass(),
                idInExternalSystem);
        if (esi == null) {
            externalSystemImportService.createExternalSystemImport(entity, idInExternalSystem);
        } else {
            externalSystemImportService.updateExternalSystemImport(entity, esi);
        }
    }

    private String toUpperCaseSnakeCase(String str) {
        return str.strip().toUpperCase().replace(" ", "_").replace("-", "_").replace("&", "AND").replace("'", "_");
    }
}
