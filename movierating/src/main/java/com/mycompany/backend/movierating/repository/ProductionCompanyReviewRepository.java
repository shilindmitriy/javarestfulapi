package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.ProductionCompanyReview;

@Repository
public interface ProductionCompanyReviewRepository extends ReviewRepository<ProductionCompanyReview> {

}
