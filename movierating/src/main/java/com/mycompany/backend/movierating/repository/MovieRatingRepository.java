package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.MovieRating;

@Repository
public interface MovieRatingRepository extends RatingRepository<MovieRating> {

}
