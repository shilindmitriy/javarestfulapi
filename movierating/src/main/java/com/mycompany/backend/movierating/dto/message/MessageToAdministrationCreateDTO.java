package com.mycompany.backend.movierating.dto.message;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.EntityType;

import lombok.Data;

@Data
public class MessageToAdministrationCreateDTO {

    @Size(max = 500)
    private String misprint;

    private Integer beginIndex;

    @NotBlank
    @Size(max = 500)
    private String message;

    @NotBlank
    @Size(max = 500)
    private String url;

    private UUID entityId;

    private EntityType entityType;

    private String entityField;

    @NotNull
    private Administration administration;

}
