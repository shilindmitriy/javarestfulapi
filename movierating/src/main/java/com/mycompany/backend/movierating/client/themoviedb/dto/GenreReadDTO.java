package com.mycompany.backend.movierating.client.themoviedb.dto;

import lombok.Data;

@Data
public class GenreReadDTO {

    String name;
}
