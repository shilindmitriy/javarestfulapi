package com.mycompany.backend.movierating.domain.constants;

public enum Gender {
    MALE, FEMALE
}
