package com.mycompany.backend.movierating.domain.constants;

public enum ImportedEntityType {

    MOVIE, CREATIVE_PERSON, CREATIVE_WORK, PRODUCTION_COMPANY

}
