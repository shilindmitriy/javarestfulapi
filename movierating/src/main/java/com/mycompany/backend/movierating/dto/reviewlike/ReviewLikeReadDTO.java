package com.mycompany.backend.movierating.dto.reviewlike;

import java.time.Instant;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.ReviewType;

import lombok.Data;

@Data
public class ReviewLikeReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Boolean like;

    private ReviewType reviewType;

    private UUID reviewId;

    private UUID userId;

}
