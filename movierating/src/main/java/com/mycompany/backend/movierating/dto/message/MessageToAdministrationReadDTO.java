package com.mycompany.backend.movierating.dto.message;

import java.time.Instant;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.EntityType;

import lombok.Data;

@Data
public class MessageToAdministrationReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String misprint;

    private Integer beginIndex;

    private String message;

    private String url;

    private UUID entityId;

    private EntityType entityType;

    private String entityField;

    private String note;

    private UUID createdById;

    private UUID lastModifiedById;

    private Administration administration;

    private Boolean solved;

}
