package com.mycompany.backend.movierating.dto.rating;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Data;

@Data
public class RatingPutDTO {

    @NotNull
    private Integer rating;

    @NotNull
    @Positive
    @Max(value = 100)
    private Integer age;

    @NotNull
    private Gender gender;

    @NotNull
    private Country countryUser;

}
