package com.mycompany.backend.movierating.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Department;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class CreativeWork extends Auditable {

    // if a person isn't an actor then null
    @Size(max = 100)
    private String character;

    // Acting, Art, Camera and etc.
    @NotNull
    @Enumerated(EnumType.STRING)
    private Department department;

    // Actor, Director, Producer, Camera operator and etc.
    @NotBlank
    private String post;

    // Short description
    @Size(max = 1500)
    private String notes;
    
    private Double averageRating;

    private Long numberOfAllVotes;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<CreativeWorkRating> creativeWorkRatings = new ArrayList<>();

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<CreativeWorkReview> creativeWorkReviews = new ArrayList<>();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "movie_id", referencedColumnName = "id")
    private Movie movie;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "creative_person_id", referencedColumnName = "id")
    private CreativePerson creativePerson;

}