package com.mycompany.backend.movierating.dto.movieinfo;

import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Department;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CastAndCrewReadDTO {

    private String name;

    private String character;

    private Department department;

    private String post;

    private UUID personId;

    private UUID workId;
}
