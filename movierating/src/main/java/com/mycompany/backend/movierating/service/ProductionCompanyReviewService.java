package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyReviewRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class ProductionCompanyReviewService {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ProductionCompanyReviewRepository reviewRepository;

    public ReviewReadDTO getReview(UUID reviewId, UUID id) {
        ProductionCompanyReview review = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyReview.class, id,
                reviewId);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public PageResult<ReviewReadDTO> getReviews(UUID reviewId, UserDetailsImpl user, Pageable pageable) {
        UUID userId = null;
        if (user != null) {
            userId = user.getId();
        }
        Page<ReviewReadDTO> reviews = reviewRepository.findAllByParentIdAndEnabledOrUserId(reviewId, userId, true,
                pageable);
        return translationService.toPageResult(reviews);
    }

    public ReviewReadDTO createReview(UUID reviewId, ReviewCreateDTO create, UserDetailsImpl user) {

        if (user.getModeratorsTrust() < 0) {
            throw new AccessDeniedException("The user with id=" + user.getId() + " is not allowed to write a review.");
        }

        ProductionCompanyReview review = translationService.translate(create, ProductionCompanyReview.class);
        review.setParentEntity(repositoryHelper.getReferenceIfExist(ProductionCompany.class, reviewId));
        review.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        if (user.getModeratorsTrust() > 99) {
            review.setEnabled(true);
        } else {
            review.setEnabled(false);
        }

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchReview(UUID reviewId, UUID id, ReviewPatchDTO patch) {

        ProductionCompanyReview review = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyReview.class, id,
                reviewId);
        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateReview(UUID reviewId, UUID id, ReviewPutDTO put) {

        ProductionCompanyReview review = repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyReview.class, id,
                reviewId);
        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    @Transactional
    public void deleteReview(UUID reviewId, UUID id) {
        reviewLikeRepository.deleteByReviewTypeAndReviewId(ReviewType.PRODUCTION_COMPANY_REVIEW, id);
        reviewRepository
                .delete(repositoryHelper.getByParentIdAndIdRequired(ProductionCompanyReview.class, id, reviewId));
    }

}
