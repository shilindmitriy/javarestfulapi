package com.mycompany.backend.movierating.dto.rating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShortRatingReadDTO {

    private Long numberOfAllVotes;

    private Double averageRating;
}
