package com.mycompany.backend.movierating.dto.message;

import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Administration;

import lombok.Data;

@Data
public class MessageToAdministrationPatchDTO {

    @Size(max = 500)
    private String note;

    private Administration administration;

    private Boolean solved;

}
