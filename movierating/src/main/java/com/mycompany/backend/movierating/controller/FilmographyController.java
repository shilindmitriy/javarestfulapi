package com.mycompany.backend.movierating.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO;
import com.mycompany.backend.movierating.service.FilmographyService;

@RestController
@RequestMapping("/api/v1/creative-persons/{personId}/filmography")
public class FilmographyController {

    @Autowired
    private FilmographyService filmographyService;

    @PreAuthorize("permitAll")
    @GetMapping("/{department}")
    public List<FilmographyReadDTO> getFilmography(@PathVariable UUID personId, @PathVariable Department department) {
        return filmographyService.getFilmography(personId, department);
    }

}
