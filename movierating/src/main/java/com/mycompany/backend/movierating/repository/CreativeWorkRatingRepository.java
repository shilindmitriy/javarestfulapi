package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativeWorkRating;

@Repository
public interface CreativeWorkRatingRepository extends RatingRepository<CreativeWorkRating> {

}
