package com.mycompany.backend.movierating.dto.user;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Data;

@Data
public class UserAccountPutDTO {

    @Size(max = 30)
    private String firstName;

    @Size(max = 30)
    private String lastName;

    @NotNull
    private Gender gender;

    @NotNull
    @Past
    private LocalDate birthday;

    @NotBlank
    @Size(max = 30)
    private String nickName;

    @NotNull
    private Country country;

    @NotBlank
    @Email
    @Size(max = 50)
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;
}
