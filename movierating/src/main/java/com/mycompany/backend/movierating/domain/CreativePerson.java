package com.mycompany.backend.movierating.domain;

import java.sql.Blob;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class CreativePerson extends Auditable {

    @NotBlank
    @Size(max = 100)
    private String name;

    @Past
    private LocalDate born;

    @Past
    private LocalDate death;

    private Blob photo;

    @Size(max = 10000)
    private String biography;

    private Double averageRating;

    private Long numberOfAllVotes;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<CreativePersonRating> creativePersonRatings = new ArrayList<>();

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<CreativePersonReview> creativePersonReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "creativePerson")
    private List<CreativeWork> creativeWorks = new ArrayList<>();

    @ManyToMany(mappedBy = "filmCrew")
    private List<Movie> movies = new ArrayList<>();

}