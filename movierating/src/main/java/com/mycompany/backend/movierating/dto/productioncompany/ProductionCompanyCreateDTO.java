package com.mycompany.backend.movierating.dto.productioncompany;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ProductionCompanyCreateDTO {

    @NotBlank
    @Size(max = 100)
    private String name;

    @Size(max = 10000)
    private String history;

}
