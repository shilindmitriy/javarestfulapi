package com.mycompany.backend.movierating.dto.review;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ReviewPatchDTO {

    @Size(max = 1500)
    private String text;
}
