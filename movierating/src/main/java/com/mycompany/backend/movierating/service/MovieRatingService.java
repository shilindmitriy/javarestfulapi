package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.exception.InappropriateEntityException;
import com.mycompany.backend.movierating.repository.MovieRatingRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class MovieRatingService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingReadDTO getRating(UUID movieId, UUID id) {
        MovieRating rating = repositoryHelper.getByParentIdAndIdRequired(MovieRating.class, id, movieId);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public ShortRatingReadDTO getShortRating(UUID movieId) {
        return ratingRepository.retrieveShortRating(movieId);
    }

    public DetailedRatingReadDTO getDetailedRating(UUID movieId, Country country) {
        if (country == null) {
            country = Country.UNITED_STATES_OF_AMERICA;
        }
        return ratingRepository.retrieveDetailedRating(movieId, country);
    }

    public RatingReadDTO createRating(UUID movieId, RatingCreateDTO create, UserDetailsImpl user) {

        if (ratingRepository.existsByParentIdAndUserId(movieId, user.getId())) {
            throw new AlreadyExistsException(Movie.class, movieId, user.getId());
        }        

        if (movieRepository.existsById(movieId)
                && movieRepository.getMovieStatusOfMovie(movieId).equals(MovieStatus.NOT_RELEASED)) {
            throw new InappropriateEntityException(
                    "Movie with id=" + movieId + " connot be rated. It has a status " + MovieStatus.NOT_RELEASED);
        }

        MovieRating rating = (MovieRating) translationService.translate(create, MovieRating.class);
        rating.setGender(user.getGender());
        rating.setCountryUser(user.getCountry());
        rating.setAge(Period.between(user.getBirthday(), LocalDate.now()).getYears());
        rating.setParentEntity(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        rating.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO patchRating(UUID movieId, UUID id, RatingPatchDTO patch) {

        MovieRating rating = repositoryHelper.getByParentIdAndIdRequired(MovieRating.class, id, movieId);
        translationService.map(patch, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO updateRating(UUID movieId, UUID id, RatingPutDTO put) {

        MovieRating rating = repositoryHelper.getByParentIdAndIdRequired(MovieRating.class, id, movieId);
        translationService.map(put, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public void deleteRating(UUID movieId, UUID id) {
        ratingRepository.delete(repositoryHelper.getByParentIdAndIdRequired(MovieRating.class, id, movieId));
    }
}
