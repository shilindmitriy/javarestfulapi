package com.mycompany.backend.movierating.security;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.UserAccountRepository;

@Component
public class BelongsToCurrentUserValidator {

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private ReviewLikeRepository reviewtLikeRepository;

    @Autowired
    private AuthenticationResolver authenticationResolver;

    public boolean reviewBelongsToCurrentUser(UUID reviewId) {
        Authentication authentication = authenticationResolver.getCurrentAuthentication();
        return userRepository.existsByReviewIdAndEmail(reviewId, authentication.getName());
    }

    public boolean ratingBelongsToCurrentUser(UUID ratingId) {
        Authentication authentication = authenticationResolver.getCurrentAuthentication();
        return userRepository.existsByRatingIdAndEmail(ratingId, authentication.getName());
    }

    public boolean reviewLikeBelongsToCurrentUser(UUID reviewLikeId) {
        Authentication authentication = authenticationResolver.getCurrentAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        return reviewtLikeRepository.existsByIdAndUserId(reviewLikeId, user.getId());
    }
}
