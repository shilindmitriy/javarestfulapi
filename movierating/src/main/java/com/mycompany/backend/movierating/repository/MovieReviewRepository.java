package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.MovieReview;

@Repository
public interface MovieReviewRepository extends ReviewRepository<MovieReview> {

}
