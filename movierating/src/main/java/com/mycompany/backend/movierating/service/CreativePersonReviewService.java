package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.CreativePersonReviewRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class CreativePersonReviewService {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private CreativePersonReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewReadDTO getReview(UUID personId, UUID id) {
        CreativePersonReview review = repositoryHelper.getByParentIdAndIdRequired(CreativePersonReview.class, id,
                personId);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public PageResult<ReviewReadDTO> getReviews(UUID personId, UserDetailsImpl user, Pageable pageable) {
        UUID userId = null;
        if (user != null) {
            userId = user.getId();
        }
        Page<ReviewReadDTO> reviews = reviewRepository.findAllByParentIdAndEnabledOrUserId(personId, userId, true,
                pageable);
        return translationService.toPageResult(reviews);
    }

    public ReviewReadDTO createReview(UUID personId, ReviewCreateDTO create, UserDetailsImpl user) {

        if (user.getModeratorsTrust() < 0) {
            throw new AccessDeniedException("The user with id=" + user.getId() + " is not allowed to write a review.");
        }

        CreativePersonReview review = translationService.translate(create, CreativePersonReview.class);
        review.setParentEntity(repositoryHelper.getReferenceIfExist(CreativePerson.class, personId));
        review.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        if (user.getModeratorsTrust() > 99) {
            review.setEnabled(true);
        } else {
            review.setEnabled(false);
        }

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchReview(UUID personId, UUID id, ReviewPatchDTO patch) {

        CreativePersonReview review = repositoryHelper.getByParentIdAndIdRequired(CreativePersonReview.class, id,
                personId);
        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateReview(UUID personId, UUID id, ReviewPutDTO put) {

        CreativePersonReview review = repositoryHelper.getByParentIdAndIdRequired(CreativePersonReview.class, id,
                personId);
        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    @Transactional
    public void deleteReview(UUID personId, UUID id) {
        reviewLikeRepository.deleteByReviewTypeAndReviewId(ReviewType.CREATIVE_PERSON_REVIEW, id);
        reviewRepository.delete(repositoryHelper.getByParentIdAndIdRequired(CreativePersonReview.class, id, personId));
    }
}
