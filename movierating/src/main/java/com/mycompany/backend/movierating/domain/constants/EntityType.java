package com.mycompany.backend.movierating.domain.constants;

import com.mycompany.backend.movierating.domain.*;

public enum EntityType {
    CREATIVE_PERSON_REVIEW(CreativePersonReview.class), CREATIVE_PERSON(CreativePerson.class),
    CREATIVE_WORK_REVIEW(CreativeWorkReview.class), MOVIE_REVIEW(MovieReview.class), MOVIE(Movie.class),
    NEWS(News.class), PRODUCTION_COMPANY_REVIEW(ProductionCompanyReview.class),
    PRODUCTION_COMPANY(ProductionCompany.class);

    private final Class<?> entityClass;

    EntityType(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }
}
