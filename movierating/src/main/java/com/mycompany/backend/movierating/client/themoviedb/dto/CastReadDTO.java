package com.mycompany.backend.movierating.client.themoviedb.dto;

import lombok.Data;

@Data
public class CastReadDTO {

    private String creditId;

    private String character;

    // person_id
    private String id;

}
