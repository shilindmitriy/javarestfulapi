package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.documentation.ApiPageable;
import com.mycompany.backend.movierating.controller.security.Moderator;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewConfirmationRequest;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.dto.user.ModeratorsTrustDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.service.ReviewService;
import com.mycompany.backend.movierating.service.UserService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/moderators")
public class ModeratorController {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private UserService userService;

    @ApiPageable
    @Moderator
    @GetMapping("/{moderatorId}/reviews-type/{reviewType}")
    public PageResult<ReviewReadDTO> getReviews(@PathVariable ReviewType reviewType, @ApiIgnore Pageable pageable) {
        return reviewService.getReviews(reviewType, pageable);
    }

    @Moderator
    @PostMapping("/{moderatorId}/confirmed-review")
    public Boolean confirmReview(@RequestBody @Valid ReviewConfirmationRequest confirmationRequest) {
        return reviewService.enableReview(confirmationRequest);
    }

    @Moderator
    @PostMapping("/{moderatorId}/users/{userId}/moderators-trust")
    public UserAccountReadDTO addModeratorsTrust(@PathVariable UUID userId,
            @RequestBody @Valid ModeratorsTrustDTO moderatorsTrust) {
        return userService.addModeratorsTrust(userId, moderatorsTrust);
    }

    @Moderator
    @PatchMapping("/{moderatorId}/users/{userId}/banned-user")
    public UserAccountReadDTO banUser(@PathVariable UUID userId) {
        return userService.banUser(userId);
    }

}
