package com.mycompany.backend.movierating.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.backend.movierating.client.themoviedb.TheMovieDbClientConfig;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.CreditsReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.MoviesPageDTO;
import com.mycompany.backend.movierating.client.themoviedb.dto.PersonReadDTO;

@FeignClient(value = "api.themoviedb.org", url = "${themoviedb.api.url}", configuration = TheMovieDbClientConfig.class)
public interface TheMovieDbClient {

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovieWhithLanguage(@PathVariable("movieId") String movieId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/top_rated")
    MoviesPageDTO getTopRatedMovies();

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/credits")
    CreditsReadDTO getCredits(@PathVariable("movieId") String movieId);
    
    @RequestMapping(method = RequestMethod.GET, value = "/credit/{creditId}")
    CreditReadDTO getCredit(@PathVariable("creditId") String creditId);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") String personId);
    
}
