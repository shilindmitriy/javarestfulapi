package com.mycompany.backend.movierating.dto.reviewlike;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewLikesReadDTO {
    
    private Long like;

    private Long dislike;

}
