package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.NewsRating;

@Repository
public interface NewsRatingRepository extends RatingRepository<NewsRating> {

}
