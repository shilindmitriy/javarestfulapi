package com.mycompany.backend.movierating.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.mycompany.backend.movierating.event.ReviewEnabledEvent;
import com.mycompany.backend.movierating.service.UserNotificationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NotifyUserThatApprovedReviewListener {

    @Autowired
    UserNotificationService userNotificationService;

    @Async
    @EventListener(condition = "#event.enabled == true")
    public void onEvent(ReviewEnabledEvent event) {
        log.info("handling {}", event);
        userNotificationService.notifyUserReviewIsEnabled(event.getReviewId());
    }

}
