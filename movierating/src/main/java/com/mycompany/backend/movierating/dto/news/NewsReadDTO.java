package com.mycompany.backend.movierating.dto.news;

import java.time.Instant;
import java.util.UUID;

import lombok.Data;

@Data
public class NewsReadDTO {

    private UUID id;

    private UUID authorId;

    private Instant createdAt;

    private Instant updatedAt;

    private String urlPhoto;

    private String text;

    private Long like;

    private Long dislike;

}
