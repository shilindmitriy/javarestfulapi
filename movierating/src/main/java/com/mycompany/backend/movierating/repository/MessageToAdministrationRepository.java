package com.mycompany.backend.movierating.repository;

import java.util.List;
import java.util.UUID;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.MessageToAdministration;

@Repository
public interface MessageToAdministrationRepository
        extends CrudRepository<MessageToAdministration, UUID>, MessageToAdministrationRepositoryCustom {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select m from MessageToAdministration m where m.url = :url and m.solved = false "
            + "and m.misprint in (:misprints)")
    List<MessageToAdministration> getMessagesForMessagesProcessing(String url, List<String> misprints);

    @Lock(LockModeType.NONE)
    @Query("select m from MessageToAdministration m where m.id in (:ids)")
    List<MessageToAdministration> getSolvedMessagesForMessagesProcessing(List<UUID> ids);

}
