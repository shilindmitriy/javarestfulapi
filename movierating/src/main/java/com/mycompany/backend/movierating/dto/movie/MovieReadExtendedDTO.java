package com.mycompany.backend.movierating.dto.movie;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;

import lombok.Data;

@Data
public class MovieReadExtendedDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String urlPoster;

    private String title;

    private LocalDate releaseDate;

    private MovieStatus status;

    private LocalTime runningTime;

    private String storyline;

    private Set<Country> productionCountries = new HashSet<>();

    private Set<Genre> genres = new HashSet<>();

    private ShortRatingReadDTO rating;
}
