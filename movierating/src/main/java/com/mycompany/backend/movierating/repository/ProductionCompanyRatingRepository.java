package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.ProductionCompanyRating;

@Repository
public interface ProductionCompanyRatingRepository extends RatingRepository<ProductionCompanyRating> {

}
