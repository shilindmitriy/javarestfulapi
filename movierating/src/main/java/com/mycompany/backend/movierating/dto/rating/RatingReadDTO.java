package com.mycompany.backend.movierating.dto.rating;

import java.time.Instant;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Data;

@Data
public class RatingReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Integer rating;

    private Integer age;

    private Gender gender;

    private Country countryUser;

    private UUID userId;

}
