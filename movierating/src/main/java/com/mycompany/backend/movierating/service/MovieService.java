package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.movie.MovieCreateDTO;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;
import com.mycompany.backend.movierating.dto.movie.MovieReadExtendedDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePutDTO;
import com.mycompany.backend.movierating.dto.movie.MovieReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.exception.LinkDuplicatedException;
import com.mycompany.backend.movierating.repository.MovieRatingRepository;
import com.mycompany.backend.movierating.repository.MovieRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;
    
    @Autowired
    private MovieRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieReadExtendedDTO getMovieExtended(UUID id) {
        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        MovieReadExtendedDTO dtoExt = translationService.translate(movie, MovieReadExtendedDTO.class);
        dtoExt.setRating(ratingRepository.retrieveShortRating(id));
        return dtoExt;
    }

    public PageResult<MovieReadDTO> getMovies(MovieFilter filter, Pageable pageable) {
        Page<Movie> movies = movieRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(movies, MovieReadDTO.class);
    }

    @Transactional
    public List<ProductionCompanyReadDTO> addCompanyToMovie(UUID id, UUID companyId) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        ProductionCompany company = repositoryHelper.getByIdRequired(ProductionCompany.class, companyId);
        if (movie.getProductionCompanies().stream().anyMatch(c -> c.getId().equals(companyId))) {
            throw new LinkDuplicatedException(
                    String.format("Movie %s already has ProductionCompany %s", id, companyId));
        }

        movie.getProductionCompanies().add(company);
        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getProductionCompanies(), ProductionCompanyReadDTO.class);
    }

    @Transactional
    public List<CreativePersonReadDTO> addCreativePersonToMovie(UUID id, UUID personId) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        CreativePerson person = repositoryHelper.getByIdRequired(CreativePerson.class, personId);
        if (movie.getFilmCrew().stream().anyMatch(c -> c.getId().equals(personId))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has CreativePerson %s", id, personId));
        }

        movie.getFilmCrew().add(person);
        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getFilmCrew(), CreativePersonReadDTO.class);
    }

    public MovieReadDTO createMovie(MovieCreateDTO create) {
        Movie movie = translationService.translate(create, Movie.class);
        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO patchMovie(UUID id, MoviePatchDTO putch) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        translationService.map(putch, movie);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO updateMovie(UUID id, MoviePutDTO put) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        translationService.map(put, movie);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public void deleteMovie(UUID id) {
        movieRepository.delete(repositoryHelper.getByIdRequired(Movie.class, id));
    }

    @Transactional
    public List<ProductionCompanyReadDTO> removeCompanyFromMovie(UUID id, UUID companyId) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);

        boolean removed = movie.getProductionCompanies().removeIf(c -> c.getId().equals(companyId));
        if (!removed) {
            throw new EntityNotFoundException(String.format("Movie %s has no ProductionCompany %s", id, companyId));
        }

        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getProductionCompanies(), ProductionCompanyReadDTO.class);
    }

    @Transactional
    public List<CreativePersonReadDTO> removeCreativePersonFromMovie(UUID id, UUID personId) {

        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);

        boolean removed = movie.getFilmCrew().removeIf(c -> c.getId().equals(personId));
        if (!removed) {
            throw new EntityNotFoundException(String.format("Movie %s has no CreativePerson %s", id, personId));
        }

        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getFilmCrew(), CreativePersonReadDTO.class);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRating(UUID id) {
        ShortRatingReadDTO shortRating = ratingRepository.retrieveShortRating(id);
        Movie movie = repositoryHelper.getByIdRequired(Movie.class, id);
        log.info(
                "Setting new average rating and number of all votes for movie: {}. "
                        + "Old average rating: {}, new average rating: {}. "
                        + "Old number of all votes: {}, new number of all votes: {}.",
                id, movie.getAverageRating(), shortRating.getAverageRating(), movie.getNumberOfAllVotes(),
                shortRating.getNumberOfAllVotes());
        movie.setAverageRating(shortRating.getAverageRating());
        movie.setNumberOfAllVotes(shortRating.getNumberOfAllVotes());
        movieRepository.save(movie);
    }
}
