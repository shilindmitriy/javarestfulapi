package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyCreateDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPatchDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPutDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.repository.ProductionCompanyRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

@Service
public class ProductionCompanyService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private ProductionCompanyRepository companyRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ProductionCompanyReadDTO getProductionCompany(UUID id) {
        ProductionCompany company = repositoryHelper.getByIdRequired(ProductionCompany.class, id);
        return translationService.translate(company, ProductionCompanyReadDTO.class);
    }

    public ProductionCompanyReadDTO createProductionCompany(ProductionCompanyCreateDTO create) {
        ProductionCompany company = translationService.translate(create, ProductionCompany.class);
        company = companyRepository.save(company);
        return translationService.translate(company, ProductionCompanyReadDTO.class);
    }

    public ProductionCompanyReadDTO patchProductionCompany(UUID id, ProductionCompanyPatchDTO patch) {

        ProductionCompany company = repositoryHelper.getByIdRequired(ProductionCompany.class, id);
        translationService.map(patch, company);

        company = companyRepository.save(company);
        return translationService.translate(company, ProductionCompanyReadDTO.class);
    }

    public ProductionCompanyReadDTO updateProductionCompany(UUID id, ProductionCompanyPutDTO put) {

        ProductionCompany company = repositoryHelper.getByIdRequired(ProductionCompany.class, id);
        translationService.map(put, company);

        company = companyRepository.save(company);
        return translationService.translate(company, ProductionCompanyReadDTO.class);
    }

    public void deleteProductionCompany(UUID id) {
        companyRepository.delete(repositoryHelper.getByIdRequired(ProductionCompany.class, id));
    }

}
