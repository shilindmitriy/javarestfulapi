package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.ReviewLikeRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkReviewRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class CreativeWorkReviewService {

    @Autowired
    private ReviewLikeRepository reviewLikeRepository;

    @Autowired
    private CreativeWorkReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewReadDTO getReview(UUID workId, UUID id) {
        CreativeWorkReview review = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkReview.class, id, workId);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public PageResult<ReviewReadDTO> getReviews(UUID workId, UserDetailsImpl user, Pageable pageable) {
        UUID userId = null;
        if (user != null) {
            userId = user.getId();
        }
        Page<ReviewReadDTO> reviews = reviewRepository.findAllByParentIdAndEnabledOrUserId(workId, userId, true,
                pageable);
        return translationService.toPageResult(reviews);
    }

    public ReviewReadDTO createReview(UUID workId, ReviewCreateDTO create, UserDetailsImpl user) {

        if (user.getModeratorsTrust() < 0) {
            throw new AccessDeniedException("The user with id=" + user.getId() + " is not allowed to write a review.");
        }

        CreativeWorkReview review = translationService.translate(create, CreativeWorkReview.class);
        review.setParentEntity(repositoryHelper.getReferenceIfExist(CreativeWork.class, workId));
        review.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        if (user.getModeratorsTrust() > 99) {
            review.setEnabled(true);
        } else {
            review.setEnabled(false);
        }

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchReview(UUID workId, UUID id, ReviewPatchDTO patch) {

        CreativeWorkReview review = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkReview.class, id, workId);
        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateReview(UUID workId, UUID id, ReviewPutDTO put) {

        CreativeWorkReview review = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkReview.class, id, workId);
        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    @Transactional
    public void deleteReview(UUID workId, UUID id) {
        reviewLikeRepository.deleteByReviewTypeAndReviewId(ReviewType.CREATIVE_WORK_REVIEW, id);
        reviewRepository.delete(repositoryHelper.getByParentIdAndIdRequired(CreativeWorkReview.class, id, workId));
    }

}