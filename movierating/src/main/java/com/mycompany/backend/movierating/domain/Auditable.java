package com.mycompany.backend.movierating.domain;

import java.time.Instant;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable extends AbstractEntity {

    @NotNull
    @PastOrPresent
    @CreatedDate
    private Instant createdAt;

    @NotNull
    @PastOrPresent
    @LastModifiedDate
    private Instant updatedAt;

}