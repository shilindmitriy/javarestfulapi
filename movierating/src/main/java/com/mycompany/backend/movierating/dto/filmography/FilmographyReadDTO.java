package com.mycompany.backend.movierating.dto.filmography;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmographyReadDTO {

    private String character;

    private Department department;

    private String post;

    private String title;

    private LocalDate releaseDate;

    private MovieStatus status;

    private LocalTime runningTime;

    private UUID movieId;

    private UUID workId;

}
