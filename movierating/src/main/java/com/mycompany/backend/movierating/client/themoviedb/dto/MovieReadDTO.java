package com.mycompany.backend.movierating.client.themoviedb.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import lombok.Data;

@Data
public class MovieReadDTO {

    private String id;

    private String originalTitle;

    private String title;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate releaseDate;

    private String status;

    private Long runtime;

    private String overview;

    private Set<CountryReadDTO> productionCountries = new HashSet<>();

    private Set<GenreReadDTO> genres = new HashSet<>();

    private Set<ProductionCompanyReadDTO> productionCompanies = new HashSet<>();

}
