package com.mycompany.backend.movierating.job.oneoff;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mycompany.backend.movierating.client.TheMovieDbClient;
import com.mycompany.backend.movierating.client.themoviedb.dto.MovieReadShortDTO;
import com.mycompany.backend.movierating.exception.ImportAlreadyPerformedException;
import com.mycompany.backend.movierating.exception.ImportedEntityAlreadyExistException;
import com.mycompany.backend.movierating.service.AsyncService;
import com.mycompany.backend.movierating.service.importer.MovieImporterService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TheMovieDbImportOneOffJob {

    @Value("${themoviedb.import.job.enabled}")
    private boolean enabled;

    @Autowired
    private TheMovieDbClient client;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private AsyncService asyncService;

    @PostConstruct
    void executeJob() {
        if (!enabled) {
            log.info("Import is disabled");
            return;
        }
        asyncService.executeAsync(this::doImport);
    }

    public void doImport() {
        log.info("Starting import");
        try {
            List<MovieReadShortDTO> moviesToImport = client.getTopRatedMovies().getResults();
            int successfullyImported = 0;
            int skipped = 0;
            int failed = 0;

            for (MovieReadShortDTO m : moviesToImport) {
                try {
                    movieImporterService.importMovie(m.getId());
                    successfullyImported++;
                } catch (ImportedEntityAlreadyExistException | ImportAlreadyPerformedException e) {
                    log.info("Can't import movie id={}, title={}: {}", m.getId(), m.getTitle(), e.getMessage());
                    skipped++;
                } catch (Exception e) {
                    log.warn("Failed to import movie id={}, title={}", m.getId(), m.getTitle(), e);
                    failed++;
                }
            }
            log.info("Total movies to import: {}, successfully imported: {}, skipped: {}, failed: {}",
                    moviesToImport.size(), successfullyImported, skipped, failed);
        } catch (Exception e) {
            log.warn("Failed to perform import", e);
        }

        log.info("Import finished");
    }

}
