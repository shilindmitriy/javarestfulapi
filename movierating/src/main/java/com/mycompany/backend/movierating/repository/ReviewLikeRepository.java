package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO;

@Repository
public interface ReviewLikeRepository extends CrudRepository<ReviewLike, UUID> {

    @Query("select new com.mycompany.backend.movierating.dto.reviewlike.ReviewLikesReadDTO("
            + "sum(case c.like when true then 1 else 0 end), sum(case c.like when false then 1 else 0 end)) "
            + "from ReviewLike c where c.reviewType = :reviewType and c.reviewId = :reviewId")
    ReviewLikesReadDTO retrieveReviewLikes(ReviewType reviewType, UUID reviewId);    
   
    boolean existsByReviewIdAndUserId(UUID reviewId, UUID userId);
    
    boolean existsByIdAndUserId(UUID id, UUID userId);
    
    void deleteByReviewTypeAndReviewId(ReviewType reviewType, UUID reviewId);
    
}
