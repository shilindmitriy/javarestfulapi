package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyCreateDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPatchDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPutDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyReadDTO;
import com.mycompany.backend.movierating.service.ProductionCompanyService;

@RestController
@RequestMapping("/api/v1/production-companies")
public class ProductionCompanyController {

    @Autowired
    private ProductionCompanyService companyService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public ProductionCompanyReadDTO getProductionCompany(@PathVariable UUID id) {
        return companyService.getProductionCompany(id);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping
    public ProductionCompanyReadDTO createProductionCompany(@RequestBody @Valid ProductionCompanyCreateDTO create) {
        return companyService.createProductionCompany(create);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public ProductionCompanyReadDTO patchProductionCompany(@PathVariable UUID id,
            @RequestBody @Valid ProductionCompanyPatchDTO patch) {
        return companyService.patchProductionCompany(id, patch);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public ProductionCompanyReadDTO updateProductionCompany(@PathVariable UUID id,
            @RequestBody @Valid ProductionCompanyPutDTO put) {
        return companyService.updateProductionCompany(id, put);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteProductionCompany(@PathVariable UUID id) {
        companyService.deleteProductionCompany(id);
    }

}
