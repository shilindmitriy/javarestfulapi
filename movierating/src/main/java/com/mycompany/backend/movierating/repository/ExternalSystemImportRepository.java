package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.ExternalSystemImport;
import com.mycompany.backend.movierating.domain.constants.ImportedEntityType;

@Repository
public interface ExternalSystemImportRepository extends CrudRepository<ExternalSystemImport, UUID> {

    ExternalSystemImport findByIdInExternalSystemAndEntityType(String idInExternalSystem,
            ImportedEntityType entityType);

}
