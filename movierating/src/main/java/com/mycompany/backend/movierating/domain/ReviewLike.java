package com.mycompany.backend.movierating.domain;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.mycompany.backend.movierating.domain.constants.ReviewType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class ReviewLike extends Auditable {
    
    @NotNull
    private  Boolean like;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private ReviewType reviewType;
    
    @NotNull
    private UUID reviewId;
    
    @NotNull
    private UUID userId;

}
