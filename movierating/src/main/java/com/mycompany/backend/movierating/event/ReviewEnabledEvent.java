package com.mycompany.backend.movierating.event;

import java.util.UUID;

import lombok.Data;

@Data
public class ReviewEnabledEvent {

    private UUID reviewId;

    private Boolean enabled;

}
