package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.dto.news.DateFilter;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsPatchDTO;
import com.mycompany.backend.movierating.dto.news.NewsPutDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.NewsRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;
    
    @Autowired
    private NewsRatingRepository ratingRepository;
    
    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsReadDTO getNews(UUID id) {
        News news = repositoryHelper.getByIdRequired(News.class, id);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public List<NewsReadDTO> getNewsByAuthor(UUID author, DateFilter date) {
        List<News> news;
        if (date.getDateFrom() != null && date.getDateTo() != null) {
            news = newsRepository.findAllByAuthorIdAndDate(author, date.getDateFrom(), date.getDateTo());
        } else {
            news = newsRepository.findAllByAuthorId(author);
        }
        return translationService.translateList(news, NewsReadDTO.class);
    }

    public NewsReadDTO createNews(NewsCreateDTO create, UserDetailsImpl user) {
        News news = translationService.translate(create, News.class);
        news.setAuthorId(user.getId());        
        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO patchNews(UUID id, NewsPatchDTO patch) {

        News news = repositoryHelper.getByIdRequired(News.class, id);
        translationService.map(patch, news);

        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO updateNews(UUID id, NewsPutDTO put) {
        News news = repositoryHelper.getByIdRequired(News.class, id);
        translationService.map(put, news);

        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public void deleteNews(UUID id) {
        newsRepository.delete(repositoryHelper.getByIdRequired(News.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRating(UUID id) {
        SimpleRatingReadDTO simpleRating = ratingRepository.retrieveSimpleRating(id);
        News news = repositoryHelper.getByIdRequired(News.class, id);
        log.info(
                "Setting new like and dislike for news: {}. Old like: {}, new like: {}. "
                        + "Old dislike: {}, new dislike: {}.",
                id, news.getLike(), simpleRating.getLike(), news.getDislike(), simpleRating.getDislike());
        news.setLike(simpleRating.getLike());
        news.setDislike(simpleRating.getDislike());
        newsRepository.save(news);
    }
}
