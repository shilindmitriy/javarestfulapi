package com.mycompany.backend.movierating.client.themoviedb.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import lombok.Data;

@Data
public class PersonReadDTO {

    private String id;

    private String name;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthday;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate deathday;

    private String biography;

}
