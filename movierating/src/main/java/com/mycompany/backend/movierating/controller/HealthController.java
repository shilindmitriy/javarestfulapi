package com.mycompany.backend.movierating.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @PreAuthorize("permitAll")
    @GetMapping("/health")
    public void health() {

    }

}
