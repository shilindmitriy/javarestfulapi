package com.mycompany.backend.movierating.domain;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.mycompany.backend.movierating.domain.constants.ImportedEntityType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class ExternalSystemImport extends Auditable {

    @NotNull
    @Enumerated(EnumType.STRING)
    private ImportedEntityType entityType;

    @NotNull
    private UUID entityId;

    @NotBlank
    private String idInExternalSystem;

}
