package com.mycompany.backend.movierating.dto.news;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class NewsPatchDTO {

    @Size(max = 10000)
    private String text;

}
