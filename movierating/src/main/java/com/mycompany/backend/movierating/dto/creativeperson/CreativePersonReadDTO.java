package com.mycompany.backend.movierating.dto.creativeperson;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import lombok.Data;

@Data
public class CreativePersonReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String name;

    private LocalDate born;

    private LocalDate death;

    private String urlPhoto;

    private String biography;

    private Double averageRating;

    private Long numberOfAllVotes;
}
