package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.security.AdminOrCurrentUser;
import com.mycompany.backend.movierating.dto.user.UserAccountCreateDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPutDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountReadDTO;
import com.mycompany.backend.movierating.service.UserService;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public UserAccountReadDTO getUser(@PathVariable UUID id) {
        return userService.getUser(id);
    }

    @PreAuthorize("permitAll")
    @PostMapping
    public UserAccountReadDTO createUser(@RequestBody @Valid UserAccountCreateDTO create) {
        return userService.createUser(create);
    }

    @AdminOrCurrentUser
    @PatchMapping("/{id}")
    public UserAccountReadDTO patchUser(@PathVariable UUID id, @RequestBody @Valid UserAccountPatchDTO patch) {
        return userService.patchUser(id, patch);
    }

    @AdminOrCurrentUser
    @PutMapping("/{id}")
    public UserAccountReadDTO updateUser(@PathVariable UUID id, @RequestBody @Valid UserAccountPutDTO put) {
        return userService.updateUser(id, put);
    }

    @AdminOrCurrentUser
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userService.deleteUser(id);
    }
}
