package com.mycompany.backend.movierating.dto.movie;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;

import lombok.Data;

@Data
public class MoviePatchDTO {

    @Size(max = 100)
    private String title;

    private LocalDate releaseDate;

    private MovieStatus status;

    private LocalTime runningTime;

    private String storyline;

    @NotEmpty
    private Set<Country> productionCountries;

    @NotEmpty
    private Set<Genre> genres;
}
