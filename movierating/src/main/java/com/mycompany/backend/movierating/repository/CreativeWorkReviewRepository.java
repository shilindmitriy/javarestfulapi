package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativeWorkReview;

@Repository
public interface CreativeWorkReviewRepository extends ReviewRepository<CreativeWorkReview> {

}
