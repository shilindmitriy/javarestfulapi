package com.mycompany.backend.movierating.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InappropriateEntityException extends RuntimeException {

    public InappropriateEntityException(String message) {
        super(message);
    }
}
