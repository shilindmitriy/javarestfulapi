package com.mycompany.backend.movierating.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;

@NoRepositoryBean
public interface RatingRepository<T> extends CrudRepository<T, UUID> {
    
    @Query("select case when (count(*) > 0)  then true else false end from #{#entityName} t "
            + "where t.parentEntity.id = ?1 and t.user.id = ?2 ")
    boolean existsByParentIdAndUserId(UUID parentId, UUID userId);

    @Query("select t from #{#entityName} t where t.parentEntity.id = ?1 and t.id = ?2")
    Optional<T> findByParentIdAndId(UUID parentId, UUID id);

    @Query("select new com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO("
            + "sum(case t.rating when 1 then 1 else 0 end), sum(case t.rating when -1 then 1 else 0 end)) "
            + "from #{#entityName} t where t.parentEntity.id = ?1")
    SimpleRatingReadDTO retrieveSimpleRating(UUID parentId);

    @Query("select new com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO(count(*), avg(t.rating)) "
            + "from #{#entityName} t where t.parentEntity.id = ?1")
    ShortRatingReadDTO retrieveShortRating(UUID parentId);

    @Query("select new com.mycompany.backend.movierating.dto.rating.DetailedRatingReadDTO("
            // numberOfAllVotes
            + "count(*), "
            // averageRating
            + "avg(t.rating), "
            // numberOfVotesWhereRatingEqualToOne
            + "sum(case t.rating when 1 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToTwo
            + "sum(case t.rating when 2 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToThree
            + "sum(case t.rating when 3 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToFour
            + "sum(case t.rating when 4 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToFive
            + "sum(case t.rating when 5 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToSix
            + "sum(case t.rating when 6 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToSeven
            + "sum(case t.rating when 7 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToEight
            + "sum(case t.rating when 8 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToNine
            + "sum(case t.rating when 9 then 1 else 0 end), "
            // numberOfVotesWhereRatingEqualToTen
            + "sum(case t.rating when 10 then 1 else 0 end), "
            // numberOfVotesWhereAgeLessThan18
            + "sum(case when t.age < 18 then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween18And29
            + "sum(case when t.age >= 18 and t.age < 30 then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween30And44
            + "sum(case when t.age >= 30 and t.age < 45 then 1 else 0 end), "
            // numberOfVotesWhereAgeOver45
            + "sum(case when t.age >= 45 then 1 else 0 end), "
            // averageRatingWhereAgeLessThan18
            + "avg(case when t.age < 18 then t.rating end), "
            // averageRatingWhereAgeBetween18And29
            + "avg(case when t.age >= 18 and t.age < 30 then t.rating end), "
            // averageRatingWhereAgeBetween30And44
            + "avg(case when t.age >= 30 and t.age < 45 then t.rating end), "
            // averageRatingWhereAgeOver45
            + "avg(case when t.age >= 45 then t.rating end), "
            // numberOfVotesWhereAgeLessThan18ForMales
            + "sum(case when t.age < 18 and t.gender = 'MALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween18And29ForMales
            + "sum(case when t.age >= 18 and t.age < 30 and t.gender = 'MALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween30And44ForMales
            + "sum(case when t.age >= 30 and t.age < 45 and t.gender = 'MALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeOver45ForMales
            + "sum(case when t.age >= 45 and t.gender = 'MALE' then 1 else 0 end), "
            // averageRatingWhereAgeLessThan18ForMales
            + "avg(case when t.age < 18 and t.gender = 'MALE' then t.rating end), "
            // averageRatingWhereAgeBetween18And29ForMales
            + "avg(case when t.age >= 18 and t.age < 30 and t.gender = 'MALE' then t.rating end), "
            // averageRatingWhereAgeBetween30And44ForMales
            + "avg(case when t.age >= 30 and t.age < 45 and t.gender = 'MALE' then t.rating end), "
            // averageRatingWhereAgeOver45ForMales
            + "avg(case when t.age >= 45 and t.gender = 'MALE' then t.rating end), "
            // numberOfVotesWhereAgeLessThan18ForFemales
            + "sum(case when t.age < 18 and t.gender = 'FEMALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween18And29ForFemales
            + "sum(case when t.age >= 18 and t.age < 30 and t.gender = 'FEMALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeBetween30And44ForFemales
            + "sum(case when t.age >= 30 and t.age < 45 and t.gender = 'FEMALE' then 1 else 0 end), "
            // numberOfVotesWhereAgeOver45ForFemales
            + "sum(case when t.age >= 45 and t.gender = 'FEMALE' then 1 else 0 end), "
            // averageRatingWhereAgeLessThan18ForFemales
            + "avg(case when t.age < 18 and t.gender = 'FEMALE' then t.rating end), "
            // averageRatingWhereAgeBetween18And29ForFemales
            + "avg(case when t.age >= 18 and t.age < 30 and t.gender = 'FEMALE' then t.rating end), "
            // averageRatingWhereAgeBetween30And44ForFemales
            + "avg(case when t.age >= 30 and t.age < 45 and t.gender = 'FEMALE' then t.rating end), "
            // averageRatingWhereAgeOver45ForFemales
            + "avg(case when t.age >= 45 and t.gender = 'FEMALE' then t.rating end), "
            // numberOfVotesAccordingToCountry
            + "sum(case when t.countryUser = :country then 1 else 0 end), "
            // averageRatingAccordingToCountry
            + "avg(case when t.countryUser = :country then t.rating end), "
            // numberOfVotesExceptCountry
            + "sum(case when t.countryUser != :country then 1 else 0 end), "
            // averageRatingExceptCountry
            + "avg(case when t.countryUser != :country then t.rating end)) "
            + "from #{#entityName} t where t.parentEntity.id = :parentId")
    DetailedRatingReadDTO retrieveDetailedRating(UUID parentId, Country country);

}
