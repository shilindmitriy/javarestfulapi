package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativePersonRating;

@Repository
public interface CreativePersonRatingRepository extends RatingRepository<CreativePersonRating> {

}
