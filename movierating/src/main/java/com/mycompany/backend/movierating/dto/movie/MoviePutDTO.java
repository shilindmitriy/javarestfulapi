package com.mycompany.backend.movierating.dto.movie;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Genre;
import com.mycompany.backend.movierating.domain.constants.MovieStatus;

import lombok.Data;

@Data
public class MoviePutDTO {

    @NotBlank
    @Size(max = 100)
    private String title;

    private LocalDate releaseDate;

    @NotNull
    private MovieStatus status;

    private LocalTime runningTime;

    @Size(max = 10000)
    private String storyline;

    @NotEmpty
    private Set<Country> productionCountries = new HashSet<>();

    @NotEmpty
    private Set<Genre> genres = new HashSet<>();
}
