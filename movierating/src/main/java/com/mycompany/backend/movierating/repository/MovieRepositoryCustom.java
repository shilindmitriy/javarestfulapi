package com.mycompany.backend.movierating.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.dto.movie.MovieFilter;

public interface MovieRepositoryCustom {

    Page<Movie> findByFilter(MovieFilter filter, Pageable pageable);

}
