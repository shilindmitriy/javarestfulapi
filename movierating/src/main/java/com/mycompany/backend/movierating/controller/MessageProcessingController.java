package com.mycompany.backend.movierating.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationSolveRequest;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.MessageProcessingService;

@RestController
@RequestMapping("/api/v1/processed-messages")
public class MessageProcessingController {

    @Autowired
    private MessageProcessingService processingService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER')")
    @PostMapping
    public List<MessageToAdministrationReadDTO> getProcessedMessages(
            @RequestBody @Valid MessageToAdministrationSolveRequest solved,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return processingService.getProcessedMessages(solved, user);
    }

}
