package com.mycompany.backend.movierating.dto.creativeperson;

import java.time.LocalDate;

import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CreativePersonPatchDTO {

    private String name;

    @Past
    private LocalDate born;

    @Past
    private LocalDate death;

    @Size(max = 10000)
    private String biography;
}
