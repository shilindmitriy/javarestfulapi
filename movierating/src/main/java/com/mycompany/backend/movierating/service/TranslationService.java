package com.mycompany.backend.movierating.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.AbstractEntity;
import com.mycompany.backend.movierating.domain.Review;
import com.mycompany.backend.movierating.domain.ReviewLike;
import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.domain.CreativePersonReview;
import com.mycompany.backend.movierating.domain.CreativePersonRating;
import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkReview;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.domain.MovieReview;
import com.mycompany.backend.movierating.domain.MovieRating;
import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.ProductionCompany;
import com.mycompany.backend.movierating.domain.ProductionCompanyReview;
import com.mycompany.backend.movierating.domain.ProductionCompanyRating;
import com.mycompany.backend.movierating.domain.Rating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationPatchDTO;
import com.mycompany.backend.movierating.dto.movie.MoviePatchDTO;
import com.mycompany.backend.movierating.dto.news.NewsPatchDTO;
import com.mycompany.backend.movierating.dto.productioncompany.ProductionCompanyPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.dto.reviewlike.ReviewLikePatchDTO;
import com.mycompany.backend.movierating.dto.user.AuthorityPatchDTO;
import com.mycompany.backend.movierating.dto.user.UserAccountPatchDTO;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    private ObjectTranslator objectTranslator = new ObjectTranslator();

    public TranslationService() {
        objectTranslator = new ObjectTranslator(createConfiguration());
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> List<T> translateList(List<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }

    public <T> PageResult<T> toPageResult(Page<T> page) {
        PageResult<T> result = toPageResult(page, null);
        result.setData(page.getContent());
        return result;
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> result = new PageResult<>();
        result.setPage(page.getNumber());
        result.setPageSize(page.getSize());
        result.setTotalPages(page.getTotalPages());
        result.setTotalElements(page.getTotalElements());
        if (dtoType == null) {
            return result;
        } else {
            result.setData(page.getContent().stream().map(c -> translate(c, dtoType)).collect(Collectors.toList()));
            return result;
        }
    }

    private Configuration createConfiguration() {
        Configuration config = new Configuration();
        configureForCreativeWork(config);
        configureForAbstractEntity(config);
        configureForReview(config);
        configureForRating(config);
        configureForPerson(config);
        configureForMessage(config);
        configureForMovie(config);
        configureForNews(config);
        configureForProductionCompany(config);
        configureForUser(config);
        configureForAuthorityAndEnabled(config);
        configureForReviewLike(config);
        return config;
    }

    private void configureForCreativeWork(Configuration config) {
        Configuration.Translation forRead = config.beanOfClass(CreativeWork.class)
                .translationTo(CreativeWorkReadDTO.class);
        forRead.srcProperty("creativePerson.id").translatesTo("personId");
        forRead.srcProperty("movie.id").translatesTo("movieId");

        Configuration.Translation forCreate = config.beanOfClass(CreativeWorkCreateDTO.class)
                .translationTo(CreativeWork.class);
        forCreate.srcProperty("personId").translatesTo("creativePerson.id");

        Configuration.Translation forUpdate = config.beanOfClass(CreativeWorkPutDTO.class)
                .translationTo(CreativeWork.class);
        forUpdate.srcProperty("personId").translatesTo("creativePerson.id");
        forUpdate.srcProperty("movieId").translatesTo("movie.id");

        Configuration.Translation forPatch = config.beanOfClass(CreativeWorkPatchDTO.class)
                .translationTo(CreativeWork.class);
        forPatch.srcProperty("personId").translatesTo("creativePerson.id");
        forPatch.srcProperty("movieId").translatesTo("movie.id");

        config.beanOfClass(CreativeWorkPatchDTO.class).translationTo(CreativeWork.class).mapOnlyNotNullProperties();
    }

    private void configureForReview(Configuration config) {
        config.beanOfClass(ReviewPatchDTO.class).translationTo(Review.class).mapOnlyNotNullProperties();
        config.beanOfClass(CreativeWorkReview.class).translationTo(ReviewReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(CreativePersonReview.class).translationTo(ReviewReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(MovieReview.class).translationTo(ReviewReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(ProductionCompanyReview.class).translationTo(ReviewReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(CreativeWorkReview.class).translationTo(ReviewReadDTO.class).srcProperty("parentEntity.id")
                .translatesTo("parentId");
        config.beanOfClass(CreativePersonReview.class).translationTo(ReviewReadDTO.class)
                .srcProperty("parentEntity.id").translatesTo("parentId");
        config.beanOfClass(MovieReview.class).translationTo(ReviewReadDTO.class).srcProperty("parentEntity.id")
                .translatesTo("parentId");
        config.beanOfClass(ProductionCompanyReview.class).translationTo(ReviewReadDTO.class)
                .srcProperty("parentEntity.id").translatesTo("parentId");
    }

    private void configureForRating(Configuration config) {
        config.beanOfClass(RatingPatchDTO.class).translationTo(Rating.class).mapOnlyNotNullProperties();
        config.beanOfClass(CreativeWorkRating.class).translationTo(RatingReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(CreativePersonRating.class).translationTo(RatingReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(MovieRating.class).translationTo(RatingReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(ProductionCompanyRating.class).translationTo(RatingReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
        config.beanOfClass(NewsRating.class).translationTo(RatingReadDTO.class).srcProperty("user.id")
                .translatesTo("userId");
    }

    private void configureForPerson(Configuration config) {
        config.beanOfClass(CreativePersonPatchDTO.class).translationTo(CreativePerson.class).mapOnlyNotNullProperties();
    }

    private void configureForMessage(Configuration config) {
        config.beanOfClass(MessageToAdministrationPatchDTO.class).translationTo(MessageToAdministration.class)
                .mapOnlyNotNullProperties();
    }

    private void configureForMovie(Configuration config) {
        config.beanOfClass(MoviePatchDTO.class).translationTo(Movie.class).mapOnlyNotNullProperties();
    }

    private void configureForNews(Configuration config) {
        config.beanOfClass(NewsPatchDTO.class).translationTo(News.class).mapOnlyNotNullProperties();
    }

    private void configureForProductionCompany(Configuration config) {
        config.beanOfClass(ProductionCompanyPatchDTO.class).translationTo(ProductionCompany.class)
                .mapOnlyNotNullProperties();
    }

    private void configureForUser(Configuration config) {
        config.beanOfClass(UserAccountPatchDTO.class).translationTo(UserAccount.class).mapOnlyNotNullProperties();
    }

    private void configureForAuthorityAndEnabled(Configuration config) {
        config.beanOfClass(AuthorityPatchDTO.class).translationTo(UserAccount.class)
                .mapOnlyNotNullProperties();
    }

    private void configureForReviewLike(Configuration config) {
        config.beanOfClass(ReviewLikePatchDTO.class).translationTo(ReviewLike.class).mapOnlyNotNullProperties();
    }

    private void configureForAbstractEntity(Configuration config) {
        config.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        config.beanOfClass(AbstractEntity.class)
                .setBeanFinder((beanClass, id) -> repositoryHelper.getReferenceIfExist(beanClass, (UUID) id));
    }

}
