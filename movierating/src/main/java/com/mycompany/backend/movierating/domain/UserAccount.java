package com.mycompany.backend.movierating.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.mycompany.backend.movierating.domain.constants.Authority;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class UserAccount extends Auditable {

    @Size(max = 30)
    private String firstName;

    @Size(max = 30)
    private String lastName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    @Past
    private LocalDate birthday;

    @NotBlank
    @Size(max = 30)
    private String nickName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Country country;

    @NotNull
    private Integer moderatorsTrust;

    private Integer usersRating;

    @NotBlank
    @Email
    @Size(max = 50)
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;

    @NotNull
    private Boolean enabled;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Authority authority;

    // News

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "user")
    private List<NewsRating> newsRatings = new ArrayList<>();

    // Movie
    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "user")
    private List<MovieReview> movieReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "user")
    private List<MovieRating> movieRatings = new ArrayList<>();

    // CreativeWork
    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "user")
    private List<CreativeWorkReview> creativeWorkReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "user")
    private List<CreativeWorkRating> creativeWorkRatings = new ArrayList<>();

    // CreativityPerson
    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "user")
    private List<CreativePersonReview> creativityPersonReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "user")
    private List<CreativePersonRating> creativityPersonRatings = new ArrayList<>();

    // ProductionCompany
    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "user")
    private List<ProductionCompanyReview> companyReviews = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "user")
    private List<ProductionCompanyRating> companyRatings = new ArrayList<>();

}
