package com.mycompany.backend.movierating.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;

public interface MessageToAdministrationRepositoryCustom {

    Page<MessageToAdministration> findByFilter(MessageToAdministrationFilter filter, Pageable pageable);
}
