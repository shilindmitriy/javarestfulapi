package com.mycompany.backend.movierating.repository;

import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativePersonReview;

@Repository
public interface CreativePersonReviewRepository extends ReviewRepository<CreativePersonReview> {

}
