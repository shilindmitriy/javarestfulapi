package com.mycompany.backend.movierating.client.themoviedb.dto;

import lombok.Data;

@Data
public class CrewReadDTO {
    
    private String creditId;

    private String department;

    private String job;

    // person_id
    private String id;

}
