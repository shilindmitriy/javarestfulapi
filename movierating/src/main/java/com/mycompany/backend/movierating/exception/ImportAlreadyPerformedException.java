package com.mycompany.backend.movierating.exception;

import com.mycompany.backend.movierating.domain.ExternalSystemImport;

import lombok.Getter;

@Getter
public class ImportAlreadyPerformedException extends Exception {

    private ExternalSystemImport externalSystemImport;

    public ImportAlreadyPerformedException(ExternalSystemImport esi) {
        super(String.format("Already performed import of %s with id=%s and id in external system=%s",
                esi.getEntityType(), esi.getEntityId(), esi.getIdInExternalSystem()));
        externalSystemImport = esi;
    }

}
