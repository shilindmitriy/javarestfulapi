package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.controller.documentation.ApiPageable;
import com.mycompany.backend.movierating.controller.security.AdminOrModeratorOrReviewOwner;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewCreateDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPatchDTO;
import com.mycompany.backend.movierating.dto.review.ReviewPutDTO;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.CreativePersonReviewService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/creative-persons/{personId}/reviews")
public class CreativePersonReviewController {

    @Autowired
    private CreativePersonReviewService reviewService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public ReviewReadDTO getReview(@PathVariable UUID personId, @PathVariable UUID id) {
        return reviewService.getReview(personId, id);
    }

    @PreAuthorize("permitAll")
    @ApiPageable
    @GetMapping
    public PageResult<ReviewReadDTO> getReviews(@PathVariable UUID personId,
            @AuthenticationPrincipal UserDetailsImpl user, @ApiIgnore Pageable pageable) {
        return reviewService.getReviews(personId, user, pageable);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MODERATOR', 'CONTENT_MANAGER', 'REGISTERED_USER')")
    @PostMapping
    public ReviewReadDTO createReview(@PathVariable UUID personId, @RequestBody @Valid ReviewCreateDTO create,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return reviewService.createReview(personId, create, user);
    }

    @AdminOrModeratorOrReviewOwner
    @PatchMapping("/{id}")
    public ReviewReadDTO patchReview(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid ReviewPatchDTO patch) {
        return reviewService.patchReview(personId, id, patch);
    }

    @AdminOrModeratorOrReviewOwner
    @PutMapping("/{id}")
    public ReviewReadDTO updateReview(@PathVariable UUID personId, @PathVariable UUID id,
            @RequestBody @Valid ReviewPutDTO put) {
        return reviewService.updateReview(personId, id, put);
    }

    @AdminOrModeratorOrReviewOwner
    @DeleteMapping("/{id}")
    public void deleteReview(@PathVariable UUID personId, @PathVariable UUID id) {
        reviewService.deleteReview(personId, id);
    }
}
