package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.CreativePerson;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonCreateDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPatchDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonPutDTO;
import com.mycompany.backend.movierating.dto.creativeperson.CreativePersonReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.repository.CreativePersonRatingRepository;
import com.mycompany.backend.movierating.repository.CreativePersonRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreativePersonService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private CreativePersonRepository personRepository;
    
    @Autowired
    private CreativePersonRatingRepository ratingRepository;

    public CreativePersonReadDTO getCreativePerson(UUID id) {
        CreativePerson person = repositoryHelper.getByIdRequired(CreativePerson.class, id);
        return translationService.translate(person, CreativePersonReadDTO.class);
    }
    
    public Double getAverageMoviesRating(UUID id) {
        return personRepository.getAverageMoviesRating(id);
    }
    
    public Double getAverageCreativeWorksRating(UUID id) {
        return personRepository.getAverageCreativeWorksRating(id);
    }

    public CreativePersonReadDTO createCreativePerson(CreativePersonCreateDTO create) {
        CreativePerson person = translationService.translate(create, CreativePerson.class);
        person = personRepository.save(person);
        return translationService.translate(person, CreativePersonReadDTO.class);
    }

    public CreativePersonReadDTO patchCreativePerson(UUID id, CreativePersonPatchDTO patch) {

        CreativePerson person = repositoryHelper.getByIdRequired(CreativePerson.class, id);
        translationService.map(patch, person);

        person = personRepository.save(person);
        return translationService.translate(person, CreativePersonReadDTO.class);
    }

    public CreativePersonReadDTO updateCreatePerson(UUID id, CreativePersonPutDTO put) {

        CreativePerson person = repositoryHelper.getByIdRequired(CreativePerson.class, id);
        translationService.map(put, person);

        person = personRepository.save(person);
        return translationService.translate(person, CreativePersonReadDTO.class);
    }

    public void deleteCreativePerson(UUID id) {
        personRepository.delete(repositoryHelper.getByIdRequired(CreativePerson.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRating(UUID id) {
        ShortRatingReadDTO shortRating = ratingRepository.retrieveShortRating(id);
        CreativePerson person = repositoryHelper.getByIdRequired(CreativePerson.class, id);
        log.info(
                "Setting new average rating and number of all votes for creative person: {}. "
                        + "Old average rating: {}, new average rating: {}. "
                        + "Old number of all votes: {}, new number of all votes: {}.",
                id, person.getAverageRating(), shortRating.getAverageRating(), person.getNumberOfAllVotes(),
                shortRating.getNumberOfAllVotes());
        person.setAverageRating(shortRating.getAverageRating());
        person.setNumberOfAllVotes(shortRating.getNumberOfAllVotes());
        personRepository.save(person);
    }
}
