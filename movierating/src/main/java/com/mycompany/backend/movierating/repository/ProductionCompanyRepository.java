package com.mycompany.backend.movierating.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.ProductionCompany;

@Repository
public interface ProductionCompanyRepository extends CrudRepository<ProductionCompany, UUID> {

    ProductionCompany findByName(String name);

}
