package com.mycompany.backend.movierating.dto.review;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ReviewPutDTO {

    @NotBlank
    @Size(max = 1500)
    private String text;
}
