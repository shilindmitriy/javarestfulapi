package com.mycompany.backend.movierating.exception;

import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Class entityClass, UUID id) {
        super(String.format("Entity %s with id=%s is not found!", entityClass.getSimpleName(), id));
    }

    public EntityNotFoundException(Class entityClass, UUID id, UUID fkId) {
        super(String.format("Entity %s with id=%s and foreign key=%s is not found!", entityClass.getSimpleName(), id,
                fkId));
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
