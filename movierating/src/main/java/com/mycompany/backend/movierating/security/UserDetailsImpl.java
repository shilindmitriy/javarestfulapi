package com.mycompany.backend.movierating.security;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.domain.constants.Country;
import com.mycompany.backend.movierating.domain.constants.Gender;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDetailsImpl extends User {

    private UUID id;

    private Gender gender;

    private Country country;

    private LocalDate birthday;
    
    private Integer moderatorsTrust;

    public UserDetailsImpl(UserAccount user) {
        super(user.getEmail(), user.getPassword(), List.of(new SimpleGrantedAuthority(user.getAuthority().name())));
        id = user.getId();
        gender = user.getGender();
        country = user.getCountry();
        birthday = user.getBirthday();
        moderatorsTrust = user.getModeratorsTrust();
    }

}
