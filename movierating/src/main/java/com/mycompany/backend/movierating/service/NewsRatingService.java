package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.News;
import com.mycompany.backend.movierating.domain.NewsRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.SimpleRatingReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.NewsRatingRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class NewsRatingService {

    @Autowired
    private NewsRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingReadDTO getRating(UUID newsId, UUID id) {
        NewsRating rating = repositoryHelper.getByParentIdAndIdRequired(NewsRating.class, id, newsId);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public SimpleRatingReadDTO getSimpleRating(UUID newsId) {
        return ratingRepository.retrieveSimpleRating(newsId);
    }

    public RatingReadDTO createRating(UUID newsId, RatingCreateDTO create, UserDetailsImpl user) {

        if (ratingRepository.existsByParentIdAndUserId(newsId, user.getId())) {
            throw new AlreadyExistsException(News.class, newsId, user.getId());
        }

        NewsRating rating = translationService.translate(create, NewsRating.class);
        rating.setGender(user.getGender());
        rating.setCountryUser(user.getCountry());
        rating.setAge(Period.between(user.getBirthday(), LocalDate.now()).getYears());
        rating.setParentEntity(repositoryHelper.getReferenceIfExist(News.class, newsId));
        rating.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO patchRating(UUID newsId, UUID id, RatingPatchDTO patch) {

        NewsRating rating = repositoryHelper.getByParentIdAndIdRequired(NewsRating.class, id, newsId);
        translationService.map(patch, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO updateRating(UUID newsId, UUID id, RatingPutDTO put) {

        NewsRating rating = repositoryHelper.getByParentIdAndIdRequired(NewsRating.class, id, newsId);
        translationService.map(put, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public void deleteRating(UUID newsId, UUID id) {
        ratingRepository.delete(repositoryHelper.getByParentIdAndIdRequired(NewsRating.class, id, newsId));
    }

}