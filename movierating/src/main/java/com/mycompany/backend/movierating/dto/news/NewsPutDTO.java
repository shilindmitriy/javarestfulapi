package com.mycompany.backend.movierating.dto.news;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class NewsPutDTO {

    @NotBlank
    @Size(max = 10000)
    private String text;

}
