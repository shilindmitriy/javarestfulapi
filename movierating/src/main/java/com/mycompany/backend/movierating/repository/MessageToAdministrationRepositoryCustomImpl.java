package com.mycompany.backend.movierating.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationFilter;

public class MessageToAdministrationRepositoryCustomImpl implements MessageToAdministrationRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<MessageToAdministration> findByFilter(MessageToAdministrationFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(em);
        qb.append("select m from MessageToAdministration m where 1=1");
        qb.append("and m.administration = :administration", filter.getAdministration());
        qb.append("and m.solved = :solved", filter.getSolved());
        qb.append("and m.entityType = :entityType", filter.getEntityType());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
