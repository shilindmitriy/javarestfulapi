package com.mycompany.backend.movierating.domain;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class News extends Auditable {

    @NotNull
    private UUID authorId;

    private Blob photo;

    @NotBlank
    @Size(max = 10000)
    private String text;

    private Long like;

    private Long dislike;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "parentEntity")
    private List<NewsRating> newsRatings = new ArrayList<>();
}
