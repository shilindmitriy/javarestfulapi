package com.mycompany.backend.movierating.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.CreativeWorkRating;
import com.mycompany.backend.movierating.domain.UserAccount;
import com.mycompany.backend.movierating.dto.rating.RatingCreateDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPatchDTO;
import com.mycompany.backend.movierating.dto.rating.RatingPutDTO;
import com.mycompany.backend.movierating.dto.rating.RatingReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.AlreadyExistsException;
import com.mycompany.backend.movierating.repository.CreativeWorkRatingRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class CreativeWorkRatingService {

    @Autowired
    private CreativeWorkRatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingReadDTO getRating(UUID workId, UUID id) {
        CreativeWorkRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkRating.class, id, workId);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public ShortRatingReadDTO getShortRating(UUID workId) {
        return ratingRepository.retrieveShortRating(workId);
    }

    public RatingReadDTO createRating(UUID workId, RatingCreateDTO create, UserDetailsImpl user) {

        if (ratingRepository.existsByParentIdAndUserId(workId, user.getId())) {
            throw new AlreadyExistsException(CreativeWork.class, workId, user.getId());
        }

        CreativeWorkRating rating = translationService.translate(create, CreativeWorkRating.class);
        rating.setGender(user.getGender());
        rating.setCountryUser(user.getCountry());
        rating.setAge(Period.between(user.getBirthday(), LocalDate.now()).getYears());
        rating.setParentEntity(repositoryHelper.getReferenceIfExist(CreativeWork.class, workId));
        rating.setUser(repositoryHelper.getReferenceIfExist(UserAccount.class, user.getId()));

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO patchRating(UUID workId, UUID id, RatingPatchDTO patch) {

        CreativeWorkRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkRating.class, id, workId);
        translationService.map(patch, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public RatingReadDTO updateRating(UUID workId, UUID id, RatingPutDTO put) {

        CreativeWorkRating rating = repositoryHelper.getByParentIdAndIdRequired(CreativeWorkRating.class, id, workId);
        translationService.map(put, rating);

        rating = ratingRepository.save(rating);
        return translationService.translate(rating, RatingReadDTO.class);
    }

    public void deleteRating(UUID workId, UUID id) {
        ratingRepository.delete(repositoryHelper.getByParentIdAndIdRequired(CreativeWorkRating.class, id, workId));
    }

}