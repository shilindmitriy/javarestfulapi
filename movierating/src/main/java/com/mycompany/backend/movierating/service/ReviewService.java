package com.mycompany.backend.movierating.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.backend.movierating.domain.constants.ReviewType;
import com.mycompany.backend.movierating.dto.PageResult;
import com.mycompany.backend.movierating.dto.review.ReviewConfirmationRequest;
import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;
import com.mycompany.backend.movierating.repository.ReviewRepository;
import com.mycompany.backend.movierating.repository.CreativePersonReviewRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkReviewRepository;
import com.mycompany.backend.movierating.repository.MovieReviewRepository;
import com.mycompany.backend.movierating.repository.ProductionCompanyReviewRepository;

@Service
public class ReviewService {

    @Autowired
    private CreativePersonReviewRepository personReviewRepository;

    @Autowired
    private CreativeWorkReviewRepository workReviewRepository;

    @Autowired
    private ProductionCompanyReviewRepository companyReviewRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TranslationService translationService;

    public PageResult<ReviewReadDTO> getReviews(ReviewType reviewType, Pageable pageable) {
        Page<ReviewReadDTO> reviews = getRepository(reviewType).findAllByEnabled(false, pageable);
        return translationService.toPageResult(reviews);
    }

    public Boolean enableReview(ReviewConfirmationRequest confirmationRequest) {
        int rows = getRepository(confirmationRequest.getReviewType()).enableReview(true,
                confirmationRequest.getParentId(), confirmationRequest.getReviewId());
        return rows == 1;
    }

    private ReviewRepository<?> getRepository(ReviewType reviewType) {
        switch (reviewType) {
          case CREATIVE_PERSON_REVIEW:
              return personReviewRepository;
          case CREATIVE_WORK_REVIEW:
              return workReviewRepository;
          case MOVIE_REVIEW:
              return movieReviewRepository;
          case PRODUCTION_COMPANY_REVIEW:
              return companyReviewRepository;
          default:
              throw new IllegalArgumentException("Entity " + reviewType + " is not supported.");
        }
    }

}
