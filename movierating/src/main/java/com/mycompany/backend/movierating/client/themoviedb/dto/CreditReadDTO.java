package com.mycompany.backend.movierating.client.themoviedb.dto;

import lombok.Data;

@Data
public class CreditReadDTO {

    private String department;

    private String job;

}
