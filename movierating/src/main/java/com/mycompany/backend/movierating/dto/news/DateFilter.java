package com.mycompany.backend.movierating.dto.news;

import java.time.Instant;
import lombok.Data;

@Data
public class DateFilter {

    private Instant dateFrom;

    private Instant dateTo;
}
