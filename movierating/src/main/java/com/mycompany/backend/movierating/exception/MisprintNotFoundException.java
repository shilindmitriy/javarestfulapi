package com.mycompany.backend.movierating.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class MisprintNotFoundException extends RuntimeException {

    public MisprintNotFoundException(String message) {
        super(message);
    }

}
