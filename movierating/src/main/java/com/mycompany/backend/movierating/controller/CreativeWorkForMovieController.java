package com.mycompany.backend.movierating.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.service.CreativeWorkService;

@RestController
@RequestMapping("/api/v1/movies/{movieId}/creative-works")
public class CreativeWorkForMovieController {

    @Autowired
    private CreativeWorkService workService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public CreativeWorkReadDTO getCreativeWork(@PathVariable UUID movieId, @PathVariable UUID id) {
        return workService.getCreativeWork(movieId, id);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping
    public CreativeWorkReadDTO createCreativeWork(@PathVariable UUID movieId,
            @RequestBody @Valid CreativeWorkCreateDTO create) {
        return workService.createCreativeWork(movieId, create);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public CreativeWorkReadDTO patchCreativeWork(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid CreativeWorkPatchDTO patch) {
        return workService.patchCreativeWork(movieId, id, patch);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public CreativeWorkReadDTO updateCreativeWork(@PathVariable UUID movieId, @PathVariable UUID id,
            @RequestBody @Valid CreativeWorkPutDTO put) {
        return workService.updateCreativeWork(movieId, id, put);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteCreativeWork(@PathVariable UUID movieId, @PathVariable UUID id) {
        workService.deleteCreativeWork(movieId, id);
    }

}
