package com.mycompany.backend.movierating.dto.message;

import com.mycompany.backend.movierating.domain.constants.Administration;
import com.mycompany.backend.movierating.domain.constants.EntityType;

import lombok.Data;

@Data
public class MessageToAdministrationFilter {

    private Administration administration;

    private Boolean solved;

    private EntityType entityType;
}
