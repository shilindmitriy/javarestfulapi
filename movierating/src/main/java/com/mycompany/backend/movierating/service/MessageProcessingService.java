package com.mycompany.backend.movierating.service;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.MessageToAdministration;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationReadDTO;
import com.mycompany.backend.movierating.dto.message.MessageToAdministrationSolveRequest;
import com.mycompany.backend.movierating.exception.EntityReflectionException;
import com.mycompany.backend.movierating.exception.InvalidRequestException;
import com.mycompany.backend.movierating.exception.MisprintNotFoundException;
import com.mycompany.backend.movierating.repository.MessageToAdministrationRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;
import com.mycompany.backend.movierating.security.UserDetailsImpl;

@Service
public class MessageProcessingService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MessageToAdministrationRepository messageRepository;

    @Transactional
    public List<MessageToAdministrationReadDTO> getProcessedMessages(MessageToAdministrationSolveRequest solved,
            UserDetailsImpl user) {

        // get messages and set LockModeType.PESSIMISTIC_WRITE
        List<MessageToAdministration> messages = messageRepository.getMessagesForMessagesProcessing(solved.getUrl(),
                solved.getMisprints());
        if (messages.isEmpty()) {
            throw new MisprintNotFoundException(
                    String.format("Messages have already been processed. Url=%s. Misprints=%s", solved.getUrl(),
                            solved.getMisprints().toString()));
        }

        // prepare the variables
        Class<?> entityClass = solved.getEntityType().getEntityClass();
        // get Field
        Field field = getField(entityClass, solved.getEntityField());
        // get entity
        Object entity = repositoryHelper.getByIdRequired(entityClass, solved.getEntityId());
        // get the value of the field
        Object value = getter(entity, field);

        // process
        value = messageProcessing(solved, field, value);

        // set the value of the field
        setter(entity, field, value);

        // update entity
        repositoryHelper.updateEntity(entityClass.cast(entity));

        // get solved messages and remove LockModeType.PESSIMISTIC_WRITE
        List<UUID> ids = new ArrayList<>();

        messages.forEach(m -> {
            m.setNote(solved.getNote());
            m.setSolved(true);
            m.setLastModifiedById(user.getId());
            ids.add(m.getId());
        });
        messageRepository.saveAll(messages);

        messages = messageRepository.getSolvedMessagesForMessagesProcessing(ids);

        return translationService.translateList(messages, MessageToAdministrationReadDTO.class);
    }

    private Object messageProcessing(MessageToAdministrationSolveRequest solved, Field field, Object value) {

        // prepare the variables
        Class<?> entityClass = solved.getEntityType().getEntityClass();
        String entityField = solved.getEntityField();
        String url = solved.getUrl();
        String correction = solved.getCorrection();
        String misprint = solved.getMisprint();

        if (isString(field.getType())) {

            String original = (String) value;
            validateFieldIsNull(original, entityClass, entityField, url, misprint);

            switch (solved.getOperation()) {
              case APPEND:
                  return value = append(url, original, correction, solved.getBeginIndex());
              case REMOVE:
                  return value = remove(url, original, misprint, solved.getBeginIndex());
              case REPLACE:
                  return value = replace(url, original, correction, misprint, solved.getBeginIndex());
            }
        }

        if (isLocalDate(field.getType())) {

            LocalDate original = (LocalDate) value;

            switch (solved.getOperation()) {
              case APPEND:
                  return value = append(url, original, LocalDate.parse(correction));
              case REMOVE:
                  return value = remove(url, original, LocalDate.parse(misprint));
              case REPLACE:
                  return value = replace(url, original, LocalDate.parse(correction), LocalDate.parse(misprint));
            }
        }
        if (isLocalTime(field.getType())) {

            LocalTime original = (LocalTime) value;

            switch (solved.getOperation()) {
              case APPEND:
                  return value = append(url, original, LocalTime.parse(correction));
              case REMOVE:
                  return value = remove(url, original, LocalTime.parse(misprint));
              case REPLACE:
                  return value = replace(url, original, LocalTime.parse(correction), LocalTime.parse(misprint));
            }
        }

        if (isEnum(field.getType())) {

            Enum<?> original = (Enum<?>) value;

            switch (solved.getOperation()) {
              case APPEND:
                  throw new InvalidRequestException(String.format("%s unsupported append operation.", entityField));
              case REMOVE:
                  throw new InvalidRequestException(String.format("%s unsupported remove operation.", entityField));
              case REPLACE:
                  return value = replace(url, original, getEnumConstant((Class<Enum>) field.getType(), correction),
                        getEnumConstant((Class<Enum>) field.getType(), misprint));
            }
        }

        if (isCollection(field.getType())) {

            Collection<Object> original = (Collection<Object>) value;
            if (!isEnum(getGenericClass(field))) {
                throw new InvalidRequestException(String.format("%s supports operations with only enum.", entityField));
            }

            switch (solved.getOperation()) {
              case APPEND:
                  return value = append(url, original, getEnumConstant((Class<Enum>) getGenericClass(field), 
                          correction));
              case REMOVE:
                  return value = remove(url, original, getEnumConstant((Class<Enum>) getGenericClass(field), 
                          misprint));
              case REPLACE:
                  return value = replace(url, original, getEnumConstant((Class<Enum>) getGenericClass(field), 
                          correction),
                        getEnumConstant((Class<Enum>) getGenericClass(field), misprint));
            }
        }

        throw new InvalidRequestException(
                String.format("Unsupported message processing with this field=%s.", entityField));
    }

    // operation
    private String append(String url, String original, String correction, Integer beginIndex) {
        int endIndex = beginIndex + correction.length();
        if (original.substring(beginIndex, endIndex).equals(correction)) {
            throw new MisprintNotFoundException(
                    String.format("Messages have already been processed. Url=%s. Correction=%s", url, correction));
        }
        return original.substring(0, beginIndex) + correction + original.substring(beginIndex);
    }

    private LocalDate append(String url, LocalDate original, LocalDate correction) {
        if (original != null) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Correction=%s", url, correction.toString()));
        }
        return correction;
    }

    private LocalTime append(String url, LocalTime original, LocalTime correction) {
        if (original != null) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Correction=%s", url, correction.toString()));
        }
        return correction;
    }

    private Collection<Object> append(String url, Collection<Object> original, Object correction) {
        if (original.contains(correction)) {
            throw new MisprintNotFoundException(
                    String.format("Messages have already been processed. Entity already has this value=%s. Url=%s.",
                            correction.toString(), url));
        }
        original.add(correction);
        return original;
    }

    private String replace(String url, String original, String correction, String misprint, Integer beginIndex) {
        int endIndex = beginIndex + misprint.length();
        if (!original.substring(beginIndex, endIndex).equals(misprint)) {
            throw new MisprintNotFoundException(
                    String.format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint));
        }
        return original.substring(0, beginIndex) + correction + original.substring(endIndex);
    }

    private LocalDate replace(String url, LocalDate original, LocalDate correction, LocalDate misprint) {
        if (!original.equals(misprint)) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint.toString()));
        }
        return correction;
    }

    private LocalTime replace(String url, LocalTime original, LocalTime correction, LocalTime misprint) {
        if (!original.equals(misprint)) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint.toString()));
        }
        return correction;
    }

    private Enum<?> replace(String url, Enum<?> original, Enum<?> correction, Enum<?> misprint) {
        if (!original.equals(misprint)) {
            throw new MisprintNotFoundException(
                    String.format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint.name()));
        }
        return correction;
    }

    private Collection<Object> replace(String url, Collection<Object> original, Object correction, Object misprint) {
        if (!original.contains(misprint)) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint.toString()));
        }
        if (original.contains(correction)) {
            throw new MisprintNotFoundException(String.format(
                    "Messages have already been processed. Entity already has this value=%s. Url=%s. Misprint=%s",
                    correction.toString(), url, misprint.toString()));
        }
        original.add(correction);
        original.remove(misprint);
        return original;
    }

    private String remove(String url, String original, String misprint, Integer beginIndex) {
        return replace(url, original, "", misprint, beginIndex);
    }

    private LocalDate remove(String url, LocalDate original, LocalDate misprint) {
        return replace(url, original, null, misprint);
    }

    private LocalTime remove(String url, LocalTime original, LocalTime misprint) {
        return replace(url, original, null, misprint);
    }

    private Collection<Object> remove(String url, Collection<Object> original, Object misprint) {
        if (!original.contains(misprint)) {
            throw new MisprintNotFoundException(String
                    .format("Messages have already been processed. Url=%s. Misprint=%s", url, misprint.toString()));
        }
        original.remove(misprint);
        return original;
    }

    // get Field
    private Field getField(Class<?> entityClass, String fieldEntity) {
        Field field;
        try {
            field = entityClass.getDeclaredField(fieldEntity);
        } catch (NoSuchFieldException e) {
            throw new InvalidRequestException(String.format("Entity %s does not contain this field=%s",
                    entityClass.getSimpleName(), fieldEntity));
        } catch (SecurityException e) {
            throw new EntityReflectionException(e.getMessage());
        }
        field.setAccessible(true);
        return field;
    }

    // check type
    private boolean isCollection(Class<?> fieldType) {
        return Collection.class.isAssignableFrom(fieldType);
    }

    private boolean isString(Class<?> fieldType) {
        return fieldType.isAssignableFrom(String.class);
    }

    private boolean isLocalDate(Class<?> fieldType) {
        return fieldType.isAssignableFrom(LocalDate.class);
    }

    private boolean isEnum(Class<?> fieldType) {
        return fieldType.isEnum();
    }

    private boolean isLocalTime(Class<?> fieldType) {
        return fieldType.isAssignableFrom(LocalTime.class);
    }

    // checking for zero
    private void validateFieldIsNull(Object origin, Class<?> entityClass, String fieldEntity, String url,
            String misprint) {
        if (origin == null) {
            throw new InvalidRequestException(String.format("Entity=%s has got a field %s=null. Url=%s. Misprint=%s",
                    entityClass.getSimpleName(), fieldEntity, url, misprint));
        }
    }

    // get generic type
    private Class<?> getGenericClass(Field field) {
        ParameterizedType type = (ParameterizedType) field.getGenericType();
        return (Class<?>) type.getActualTypeArguments()[0];
    }

    // get enum from string
    private <T extends Enum<T>> T getEnumConstant(Class<T> enumClass, String enumConstant) {
        return Enum.valueOf(enumClass, enumConstant);
    }

    // get
    private Object getter(Object entity, Field field) {
        Object o;
        try {
            o = MethodHandles.lookup().unreflectGetter(field).invoke(entity);
        } catch (Throwable e) {
            throw new EntityReflectionException(e.getMessage());
        }
        return o;
    }

    // set
    private void setter(Object entity, Field field, Object value) {
        try {
            MethodHandles.lookup().unreflectSetter(field).invoke(entity, value);
        } catch (Throwable e) {
            throw new EntityReflectionException(e.getMessage());
        }
    }

}
