package com.mycompany.backend.movierating.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.dto.news.DateFilter;
import com.mycompany.backend.movierating.dto.news.NewsCreateDTO;
import com.mycompany.backend.movierating.dto.news.NewsPatchDTO;
import com.mycompany.backend.movierating.dto.news.NewsPutDTO;
import com.mycompany.backend.movierating.dto.news.NewsReadDTO;
import com.mycompany.backend.movierating.security.UserDetailsImpl;
import com.mycompany.backend.movierating.service.NewsService;

@RestController
@RequestMapping("/api/v1/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @PreAuthorize("permitAll")
    @GetMapping("/{id}")
    public NewsReadDTO getNews(@PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @PreAuthorize("permitAll")
    @GetMapping("/list/{author}")
    public List<NewsReadDTO> getNewsByAuthor(@PathVariable UUID author, DateFilter date) {
        return newsService.getNewsByAuthor(author, date);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PostMapping
    public NewsReadDTO createNews(@RequestBody @Valid NewsCreateDTO createNews,
            @AuthenticationPrincipal UserDetailsImpl user) {
        return newsService.createNews(createNews, user);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PatchMapping("/{id}")
    public NewsReadDTO patchNews(@PathVariable UUID id, @RequestBody @Valid NewsPatchDTO patchNews) {
        return newsService.patchNews(id, patchNews);
    }

    @PreAuthorize("hasAuthority('CONTENT_MANAGER')")
    @PutMapping("/{id}")
    public NewsReadDTO updateNews(@PathVariable UUID id, @RequestBody @Valid NewsPutDTO putNews) {
        return newsService.updateNews(id, putNews);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'CONTENT_MANAGER')")
    @DeleteMapping("/{id}")
    public void deleteNews(@PathVariable UUID id) {
        newsService.deleteNews(id);
    }
}
