package com.mycompany.backend.movierating.dto.movieinfo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyReadDTO {

    private UUID companyId;

    private String name;

}
