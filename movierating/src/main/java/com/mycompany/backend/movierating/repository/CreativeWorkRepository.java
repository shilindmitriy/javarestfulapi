package com.mycompany.backend.movierating.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;

@Repository
public interface CreativeWorkRepository extends CrudRepository<CreativeWork, UUID> {

    @Query("select w from CreativeWork w where w.id = :id and "
            + "(w.creativePerson.id = :parentId or w.movie.id = :parentId)")
    Optional<CreativeWork> findByParentIdAndId(UUID parentId, UUID id);

    @Query("select new com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO(w.creativePerson.name, "
            + "w.character, w.department, w.post, w.creativePerson.id, w.id) from CreativeWork w where "
            + "w.movie.id = :movieId and w.department = :department order by w.creativePerson.name")
    List<CastAndCrewReadDTO> findCastAndCrew(UUID movieId, Department department);

    @Query("select new com.mycompany.backend.movierating.dto.filmography.FilmographyReadDTO(w.character, w.department, "
            + "w.post, w.movie.title, w.movie.releaseDate, w.movie.status, w.movie.runningTime, w.movie.id, w.id) "
            + "from CreativeWork w where w.creativePerson.id = :personId and w.department = :department "
            + "order by w.movie.releaseDate desc")
    List<FilmographyReadDTO> findFilmography(UUID personId, Department department);
    
    @Query("select w.id from CreativeWork w")
    Stream<UUID> getIdsOfCreativeWorks();

}