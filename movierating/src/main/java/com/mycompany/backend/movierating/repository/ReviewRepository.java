package com.mycompany.backend.movierating.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.dto.review.ReviewReadDTO;

@NoRepositoryBean
public interface ReviewRepository<T> extends CrudRepository<T, UUID> {

    @Query("select new com.mycompany.backend.movierating.dto.review.ReviewReadDTO("
            + "t.id, t.createdAt, t.updatedAt, t.text, t.enabled, t.user.id,  t.parentEntity.id) "
            + "from #{#entityName} t where t.parentEntity.id = :parentId and "
            + "(t.enabled = :enabled or t.user.id = :userId)")
    Page<ReviewReadDTO> findAllByParentIdAndEnabledOrUserId(UUID parentId, UUID userId, Boolean enabled,
            Pageable pageable);

    @Query("select new com.mycompany.backend.movierating.dto.review.ReviewReadDTO("
            + "t.id, t.createdAt, t.updatedAt, t.text, t.enabled, t.user.id,  t.parentEntity.id) "
            + "from #{#entityName} t where t.enabled = :enabled")
    Page<ReviewReadDTO> findAllByEnabled(Boolean enabled, Pageable pageable);

    @Query("select t from #{#entityName} t where t.parentEntity.id = ?1 and t.id = ?2")
    Optional<T> findByParentIdAndId(UUID parentId, UUID id);

    @Transactional
    @Modifying
    @Query("update #{#entityName} t set t.enabled = :enabled where t.parentEntity.id = :parentId and t.id = :id")
    int enableReview(Boolean enabled, UUID parentId, UUID id);
}