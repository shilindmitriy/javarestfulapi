package com.mycompany.backend.movierating.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.backend.movierating.domain.CreativeWork;
import com.mycompany.backend.movierating.domain.Movie;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkCreateDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPatchDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkPutDTO;
import com.mycompany.backend.movierating.dto.creativework.CreativeWorkReadDTO;
import com.mycompany.backend.movierating.dto.rating.ShortRatingReadDTO;
import com.mycompany.backend.movierating.exception.EntityNotFoundException;
import com.mycompany.backend.movierating.repository.CreativeWorkRatingRepository;
import com.mycompany.backend.movierating.repository.CreativeWorkRepository;
import com.mycompany.backend.movierating.repository.RepositoryHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreativeWorkService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private CreativeWorkRepository workRepository;
    
    @Autowired
    private CreativeWorkRatingRepository ratingRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public CreativeWorkReadDTO getCreativeWork(UUID parentId, UUID id) {
        CreativeWork work = getRequired(parentId, id);
        return translationService.translate(work, CreativeWorkReadDTO.class);
    }

    public CreativeWorkReadDTO createCreativeWork(UUID movieId, CreativeWorkCreateDTO create) {

        CreativeWork work = translationService.translate(create, CreativeWork.class);
        work.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));

        work = workRepository.save(work);
        return translationService.translate(work, CreativeWorkReadDTO.class);
    }

    public CreativeWorkReadDTO patchCreativeWork(UUID parentId, UUID id, CreativeWorkPatchDTO patch) {

        CreativeWork work = getRequired(parentId, id);
        translationService.map(patch, work);

        work = workRepository.save(work);
        return translationService.translate(work, CreativeWorkReadDTO.class);
    }

    public CreativeWorkReadDTO updateCreativeWork(UUID parentId, UUID id, CreativeWorkPutDTO put) {

        CreativeWork work = getRequired(parentId, id);
        translationService.map(put, work);

        work = workRepository.save(work);
        return translationService.translate(work, CreativeWorkReadDTO.class);
    }

    public void deleteCreativeWork(UUID parentId, UUID id) {
        workRepository.delete(getRequired(parentId, id));
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRating(UUID id) {
        ShortRatingReadDTO shortRating = ratingRepository.retrieveShortRating(id);
        CreativeWork work = repositoryHelper.getByIdRequired(CreativeWork.class, id);
        log.info(
                "Setting new average rating and number of all votes for creative work: {}. "
                        + "Old average rating: {}, new average rating: {}. "
                        + "Old number of all votes: {}, new number of all votes: {}.",
                id, work.getAverageRating(), shortRating.getAverageRating(), work.getNumberOfAllVotes(),
                shortRating.getNumberOfAllVotes());
        work.setAverageRating(shortRating.getAverageRating());
        work.setNumberOfAllVotes(shortRating.getNumberOfAllVotes());
        workRepository.save(work);
    }

    private CreativeWork getRequired(UUID parentId, UUID id) {
        return workRepository.findByParentIdAndId(parentId, id)
                .orElseThrow(() -> new EntityNotFoundException(CreativeWork.class, id, parentId));
    }

}
