package com.mycompany.backend.movierating.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.backend.movierating.domain.constants.Department;
import com.mycompany.backend.movierating.dto.movieinfo.CastAndCrewReadDTO;
import com.mycompany.backend.movierating.dto.movieinfo.CompanyReadDTO;
import com.mycompany.backend.movierating.service.MovieInfoService;

@RestController
@RequestMapping("/api/v1/movies/{movieId}")
public class MovieInfoController {

    @Autowired
    private MovieInfoService infoService;

    @PreAuthorize("permitAll")
    @GetMapping("/production-companies")
    public List<CompanyReadDTO> getCompanies(@PathVariable UUID movieId) {
        return infoService.getCompanies(movieId);
    }

    @PreAuthorize("permitAll")
    @GetMapping("/cast-and-crew/{department}")
    public List<CastAndCrewReadDTO> getCastAndCrew(@PathVariable UUID movieId, @PathVariable Department department) {
        return infoService.getCastAndCrew(movieId, department);
    }

}
